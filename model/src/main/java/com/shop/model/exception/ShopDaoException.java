package com.shop.model.exception;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 03.03.12
 */
public class ShopDaoException extends Exception{
    private static Logger logger = Logger.getLogger(ShopDaoException.class);

    public ShopDaoException() {
        logger.error("Error in dao");
    }

    public ShopDaoException(String message) {
        super(message);
        logger.error(message);
    }

    public ShopDaoException(String message, Throwable cause) {
        super(message, cause);
        logger.error(message);
    }

    public ShopDaoException(Throwable cause) {
        super(cause);
        logger.error(cause.getMessage());
    }
}
