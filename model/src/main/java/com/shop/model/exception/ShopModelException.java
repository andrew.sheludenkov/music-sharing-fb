package com.shop.model.exception;

import org.apache.log4j.Logger;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 03.03.12
 */
public class ShopModelException extends Exception{
    private static Logger logger = Logger.getLogger(ShopModelException.class);

    public ShopModelException() {
        logger.error("Model service error");
    }

    public ShopModelException(String message) {
        super(message);
        logger.error(message);
    }

    public ShopModelException(String message, Throwable cause) {
        super(message, cause);
        logger.error(message);
    }

    public ShopModelException(Throwable cause) {
        super(cause);
        logger.error(cause.getMessage());
    }
}
