package com.shop.model.form;

/**
 * Created with IntelliJ IDEA.
 * User: shell
 * Date: 6/21/15
 * Time: 8:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class CaptchaModel {

    String imgUrl;
    String postUrl;
    String captchaSid;
    String accessToken;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public String getCaptchaSid() {
        return captchaSid;
    }

    public void setCaptchaSid(String captchaSid) {
        this.captchaSid = captchaSid;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
