package com.shop.model.auth;

import com.shop.model.persistence.User;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
public class AuthenticatedUserHolder {
    private User user;

    public User getLoggedInUser(){
        return user;
    }

    public boolean isLoggedIn(){
        return user != null;
    }

    public void loginUser(User user){
        this.user = user;
    }

    public void logoutUser(){
        user = null;
    }
}
