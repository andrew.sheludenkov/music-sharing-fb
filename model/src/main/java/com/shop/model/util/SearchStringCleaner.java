package com.shop.model.util;


/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 03.03.12
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */
public class SearchStringCleaner {

    private static String CYRILLIC_ALPHABET = "бвгдёжзийлмнпрстуфхцчшщъыьэюяБВГДЁЖЗИЙЛМНПУФХЦЧШЩЪЫЬЭЮЯ";
    private static char[] DELIMETERS = {'/', ':', '-', '&', ','};

    private static SearchStringCleaner nameCleaner;


    public static SearchStringCleaner get() {
        if (nameCleaner == null) {
            nameCleaner = new SearchStringCleaner();
        }
        return nameCleaner;
    }

    public String clean(String value) {




//            for (char c : CYRILLIC_ALPHABET.toCharArray()) {
//                value = value.replaceAll("" + c, "");
//            }

        for (char c : DELIMETERS) {
//            String str = "" + c;
//            if (str.equals("+")){
//                str = "\\" + str;
//            }
            value = value.replaceAll("" + c, " ");
        }

        value = value.replaceAll("\\(", " ")
                .replaceAll("\\)", " ");

        return value.trim().toUpperCase();

    }




}
