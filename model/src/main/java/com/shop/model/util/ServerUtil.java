package com.shop.model.util;

/**
 * Created with IntelliJ IDEA.
 * User: shell
 * Date: 12/21/14
 * Time: 9:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class ServerUtil {

    public static ServerUtil util;

    public static ServerUtil get(){
        if (util == null){
            util = new ServerUtil();
        }
        return util;
    }

    public String cleanBeforeSearch(String productName) {
        productName = productName.replaceAll("\\(", "").replaceAll("\\)", "");
        productName = productName.replaceAll("\\&nbsp;", "");
        productName = productName.replaceAll("-", " ");
        productName = productName.replaceAll("/", " ");
        productName = productName.replaceAll("=", " ").replaceAll("\\+", " ").replaceAll("!", " ").replaceAll("\\.", " ").replaceAll("\'", " ").replaceAll("\"", " ");
        productName = productName.trim();
        return productName;
    }

}
