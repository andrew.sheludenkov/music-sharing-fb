package com.shop.model.util;


import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;
import org.hibernate.util.StringHelper;


/**
 * User: Roman
 * Timestamp: 07.08.12 18:36
 */
public class NativeSQLOrder extends Order {
    private static final long serialVersionUID = 1L;
    private final static String PROPERTY_NAME = "uselessAnyways";
    private final String sql;

    public NativeSQLOrder(final String sql) {
        super(PROPERTY_NAME, true);
        this.sql = sql;

    }

    @Override
    public String toSqlString(final Criteria criteria,
                              final CriteriaQuery criteriaQuery) throws HibernateException {
        final StringBuilder fragment = new StringBuilder();
        fragment.append("(");
        fragment.append(sql);
        fragment.append(")");
        return StringHelper.replace(fragment.toString(), "{alias}",
                criteriaQuery.getSQLAlias(criteria));
    }
}