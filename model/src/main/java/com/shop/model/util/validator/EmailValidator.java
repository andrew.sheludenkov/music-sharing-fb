package com.shop.model.util.validator;

import org.apache.commons.lang.StringUtils;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * User: Roman
 * Timestamp: 06.12.12 13:44
 */
public class EmailValidator {
    public static boolean isValidEmailAddress(String aEmailAddress) {
        if (aEmailAddress == null) return false;
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(aEmailAddress);
            if (!hasNameAndDomain(aEmailAddress)) {
                result = false;
            }
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    private static boolean hasNameAndDomain(String aEmailAddress) {
        String[] tokens = aEmailAddress.split("@");
        return
                tokens.length == 2 &&
                        StringUtils.isNotEmpty(tokens[0]) &&
                        StringUtils.isNotEmpty(tokens[1]);
    }
}
