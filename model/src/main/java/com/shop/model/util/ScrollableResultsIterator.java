package com.shop.model.util;

import org.hibernate.ScrollableResults;

import java.util.Iterator;

/**
 * User: Roman
 * Timestamp: 20.07.12 17:21
 */
public class ScrollableResultsIterator<T> implements Iterator<T> {

    private ScrollableResults scrollableResults;

    public ScrollableResultsIterator(ScrollableResults scrollableResults) {
        this.scrollableResults = scrollableResults;
    }

    @Override
    public boolean hasNext() {
        return scrollableResults.next();
    }

    @Override
    public T next() {
        return (T) scrollableResults.get(0);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove not supported");
    }
}
