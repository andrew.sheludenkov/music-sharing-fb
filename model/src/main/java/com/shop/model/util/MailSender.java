package com.shop.model.util;

import com.shop.model.persistence.User;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.text.MessageFormat;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.06.12
 */
public class MailSender {

    private JavaMailSender javaMailSender;

    private String fromAddress;
    private String orderMessagePattern = "<html><body>" +
            "User {0} " +
            "have ordered next goods:" +
            "<ul>{1}</ul>" +
            "</body></html>";

    private String registrationMessagePattern = "<html><body>" +
            "Hello {0}! <br/> <br/>" +
            "Welcome to EmbedStore.com! <br/> <br/>" +
            "http://embedstore.com/";

    private String embedStoreRegistrationMessagePattern = "<html><body>" +
            "Hello {0}! <br/> <br/>" +
            "Welcome to EmbedStore.com! <br/> <br/>" +
            "http://embedstore.com/";

    private String intervioNewVideoMessagePattern = "<html><body>" +
            "Hello {0}! <br/> <br/>" +
            "Your video has been processed! <br/> <br/>" +
            "{1}";

    private String temyCoPattern = "<html><body>" +
            "Temy.co visit<br/> <br/> " +
            "IP = {0} <br/>" +
            "Link: <a href=\"http://www.infosniper.net/index.php?ip_address={1}\">location</a> <br/>" +
            "Section: {2} <br/> " +
            "IT section {3}";



    public void sendIntervioNewVideoEmail(User user, String url) {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message);

            messageHelper.setTo(user.getEmail());
            messageHelper.setSubject("You have new video");
            messageHelper.setText(MessageFormat.format(intervioNewVideoMessagePattern, user.getFullName(), url), true);

            javaMailSender.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

    }


    public void sendTemycoVisitEmail(String ipAddress, String scale, String itSection) {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message);

            messageHelper.setTo("andrew.sheludenkov@gmail.com");
            messageHelper.setSubject("== Temyco visit");
            messageHelper.setText(MessageFormat.format(temyCoPattern, ipAddress, ipAddress, scale, itSection), true);

            javaMailSender.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

    }


    public void generateAndSendEmail() throws AddressException, MessagingException {

        Properties mailServerProperties;
        Session getMailSession;
        MimeMessage generateMailMessage;

        // Step1
        System.out.println("\n 1st ===> setup Mail Server Properties..");
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        System.out.println("Mail Server Properties have been setup successfully..");

        // Step2
        System.out.println("\n\n 2nd ===> get Mail Session..");
        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        generateMailMessage = new MimeMessage(getMailSession);
        generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("andrew.sheludenkov@gmail.com"));
//        generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("test2@crunchify.com"));
        generateMailMessage.setSubject("Greetings from Crunchify..");
        String emailBody = "Test email by Crunchify.com JavaMail API example. " + "<br><br> Regards, <br>Crunchify Admin";
        generateMailMessage.setContent(emailBody, "text/html");
        System.out.println("Mail Session has been created successfully..");

        // Step3
        System.out.println("\n\n 3rd ===> Get Session and Send mail");
        Transport transport = getMailSession.getTransport("smtp");

        // Enter your correct gmail UserID and Password
        // if you have 2FA enabled then provide App Specific Password
        transport.connect("smtp.gmail.com", "info@citymart.dn.ua", "streamfront123");
        transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
        transport.close();
    }


    /* Setters */

    public void setJavaMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }
}
