package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.MuzekQueryCacheDao;
import com.shop.model.persistence.MuzekQueryCache;
import com.shop.model.persistence.impl.MuzekQueryCacheImpl;
import org.hibernate.Query;

import java.util.List;


public class MuzekQueryCacheDaoImpl extends BaseDaoImpl<MuzekQueryCache, MuzekQueryCacheImpl> implements MuzekQueryCacheDao {

    @Override
    public Class<MuzekQueryCacheImpl> getPersistenceClass() {
        return MuzekQueryCacheImpl.class;
    }



    public MuzekQueryCache findRequest(String queryString){


        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekQueryCacheImpl t where t.queryString = :queryString and t.validation = :validation "
        );
        query = query.setParameter("queryString", queryString);
        query = query.setParameter("validation", MuzekQueryCacheImpl.VALIDATION.VALID);


        query.setMaxResults(1);

        List<MuzekQueryCache> list = query.list();
        if (list.size() == 0){
            return null;
        }

        return list.get(0);
    }
//
//    public List<MuzekFriends> findFriends(String userId){
//
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekFriendsImpl t where t.userId = :userId  " +
//                        " and not t.friendId = :userId "
//        );
//        query = query.setParameter("userId", userId);
//
//        query.setMaxResults(10);
//
//        List<MuzekFriends> list = query.list();
//        if (list.size() == 0){
//            return null;
//        }
//
//        return list;
//    }


}
