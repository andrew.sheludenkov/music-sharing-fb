package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.MuzekAlbumsDao;
import com.shop.model.dao.MuzekTrackListDao;
import com.shop.model.persistence.MuzekAlbums;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.impl.MuzekAlbumsImpl;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import org.hibernate.Query;

import java.util.List;


public class MuzekAlbumsDaoImpl extends BaseDaoImpl<MuzekAlbums, MuzekAlbumsImpl> implements MuzekAlbumsDao {

    @Override
    public Class<MuzekAlbumsImpl> getPersistenceClass() {
        return MuzekAlbumsImpl.class;
    }

    public List<MuzekAlbums> findTracksByUserFBidAndAlbumId(Long userId, Long albumId){

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekAlbumsImpl t where " +
                        " t.vkAlbumId = :albumId and " +
                        " t.userId = :userId "
        );
        query = query.setParameter("albumId", albumId);
        query = query.setParameter("userId", userId);

        return query.list();
    }

    public List<MuzekAlbums> findTracksByUserFBidAndAlbumIdYoutube(Long userId, String albumId){

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekAlbumsImpl t where " +
                        " t.youtubeAlbumId = :albumId and " +
                        " t.userId = :userId "
        );
        query = query.setParameter("albumId", albumId);
        query = query.setParameter("userId", userId);

        return query.list();
    }

    public List<MuzekAlbums> findAlbumsByUserFBid(String muzekFBid){

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekAlbumsImpl t where t.userFBid = :muzekFBid" +
                        " "
        );
        query = query.setParameter("muzekFBid", muzekFBid);

        return query.list();
    }
//
//    public MuzekTrackList findTracksByUserFBidAndTrackId(String muzekFBid, String trackId){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekTrackListImpl t where t.userFBid = :muzekFBid " +
//                        " and t.trackId = :trackId and " +
//                        " t.status is null "
//        );
//        query = query.setParameter("muzekFBid", muzekFBid);
//        query = query.setParameter("trackId", trackId);
//
//        return getSingleResult(query);
//    }
//
//    public List<MuzekTrackList> findDefaultTracks(){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekTrackListImpl t where " +
//                        " t.status = 'DEFAULT_1' " +
//                        " order by creationDate "
//        );
//        return query.list();
//    }

}
