package com.shop.model.dao;

import com.shop.model.persistence.MuzekPeerRecord;

import java.util.List;

public interface MuzekPeerRecordDao extends BaseDao<MuzekPeerRecord> {



//    public List<MuzekFeed> fetchLastRecords();
//
    public MuzekPeerRecord findPeer(String userId);

}
