package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.MuzekVkTrackDao;
import com.shop.model.dao.MuzekVkUserDao;
import com.shop.model.persistence.MuzekVkTrack;
import com.shop.model.persistence.MuzekVkUser;
import com.shop.model.persistence.impl.MuzekVkTrackImpl;
import com.shop.model.persistence.impl.MuzekVkUserImpl;


public class MuzekVkUserDaoImpl extends BaseDaoImpl<MuzekVkUser, MuzekVkUserImpl> implements MuzekVkUserDao {

    @Override
    public Class<MuzekVkUserImpl> getPersistenceClass() {
        return MuzekVkUserImpl.class;
    }

}
