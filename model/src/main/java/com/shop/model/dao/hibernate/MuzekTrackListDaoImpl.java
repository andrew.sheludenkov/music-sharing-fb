package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.MuzekTrackListDao;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import org.hibernate.Query;

import java.util.List;


public class MuzekTrackListDaoImpl extends BaseDaoImpl<MuzekTrackList, MuzekTrackListImpl> implements MuzekTrackListDao {

    @Override
    public Class<MuzekTrackListImpl> getPersistenceClass() {
        return MuzekTrackListImpl.class;
    }

    public List<MuzekTrackList> findTracksByUserFBid(String muzekFBid, Integer offset, Integer limit){


        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekTrackListImpl t where t.userFBid = :muzekFBid" +
                        " and t.status is null " +
                        " order by t.sortField desc "
        );
        query = query.setParameter("muzekFBid", muzekFBid);

        query.setFirstResult(offset);
        query.setMaxResults(limit);

        return query.list();
    }

    public MuzekTrackList findTrackByTrackId(String trackId){


        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekTrackListImpl t where t.trackId = :trackId "
        );
        query = query.setParameter("trackId", trackId);


        query.setMaxResults(1);

        List<MuzekTrackList> list = query.list();
        if (list.size() == 0){
            return null;
        }

        return list.get(0);
    }

    public List<MuzekTrackList> findTracksByTrackId(String muzekFBid, String trackId){


        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekTrackListImpl t where t.userFBid = :muzekFBid" +
                        " and t.status is null " +
                        " and t.trackId = :trackId "
        );
        query = query.setParameter("muzekFBid", muzekFBid);
        query = query.setParameter("trackId", trackId);


        return query.list();
    }

    public List<MuzekTrackList> findTracksByUserFBid(String muzekFBid, String albumId, String source, Integer offset, Integer limit){

        Query query = null;

        if (source.equals("vk")){
            Long vkAlbumId = Long.valueOf(albumId);

            query = getSessionFactory().getCurrentSession().createQuery(
                    "from MuzekTrackListImpl t where t.userFBid = :muzekFBid " +
                            " and t.vkAlbumId = :vkAlbumId " +
                            " and t.status is null " +
                            " order by t.sortField desc "
            );
            query = query.setParameter("muzekFBid", muzekFBid);
            query = query.setLong("vkAlbumId", vkAlbumId);
        }

        if (source.equals("youtube")){
            query = getSessionFactory().getCurrentSession().createQuery(
                    "from MuzekTrackListImpl t where t.userFBid = :muzekFBid " +
                            " and t.youtubeAlbumId = :albumId " +
                            " and t.status is null " +
                            " order by t.sortField desc "
            );
            query = query.setParameter("muzekFBid", muzekFBid);
            query = query.setString("albumId", albumId);
        }


        query.setFirstResult(offset);
        query.setMaxResults(limit);

        return query.list();
    }

    public MuzekTrackList findTracksByUserFBidAndTrackId(String muzekFBid, String trackId){

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekTrackListImpl t where t.userFBid = :muzekFBid " +
                        " and t.trackId = :trackId and " +
                        " t.status is null "
        );
        query = query.setParameter("muzekFBid", muzekFBid);
        query = query.setParameter("trackId", trackId);

        return getSingleResult(query);
    }

    public List<MuzekTrackList> findDefaultTracks(){

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekTrackListImpl t where " +
                        " t.status = 'DEFAULT_1' " +
                        " order by creationDate "
        );
        return query.list();
    }

    public List<MuzekTrackList> findTrackByUserAndVkId(Long userId, Long vkTrackId){

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekTrackListImpl t where " +
                        " t.userId = :userId " +
                        " and t.vkTrackId = :vkTrackId "
        );
        query = query.setParameter("userId", userId);
        query = query.setParameter("vkTrackId", vkTrackId);

        return query.list();
    }


    public List<MuzekTrackList> findTrackListeners(Integer limit, String trackId, String userId, String friendsId){

        String ids = friendsId;

        if (ids == null || ids.length() == 0){
            ids = "1222288434";
        }

        Query query = getSessionFactory().getCurrentSession().createSQLQuery(
                "select * from muzek_track_list\n" +
                        "where track_id = '"+trackId+"'\n" +
                        "and user_fb_id in ("+ids+")\n" +
                        "\n" +
                        "union \n" +
                        "\n" +
                        "(select * from muzek_track_list\n" +
                        "where track_id = '"+trackId+"' " +
                        " and not user_fb_id is null " +
                        " and not user_fb_id = " +userId+" \n" +
                        " and not user_fb_id = 'pop_hits' \n" +
                        " and not user_fb_id = 'dance' \n" +
                        " and not user_fb_id = 'jazz' \n" +
                        " and not user_fb_id = 'classic_rock' \n" +
                        " and not user_fb_id = 'electronic' \n" +
                        " and not user_fb_id = 'reggae' \n" +
                        " and status is null \n" +
                        " and LENGTH(user_fb_id) > 0 " +
                        "group by user_fb_id \n" +
                        "order by creation_date desc\n" +
                        ") "

        ).addEntity(MuzekTrackListImpl.class);


        query.setFirstResult(0);
        if (limit > 600){
            limit = 600;
        }
        query.setMaxResults(limit);

        return query.list();


    }

    public List<MuzekTrackList> findTrackListenersWithoutFriends(Integer limit, String trackId, String userId){



        Query query = getSessionFactory().getCurrentSession().createSQLQuery(

                        "select * from muzek_track_list\n" +
                        "where track_id = '"+trackId+"' " +
                        " and not user_fb_id is null " +
                        " and not user_fb_id = " +userId+" \n" +
                        " and not user_fb_id = 'pop_hits' \n" +
                        " and not user_fb_id = 'dance' \n" +
                        " and not user_fb_id = 'jazz' \n" +
                        " and not user_fb_id = 'classic_rock' \n" +
                        " and not user_fb_id = 'electronic' \n" +
                        " and not user_fb_id = 'reggae' \n" +
                        " and status is null \n" +
                        " and LENGTH(user_fb_id) > 0 " +
                        "group by user_fb_id \n" +
                        "order by creation_date desc\n" +
                        " "

        ).addEntity(MuzekTrackListImpl.class);


        query.setFirstResult(0);
        if (limit > 600){
            limit = 600;
        }
        query.setMaxResults(limit);

        return query.list();


    }

}
