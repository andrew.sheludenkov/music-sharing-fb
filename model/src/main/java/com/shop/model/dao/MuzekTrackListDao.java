package com.shop.model.dao;

import com.shop.model.persistence.MuzekTrackList;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 24.04.12
 */
public interface MuzekTrackListDao extends BaseDao<MuzekTrackList> {


    public List<MuzekTrackList> findTracksByUserFBid(String muzekFBid, Integer offset, Integer limit);

    public List<MuzekTrackList> findTracksByUserFBid(String muzekFBid, String albumId, String source, Integer offset, Integer limit);

    public MuzekTrackList findTracksByUserFBidAndTrackId(String muzekFBid, String trackId);

    public List<MuzekTrackList> findDefaultTracks();

    public List<MuzekTrackList> findTrackByUserAndVkId(Long userId, Long vkTrackId);

    public List<MuzekTrackList> findTracksByTrackId(String muzekFBid, String trackId);

    public MuzekTrackList findTrackByTrackId(String trackId);

    public List<MuzekTrackList> findTrackListeners(Integer limit, String trackId, String userId, String friendsId);

    public List<MuzekTrackList> findTrackListenersWithoutFriends(Integer limit, String trackId, String userId);

}
