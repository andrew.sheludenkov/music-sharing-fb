package com.shop.model.dao.hibernate;

import com.shop.model.dao.ActivityLogDao;
import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.persistence.ActivityLog;
import com.shop.model.persistence.impl.ActivityLogImpl;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
public class ActivityLogDaoImpl extends BaseDaoImpl<ActivityLog, ActivityLogImpl> implements ActivityLogDao {
    @Override
    public Class<ActivityLogImpl> getPersistenceClass() {
        return ActivityLogImpl.class;
    }

}
