package com.shop.model.dao;

import com.shop.model.persistence.MuzekFeed;

import java.util.List;


public interface MuzekFeedDao extends BaseDao<MuzekFeed> {


    public List<MuzekFeed> fetchLastRecords();

    public List<MuzekFeed> getFeed(Integer offset, Integer limit);

}
