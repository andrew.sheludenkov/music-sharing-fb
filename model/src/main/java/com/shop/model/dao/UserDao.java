package com.shop.model.dao;

import com.shop.model.persistence.User;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 24.04.12
 */
public interface UserDao extends BaseDao<User> {
    User findUserByCredentials(String username, String password);

    public User findUserByFastHash(String fastHash);

    User findUserByLoginHash(String loginHash);


    public User findUserBySessionId(String sessionId);

    public User findByNick(String nick);

    User findFacebookUserByEmail(String email);

    public List<User> findFacebookUserByName(String name);

    User findUserByFacebookId(String uid);

    User findUserByUnboxerHash(String uid);

    public User findUserByReferralCode(String referralCode);

    public User findUserByMuzekFBid(String muzekFBid);

    List<User> findUserByFacebookIds(List<String> uids);

    public List<User> findUsesWithoutPeers();

//    public List<User> findLastUsersList();
//
//    public void flushTwapRankingStatus();
}
