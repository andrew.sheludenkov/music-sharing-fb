package com.shop.model.dao;

import com.shop.model.persistence.Persistence;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 02.03.12
 */
public abstract class BaseDaoImpl<C extends Persistence, B extends C> implements BaseDao<C> {
    private SessionFactory sessionFactory;

    public abstract Class<B> getPersistenceClass();



    public C createPersistence(){
        try {
            return getPersistenceClass().newInstance();
        } catch (Exception e){
            return null;
        }
    }

    public void save(C persistence){
        sessionFactory.getCurrentSession().saveOrUpdate(persistence);
    }

    public C findById(Long identifier){
        C res = (C)sessionFactory.getCurrentSession().load(getPersistenceClass().getName(), identifier);
        res.getIdentifier();
        return res;
    }

    public List<C> findAll(){
        return sessionFactory.getCurrentSession().createQuery("from " + getPersistenceClass().getName()).list();
    }

    public void remove(Long identifier){
        C persistence = findById(identifier);
        if(persistence != null){
            sessionFactory.getCurrentSession().delete(persistence);
        }
    }

    @Override
    public C getReference(Long identifier) {
        return (C) sessionFactory.getCurrentSession().get(getPersistenceClass(), identifier);
    }

    protected <O> O getSingleResult(Query query){
        query.setMaxResults(1);

        List res = query.list();
        if(res != null && !res.isEmpty()){
            return (O)res.iterator().next();
        }

        return null;
    }

    public List<C> find(Integer from, Integer limit) {

        Query query = sessionFactory.getCurrentSession().createQuery("from " + getPersistenceClass().getSimpleName());
        query.setFirstResult(from);
        query.setMaxResults(limit);

        return query.list();
    }

    /*Setters and getters*/

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
