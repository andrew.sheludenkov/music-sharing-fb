package com.shop.model.dao;

import com.shop.model.persistence.Persistence;


import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 02.03.12
 */
public interface BaseDao<C extends Persistence> {

    C createPersistence();

    void save(C persistence);

    C findById(Long identifier);

    List<C> findAll();

    List<C> find(Integer from, Integer limit);

    void remove(Long identifier);

    C getReference(Long identifier);
}
