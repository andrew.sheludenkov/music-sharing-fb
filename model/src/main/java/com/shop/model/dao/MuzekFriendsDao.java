package com.shop.model.dao;

import com.shop.model.persistence.MuzekFriends;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 24.04.12
 */
public interface MuzekFriendsDao extends BaseDao<MuzekFriends> {

    public MuzekFriends findFriend(String userId, String friendId);

    public List<MuzekFriends> findFriends(String userId);

}
