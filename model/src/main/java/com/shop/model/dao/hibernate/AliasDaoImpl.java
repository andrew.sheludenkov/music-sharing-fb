package com.shop.model.dao.hibernate;

import com.shop.model.dao.AliasDao;
import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.persistence.Alias;
import com.shop.model.persistence.impl.AliasImpl;


public class AliasDaoImpl extends BaseDaoImpl<Alias, AliasImpl> implements AliasDao {

   @Override
    public Class<AliasImpl> getPersistenceClass() {
        return AliasImpl.class;
    }

}
