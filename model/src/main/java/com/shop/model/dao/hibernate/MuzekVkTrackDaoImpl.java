package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.MuzekFeedDao;
import com.shop.model.dao.MuzekVkTrackDao;
import com.shop.model.persistence.MuzekFeed;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.MuzekVkTrack;
import com.shop.model.persistence.impl.MuzekFeedImpl;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import com.shop.model.persistence.impl.MuzekVkTrackImpl;
import org.hibernate.Query;

import java.util.Date;
import java.util.List;


public class MuzekVkTrackDaoImpl extends BaseDaoImpl<MuzekVkTrack, MuzekVkTrackImpl> implements MuzekVkTrackDao {

    @Override
    public Class<MuzekVkTrackImpl> getPersistenceClass() {
        return MuzekVkTrackImpl.class;
    }

//    public void create (String fbUserId, String vkUserId, String total, String offset, String name){
//        MuzekVkTrackImpl track = new MuzekVkTrackImpl();
//        track.setFbUserId(fbUserId);
//        track.setCreationDate(new Date());
//        track.setName(name);
//        track.setOffset(offset);
//        track.setTotalNrUserTracks(total);
//        track.setVkUserId(vkUserId);
//
//
//
//    }

//    public List<MuzekFeed> fetchLastRecords(){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekFeedImpl f order by f.creationDate desc "
//        );
//        query = query.setMaxResults(10);
//
//        return query.list();
//    }
//
//    public List<MuzekFeed> getFeed(Integer offset, Integer limit){
//
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekFeedImpl t " +
//                        " where not t.user_name is null " +
//                        " group by t.creationDate " +
//                        " order by t.creationDate desc "
//        );
//
//        query.setFirstResult(offset);
//        query.setMaxResults(limit);
//
//        return query.list();
//    }
//
//    public MuzekTrackList findTrackByTrackId(String trackId){
//
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekTrackListImpl t where t.trackId = :trackId "
//        );
//        query = query.setParameter("trackId", trackId);
//
//
//        query.setMaxResults(1);
//
//        List<MuzekTrackList> list = query.list();
//        if (list.size() == 0){
//            return null;
//        }
//
//        return list.get(0);
//    }
//
//    public List<MuzekTrackList> findTracksByTrackId(String muzekFBid, String trackId){
//
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekTrackListImpl t where t.userFBid = :muzekFBid" +
//                        " and t.status is null " +
//                        " and t.trackId = :trackId "
//        );
//        query = query.setParameter("muzekFBid", muzekFBid);
//        query = query.setParameter("trackId", trackId);
//
//
//        return query.list();
//    }
//
//    public List<MuzekTrackList> findTracksByUserFBid(String muzekFBid, String albumId, String source, Integer offset, Integer limit){
//
//        Query query = null;
//
//        if (source.equals("vk")){
//            Long vkAlbumId = Long.valueOf(albumId);
//
//            query = getSessionFactory().getCurrentSession().createQuery(
//                    "from MuzekTrackListImpl t where t.userFBid = :muzekFBid " +
//                            " and t.vkAlbumId = :vkAlbumId " +
//                            " and t.status is null " +
//                            " order by t.sortField desc "
//            );
//            query = query.setParameter("muzekFBid", muzekFBid);
//            query = query.setLong("vkAlbumId", vkAlbumId);
//        }
//
//        if (source.equals("youtube")){
//            query = getSessionFactory().getCurrentSession().createQuery(
//                    "from MuzekTrackListImpl t where t.userFBid = :muzekFBid " +
//                            " and t.youtubeAlbumId = :albumId " +
//                            " and t.status is null " +
//                            " order by t.sortField desc "
//            );
//            query = query.setParameter("muzekFBid", muzekFBid);
//            query = query.setString("albumId", albumId);
//        }
//
//
//        query.setFirstResult(offset);
//        query.setMaxResults(limit);
//
//        return query.list();
//    }
//
//    public MuzekTrackList findTracksByUserFBidAndTrackId(String muzekFBid, String trackId){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekTrackListImpl t where t.userFBid = :muzekFBid " +
//                        " and t.trackId = :trackId and " +
//                        " t.status is null "
//        );
//        query = query.setParameter("muzekFBid", muzekFBid);
//        query = query.setParameter("trackId", trackId);
//
//        return getSingleResult(query);
//    }
//
//    public List<MuzekTrackList> findDefaultTracks(){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekTrackListImpl t where " +
//                        " t.status = 'DEFAULT_1' " +
//                        " order by creationDate "
//        );
//        return query.list();
//    }
//
//    public List<MuzekTrackList> findTrackByUserAndVkId(Long userId, Long vkTrackId){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekTrackListImpl t where " +
//                        " t.userId = :userId " +
//                        " and t.vkTrackId = :vkTrackId "
//        );
//        query = query.setParameter("userId", userId);
//        query = query.setParameter("vkTrackId", vkTrackId);
//
//        return query.list();
//    }
//
//    public List<MuzekTrackList> findTrackListeners(Integer limit, String trackId, String userId, String friendsId){
//
//        String ids = friendsId;
//
//        Query query = getSessionFactory().getCurrentSession().createSQLQuery(
//                "select * from muzek_track_list\n" +
//                        "where track_id = '"+trackId+"'\n" +
//                        "and user_fb_id in ("+ids+")\n" +
//                        "\n" +
//                        "union \n" +
//                        "\n" +
//                        "(select * from muzek_track_list\n" +
//                        "where track_id = '"+trackId+"' " +
//                        " and not user_fb_id is null " +
//                        " and not user_fb_id = " +userId+" \n" +
//                        " and not user_fb_id = 'pop_hits' \n" +
//                        " and not user_fb_id = 'dance' \n" +
//                        " and not user_fb_id = 'jazz' \n" +
//                        " and not user_fb_id = 'classic_rock' \n" +
//                        " and not user_fb_id = 'electronic' \n" +
//                        " and not user_fb_id = 'reggae' \n" +
//                        " and status is null \n" +
//                        " and LENGTH(user_fb_id) > 0 " +
//                        "group by user_fb_id \n" +
//                        "order by creation_date desc\n" +
//                        ") "
//
//        ).addEntity(MuzekTrackListImpl.class);
//
//
//        query.setFirstResult(0);
//        if (limit > 600){
//            limit = 600;
//        }
//        query.setMaxResults(limit);
//
//        return query.list();
//
//
//    }

}
