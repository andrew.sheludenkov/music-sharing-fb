package com.shop.model.dao;

import com.shop.model.persistence.MuzekFeed;
import com.shop.model.persistence.MuzekMessage;

import java.util.List;


public interface MuzekMessageDao extends BaseDao<MuzekMessage> {


//    public List<MuzekFeed> fetchLastRecords();
//
    public List<MuzekMessage> getMessages(Integer offset, Integer limit, String userBfId);

    List<MuzekMessage> findTracksByUser(String userFbId, String trackId);

}
