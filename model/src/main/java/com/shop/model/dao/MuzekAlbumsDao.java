package com.shop.model.dao;

import com.shop.model.persistence.MuzekAlbums;
import com.shop.model.persistence.MuzekTrackList;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 24.04.12
 */
public interface MuzekAlbumsDao extends BaseDao<MuzekAlbums> {

//
//    public List<MuzekTrackList> findTracksByUserFBid(String muzekFBid);
//
//    public MuzekTrackList findTracksByUserFBidAndTrackId(String muzekFBid, String trackId);
//
//    public List<MuzekTrackList> findDefaultTracks();
    public List<MuzekAlbums> findTracksByUserFBidAndAlbumIdYoutube(Long userId, String albumId);

    public List<MuzekAlbums> findTracksByUserFBidAndAlbumId(Long userId, Long albumId);

    public List<MuzekAlbums> findAlbumsByUserFBid(String muzekFBid);

}
