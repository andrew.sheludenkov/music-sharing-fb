package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.UserDao;
import com.shop.model.persistence.LoginType;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.UserImpl;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 23.04.12
 */
public class UserDaoImpl extends BaseDaoImpl<User, UserImpl> implements UserDao {

    @Override
    public Class<UserImpl> getPersistenceClass() {
        return UserImpl.class;
    }

    @Override
    public User findUserByCredentials(String username, String password){
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.username = :userName and usr.password = :pas"
        );
        query = query.setParameter("userName", username).setParameter("pas", password);

        return getSingleResult(query);
    }

    @Override
    public User findUserByFastHash(String fastHash){
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.fastLogin = :fastHash"
        );
        query = query.setParameter("fastHash", fastHash);

        return getSingleResult(query);
    }

//    @Override
//    public List<User> findLastUsersList(){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from UserImpl usr where usr.role = 'TWAP_USER' and usr.creationDate > '2014-08-01' " +
//                        " and bot is null " +
//                        " order by usr.creationDate desc "
//        );
//
//        query.setMaxResults(50);
//
//        return query.list();
//    }


//    @Override
//    public void flushTwapRankingStatus(){
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "update UserImpl usr set usr.RANKING_STATUS = 'NOT_RANKED' where usr.role = 'TWAP_USER' and usr.creationDate > '2014-08-01' " +
//                        " "
//        );
//
//        query.executeUpdate();
//    }

    @Override
    public User findUserBySessionId(String sessionId){
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.sessionId = :sessionId"
        );
        query = query.setParameter("sessionId", sessionId);

        return getSingleResult(query);
    }

    @Override
    public User findByNick(String nick){
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.upperName = :nick"
        );
        query = query.setParameter("nick", nick);

        return getSingleResult(query);
    }

    @Override
    public User findUserByLoginHash(String loginHash) {
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.loginHash = :loginHash"
        );
        query = query.setParameter("loginHash", loginHash);

        return getSingleResult(query);
    }



    public User findFacebookUserByEmail(String email){
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(UserImpl.class)
                .add(Restrictions.eq("email", email)).add(Restrictions.eq("loginType", LoginType.FACEBOOK));

        return (User)criteria.uniqueResult();
    }

    public List<User> findFacebookUserByName(String name){

        name = name.trim().replaceAll(" ", "%");
        name = "%" + name + "%";

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.name like :name"
        );
        query = query.setParameter("name", name);
        query.setMaxResults(150);

        return query.list();
    }

    public User findUserByFacebookId(String uid){
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(UserImpl.class)
                .add(Restrictions.eq("facebookUid", uid)).add(Restrictions.eq("loginType", LoginType.FACEBOOK));

        return (User)criteria.uniqueResult();
    }

    public User findUserByUnboxerHash(String unboxerUserHash){
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.unboxerUserHash = :unboxerUserHash"
        );
        query = query.setParameter("unboxerUserHash", unboxerUserHash);

        return getSingleResult(query);
    }

    public User findUserByReferralCode(String referralCode){
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.referralCode = :referralCode"
        );
        query = query.setParameter("referralCode", referralCode);

        return getSingleResult(query);
    }

    public User findUserByMuzekFBid(String muzekFBid){
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.muzekFBid = :muzekFBid"
        );
        query = query.setParameter("muzekFBid", muzekFBid);

        return getSingleResult(query);
    }

    public List<User> findUserByFacebookIds(List<String> uids){
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(UserImpl.class)
                .add(Restrictions.in("facebookUid", uids)).add(Restrictions.eq("loginType", LoginType.FACEBOOK));

        return criteria.list();
    }


    public List<User> findUsesWithoutPeers() {
        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from UserImpl usr where usr.username = 'Muzek User' AND usr.muzekStatus = :muzekStatus AND (usr.peersStatus is null OR usr.peersStatus = :userStatus ) "
        );
        query = query.setParameter("userStatus", UserImpl.USER_PEERS_STATUS.PEERS_EXCEEDED);
        query = query.setParameter("muzekStatus", UserImpl.MUZEK_STATUS.ACTIVATED);
        query.setMaxResults(10);

        return query.list();
    }


}
