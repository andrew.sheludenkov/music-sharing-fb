package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.MuzekFriendsDao;
import com.shop.model.persistence.MuzekFriends;
import com.shop.model.persistence.impl.MuzekFriendsImpl;
import org.hibernate.Query;

import java.util.List;


public class MuzekFriendsDaoImpl extends BaseDaoImpl<MuzekFriends, MuzekFriendsImpl> implements MuzekFriendsDao {

    @Override
    public Class<MuzekFriendsImpl> getPersistenceClass() {
        return MuzekFriendsImpl.class;
    }



    public MuzekFriends findFriend(String userId, String friendId){


        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekFriendsImpl t where t.userId = :userId and friendId = :friendId "
        );
        query = query.setParameter("userId", userId);
        query = query.setParameter("friendId", friendId);


        query.setMaxResults(1);

        List<MuzekFriends> list = query.list();
        if (list.size() == 0){
            return null;
        }

        return list.get(0);
    }

    public List<MuzekFriends> findFriends(String userId){


        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekFriendsImpl t where t.userId = :userId  " +
                        " and not t.friendId = :userId "
        );
        query = query.setParameter("userId", userId);

        query.setMaxResults(10);

        List<MuzekFriends> list = query.list();
        if (list.size() == 0){
            return null;
        }

        return list;
    }


}
