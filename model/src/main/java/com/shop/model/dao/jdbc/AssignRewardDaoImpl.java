package com.shop.model.dao.jdbc;

import com.shop.model.dao.AssignRewardDao;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 15.02.13
 */
public class AssignRewardDaoImpl extends JdbcDaoSupport implements AssignRewardDao {

    @Override
    public void assignCategoryProductReward(Long catId, Long appMarketId, Float reward) throws SQLException {
        if(catId == null){
            return;
        }

        Connection connection = getConnection();
        connection.setAutoCommit(false);

        PreparedStatement statement = connection.prepareStatement("update link_share_product set reward=? where app_market_id = ? and category_id = ? and not removed");
        statement.setFloat(1, reward);
        statement.setLong(2, appMarketId);
        statement.setLong(3, catId);



        statement.execute();

        statement.close();

        connection.commit();
        connection.close();

    }

    @Override
    public void assignMarketProductReward(Long appMarketId, Float reward) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("update link_share_product set reward=? where app_market_id = ?  and not removed");
        statement.setFloat(1, reward);
        statement.setLong(2, appMarketId);

        statement.executeUpdate();
    }
}
