package com.shop.model.dao;

import com.shop.model.persistence.MuzekQueryCache;


public interface MuzekQueryCacheDao extends BaseDao<MuzekQueryCache> {


    public MuzekQueryCache findRequest(String queryString);

//    public List<MuzekFeed> fetchLastRecords();
//
//    public List<MuzekFeed> getFeed(Integer offset, Integer limit);

}
