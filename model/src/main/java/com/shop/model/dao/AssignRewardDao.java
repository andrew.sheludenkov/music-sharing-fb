package com.shop.model.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 15.02.13
 */
public interface AssignRewardDao {
    void assignCategoryProductReward(Long catId, Long appMarketId, Float reward) throws SQLException;

    void assignMarketProductReward(Long appMarketId, Float reward) throws SQLException;
}
