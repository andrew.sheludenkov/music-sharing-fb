package com.shop.model.dao.hibernate;

import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.dao.MuzekPeerRecordDao;
import com.shop.model.persistence.MuzekPeerRecord;
import com.shop.model.persistence.impl.MuzekPeerRecordImpl;
import org.hibernate.Query;

import java.util.List;


public class MuzekPeerRecordDaoImpl extends BaseDaoImpl<MuzekPeerRecord, MuzekPeerRecordImpl> implements MuzekPeerRecordDao {

    @Override
    public Class<MuzekPeerRecordImpl> getPersistenceClass() {
        return MuzekPeerRecordImpl.class;
    }

    @Override
    public MuzekPeerRecord findPeer(String userFacebookId) {

        Query query = getSessionFactory().getCurrentSession().createQuery(
                "from MuzekPeerRecordImpl t where t.userFBid = :userFacebookId " +
                        " AND t.status = :peerStatus " +
                        " order by t.creationDate asc "
        );
        query = query.setParameter("userFacebookId", userFacebookId);
        query = query.setParameter("peerStatus", MuzekPeerRecordImpl.MUZEK_PEER_STATUS.NOT_SHOWED);

        query.setMaxResults(1);

        List<MuzekPeerRecord> list = query.list();
        if (list.size() == 0){
            return null;
        }

        return list.get(0);

    }


    //    public MuzekFriends findFriend(String userId, String friendId){
//
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekFriendsImpl t where t.userId = :userId and friendId = :friendId "
//        );
//        query = query.setParameter("userId", userId);
//        query = query.setParameter("friendId", friendId);
//
//
//        query.setMaxResults(1);
//
//        List<MuzekFriends> list = query.list();
//        if (list.size() == 0){
//            return null;
//        }
//
//        return list.get(0);
//    }
//
//    public List<MuzekFriends> findFriends(String userId){
//
//
//        Query query = getSessionFactory().getCurrentSession().createQuery(
//                "from MuzekFriendsImpl t where t.userId = :userId  " +
//                        " and not t.friendId = :userId "
//        );
//        query = query.setParameter("userId", userId);
//
//        query.setMaxResults(10);
//
//        List<MuzekFriends> list = query.list();
//        if (list.size() == 0){
//            return null;
//        }
//
//        return list;
//    }


}
