package com.shop.model.dao;

import com.shop.model.persistence.ActivityLog;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
public interface ActivityLogDao extends BaseDao<ActivityLog> {
}
