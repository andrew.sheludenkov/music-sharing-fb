package com.shop.model.service.yadirect.generator.configuration;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

/**
 * User: Roman
 * Timestamp: 19.07.12 18:16
 */
@XStreamAlias("dconfig")
public class CampaignConfiguration {

    private Long campaignId;
    private String campaignName;

    @XStreamAlias("banners")
    private List<BannerConfiguration> bannerConfigurations;

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public List<BannerConfiguration> getBannerConfigurations() {
        return bannerConfigurations;
    }

    public void setBannerConfigurations(List<BannerConfiguration> bannerConfigurations) {
        this.bannerConfigurations = bannerConfigurations;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }
}
