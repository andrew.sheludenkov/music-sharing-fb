package com.shop.model.service;

import com.shop.model.dao.MuzekFriendsDao;
import com.shop.model.dao.MuzekPeerRecordDao;
import com.shop.model.dao.UserDao;
import com.shop.model.persistence.MuzekFriends;
import com.shop.model.persistence.MuzekPeerRecord;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.MuzekFriendsImpl;
import com.shop.model.persistence.impl.MuzekPeerRecordImpl;
import com.shop.model.persistence.impl.UserImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


public class MuzekPeerRecordService extends BaseModelService<MuzekPeerRecord, MuzekPeerRecordDao> {


    UserDao userDao;


    @Transactional
    public void createListOfPeers(List<MuzekPeerRecord> peers) {

        for (MuzekPeerRecord peer : peers) {
            dao.save(peer);
        }

    }


    @Transactional
    public MuzekPeerRecord findPeer(String userId) {

        MuzekPeerRecord peer = dao.findPeer(userId);
        User user = userDao.findUserByMuzekFBid(userId);

        if (peer != null) {
            peer.setStatus(MuzekPeerRecordImpl.MUZEK_PEER_STATUS.SHOWED);
            peer.setShowedTime(new Date());
            dao.save(peer);

            user.setPeerShowedTime(new Date());
        } else {
            user.setPeersStatus(UserImpl.USER_PEERS_STATUS.PEERS_EXCEEDED);
        }


        userDao.save(user);

        return peer;

    }


    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
