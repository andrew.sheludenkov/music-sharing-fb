package com.shop.model.service;

import com.shop.model.dao.MuzekAlbumsDao;
import com.shop.model.dao.MuzekQueryCacheDao;
import com.shop.model.persistence.MuzekAlbums;
import com.shop.model.persistence.MuzekQueryCache;
import com.shop.model.persistence.impl.MuzekAlbumsImpl;
import com.shop.model.persistence.impl.MuzekQueryCacheImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public class MuzekQueryCacheService extends BaseModelService<MuzekQueryCache, MuzekQueryCacheDao> {


    @Transactional
    public MuzekQueryCache create(String query, String response, MuzekQueryCacheImpl.QUERY_TYPE queryType) {

        MuzekQueryCache queryCache = new MuzekQueryCacheImpl();

        queryCache.setCreationDate(new Date());
        queryCache.setQueryString(query);
        queryCache.setResponseString(response);
        queryCache.setQueryType(queryType);
        queryCache.setValidation(MuzekQueryCacheImpl.VALIDATION.VALID);


        dao.save(queryCache);

        return queryCache;
    }


    @Transactional
    public String findRequest(String query, String userId) {

        MuzekQueryCache cache = dao.findRequest(query);
        if (cache != null) {
            if (cache.getNrOfUsages() == null) {
                cache.setNrOfUsages(1l);
            } else {
                cache.setNrOfUsages(cache.getNrOfUsages() + 1);
            }

            cache.setLastUserUsed(userId);

            return cache.getResponseString();
        } else {
            return null;
        }
    }


}
