package com.shop.model.service.yadirect.generator.configuration;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

/**
 * User: Roman
 * Timestamp: 19.07.12 18:17
 */
@XStreamAlias("banner")
public class BannerConfiguration {

    private Long id;

    private String categoryName;

    private Double minProductPrice;

    private Double maxProductPrice;

    private String title;

    private String text;

    private String href;

    private Float clickPrice;

    private boolean updateExsistingPrice;

    @XStreamAlias("phrases")
    private List<PhraseConfiguration> phrasesConfiguration;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getMinProductPrice() {
        return minProductPrice;
    }

    public void setMinProductPrice(Double minProductPrice) {
        this.minProductPrice = minProductPrice;
    }

    public Double getMaxProductPrice() {
        return maxProductPrice;
    }

    public void setMaxProductPrice(Double maxProductPrice) {
        this.maxProductPrice = maxProductPrice;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Float getClickPrice() {
        return clickPrice;
    }

    public void setClickPrice(Float clickPrice) {
        this.clickPrice = clickPrice;
    }

    public boolean isUpdateExsistingPrice() {
        return updateExsistingPrice;
    }

    public void setUpdateExsistingPrice(boolean updateExsistingPrice) {
        this.updateExsistingPrice = updateExsistingPrice;
    }

    public List<PhraseConfiguration> getPhrasesConfiguration() {
        return phrasesConfiguration;
    }

    public boolean isCategoryBanner() {
        return categoryName != null && !categoryName.isEmpty();
    }

    @XStreamAlias("phrase")
    public static class PhraseConfiguration {
        private Long id;
        private String text;
        private Float clickPrice;
        private String urlParam;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Float getClickPrice() {
            return clickPrice;
        }

        public void setClickPrice(Float clickPrice) {
            this.clickPrice = clickPrice;
        }

        public String getUrlParam() {
            return urlParam;
        }

        public void setUrlParam(String urlParam) {
            this.urlParam = urlParam;
        }
    }
}
