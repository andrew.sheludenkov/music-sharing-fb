package com.shop.model.service;

import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 10.04.12
 */
public class FileSystemModelService {
    private String priceStorageDir;
    private String imageStorageDir;
    private String resizedImageStorageDir;
    private String exportStorageDir;
    private String sitemapStorageDir;

    public List<String> getAllReportDirs() {
        File rootDir = new File(priceStorageDir);
        if(!rootDir.isDirectory()){
            return null;
        }
        return CollectionUtils.arrayToList(rootDir.list());
    }

    public boolean isPriceFileExists(final String reportName, String dirName){
        File reportDir = new File(priceStorageDir + "/" + dirName);
        if(!reportDir.exists()){
            return false;
        }
        File[] found = reportDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().contains(reportName);
            }
        });

        return found != null && found.length > 0;
    }

    public File getPriceFile(final String fileName) {
        File reportDir = new File(priceStorageDir + "/" + fileName.split("/")[0]);
        if (!reportDir.exists()) {
            return null;
        }

        File[] finded = reportDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().split("\\.")[0].equals(fileName.split("/")[1]);
            }
        });

        return finded.length > 0 ? finded[0] : null;
    }

    public File getImageFile(final String fileName) {
        File imgFile = new File(resizedImageStorageDir + "/" + fileName);
        if (!imgFile.exists()) {
            imgFile = new File(imageStorageDir + "/" + fileName);
            if (!imgFile.exists()) {
                return null;
            }
        }

        return imgFile;
    }

    public File getXmlPriceFile(final String fileName) {
        File imgFile = new File(exportStorageDir + "/price/" + fileName);
        if (!imgFile.exists()) {
                return null;
        }

        return imgFile;
    }

    public File getSitemapXmlFile(){
        File sitemapFile = new File(sitemapStorageDir + "/sitemap.xml");
        if (!sitemapFile.exists()) {
                return null;
        }

        return sitemapFile;
    }

    public void setPriceStorageDir(String priceStorageDir) {
        this.priceStorageDir = priceStorageDir;
    }

    public void setImageStorageDir(String imageStorageDir) {
        this.imageStorageDir = imageStorageDir;
    }

    public void setResizedImageStorageDir(String resizedImageStorageDir) {
        this.resizedImageStorageDir = resizedImageStorageDir;
    }

    public void setExportStorageDir(String exportStorageDir) {
        this.exportStorageDir = exportStorageDir;
    }

    public void setSitemapStorageDir(String sitemapStorageDir) {
        this.sitemapStorageDir = sitemapStorageDir;
    }
}
