package com.shop.model.service;

import com.shop.model.dao.MuzekVkTrackDao;
import com.shop.model.persistence.MuzekVkTrack;
import com.shop.model.persistence.impl.MuzekVkTrackImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


public class MuzekVkTrackService extends BaseModelService<MuzekVkTrack, MuzekVkTrackDao> {



    @Transactional
    public void create(String fbUserId, String vkUserId, String total, String offset, String name) {


        MuzekVkTrackImpl track = new MuzekVkTrackImpl();
        track.setFbUserId(fbUserId);
        track.setCreationDate(new Date());
        track.setName(name);
        track.setOffset(offset);
        track.setTotalNrUserTracks(total);
        track.setVkUserId(vkUserId);

        dao.save(track);

    }


}
