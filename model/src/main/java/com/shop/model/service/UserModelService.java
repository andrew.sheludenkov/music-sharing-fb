package com.shop.model.service;

import com.shop.model.dao.UserDao;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.UserImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 24.04.12
 */
public class UserModelService extends BaseModelService<User, UserDao> {

    @Transactional
    public User findUserByCredentials(String userName, String password) {
        return dao.findUserByCredentials(userName, password);
    }

    @Transactional
    public User findUserByFastHash(String fastHash) {
        return dao.findUserByFastHash(fastHash);
    }

    @Transactional
    public User findUserBySessionId(String sessionid) {
        return dao.findUserBySessionId(sessionid);
    }

    @Transactional
    public User findUserByNick(String sessionid) {
        return dao.findByNick(sessionid);
    }

    @Transactional
    public User findFacebookUserByEmail(String email) {
        return dao.findFacebookUserByEmail(email);
    }

    @Transactional
    public List<User> findFacebookUserByName(String name) {
        return dao.findFacebookUserByName(name);
    }

    @Transactional
    public User findUserByFacebookUid(String uid) {
        return dao.findUserByFacebookId(uid);
    }

    @Transactional
    public User findUserByUnboxerHash(String uid) {
        return dao.findUserByUnboxerHash(uid);
    }

    @Transactional
    public User findUserByReferralCode(String uid) {
        return dao.findUserByReferralCode(uid);
    }

    @Transactional
    public User findUserByMuzekFBid(String id) {
        return dao.findUserByMuzekFBid(id);
    }

    @Transactional
    public void updateUserLastActivityTime(String id) {
        User user = dao.findUserByMuzekFBid(id);
        user.setLastActivityTime(new Date());
        dao.save(user);
    }

    @Transactional
    public User deactivateCoupon(String uid) {
        User user = dao.findUserByReferralCode(uid);
        Integer promoUsage = user.getUnboxerPromoUsage();
        user.setUnboxerPromoUsage(--promoUsage);
        dao.save(user);

        return user;
    }


    @Transactional
    public User updateUserNameEmailByUnboxerID(String unboxerId, String name, String email) {

        User user = dao.findUserByUnboxerHash(unboxerId);

        if (name != null && name.length() > 0) {
            user.setUsername(name);
        }
        if (email != null && email.length() > 0) {
            user.setEmail(email);
        }
        dao.save(user);

        return user;
    }

    @Transactional
    public User updateVkImportStatus(Long userId, String statistics, String vkAlbumsRaw, UserImpl.USER_TRACK_VK_IMPORT_STATUS status) {

        User user = dao.findById(userId);
        user.setVk_import_status(status);
        user.setVkImportStatistics(statistics);
        user.setVkAlbumsRaw(vkAlbumsRaw);

        dao.save(user);

        return user;
    }

    @Transactional
    public User updateUnreadMessagesStatus(Long userId, String status) {

        User user = dao.findById(userId);
        user.setHaveUnreadMessages(status);
        dao.save(user);

        return user;
    }

    @Transactional
    public User updateYoutubeImportStatus(Long userId, String statistics, UserImpl.USER_TRACK_VK_IMPORT_STATUS status) {

        User user = dao.findById(userId);
        user.setYoutube_import_status(status);
        if (statistics != null) {
            user.setYoutubeImportStatistics(statistics);
        }

        dao.save(user);

        return user;
    }

    @Transactional
    public User updateVkImportButtonVisibilitySetInvisible(Long userId) {

        User user = dao.findById(userId);
        user.setMuzekVkImportStatus("import complete");

        dao.save(user);

        return user;
    }

    @Transactional
    public User updateUserMuzekGraph(Long userId, String graph) {

        User user = dao.findById(userId);

        user.setGraph(graph);
        dao.save(user);

        return user;
    }

    @Transactional
    public User updateMuzekFeedback(Long userId, String rate, String feedback) {

        User user = dao.findById(userId);

        if (user.getMuzekFeedbackStatus().equals("ask")) {
            user.setMuzekFeedbackStatus(rate);
            user.setMuzekFeedback(feedback);
            dao.save(user);
        }

        return user;
    }

    @Transactional
    public User updateMuzekUsername(String userId, String userName) {

        User user = dao.findUserByMuzekFBid(userId);
        if (user.getName() == null) {
            user.setName(userName);
            dao.save(user);
        }

        return user;
    }

    @Transactional
    public User activate(Long userId) {

        User user = dao.findById(userId);

        user.setMuzekStatus(UserImpl.MUZEK_STATUS.ACTIVATED);
        dao.save(user);

        return user;
    }

    @Transactional
    public User updateStoreByUnboxerID(String unboxerId, String storeName) {

        User user = dao.findUserByUnboxerHash(unboxerId);

        if (storeName != null && storeName.length() > 0) {
            user.setStoreName(storeName);
        }
        dao.save(user);

        return user;
    }


    @Transactional
    public List<User> findUserByFacebookUids(List<String> uids) {
        return dao.findUserByFacebookIds(uids);
    }

    @Transactional
    public User loginUser(User user) {
//        String hashSrc = user.getUsername() + user.getPassword() + System.currentTimeMillis();
//        try {
//            MessageDigest  md5 = MessageDigest.getInstance("MD5");
//            md5.update(hashSrc.getBytes());
//            byte[] hash = md5.digest();
//            user.setLoginHash(Hex.encodeHexString(hash));

        String uuid = UUID.randomUUID().toString();
        user.setLoginHash(uuid);
        dao.save(user);
//        } catch (NoSuchAlgorithmException e) {
        //no action;
//        }

        return user;
    }

    @Transactional
    public User createUser(User user) {

        dao.save(user);

        return user;
    }

    @Transactional
    public User createUnboxerUser(String promoCode) {

        User user = new UserImpl();
        user.setAppMarketId(2L);

        user.setPromoCode(promoCode);
        user.setReferralCode(UUID.randomUUID().toString().split("-")[1].toUpperCase());
        user.setUnboxerUserHash(UUID.randomUUID().toString());
        user.setUnboxerPromoUsage(3);
        user.setUsername("Unboxer");
        dao.save(user);

        return user;
    }


    @Transactional
    public User createMuzekUser(String userFBid, String browserName) {

        User user = new UserImpl();
        user.setAppMarketId(2L);

        user.setMuzekFBid(userFBid);
        user.setUsername("Muzek User");
        user.setMuzekStatus(UserImpl.MUZEK_STATUS.JUST_CREATED);

        user.setMuzekVkImportStatus("import");

        user.setUserAgent(browserName);
        dao.save(user);

        return user;
    }

    @Transactional
    public void selectUserShop(User user, Long userShopId) {

        User userFetched = dao.findById(user.getIdentifier());
        userFetched.setActiveShopId(userShopId);
        dao.save(userFetched);

    }


    @Transactional
    public User findUserByLoginHash(String loginHash) {
        return dao.findUserByLoginHash(loginHash);
    }

    @Transactional
    public List<User> findUsesWithoutPeers() {
        return dao.findUsesWithoutPeers();
    }

    @Transactional
    public void updatePeerRecordsStatus(Long userId, UserImpl.USER_PEERS_STATUS peersStatus) {

        User user = dao.findById(userId);
        user.setPeersStatus(peersStatus);
        dao.save(user);
    }

}
