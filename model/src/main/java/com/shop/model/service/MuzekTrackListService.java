package com.shop.model.service;

import com.shop.model.dao.MuzekTrackListDao;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class MuzekTrackListService extends BaseModelService<MuzekTrackList, MuzekTrackListDao> {



    @Transactional
    public MuzekTrackList create(Long userId, String userFBId, String trackId, MuzekTrackListImpl.MUZEK_TRACK_SOURCE source, String name,
                                 String artworkUrl, String trackPermalink, String username, String userPermalink) {

        MuzekTrackList trackList = new MuzekTrackListImpl();
        trackList.setUserFBid(userFBId);
        trackList.setTrackId(trackId);
        trackList.setUserId(userId);
        trackList.setSource(source);
        trackList.setName(name);


        trackList.setArtworkUrl(artworkUrl);
        trackList.setTrackPermalink(trackPermalink);
        trackList.setUsername(username);
        trackList.setUser_permalink_url(userPermalink);



        dao.save(trackList);

        trackList.setSortField(Double.valueOf(trackList.getIdentifier()));
        dao.save(trackList);

        return trackList;
    }

    @Transactional
    public MuzekTrackList create(Long userId, String userFBId, String trackId, MuzekTrackListImpl.MUZEK_TRACK_SOURCE source, String name,
                                 String artworkUrl, String trackPermalink, String username, String userPermalink,
                                 String vkTitle, String vkArtist, Long vkDuration,
                                 Long vk_track_id,
                                 MuzekTrackListImpl.VK_IMPORT_STATUS importStatus, String youtubeAlbumId) {

        MuzekTrackList trackList = new MuzekTrackListImpl();

        if (trackId != null && trackId.length() > 0) {
            List<MuzekTrackList> list = dao.findTracksByTrackId(userFBId, trackId);
            if (list != null && list.size() > 0) {
                trackList = list.get(0);
            }
        }

        trackList.setUserFBid(userFBId);
        trackList.setTrackId(trackId);
        trackList.setUserId(userId);
        trackList.setSource(source);
        trackList.setName(name);
        trackList.setVkTrackId(vk_track_id);


        trackList.setArtworkUrl(artworkUrl);
        trackList.setTrackPermalink(trackPermalink);
        trackList.setUsername(username);
        trackList.setUser_permalink_url(userPermalink);

        trackList.setVkTitle(vkTitle);
        trackList.setVkArtist(vkArtist);
        trackList.setVkDuration(vkDuration);
        trackList.setVkImportStatus(importStatus);

        trackList.setYoutubeAlbumId(youtubeAlbumId);

        dao.save(trackList);

        trackList.setSortField(Double.valueOf(trackList.getIdentifier()));
        dao.save(trackList);

        return trackList;
    }

    @Transactional
    public MuzekTrackList createHidden(Long userId, String userFBId, MuzekTrackListImpl.MUZEK_TRACK_SOURCE source,

                                 String vkTitle, String vkArtist, Long vkDuration,
                                 MuzekTrackListImpl.VK_IMPORT_STATUS importStatus) {

        MuzekTrackList trackList = new MuzekTrackListImpl();
        trackList.setUserFBid(userFBId);
        trackList.setUserId(userId);
        trackList.setSource(source);

        trackList.setStatus(MuzekTrackListImpl.MUZEK_TRACK_STATUS.HIDDEN);

        trackList.setVkTitle(vkTitle);
        trackList.setVkArtist(vkArtist);
        trackList.setVkDuration(vkDuration);
        trackList.setVkImportStatus(importStatus);

        dao.save(trackList);

        trackList.setSortField(Double.valueOf(trackList.getIdentifier()));
        dao.save(trackList);

        return trackList;
    }

    @Transactional
    public MuzekTrackList remove(String userFBId, String trackId) {

        MuzekTrackList muzekTrackList = dao.findTracksByUserFBidAndTrackId(userFBId, trackId);
        muzekTrackList.setStatus(MuzekTrackListImpl.MUZEK_TRACK_STATUS.HIDDEN);

        dao.save(muzekTrackList);

        return muzekTrackList;
    }

    @Transactional
    public void updateSortField(Long trackId, Double sortField) {

        MuzekTrackList muzekTrackList = dao.findById(trackId);
        muzekTrackList.setSortField(sortField);

        dao.save(muzekTrackList);
    }

    @Transactional
    public void setAlbumId(List<MuzekTrackList> tracks, Long userId) {

        for (MuzekTrackList track : tracks) {
            List<MuzekTrackList> fetchedTracks = dao.findTrackByUserAndVkId(userId, track.getVkTrackId());
            if (fetchedTracks != null && fetchedTracks.size() > 0){
                MuzekTrackList fetchedTrack = fetchedTracks.get(0);

                fetchedTrack.setVkAlbumId(track.getVkAlbumId());
                dao.save(fetchedTrack);
            }

        }

    }

    @Transactional
    public List<MuzekTrackList> findTracksByUserFBid(String muzekFBid, String albumId, String source, Integer offset, Integer limit){
        if (albumId == null || source == null){
            return dao.findTracksByUserFBid(muzekFBid, offset, limit);
        }else {
            return dao.findTracksByUserFBid(muzekFBid, albumId, source, offset, limit);
        }
    }

    @Transactional
    public MuzekTrackList findTrackByTrackId(String trackId){

        return dao.findTrackByTrackId(trackId);

    }

    @Transactional
    public MuzekTrackList findById(String identifier){

        return dao.findById(Long.valueOf(identifier));

    }

    @Transactional
    public void setDefaultTracks(User user){

        List<MuzekTrackList> defaultTracks = dao.findDefaultTracks();

        for (MuzekTrackList defaultTrack : defaultTracks) {

            create(user.getIdentifier(), user.getMuzekFBid(), defaultTrack.getTrackId(), defaultTrack.getSource(), defaultTrack.getName(),
                    defaultTrack.getArtworkUrl(), defaultTrack.getTrackPermalink(), defaultTrack.getUsername(), defaultTrack.getUser_permalink_url());
        }

    }

    @Transactional
    public List<MuzekTrackList> findTrackListeners(Integer limit, String trackId, String userId, String friendsId){

        return dao.findTrackListeners(limit, trackId, userId, friendsId);

    }

    @Transactional
    public List<MuzekTrackList> findTrackListenersWithoutFriends(Integer limit, String trackId, String userId){

        try{
            return dao.findTrackListenersWithoutFriends(limit, trackId, userId);
        }catch (Exception e){
            return new ArrayList<MuzekTrackList>();
        }

    }

}
