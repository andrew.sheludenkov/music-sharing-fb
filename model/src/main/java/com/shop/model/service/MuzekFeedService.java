package com.shop.model.service;

import com.shop.model.dao.MuzekFeedDao;
import com.shop.model.dao.MuzekFriendsDao;
import com.shop.model.persistence.MuzekFeed;
import com.shop.model.persistence.MuzekFriends;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.MuzekFeedImpl;
import com.shop.model.persistence.impl.MuzekFriendsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public class MuzekFeedService extends BaseModelService<MuzekFeed, MuzekFeedDao> {

    private MuzekTrackListService muzekTrackListService;
    UserModelService userModelService;


    @Transactional
    public void create(String userFacebookId, String trackId, MuzekFeedImpl.MUZEK_FEED_ITEM_TYPE itemType) {


        if (itemType.equals(MuzekFeedImpl.MUZEK_FEED_ITEM_TYPE.LISTEN_TO)) {

            Boolean repeatedRecord = false;
            List<MuzekFeed> feed = dao.fetchLastRecords();

            for (MuzekFeed feedItem : feed) {
                if (feedItem.getUserFBid().equals(userFacebookId) && feedItem.getTrackId().equals(trackId)) {
                    repeatedRecord = true;
                    break;
                }
            }

            if (!repeatedRecord) {

                User user = userModelService.findUserByMuzekFBid(userFacebookId);
                MuzekTrackList track = muzekTrackListService.findTrackByTrackId(trackId);

                if (track != null) {

                    MuzekFeed feedItem = new MuzekFeedImpl();
                    feedItem.setArtworkUrl(track.getArtworkUrl());
                    feedItem.setStatus(itemType);
                    feedItem.setTrack_name(track.getName());
                    feedItem.setTrackId(track.getTrackId());
                    feedItem.setTrackIdentifier(track.getIdentifier());
                    feedItem.setUser_name(user.getName());
                    feedItem.setUserFBid(user.getMuzekFBid());
                    feedItem.setUserId(user.getIdentifier());
                    feedItem.setTrackSource(track.getSource().toString());

                    dao.save(feedItem);
                }
            }
        }

    }


    @Transactional
    public List<MuzekFeed> getFeed(Integer offset, Integer limit) {

        return dao.getFeed(offset, limit);

    }


    @Autowired
    public void setMuzekTrackListService(MuzekTrackListService muzekTrackListService) {
        this.muzekTrackListService = muzekTrackListService;
    }

    @Autowired
    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }
}
