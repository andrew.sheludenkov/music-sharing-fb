package com.shop.model.service.yadirect.generator.configuration.reader;

import com.shop.model.service.yadirect.generator.configuration.BannerConfiguration;
import com.shop.model.service.yadirect.generator.configuration.CampaignConfiguration;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.*;
import java.util.List;
import java.util.regex.Pattern;

/**
 * User: Roman
 * Timestamp: 19.07.12 18:44
 */

public class ConfigurationProcessor {

//    private static Logger logger = Logger.getLogger(ConfigurationProcessor.class);

    private String configurationPath;

    public CampaignConfiguration readConfiguration() {
        try {
            XStream xStream = createXstream();
            CampaignConfiguration campaignConfiguration = (CampaignConfiguration) xStream.fromXML(new File(configurationPath));
            validateConfig(campaignConfiguration);
            if (campaignConfiguration.getCampaignId() == 0) {
                campaignConfiguration.setCampaignId(null);
            }
            return campaignConfiguration;
        } catch (Exception ex) {
//            logger.error("YaDirect configuration parser error", ex);
        }
        return null;
    }

    public void saveConfiguration(CampaignConfiguration campaignConfiguration) {
        try {
            XStream xStream = createXstream();
            FileOutputStream fileOutputStream = new FileOutputStream(configurationPath);
            Writer writer = new OutputStreamWriter(fileOutputStream, "UTF8");
            xStream.marshal(campaignConfiguration, new PrettyPrintWriter(writer));
            fileOutputStream.close();
        } catch (Exception ex) {
//            logger.error("YaDirect configuration saving error", ex);
        }
    }

    private void validateConfig(CampaignConfiguration campaignConfiguration) {
        assert campaignConfiguration != null : "Error reading configuration file";
        assert !StringUtils.isEmpty(campaignConfiguration.getCampaignName()) : "No name specified for campaign";
        for (BannerConfiguration bannerConfiguration : campaignConfiguration.getBannerConfigurations()) {
            validateBannerConfig(bannerConfiguration);
        }
    }

    private void validateBannerConfig(BannerConfiguration bannerConfiguration) {
        String bannerName = "Banner " + (bannerConfiguration.getTitle() != null ? bannerConfiguration.getTitle() : "has no title");
        assert !StringUtils.isEmpty(bannerConfiguration.getTitle()) : bannerName;
        assert !StringUtils.isEmpty(bannerConfiguration.getText()) : bannerName + " has no <text> element";
        assert bannerConfiguration.getTitle().length() < 32 : " too long title (max 32)";
        assert bannerConfiguration.getText().length() < 75 : " too long text (max 75)";

        if (bannerConfiguration.isCategoryBanner()) {
            assert !StringUtils.isEmpty(bannerConfiguration.getCategoryName()) : " has no <categoryName> element";
            assert bannerConfiguration.getClickPrice() != null ||  bannerConfiguration.getClickPrice() < 0.01 : " invalid <clickPrice> value";
        } else {
            List<BannerConfiguration.PhraseConfiguration> phrasesConfiguration = bannerConfiguration.getPhrasesConfiguration();
            //assert CollectionUtils.isEmpty(phrasesConfiguration) : " has no <phrases>";
            assert !StringUtils.isEmpty(bannerConfiguration.getHref()) : " has no <href> element";
            for (BannerConfiguration.PhraseConfiguration phraseConfiguration : phrasesConfiguration) {
                assert !StringUtils.isEmpty(phraseConfiguration.getText()) : " one of phrases has no <text> element";
            }
        }
    }

    private XStream createXstream() {
        XStream xStream = new XStream(new StaxDriver()) {
            @Override
            protected MapperWrapper wrapMapper(MapperWrapper next) {
                return new MapperWrapper(next) {
                    @Override
                    public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                        if (definedIn == Object.class) {
                            return false;
                        }
                        return super.shouldSerializeMember(definedIn, fieldName);
                    }
                };
            }
        };
        xStream.processAnnotations(CampaignConfiguration.class);
        xStream.processAnnotations(BannerConfiguration.class);

        return xStream;

    }


    @Required
    public void setConfigurationPath(String configurationPath) {
        this.configurationPath = configurationPath;
    }
}
