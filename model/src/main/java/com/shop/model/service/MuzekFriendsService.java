package com.shop.model.service;

import com.shop.model.dao.MuzekFriendsDao;
import com.shop.model.dao.MuzekTrackListDao;
import com.shop.model.persistence.MuzekFriends;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.MuzekFriendsImpl;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public class MuzekFriendsService extends BaseModelService<MuzekFriends, MuzekFriendsDao> {



    @Transactional
    public void create(String userId, List<String> friendsId) {


        for (String friendId : friendsId) {

            if (dao.findFriend(userId, friendId) == null){

                MuzekFriends friend = new MuzekFriendsImpl();
                friend.setUserId(userId);
                friend.setFriendId(friendId);

                dao.save(friend);

            }
        }


    }


    @Transactional
    public List<MuzekFriends> findFriends(String userId) {

        return dao.findFriends(userId);

    }


}
