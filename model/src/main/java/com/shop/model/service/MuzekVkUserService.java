package com.shop.model.service;

import com.shop.model.dao.MuzekVkTrackDao;
import com.shop.model.dao.MuzekVkUserDao;
import com.shop.model.persistence.MuzekVkTrack;
import com.shop.model.persistence.MuzekVkUser;
import com.shop.model.persistence.impl.MuzekVkTrackImpl;
import com.shop.model.persistence.impl.MuzekVkUserImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


public class MuzekVkUserService extends BaseModelService<MuzekVkUser, MuzekVkUserDao> {



    @Transactional
    public void create(String fbUserId, String vkUserId, String img, String city, String offset, String name) {


        MuzekVkUserImpl track = new MuzekVkUserImpl();
        track.setFbUserId(fbUserId);
        track.setCreationDate(new Date());
        track.setName(name);
        track.setOffset(offset);
        track.setImg(img);
        track.setCity(city);
        track.setVkUserId(vkUserId);

        dao.save(track);

    }


}
