package com.shop.model.service;

import com.shop.model.dao.hibernate.ActivityLogDaoImpl;
import com.shop.model.persistence.ActivityLog;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.ActivityLogImpl;
import org.hibernate.Query;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
public class ActivityLogModelService extends BaseModelService<ActivityLog, ActivityLogDaoImpl> {

    @Override
    @Transactional
    public List<ActivityLog> findAll(Integer from, Integer limit) {

        Query query = dao.getSessionFactory().getCurrentSession().createQuery("from ActivityLogImpl a " +
                " where a.level = 'USER' " +
                " order by dateOfAction desc ");
        query.setFirstResult(from);
        query.setMaxResults(limit);

        return query.list();

    }

    @Transactional
    public ActivityLog createActivityLog(User user, String description) {

        ActivityLog activityLog = createActivityLogObject(user, ActivityLogImpl.ACTIVITY_LOG_TYPE.GENERAL, ActivityLogImpl.LEVEL.ADMIN, description);
        return activityLog;

    }

    @Transactional
    public ActivityLog createActivityLog(User user, ActivityLogImpl.ACTIVITY_LOG_TYPE type, String description) {

        ActivityLog activityLog = createActivityLogObject(user, type, ActivityLogImpl.LEVEL.ADMIN, description);
        return activityLog;

    }

    @Transactional
    public ActivityLog createHemechooLog(String description, String searchString, String clientUrl, String userAgent, String ip) {

        ActivityLog activityLog = dao.createPersistence();
        activityLog.setDescription(description);
        activityLog.setLevel(ActivityLogImpl.LEVEL.USER);
        activityLog.setClientUrl(clientUrl);
        activityLog.setSearchString(searchString);
        activityLog.setType(ActivityLogImpl.ACTIVITY_LOG_TYPE.HEMECHOO_LOG);
        activityLog.setIp(ip);

        activityLog.setUserAgent(userAgent);

        dao.save(activityLog);
        return activityLog;

    }

    @Transactional
    public ActivityLog createHemechooLog(String description, String searchString, String clientUrl, String userAgent, Long productId, String ip) {

        ActivityLog activityLog = dao.createPersistence();
        activityLog.setDescription(description);
        activityLog.setLevel(ActivityLogImpl.LEVEL.USER);
        activityLog.setClientUrl(clientUrl);
        activityLog.setSearchString(searchString);
        activityLog.setType(ActivityLogImpl.ACTIVITY_LOG_TYPE.HEMECHOO_LOG);
        activityLog.setProductId(productId);
        activityLog.setIp(ip);

        activityLog.setUserAgent(userAgent);

        dao.save(activityLog);
        return activityLog;

    }

    @Transactional
    public ActivityLog createHemechooLog(String description, String searchString, String clientUrl, String userAgent, Long productId, String ip,
                                         String version) {

        ActivityLog activityLog = dao.createPersistence();
        activityLog.setDescription(description);
        activityLog.setLevel(ActivityLogImpl.LEVEL.USER);
        activityLog.setClientUrl(clientUrl);
        activityLog.setSearchString(searchString);
        activityLog.setType(ActivityLogImpl.ACTIVITY_LOG_TYPE.HEMECHOO_LOG);
        activityLog.setProductId(productId);
        activityLog.setIp(ip);
        activityLog.setVersion(version);

        activityLog.setUserAgent(userAgent);

        dao.save(activityLog);
        return activityLog;

    }

    @Transactional
    public ActivityLog createHemechooLog(String description, String searchString, String clientUrl) {

        return createHemechooLog(description, searchString, clientUrl, null, null);

    }

    @Transactional
    public ActivityLog createActivityLog(User user, ActivityLogImpl.ACTIVITY_LOG_TYPE type, ActivityLogImpl.LEVEL level, String description) {

        ActivityLog activityLog = createActivityLogObject(user, type, level, description);
        return activityLog;

    }

    @Transactional
    public ActivityLog findLogRecord(String description, String clientUrl) {

        Query query = dao.getSessionFactory().getCurrentSession().createQuery("from ActivityLogImpl a " +
                " where a.clientUrl = :clientUrl and a.description = :description" +
                " order by dateOfAction desc ");

        query.setMaxResults(1);

        query.setString("description", description);
        query.setString("clientUrl", clientUrl);

        List<ActivityLog> list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }

        return null;

    }

    @Transactional
    public List<ActivityLog> findLogRecordsForPageProcessing() {

        Query query = dao.getSessionFactory().getCurrentSession().createQuery("from ActivityLogImpl a " +
                " where a.description = :description  and a.status is null and a.productId is not null " +
                " order by dateOfAction asc ");

        query.setMaxResults(10);

        query.setString("description", "found");

        List<ActivityLog> list = query.list();

        return list;

    }

    @Transactional
    public List<ActivityLog> findLogRecordsOfImages(String date1, String date2) {

        Query query = dao.getSessionFactory().getCurrentSession().createQuery("from ActivityLogImpl a " +
                " where a.dateOfAction < '"+ date1 +"' and a.dateOfAction > '"+ date2 +"' " +
                " and a.description = 'images found' " +
                " group by a.searchString " +
                " order by a.dateOfAction desc ");

        query.setMaxResults(10000);

        List<ActivityLog> list = query.list();

        return list;

    }

    @Transactional
    public ActivityLog updateStatus(Long id, ActivityLogImpl.STATUS status) {

        ActivityLog activityLog = dao.findById(id);
        activityLog.setStatus(status);
        dao.save(activityLog);

        return activityLog;
    }

    private ActivityLog createActivityLogObject(User user, ActivityLogImpl.ACTIVITY_LOG_TYPE type, ActivityLogImpl.LEVEL level, String description) {
        ActivityLog activityLog = dao.createPersistence();
        activityLog.setUser(user);
        activityLog.setDescription(description);
        activityLog.setLevel(level);
        activityLog.setType(type);
        dao.save(activityLog);
        return activityLog;
    }
}
