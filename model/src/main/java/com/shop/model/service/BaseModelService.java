package com.shop.model.service;

import com.shop.model.dao.BaseDao;
import com.shop.model.dao.BaseDaoImpl;
import com.shop.model.persistence.Persistence;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 07.03.12
 */
public abstract class BaseModelService<C extends Persistence, B extends BaseDao<C>> {
    protected B dao;

    @Transactional
    public void save(C persistence){
        dao.save(persistence);
    }

    @Transactional
    public void remove(Long id){
        dao.remove(id);
    }

    @Transactional
    public List<C> findAll(){
        return dao.findAll();
    }

    @Transactional
    public List<C> findAll(Integer from, Integer limit){
        return dao.find(from, limit);
    }

    @Transactional
    public C findById(Long id){
        return dao.findById(id);
    }

    @Transactional
    public C createPersistence(){
        return dao.createPersistence();
    }

    /* Setters */

    public void setDao(B dao) {
        this.dao = dao;
    }
}
