package com.shop.model.service;

import com.shop.model.dao.MuzekAlbumsDao;
import com.shop.model.persistence.MuzekAlbums;
import com.shop.model.persistence.impl.MuzekAlbumsImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public class MuzekAlbumsService extends BaseModelService<MuzekAlbums, MuzekAlbumsDao> {



    @Transactional
    public MuzekAlbums create(MuzekAlbumsImpl.ALBUM_SOURCE source, Long userId, String userFBId, Long vkAlbumId, Long vkOwnerId, String vkAlbumTitle, String youtubeAlbumId, String youtubeAlbumTitle) {

        MuzekAlbums album = new MuzekAlbumsImpl();

        List<MuzekAlbums> albums = null;
        if (vkAlbumId != null){
            albums = dao.findTracksByUserFBidAndAlbumId(userId, vkAlbumId);
        }
        if (youtubeAlbumId != null){
            albums = dao.findTracksByUserFBidAndAlbumIdYoutube(userId, youtubeAlbumId);
        }
        if (albums != null && albums.size() > 0){
            album = albums.get(0);
        }


        album.setVkAlbumId(vkAlbumId);
        album.setAlbumSource(source);
        album.setCreationDate(new Date());
        album.setUserFBid(userFBId);
        album.setUserId(userId);
        album.setVkAlbumTitle(vkAlbumTitle);
        album.setVkOwnerId(vkOwnerId);

        album.setYoutubeAlbumId(youtubeAlbumId);
        album.setYoutubeAlbumTitle(youtubeAlbumTitle);

        dao.save(album);

        return album;
    }

//    @Transactional
//    public MuzekTrackList create(Long userId, String userFBId, String trackId, MuzekTrackListImpl.MUZEK_TRACK_SOURCE source, String name,
//                                 String artworkUrl, String trackPermalink, String username, String userPermalink,
//                                 String vkTitle, String vkArtist, Long vkDuration,
//                                 Long vkAlbumId,
//                                 MuzekTrackListImpl.VK_IMPORT_STATUS importStatus) {
//
//        MuzekTrackList trackList = new MuzekTrackListImpl();
//        trackList.setUserFBid(userFBId);
//        trackList.setTrackId(trackId);
//        trackList.setUserId(userId);
//        trackList.setSource(source);
//        trackList.setName(name);
//
//
//        trackList.setArtworkUrl(artworkUrl);
//        trackList.setTrackPermalink(trackPermalink);
//        trackList.setUsername(username);
//        trackList.setUser_permalink_url(userPermalink);
//
//        trackList.setVkTitle(vkTitle);
//        trackList.setVkArtist(vkArtist);
//        trackList.setVkDuration(vkDuration);
//        trackList.setVkImportStatus(importStatus);
//        trackList.setVkAlbumId(vkAlbumId);
//
//        dao.save(trackList);
//
//        return trackList;
//    }
//
//    @Transactional
//    public MuzekTrackList createHidden(Long userId, String userFBId, MuzekTrackListImpl.MUZEK_TRACK_SOURCE source,
//
//                                 String vkTitle, String vkArtist, Long vkDuration,
//                                 MuzekTrackListImpl.VK_IMPORT_STATUS importStatus) {
//
//        MuzekTrackList trackList = new MuzekTrackListImpl();
//        trackList.setUserFBid(userFBId);
//        trackList.setUserId(userId);
//        trackList.setSource(source);
//
//        trackList.setStatus(MuzekTrackListImpl.MUZEK_TRACK_STATUS.HIDDEN);
//
//        trackList.setVkTitle(vkTitle);
//        trackList.setVkArtist(vkArtist);
//        trackList.setVkDuration(vkDuration);
//        trackList.setVkImportStatus(importStatus);
//
//        dao.save(trackList);
//
//        return trackList;
//    }
//
//    @Transactional
//    public MuzekTrackList remove(String userFBId, String trackId) {
//
//        MuzekTrackList muzekTrackList = dao.findTracksByUserFBidAndTrackId(userFBId, trackId);
//        muzekTrackList.setStatus(MuzekTrackListImpl.MUZEK_TRACK_STATUS.HIDDEN);
//
//        dao.save(muzekTrackList);
//
//        return muzekTrackList;
//    }
//
    @Transactional
    public List<MuzekAlbums> findTracksByUserFBid(String muzekFBid){
        return dao.findAlbumsByUserFBid(muzekFBid);
    }
//
//    @Transactional
//    public void setDefaultTracks(User user){
//
//        List<MuzekTrackList> defaultTracks = dao.findDefaultTracks();
//
//        for (MuzekTrackList defaultTrack : defaultTracks) {
//
//            create(user.getIdentifier(), user.getMuzekFBid(), defaultTrack.getTrackId(), defaultTrack.getSource(), defaultTrack.getName(),
//                    defaultTrack.getArtworkUrl(), defaultTrack.getTrackPermalink(), defaultTrack.getUsername(), defaultTrack.getUser_permalink_url());
//        }
//
//    }


}
