package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekTrackList;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_track_list")
public class MuzekTrackListImpl extends PersistenceImpl implements MuzekTrackList {

    public enum MUZEK_TRACK_SOURCE{
        SOUNDCLOUD,
        YOUTUBE,
    }

    public enum VK_IMPORT_STATUS{
        IMPORTED,
        NOT_FOUND,

    }

    public enum MUZEK_TRACK_STATUS{
        HIDDEN,
        DEFAULT_1,
        DEFAULT_2,
        DEFAULT_3,
        DEFAULT_4,
        DEFAULT_5,
    }

    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_fb_id")
    private String userFBid;

    @Column(name = "track_id")
    private String trackId;

    @Column(name = "name")
    private String name;

    @Column(name = "source")
    @Enumerated(value = EnumType.STRING)
    private MUZEK_TRACK_SOURCE source;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private MUZEK_TRACK_STATUS status;

    @Column(name = "artwork_url")
    private String artworkUrl;

    @Column(name = "track_permalink_url")
    private String trackPermalink;

    @Column(name = "username")
    private String username;

    @Column(name = "user_permalink_url")
    private String user_permalink_url;

    @Column(name = "vk_import_status")
    @Enumerated(value = EnumType.STRING)
    private VK_IMPORT_STATUS vkImportStatus;

    @Column(name = "vk_title")
    private String vkTitle;

    @Column(name = "vk_artist")
    private String vkArtist;

    @Column(name = "vk_duration")
    private Long vkDuration;

    @Column(name = "vk_album_id")
    private Long vkAlbumId;

    @Column(name = "youtube_album_id")
    private String youtubeAlbumId;

    @Column(name = "vk_track_id")
    private Long vkTrackId;

    @Column(name = "sort_field")
    private Double sortField;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MUZEK_TRACK_SOURCE getSource() {
        return source;
    }

    public void setSource(MUZEK_TRACK_SOURCE source) {
        this.source = source;
    }

    public String getUserFBid() {
        return userFBid;
    }

    public void setUserFBid(String userFBid) {
        this.userFBid = userFBid;
    }

    public MUZEK_TRACK_STATUS getStatus() {
        return status;
    }

    public void setStatus(MUZEK_TRACK_STATUS status) {
        this.status = status;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    public String getTrackPermalink() {
        return trackPermalink;
    }

    public void setTrackPermalink(String trackPermalink) {
        this.trackPermalink = trackPermalink;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_permalink_url() {
        return user_permalink_url;
    }

    public void setUser_permalink_url(String user_permalink_url) {
        this.user_permalink_url = user_permalink_url;
    }

    public VK_IMPORT_STATUS getVkImportStatus() {
        return vkImportStatus;
    }

    public void setVkImportStatus(VK_IMPORT_STATUS vkImportStatus) {
        this.vkImportStatus = vkImportStatus;
    }

    public String getVkTitle() {
        return vkTitle;
    }

    public void setVkTitle(String vkTitle) {
        this.vkTitle = vkTitle;
    }

    public String getVkArtist() {
        return vkArtist;
    }

    public void setVkArtist(String vkArtist) {
        this.vkArtist = vkArtist;
    }

    public Long getVkDuration() {
        return vkDuration;
    }

    public void setVkDuration(Long vkDuration) {
        this.vkDuration = vkDuration;
    }

    public Long getVkAlbumId() {
        return vkAlbumId;
    }

    public void setVkAlbumId(Long vkAlbumId) {
        this.vkAlbumId = vkAlbumId;
    }

    public Long getVkTrackId() {
        return vkTrackId;
    }

    public void setVkTrackId(Long vkTrackId) {
        this.vkTrackId = vkTrackId;
    }

    public Double getSortField() {
        return sortField;
    }

    public void setSortField(Double sortField) {
        this.sortField = sortField;
    }

    public String getYoutubeAlbumId() {
        return youtubeAlbumId;
    }

    public void setYoutubeAlbumId(String youtubeAlbumId) {
        this.youtubeAlbumId = youtubeAlbumId;
    }
}
