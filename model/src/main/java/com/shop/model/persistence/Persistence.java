package com.shop.model.persistence;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 02.03.12
 */
public interface Persistence {
    Long getIdentifier();

    void setIdentifier(Long identifier);
}
