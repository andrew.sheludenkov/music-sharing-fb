package com.shop.model.persistence.impl;

import com.shop.model.persistence.ActivityLog;
import com.shop.model.persistence.User;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
@Entity
@Table(name = "activity_log")
public class ActivityLogImpl extends PersistenceImpl implements ActivityLog {

    public enum ACTIVITY_LOG_TYPE{
        GENERAL,
        SEARCH_PRODUCT,
        SIGN_IN,
        CHANGE_USER_CREDENTIALS,
        DISCOUNT,
        VARIABLE,
        CONSOLIDATION,
        DECONSOLIDATION,
        EDIT_CATEGORY,
        ASSIGN_CATEGORY_FOR_PRODUCT,
        DESCRIPTION,

        ERROR,
        HEMECHOO_LOG,
    }

    public enum LEVEL{
        USER,
        ADMIN,
        ERROR,
    }

    public enum STATUS{
        PROCESS,
        ERROR,
        SUCCESS,
        NO_PARSER_FOUND,
        NO_COMMENTS_FOUND,
        ALREADY_PROCESSED,
    }


    @ManyToOne(targetEntity = UserImpl.class)
    @JoinColumn(name = "user_id", referencedColumnName = "identifier")
    private User user;
    @Column(name = "description")
    private String description;
    @Column(name = "search_string")
    private String searchString;
    @Column(name = "client_url")
    private String clientUrl;
    @Column(name = "date_of_action")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfAction = new Date();

    @Column(name = "type")
    @Enumerated(value = EnumType.STRING)
    private ACTIVITY_LOG_TYPE type;

    @Column(name = "level")
    @Enumerated(value = EnumType.STRING)
    private LEVEL level;

    @Column(name = "user_agent")
    private String userAgent;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private STATUS status;

    @Column(name = "ip")
    private String ip;

    @Column(name = "version")
    private String version;

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Date getDateOfAction() {
        return dateOfAction;
    }

    @Override
    public void setDateOfAction(Date dateOfAction) {
        this.dateOfAction = dateOfAction;
    }

    public ACTIVITY_LOG_TYPE getType() {
        return type;
    }

    public void setType(ACTIVITY_LOG_TYPE type) {
        this.type = type;
    }

    public LEVEL getLevel() {
        return level;
    }

    public void setLevel(LEVEL level) {
        this.level = level;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
