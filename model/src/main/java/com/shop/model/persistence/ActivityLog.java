package com.shop.model.persistence;

import com.shop.model.persistence.impl.ActivityLogImpl;

import java.util.Date;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
public interface ActivityLog extends Persistence {
    User getUser();

    void setUser(User user);

    String getDescription();

    void setDescription(String description);

    Date getDateOfAction();

    void setDateOfAction(Date dateOfAction);

    public ActivityLogImpl.ACTIVITY_LOG_TYPE getType();

    public void setType(ActivityLogImpl.ACTIVITY_LOG_TYPE type);

    public ActivityLogImpl.LEVEL getLevel();

    public void setLevel(ActivityLogImpl.LEVEL level);

    public String getSearchString();

    public void setSearchString(String searchString) ;

    public String getClientUrl();

    public void setClientUrl(String clientUrl);

    public String getUserAgent();

    public void setUserAgent(String userAgent);

    public Long getProductId() ;

    public void setProductId(Long productId);

    public ActivityLogImpl.STATUS getStatus() ;

    public void setStatus(ActivityLogImpl.STATUS status) ;

    public String getIp() ;

    public void setIp(String ip) ;

    public String getVersion();

    public void setVersion(String version) ;


}
