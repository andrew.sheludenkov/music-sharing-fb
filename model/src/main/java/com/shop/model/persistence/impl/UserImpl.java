package com.shop.model.persistence.impl;


import com.shop.model.persistence.User;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 23.04.12
 */
@Entity
@Table(name = "shop_user", uniqueConstraints = {@UniqueConstraint(name = "loginpass", columnNames = {"user_username", "user_password"})})
public class UserImpl extends PersistenceImpl implements User {

    public enum USER_PEERS_STATUS{
        HAS_PEERS,
        NO_ANY_PEERS,
        PEERS_EXCEEDED,
        PROGRESS,
    }
    public enum RANKING_STATUS{
        NOT_RANKED,
        COMPLETE,
    }
    public enum MUZEK_STATUS{
        ACTIVATED,
        JUST_CREATED,
    }

    public enum USER_TRACK_VK_IMPORT_STATUS{
        PROGRESS,
        COMPLETE,
        FAILED
    }

    @Column(name = "creation_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "user_name")
    private String name;
    @Column(name = "user_username", nullable = false)
    private String username;
    @Column(name = "user_password")
    private String password;
    @Column(name = "login_hash", unique = true)
    private String loginHash;
    @Column(name = "fast_login", unique = true)
    private String fastLogin;
    @Column(name = "app_market_id")
    private Long appMarketId;
    @Column(name = "session_id", unique = true)
    private String sessionId;
    @Column(name = "email")
    private String email;
    @Column(name = "delivery_address")
    private String deliveryAddress;
    @Column(name = "phone")
    private String phone;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "upper_name")
    private String upperName;

    @Column(name = "car_made")
    private String carMade;
    @Column(name = "car_model")
    private String carModel;
    @Column(name = "car_color")
    private String carColor;
    @Column(name = "wash_type")
    private String washType;
    @Column(name = "lat")
    private String lat;
    @Column(name = "lon")
    private String lon;

    @Column(name = "facebook_uid")
    private String facebookUid;
    @Column(name = "facebook_token")
    private String facebookToken;
    @Column(name = "linkedin_token")
    private String linkedinToken;
    @Column(name = "device_token")
    private String deviceToken;

    @Column(name = "paypal_email")
    private String paypalEmail;
    @Column(name = "yo_key")
    private String yoKey;
    @Column(name = "active_shop_id")
    private Long activeShopId;
    @Column(name = "locale")
    private String locale;

    @Column(name = "ranking_status")
    @Enumerated(value = EnumType.STRING)
    private RANKING_STATUS type;
    @Column(name = "msg_rank")
    private Long msgRank;
    @Column(name = "bot")
    private String bot;
    @Column(name = "promo_code")
    private String promoCode;
    @Column(name = "referral_core")
    private String referralCode;
    @Column(name = "unboxer_user_hash")
    private String unboxerUserHash;
    @Column(name = "store_name")
    private String storeName;
    @Column(name = "unboxer_promo_usage")
    private Integer unboxerPromoUsage;

    @Column(name = "muzek_facebook_id")
    private String muzekFBid;

    @Column(name = "muzek_share")
    private String muzekShare;
    @Column(name = "muzek_feedback_status")
    private String muzekFeedbackStatus;
    @Column(name = "muzek_feedback", columnDefinition = "text")
    private String muzekFeedback;

    @Column(name = "muzek_vk_import_status", columnDefinition = "text")
    private String muzekVkImportStatus;


    @Column(name = "muzek_graph", columnDefinition = "text")
    private String graph;

    @Column(name = "muzek_status")
    @Enumerated(value = EnumType.STRING)
    private MUZEK_STATUS muzekStatus;

    @Column(name = "muzek_agent", columnDefinition = "text")
    private String userAgent;

    @Column(name = "vk_import_status")
    @Enumerated(value = EnumType.STRING)
    private USER_TRACK_VK_IMPORT_STATUS vk_import_status;

    @Column(name = "vk_import_statistics")
    private String vkImportStatistics;

    @Column(name = "vk_albums_raw", columnDefinition = "text")
    private String vkAlbumsRaw;


    @Column(name = "youtube_import_status")
    @Enumerated(value = EnumType.STRING)
    private USER_TRACK_VK_IMPORT_STATUS youtube_import_status;

    @Column(name = "youtube_import_statistics")
    private String youtubeImportStatistics;


    @Column(name = "peers_status")
    @Enumerated(value = EnumType.STRING)
    private USER_PEERS_STATUS peersStatus;

    @Column(name = "peer_showed_time")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date peerShowedTime;

    @Column(name = "last_activity_time")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date lastActivityTime;

    @Column(name = "have_unread_messages")
    private String haveUnreadMessages;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginHash() {
        return loginHash;
    }

    public void setLoginHash(String loginHash) {
        this.loginHash = loginHash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getAppMarketId() {
        return appMarketId;
    }

    @Override
    public void setAppMarketId(Long appMarketId) {
        this.appMarketId = appMarketId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCarMade() {
        return carMade;
    }

    public void setCarMade(String carMade) {
        this.carMade = carMade;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getWashType() {
        return washType;
    }

    public void setWashType(String washType) {
        this.washType = washType;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }



    @Override
    public String getFacebookUid() {
        return facebookUid;
    }

    @Override
    public void setFacebookUid(String facebookUid) {
        this.facebookUid = facebookUid;
    }

    @Override
    public String getFacebookToken() {
        return facebookToken;
    }

    @Override
    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public String getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public Long getActiveShopId() {
        return activeShopId;
    }

    public void setActiveShopId(Long activeShopId) {
        this.activeShopId = activeShopId;
    }

    public String getLinkedinToken() {
        return linkedinToken;
    }

    public void setLinkedinToken(String linkedinToken) {
        this.linkedinToken = linkedinToken;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getYoKey() {
        return yoKey;
    }

    public void setYoKey(String yoKey) {
        this.yoKey = yoKey;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getUpperName() {
        return upperName;
    }

    public void setUpperName(String upperName) {
        this.upperName = upperName;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public RANKING_STATUS getType() {
        return type;
    }

    public void setType(RANKING_STATUS type) {
        this.type = type;
    }

    public Long getMsgRank() {
        return msgRank;
    }

    public void setMsgRank(Long msgRank) {
        this.msgRank = msgRank;
    }

    public String getBot() {
        return bot;
    }

    public void setBot(String bot) {
        this.bot = bot;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getUnboxerUserHash() {
        return unboxerUserHash;
    }

    public void setUnboxerUserHash(String unboxerUserHash) {
        this.unboxerUserHash = unboxerUserHash;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getFastLogin() {
        return fastLogin;
    }

    public void setFastLogin(String fastLogin) {
        this.fastLogin = fastLogin;
    }

    public Integer getUnboxerPromoUsage() {
        return unboxerPromoUsage;
    }

    public void setUnboxerPromoUsage(Integer unboxerPromoUsage) {
        this.unboxerPromoUsage = unboxerPromoUsage;
    }

    public String getMuzekFBid() {
        return muzekFBid;
    }

    public void setMuzekFBid(String muzekFBid) {
        this.muzekFBid = muzekFBid;
    }

    public String getMuzekShare() {
        return muzekShare;
    }

    public void setMuzekShare(String muzekShare) {
        this.muzekShare = muzekShare;
    }

    public String getMuzekFeedback() {
        return muzekFeedback;
    }

    public void setMuzekFeedback(String muzekFeedback) {
        this.muzekFeedback = muzekFeedback;
    }

    public String getMuzekFeedbackStatus() {
        return muzekFeedbackStatus;
    }

    public void setMuzekFeedbackStatus(String muzekFeedbackStatus) {
        this.muzekFeedbackStatus = muzekFeedbackStatus;
    }

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }

    public MUZEK_STATUS getMuzekStatus() {
        return muzekStatus;
    }

    public void setMuzekStatus(MUZEK_STATUS muzekStatus) {
        this.muzekStatus = muzekStatus;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getMuzekVkImportStatus() {
        return muzekVkImportStatus;
    }

    public void setMuzekVkImportStatus(String muzekVkImportStatus) {
        this.muzekVkImportStatus = muzekVkImportStatus;
    }

    public USER_TRACK_VK_IMPORT_STATUS getVk_import_status() {
        return vk_import_status;
    }

    public void setVk_import_status(USER_TRACK_VK_IMPORT_STATUS vk_import_status) {
        this.vk_import_status = vk_import_status;
    }

    public String getVkImportStatistics() {
        return vkImportStatistics;
    }

    public void setVkImportStatistics(String vkImportStatistics) {
        this.vkImportStatistics = vkImportStatistics;
    }

    public String getVkAlbumsRaw() {
        return vkAlbumsRaw;
    }

    public void setVkAlbumsRaw(String vkAlbumsRaw) {
        this.vkAlbumsRaw = vkAlbumsRaw;
    }

    public USER_TRACK_VK_IMPORT_STATUS getYoutube_import_status() {
        return youtube_import_status;
    }

    public void setYoutube_import_status(USER_TRACK_VK_IMPORT_STATUS youtube_import_status) {
        this.youtube_import_status = youtube_import_status;
    }

    public String getYoutubeImportStatistics() {
        return youtubeImportStatistics;
    }

    public void setYoutubeImportStatistics(String youtubeImportStatistics) {
        this.youtubeImportStatistics = youtubeImportStatistics;
    }

    public USER_PEERS_STATUS getPeersStatus() {
        return peersStatus;
    }

    public void setPeersStatus(USER_PEERS_STATUS peersStatus) {
        this.peersStatus = peersStatus;
    }

    public Date getPeerShowedTime() {
        return peerShowedTime;
    }

    public void setPeerShowedTime(Date peerShowedTime) {
        this.peerShowedTime = peerShowedTime;
    }

    public Date getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastActivityTime(Date lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

    public String getHaveUnreadMessages() {
        return haveUnreadMessages;
    }

    public void setHaveUnreadMessages(String haveUnreadMessages) {
        this.haveUnreadMessages = haveUnreadMessages;
    }
}
