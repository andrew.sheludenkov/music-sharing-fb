package com.shop.model.persistence;

import com.shop.model.persistence.impl.MuzekFeedImpl;

import java.util.Date;


public interface MuzekMessage extends Persistence {

    public Date getCreationDate() ;

    public void setCreationDate(Date creationDate) ;

    public Long getUserId() ;

    public void setUserId(Long userId) ;

    public String getSenderFBid() ;

    public void setSenderFBid(String senderFBid);

    public String getSender_name();

    public void setSender_name(String sender_name) ;

    public String getTrackId() ;

    public void setTrackId(String trackId) ;

    public Long getTrackIdentifier();

    public void setTrackIdentifier(Long trackIdentifier) ;

    public String getTrack_name() ;

    public void setTrack_name(String track_name);

    public String getAdded() ;

    public void setAdded(String added);

    public String getListened() ;

    public void setListened(String listened) ;

    public String getRecipientFbId() ;

    public void setRecipientFbId(String recipientFbId) ;

    public String getRecipient_name() ;

    public void setRecipient_name(String recipient_name) ;

    public String getArtworkUrl() ;

    public void setArtworkUrl(String artworkUrl) ;

    public String getTrackSource() ;

    public void setTrackSource(String trackSource);

}
