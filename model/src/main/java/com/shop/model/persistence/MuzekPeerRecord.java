package com.shop.model.persistence;

import com.shop.model.persistence.impl.MuzekFeedImpl;
import com.shop.model.persistence.impl.MuzekPeerRecordImpl;

import java.util.Date;


public interface MuzekPeerRecord extends Persistence {

    public Date getCreationDate() ;

    public void setCreationDate(Date creationDate) ;

    public Long getUserId() ;

    public void setUserId(Long userId);

    public String getUserFBid();

    public void setUserFBid(String userFBid) ;

    public Long getPeerId() ;

    public void setPeerId(Long peerId) ;

    public String getPeerFBid() ;

    public void setPeerFBid(String peerFBid);

    public String getPeerName() ;

    public void setPeerName(String peerName) ;

    public String getTrackId() ;

    public void setTrackId(String trackId) ;

    public Long getTrackIdentifier() ;

    public void setTrackIdentifier(Long trackIdentifier) ;

    public String getTrack_name() ;

    public void setTrack_name(String track_name) ;

    public MuzekPeerRecordImpl.MUZEK_PEER_STATUS getStatus();

    public void setStatus(MuzekPeerRecordImpl.MUZEK_PEER_STATUS status) ;

    public String getArtworkUrl();

    public void setArtworkUrl(String artworkUrl) ;

    public String getTrackSource() ;

    public void setTrackSource(String trackSource) ;

    public Date getShowedTime();

    public void setShowedTime(Date showedTime) ;

}
