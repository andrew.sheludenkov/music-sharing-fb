package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekAlbums;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_albums")
public class MuzekAlbumsImpl extends PersistenceImpl implements MuzekAlbums {

    public enum ALBUM_SOURCE{
        VK,
        YOUTUBE,
        NATIVE,
    }



    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_fb_id")
    private String userFBid;

    @Column(name = "vk_album_id")
    private Long vkAlbumId;

    @Column(name = "youtube_album_id")
    private String youtubeAlbumId;

    @Column(name = "vk_owner_id")
    private Long vkOwnerId;

    @Column(name = "vk_album_titme")
    private String vkAlbumTitle;

    @Column(name = "youtube_album_titme")
    private String youtubeAlbumTitle;

    @Column(name = "muzek_album_source")
    @Enumerated(value = EnumType.STRING)
    private ALBUM_SOURCE albumSource;



    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserFBid() {
        return userFBid;
    }

    public void setUserFBid(String userFBid) {
        this.userFBid = userFBid;
    }

    public Long getVkAlbumId() {
        return vkAlbumId;
    }

    public void setVkAlbumId(Long vkAlbumId) {
        this.vkAlbumId = vkAlbumId;
    }

    public Long getVkOwnerId() {
        return vkOwnerId;
    }

    public void setVkOwnerId(Long vkOwnerId) {
        this.vkOwnerId = vkOwnerId;
    }

    public String getVkAlbumTitle() {
        return vkAlbumTitle;
    }

    public void setVkAlbumTitle(String vkAlbumTitle) {
        this.vkAlbumTitle = vkAlbumTitle;
    }

    public ALBUM_SOURCE getAlbumSource() {
        return albumSource;
    }

    public void setAlbumSource(ALBUM_SOURCE albumSource) {
        this.albumSource = albumSource;
    }

    public String getYoutubeAlbumId() {
        return youtubeAlbumId;
    }

    public void setYoutubeAlbumId(String youtubeAlbumId) {
        this.youtubeAlbumId = youtubeAlbumId;
    }

    public String getYoutubeAlbumTitle() {
        return youtubeAlbumTitle;
    }

    public void setYoutubeAlbumTitle(String youtubeAlbumTitle) {
        this.youtubeAlbumTitle = youtubeAlbumTitle;
    }
}
