package com.shop.model.persistence;

import com.shop.model.persistence.impl.MuzekAlbumsImpl;

import java.util.Date;


public interface MuzekAlbums extends Persistence {

    public Date getCreationDate() ;

    public void setCreationDate(Date creationDate) ;

    public Long getUserId();

    public void setUserId(Long userId) ;

    public String getUserFBid() ;

    public void setUserFBid(String userFBid) ;

    public Long getVkAlbumId();

    public void setVkAlbumId(Long vkAlbumId);

    public Long getVkOwnerId();

    public void setVkOwnerId(Long vkOwnerId);

    public String getVkAlbumTitle();

    public void setVkAlbumTitle(String vkAlbumTitle) ;

    public MuzekAlbumsImpl.ALBUM_SOURCE getAlbumSource();

    public void setAlbumSource(MuzekAlbumsImpl.ALBUM_SOURCE albumSource) ;

    public String getYoutubeAlbumId() ;

    public void setYoutubeAlbumId(String youtubeAlbumId) ;

    public String getYoutubeAlbumTitle() ;

    public void setYoutubeAlbumTitle(String youtubeAlbumTitle) ;

}
