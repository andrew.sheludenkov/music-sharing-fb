package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekFriends;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_friends")
public class MuzekFriendsImpl extends PersistenceImpl implements MuzekFriends {


    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "user_id")
    private String userId;

    @Column(name = "friend_id")
    private String friendId;

    public Date getCreationDate() {
        return creationDate;
    }


    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }
}
