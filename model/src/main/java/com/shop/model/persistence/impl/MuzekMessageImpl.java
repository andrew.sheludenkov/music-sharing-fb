package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekFeed;
import com.shop.model.persistence.MuzekMessage;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_message")
public class MuzekMessageImpl extends PersistenceImpl implements MuzekMessage {




    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "sender_user_id")
    private Long userId;

    @Column(name = "sender_user_fb_id")
    private String senderFBid;

    @Column(name = "sender_user_name")
    private String sender_name;

    @Column(name = "track_id")
    private String trackId;

    @Column(name = "track_identifier")
    private Long trackIdentifier;

    @Column(name = "track_name")
    private String track_name;

    @Column(name = "added")
    private String added;

    @Column(name = "listened")
    private String listened;

    @Column(name = "recepient_user_fb_id")
    private String recipientFbId;

    @Column(name = "recipient_user_name")
    private String recipient_name;

    @Column(name = "artwork_url")
    private String artworkUrl;

    @Column(name = "track_source")
    private String trackSource;


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSenderFBid() {
        return senderFBid;
    }

    public void setSenderFBid(String senderFBid) {
        this.senderFBid = senderFBid;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public Long getTrackIdentifier() {
        return trackIdentifier;
    }

    public void setTrackIdentifier(Long trackIdentifier) {
        this.trackIdentifier = trackIdentifier;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getListened() {
        return listened;
    }

    public void setListened(String listened) {
        this.listened = listened;
    }

    public String getRecipientFbId() {
        return recipientFbId;
    }

    public void setRecipientFbId(String recipientFbId) {
        this.recipientFbId = recipientFbId;
    }

    public String getRecipient_name() {
        return recipient_name;
    }

    public void setRecipient_name(String recipient_name) {
        this.recipient_name = recipient_name;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    public String getTrackSource() {
        return trackSource;
    }

    public void setTrackSource(String trackSource) {
        this.trackSource = trackSource;
    }
}
