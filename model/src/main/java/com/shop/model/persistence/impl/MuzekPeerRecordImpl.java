package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekPeerRecord;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_peer_records")
public class MuzekPeerRecordImpl extends PersistenceImpl implements MuzekPeerRecord {

    public enum MUZEK_PEER_STATUS {
        NOT_SHOWED,
        SHOWED,
    }


    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_fb_id")
    private String userFBid;

    @Column(name = "peer_id")
    private Long peerId;

    @Column(name = "peer_fb_id")
    private String peerFBid;

    @Column(name = "peer_name")
    private String peerName;


    @Column(name = "track_id")
    private String trackId;

    @Column(name = "track_identifier")
    private Long trackIdentifier;

    @Column(name = "track_name")
    private String track_name;



    @Column(name = "artwork_url")
    private String artworkUrl;

    @Column(name = "track_source")
    private String trackSource;

    @Column(name = "peerststaus")
    @Enumerated(value = EnumType.STRING)
    private MUZEK_PEER_STATUS status;

    @Column(name = "showed_time")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date showedTime;


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserFBid() {
        return userFBid;
    }

    public void setUserFBid(String userFBid) {
        this.userFBid = userFBid;
    }

    public Long getPeerId() {
        return peerId;
    }

    public void setPeerId(Long peerId) {
        this.peerId = peerId;
    }

    public String getPeerFBid() {
        return peerFBid;
    }

    public void setPeerFBid(String peerFBid) {
        this.peerFBid = peerFBid;
    }

    public String getPeerName() {
        return peerName;
    }

    public void setPeerName(String peerName) {
        this.peerName = peerName;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public Long getTrackIdentifier() {
        return trackIdentifier;
    }

    public void setTrackIdentifier(Long trackIdentifier) {
        this.trackIdentifier = trackIdentifier;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public MUZEK_PEER_STATUS getStatus() {
        return status;
    }

    public void setStatus(MUZEK_PEER_STATUS status) {
        this.status = status;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    public String getTrackSource() {
        return trackSource;
    }

    public void setTrackSource(String trackSource) {
        this.trackSource = trackSource;
    }

    public Date getShowedTime() {
        return showedTime;
    }

    public void setShowedTime(Date showedTime) {
        this.showedTime = showedTime;
    }
}
