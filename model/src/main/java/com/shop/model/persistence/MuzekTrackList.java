package com.shop.model.persistence;

import com.shop.model.persistence.impl.MuzekTrackListImpl;

import java.util.Date;


public interface MuzekTrackList extends Persistence {
    public Date getCreationDate();

    public void setCreationDate(Date creationDate) ;

    public Long getUserId();

    public void setUserId(Long userId);

    public String getTrackId() ;

    public void setTrackId(String trackId) ;

    public String getName() ;

    public void setName(String name) ;

    public MuzekTrackListImpl.MUZEK_TRACK_SOURCE getSource() ;

    public void setSource(MuzekTrackListImpl.MUZEK_TRACK_SOURCE source) ;

    public String getUserFBid() ;

    public void setUserFBid(String userFBid) ;

    public MuzekTrackListImpl.MUZEK_TRACK_STATUS getStatus() ;

    public void setStatus(MuzekTrackListImpl.MUZEK_TRACK_STATUS status) ;

    public String getArtworkUrl() ;

    public void setArtworkUrl(String artworkUrl) ;

    public String getTrackPermalink();

    public void setTrackPermalink(String trackPermalink);

    public String getUsername() ;

    public void setUsername(String username);

    public String getUser_permalink_url() ;

    public void setUser_permalink_url(String user_permalink_url) ;

    public MuzekTrackListImpl.VK_IMPORT_STATUS getVkImportStatus() ;

    public void setVkImportStatus(MuzekTrackListImpl.VK_IMPORT_STATUS vkImportStatus);

    public String getVkTitle();

    public void setVkTitle(String vkTitle);

    public String getVkArtist();

    public void setVkArtist(String vkArtist) ;

    public Long getVkDuration();

    public void setVkDuration(Long vkDuration) ;

    public Long getVkAlbumId();

    public void setVkAlbumId(Long vkAlbumId) ;

    public Long getVkTrackId();

    public void setVkTrackId(Long vkTrackId) ;

    public Double getSortField();

    public void setSortField(Double sortField);

    public String getYoutubeAlbumId() ;

    public void setYoutubeAlbumId(String youtubeAlbumId);
}
