package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekFeed;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_feed")
public class MuzekFeedImpl extends PersistenceImpl implements MuzekFeed {

    public enum MUZEK_FEED_ITEM_TYPE{
        LISTEN_TO,
        ADD_TO_PLAYLIST,
        IMPORTED_PLAYLIST_VK,
        IMPORTED_PLAYLIST_YT
    }


    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_fb_id")
    private String userFBid;

    @Column(name = "track_id")
    private String trackId;

    @Column(name = "track_identifier")
    private Long trackIdentifier;

    @Column(name = "user_name")
    private String user_name;

    @Column(name = "track_name")
    private String track_name;

    @Column(name = "likes_number")
    private Integer likes_number;

    @Column(name = "dislikes_number")
    private Integer dislikes_number;

    @Column(name = "feed_item_type")
    @Enumerated(value = EnumType.STRING)
    private MUZEK_FEED_ITEM_TYPE status;

    @Column(name = "artwork_url")
    private String artworkUrl;

    @Column(name = "track_source")
    private String trackSource;


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }



    public String getUserFBid() {
        return userFBid;
    }

    public void setUserFBid(String userFBid) {
        this.userFBid = userFBid;
    }



    public String getArtworkUrl() {
        return artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public Integer getLikes_number() {
        return likes_number;
    }

    public void setLikes_number(Integer likes_number) {
        this.likes_number = likes_number;
    }

    public Integer getDislikes_number() {
        return dislikes_number;
    }

    public void setDislikes_number(Integer dislikes_number) {
        this.dislikes_number = dislikes_number;
    }

    public MUZEK_FEED_ITEM_TYPE getStatus() {
        return status;
    }

    public void setStatus(MUZEK_FEED_ITEM_TYPE status) {
        this.status = status;
    }

    public Long getTrackIdentifier() {
        return trackIdentifier;
    }

    public void setTrackIdentifier(Long trackIdentifier) {
        this.trackIdentifier = trackIdentifier;
    }

    public String getTrackSource() {
        return trackSource;
    }

    public void setTrackSource(String trackSource) {
        this.trackSource = trackSource;
    }
}
