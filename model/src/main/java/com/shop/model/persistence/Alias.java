package com.shop.model.persistence;


import com.shop.model.persistence.impl.AliasImpl;


import java.util.Date;

public interface Alias extends Persistence {


    public Date getCreationDate();

    public void setCreationDate(Date creationDate) ;

    public String getFrom();

    public void setFrom(String from);

    public String getTo() ;

    public void setTo(String to) ;

    public String getText();

    public void setText(String text);

    public AliasImpl.ALIAS_TYPE getAliasType();

    public void setAliasType(AliasImpl.ALIAS_TYPE aliasType);

    public Boolean getRemove();

    public void setRemove(Boolean remove);

}
