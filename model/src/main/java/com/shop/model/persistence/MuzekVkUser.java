package com.shop.model.persistence;

import java.util.Date;


public interface MuzekVkUser extends Persistence {

    public Date getCreationDate() ;

    public void setCreationDate(Date creationDate) ;

    public String getFbUserId() ;

    public void setFbUserId(String fbUserId) ;

    public String getVkUserId() ;

    public void setVkUserId(String vkUserId);

    public String getName();

    public void setName(String name) ;

    public String getImg() ;

    public void setImg(String img) ;

    public String getCity() ;

    public void setCity(String city);

    public String getOffset() ;

    public void setOffset(String offset) ;

}
