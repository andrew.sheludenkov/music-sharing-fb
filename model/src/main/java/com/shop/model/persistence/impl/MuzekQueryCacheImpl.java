package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekQueryCache;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_query_cache")
public class MuzekQueryCacheImpl extends PersistenceImpl implements MuzekQueryCache {

    public enum QUERY_TYPE{
        YOU_TUBE_QUERY,
        YOU_TUBE_IMPORT,
        SOUNDCLOUD,
    }

    public enum VALIDATION{
        VALID,
        INVALID,
    }


    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "query_string")
    private String queryString;

    @Column(name = "response_string")
    private String responseString;

    @Column(name = "query_type")
    @Enumerated(value = EnumType.STRING)
    private QUERY_TYPE queryType;

    @Column(name = "validation")
    @Enumerated(value = EnumType.STRING)
    private VALIDATION validation;

    @Column(name = "nr_of_usages")
    private Long nrOfUsages;

    @Column(name = "last_user_used")
    private String lastUserUsed;


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }

    public QUERY_TYPE getQueryType() {
        return queryType;
    }

    public void setQueryType(QUERY_TYPE queryType) {
        this.queryType = queryType;
    }

    public VALIDATION getValidation() {
        return validation;
    }

    public void setValidation(VALIDATION validation) {
        this.validation = validation;
    }

    public Long getNrOfUsages() {
        return nrOfUsages;
    }

    public void setNrOfUsages(Long nrOfUsages) {
        this.nrOfUsages = nrOfUsages;
    }

    public String getLastUserUsed() {
        return lastUserUsed;
    }

    public void setLastUserUsed(String lastUserUsed) {
        this.lastUserUsed = lastUserUsed;
    }
}
