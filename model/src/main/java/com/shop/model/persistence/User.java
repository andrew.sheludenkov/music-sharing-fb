package com.shop.model.persistence;


import com.shop.model.persistence.impl.UserImpl;


import java.util.Date;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 23.04.12
 */
public interface User extends Persistence {
    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getLoginHash();

    void setLoginHash(String loginHash);

    String getName();

    void setName(String name);

    Long getAppMarketId();

    void setAppMarketId(Long appMarketId);

    public String getSessionId();

    public void setSessionId(String sessionId);

    public String getEmail();

    public void setEmail(String email);

    public String getDeliveryAddress();

    public void setDeliveryAddress(String deliveryAddress);

    public String getPhone();

    public void setPhone(String phone);

    public String getFullName();

    public void setFullName(String fullName);


    public String getCarMade();

    public void setCarMade(String carMade);

    public String getCarModel();

    public void setCarModel(String carModel);

    public String getCarColor();

    public void setCarColor(String carColor);

    public String getWashType();

    public void setWashType(String washType);

    public String getLat();

    public void setLat(String lat);

    public String getLon();

    public void setLon(String lon);



    String getFacebookUid();

    void setFacebookUid(String facebookUid);

    String getFacebookToken();

    void setFacebookToken(String facebookToken);

    public String getPaypalEmail();

    public void setPaypalEmail(String paypalEmail);

    public Long getActiveShopId();

    public void setActiveShopId(Long activeShopId);

    public String getLinkedinToken();

    public void setLinkedinToken(String linkedinToken);

    public String getDeviceToken();

    public void setDeviceToken(String deviceToken);

    public String getYoKey();

    public void setYoKey(String yoKey) ;

    public Date getCreationDate() ;

    public void setCreationDate(Date creationDate);

    public String getUpperName();

    public void setUpperName(String upperName) ;

    public String getLocale();

    public void setLocale(String locale);

    public UserImpl.RANKING_STATUS getType();

    public void setType(UserImpl.RANKING_STATUS type) ;

    public Long getMsgRank();

    public void setMsgRank(Long msgRank) ;

    public String getBot();

    public void setBot(String bot);

    public String getPromoCode() ;

    public void setPromoCode(String promoCode);

    public String getUnboxerUserHash() ;

    public void setUnboxerUserHash(String unboxerUserHash) ;

    public String getReferralCode();

    public void setReferralCode(String referralCode);

    public String getStoreName() ;

    public void setStoreName(String storeName) ;

    public String getFastLogin();

    public void setFastLogin(String fastLogin);

    public Integer getUnboxerPromoUsage();

    public void setUnboxerPromoUsage(Integer unboxerPromoUsage);

    public String getMuzekFBid();

    public void setMuzekFBid(String muzekFBid);

    public String getMuzekShare();

    public void setMuzekShare(String muzekShare);

    public String getMuzekFeedback();

    public void setMuzekFeedback(String muzekFeedback) ;

    public String getMuzekFeedbackStatus();

    public void setMuzekFeedbackStatus(String muzekFeedbackStatus);

    public String getGraph() ;

    public void setGraph(String graph);

    public UserImpl.MUZEK_STATUS getMuzekStatus() ;

    public void setMuzekStatus(UserImpl.MUZEK_STATUS muzekStatus) ;

    public String getUserAgent();

    public void setUserAgent(String userAgent) ;

    public String getMuzekVkImportStatus();

    public void setMuzekVkImportStatus(String muzekVkImportStatus) ;

    public UserImpl.USER_TRACK_VK_IMPORT_STATUS getVk_import_status() ;

    public void setVk_import_status(UserImpl.USER_TRACK_VK_IMPORT_STATUS vk_import_status);

    public String getVkImportStatistics();

    public void setVkImportStatistics(String vkImportStatistics);

    public String getVkAlbumsRaw();

    public void setVkAlbumsRaw(String vkAlbumsRaw) ;

    public UserImpl.USER_TRACK_VK_IMPORT_STATUS getYoutube_import_status() ;

    public void setYoutube_import_status(UserImpl.USER_TRACK_VK_IMPORT_STATUS youtube_import_status);

    public String getYoutubeImportStatistics();

    public void setYoutubeImportStatistics(String youtubeImportStatistics) ;

    public UserImpl.USER_PEERS_STATUS getPeersStatus();

    public void setPeersStatus(UserImpl.USER_PEERS_STATUS peersStatus) ;

    public Date getPeerShowedTime() ;

    public void setPeerShowedTime(Date peerShowedTime);

    public Date getLastActivityTime();

    public void setLastActivityTime(Date lastActivityTime);

    public String getHaveUnreadMessages() ;

    public void setHaveUnreadMessages(String haveUnreadMessages) ;

}
