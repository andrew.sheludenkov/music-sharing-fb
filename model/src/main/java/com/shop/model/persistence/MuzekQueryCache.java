package com.shop.model.persistence;

import com.shop.model.persistence.impl.MuzekQueryCacheImpl;

import java.util.Date;


public interface MuzekQueryCache extends Persistence {

    public Date getCreationDate();

    public void setCreationDate(Date creationDate);

    public String getQueryString();

    public void setQueryString(String queryString) ;

    public String getResponseString() ;

    public void setResponseString(String responseString) ;

    public MuzekQueryCacheImpl.QUERY_TYPE getQueryType();

    public void setQueryType(MuzekQueryCacheImpl.QUERY_TYPE queryType) ;

    public MuzekQueryCacheImpl.VALIDATION getValidation();

    public void setValidation(MuzekQueryCacheImpl.VALIDATION validation) ;

    public Long getNrOfUsages();

    public void setNrOfUsages(Long nrOfUsages) ;

    public String getLastUserUsed() ;

    public void setLastUserUsed(String lastUserUsed) ;

}
