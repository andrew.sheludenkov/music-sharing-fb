package com.shop.model.persistence;

import java.util.Date;


public interface MuzekFriends extends Persistence {
    public Date getCreationDate();

    public void setCreationDate(Date creationDate) ;

    public String getUserId() ;

    public void setUserId(String userId) ;

    public String getFriendId() ;

    public void setFriendId(String friendId) ;

}
