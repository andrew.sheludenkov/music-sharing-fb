package com.shop.model.persistence.impl;

import com.shop.model.persistence.MuzekVkTrack;
import com.shop.model.persistence.MuzekVkUser;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "muzek_vk_user")
public class MuzekVkUserImpl extends PersistenceImpl implements MuzekVkUser {




    @Column(name = "creation_date", nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "fb_user_id")
    private String fbUserId;

    @Column(name = "vk_user_id")
    private String vkUserId;

    @Column(name = "name")
    private String name;

    @Column(name = "img")
    private String img;

    @Column(name = "city")
    private String city;

    @Column(name = "offset")
    private String offset;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getFbUserId() {
        return fbUserId;
    }

    public void setFbUserId(String fbUserId) {
        this.fbUserId = fbUserId;
    }

    public String getVkUserId() {
        return vkUserId;
    }

    public void setVkUserId(String vkUserId) {
        this.vkUserId = vkUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }
}
