package com.shop.model.persistence.impl;

import com.shop.model.persistence.Alias;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "alias")
public class AliasImpl extends PersistenceImpl implements Alias {


    public enum ALIAS_TYPE{
        PRODUCT_CHAR
    }

    @Column(name = "creation_date", nullable = true)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date creationDate = new Date();


    @Column(name = "from_text", nullable = true)
    private String from;

    @Column(name = "to_text", nullable = true)
    private String to;


    @Column(name = "text", nullable = true)
    String text;

    @Column(name = "alias_type")
    @Enumerated(value = EnumType.STRING)
    private ALIAS_TYPE aliasType = ALIAS_TYPE.PRODUCT_CHAR;

    @Column(name = "remove")
    Boolean remove;


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ALIAS_TYPE getAliasType() {
        return aliasType;
    }

    public void setAliasType(ALIAS_TYPE aliasType) {
        this.aliasType = aliasType;
    }

    public Boolean getRemove() {
        return remove;
    }

    public void setRemove(Boolean remove) {
        this.remove = remove;
    }
}
