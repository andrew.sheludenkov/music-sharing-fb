package com.shop.model.persistence.impl;

import com.shop.model.persistence.Persistence;

import javax.persistence.*;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 02.03.12
 */
@MappedSuperclass
public class PersistenceImpl implements Persistence {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "identifier", nullable = false, unique = true)
    private Long identifier;

    public PersistenceImpl() {
    }

    public PersistenceImpl(Long identifier) {
        this.identifier = identifier;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersistenceImpl)) return false;

        PersistenceImpl that = (PersistenceImpl) o;

        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return identifier != null ? identifier.hashCode() : 0;
    }
}
