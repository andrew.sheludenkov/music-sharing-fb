package com.shop.model.persistence;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 31.01.13
 */
public enum LoginType {
    CREDENTIALS,
    FACEBOOK
}
