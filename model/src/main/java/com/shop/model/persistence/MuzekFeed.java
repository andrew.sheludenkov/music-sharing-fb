package com.shop.model.persistence;

import com.shop.model.persistence.impl.MuzekFeedImpl;

import java.util.Date;


public interface MuzekFeed extends Persistence {

    public Date getCreationDate() ;

    public void setCreationDate(Date creationDate);

    public Long getUserId() ;

    public void setUserId(Long userId) ;

    public String getTrackId() ;

    public void setTrackId(String trackId) ;

    public String getUserFBid() ;

    public void setUserFBid(String userFBid) ;

    public String getArtworkUrl() ;

    public void setArtworkUrl(String artworkUrl) ;

    public String getUser_name() ;

    public void setUser_name(String user_name) ;

    public String getTrack_name() ;

    public void setTrack_name(String track_name) ;

    public Integer getLikes_number() ;

    public void setLikes_number(Integer likes_number) ;

    public Integer getDislikes_number() ;

    public void setDislikes_number(Integer dislikes_number) ;

    public MuzekFeedImpl.MUZEK_FEED_ITEM_TYPE getStatus() ;

    public void setStatus(MuzekFeedImpl.MUZEK_FEED_ITEM_TYPE status) ;

    public Long getTrackIdentifier() ;

    public void setTrackIdentifier(Long trackIdentifier);

    public String getTrackSource() ;

    public void setTrackSource(String trackSource) ;

}
