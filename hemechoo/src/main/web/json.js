"var __gwt_jsonp__ = new Object();                                                                       "+
"for (var i = 0; i < 10000; i++) {                                                                       "+
"    __gwt_jsonp__['P' + i] = new Object();                                                              "+
"    __gwt_jsonp__['P' + i].onSuccess = function (content) {                                             "+
"        var num = document.currentScript.id;                                                            "+
"        window.postMessage({ type: 'FROM_PAGE_MUZEKBOX_FUNC', number: num, object: content }, '*');     "+
"    }                                                                                                   "+
"}                                                                                                       "+
"                                                                                                        "+
"function initializePlayerEvents() {                                                                     "+
"                                                                                                        "+
"                                                                                                        "+
"    window.addEventListener('message', function (event) {                                               "+
"        if (event.data.type && (event.data.type == 'MUZEKBOX_YOUTUBE_EVENT')) {                         "+
"                                                                                                        "+
"            if (event.data.action == 'INSERT_VIDEO') {                                                  "+
"                                                                                                        "+
"                muzek_yt_player = new window.YT.Player('muzek_youtube_player', {                        "+
"                    height: '144',                                                                      "+
"                    width: '256',                                                                       "+
"                    videoId: event.data.object,                                                         "+
"                    playerVars: {'controls': 0 },                                                       "+
"                    events: {                                                                           "+
"                        'onReady': onPlayerReady,                                                       "+
"                        'onStateChange': onStateChange                                                  "+
"                    }                                                                                   "+
"                });                                                                                     "+
"                                                                                                        "+
"                function onPlayerReady(event) {                                                         "+
"                    event.target.playVideo();                                                           "+
"                }                                                                                       "+
"                                                                                                        "+
"                function onStateChange(event) {                                                         "+
"                                                                                                        "+
"                    if (event.data == window.YT.PlayerState.ENDED) {                                    "+
"                        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT_CALLBACK',                   "+
"                            action: 'STATE_ENDED_CALLBACK'}, '*');                                      "+
"                    }                                                                                   "+
"                                                                                                        "+
"                }                                                                                       "+
"            }                                                                                           "+
"                                                                                                        "+
"            if (event.data.action == 'PLAY') {                                                          "+
"                muzek_yt_player.playVideo();                                                            "+
"            }                                                                                           "+
"            if (event.data.action == 'STOP') {                                                          "+
"                muzek_yt_player.stopVideo();                                                            "+
"                muzek_yt_player.destroy();                                                              "+
"            }                                                                                           "+
"            if (event.data.action == 'SET_VOLUME') {                                                    "+
"                muzek_yt_player.setVolume(event.data.object);                                           "+
"            }                                                                                           "+
"            if (event.data.action == 'SET_PROGRESS') {                                                  "+
"                muzek_yt_player.seekTo(event.data.object, true);                                        "+
"            }                                                                                           "+
"            if (event.data.action == 'PAUSE') {                                                         "+
"                muzek_yt_player.pauseVideo();                                                           "+
"            }                                                                                           "+
"            if (event.data.action == 'LOAD_VIDEO_BY_ID') {                                              "+
"                muzek_yt_player.loadVideoById(event.data.object, 5, 'large');                           "+
"            }                                                                                           "+
"            if (event.data.action == 'GET_STATS') {                                                     "+
"                try {                                                                                   "+
"                                                                                                        "+
"                    if (muzek_yt_player !== 'undefined') {                                              "+
"                                                                                                        "+
"                        var volume = muzek_yt_player.getVolume();                                       "+
"                        var duration = muzek_yt_player.getDuration();                                   "+
"                        var currentTime = muzek_yt_player.getCurrentTime();                             "+
"                                                                                                        "+
"                        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT_CALLBACK',                   "+
"                            action: 'GET_STATS_CALLBACK',                                               "+
"                            volume: volume,                                                             "+
"                            duration: duration,                                                         "+
"                            currentTime: currentTime                                                    "+
"                        }, '*');                                                                        "+
"                                                                                                        "+
"                    }                                                                                   "+
"                } catch (e) {                                                                           "+
"                    console.log('GET_STATS error 2');                                                   "+
"                }                                                                                       "+
"            }                                                                                           "+
"                                                                                                        "+
"        }                                                                                               "+
"                                                                                                        "+
"                                                                                                        "+
"    }, false);                                                                                          "+
"                                                                                                        "+
"                                                                                                        "+
"}                                                                                                       "+
"                                                                                                        "+
"initializePlayerEvents();                                                                               "+
