<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <style>
        .caption {
            font-size: 34px;
            margin: 0 0 0 32px 0;
            float: left;
            display: block;
            margin-bottom: 20px;
        }
        .submit_button{
            -webkit-appearance: none;
            -webkit-box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px;
            -webkit-font-smoothing: antialiased;
            -webkit-writing-mode: horizontal-tb;
            align-items: flex-start;
            background-color: rgb(78, 105, 162);

            background-position: 0px -335px;
            background-repeat: repeat-x;
            border-bottom-color: rgb(51, 76, 131);
            border-bottom-left-radius: 2px;
            border-bottom-right-radius: 2px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(60, 84, 136);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(60, 84, 136);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(67, 90, 139);
            border-top-left-radius: 2px;
            border-top-right-radius: 2px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px;
            box-sizing: content-box;
            color: rgb(255, 255, 255);
            cursor: pointer;
            direction: ltr;
            display: inline-block;
            font-family: helvetica, arial, 'lucida grande', sans-serif;
            font-size: 12px;
            font-style: normal;
            font-variant: normal;
            font-weight: bold;
            height: 22px;
            letter-spacing: normal;
            line-height: 22px;
            list-style-type: none;
            margin-bottom: 0px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 0px;
            padding-bottom: 0px;
            padding-left: 16px;
            padding-right: 16px;
            padding-top: 0px;
            position: relative;
            text-align: center;
            text-decoration: none;
            text-indent: 0px;
            text-rendering: auto;
            text-shadow: rgba(0, 0, 0, 0.2) 0px -1px 0px;
            text-transform: none;
            vertical-align: middle;
            white-space: nowrap;
            width: 162px;
            word-spacing: 0px;
            word-wrap: break-word;
            writing-mode: lr-tb;
            zoom: 1;
            width: 95px;
            height: 32px;
            font-size: 20px;
            margin-top: 20px;
            display: block;
            float: right;
        }
        .input_field{
            height: 46px;
            width: 192px;
            font-size: 41px;
            margin: 0 0 0 12px;
            padding: 0px 0 0 10px;
            border: 1px solid gray;
        }
    </style>

    <script>
        var setfocus = function(){
            var input_field = document.getElementById("captcha_text");
            input_field.focus();
        }
    </script>

</head>
<body onload="setfocus();">


<div style="font-family: verdana; padding: 10px; border-radius: 10px; font-size: 12px; text-align:center;">

    <form action="${model.postUrl}" method="POST">

        <table>
            <tr>
                <td>
                    <span class="caption">Enter Captcha</span>
                </td>
            </tr>
            <tr>
                <td>

                    <table>
                        <tr>
                            <td>
                                <img src="${model.imgUrl}">
                            </td>
                            <td>
                                <input type="text" name="captcha_text" class="input_field" id="captcha_text">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit" class="submit_button">Submit</button>
                </td>
            </tr>
        </table>


    </form>
</div>
</body>
</html>