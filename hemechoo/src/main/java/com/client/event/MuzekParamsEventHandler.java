package com.client.event;

import com.google.gwt.event.shared.EventHandler;


public interface MuzekParamsEventHandler extends EventHandler {
    void onEvent(MuzekParamsEvent event);
}