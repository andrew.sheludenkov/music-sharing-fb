package com.client.event;

import com.google.gwt.event.shared.EventHandler;


public interface MuzekRadioEventHandler extends EventHandler {
    void onEvent(MuzekRadioEvent event);
}