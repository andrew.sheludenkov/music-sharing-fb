package com.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;

import java.util.ArrayList;
import java.util.List;


/**
 * Event Bus manages events: subscribe on and fire events
 */
public class EventBus {

    protected String SOURCE_PAGE = "session";

    private static EventBus instance;

    HandlerManager pageHandlerManager;

    List<HandlerRegistration> pageHandlerRegistration;

    public static EventBus get() {
        if (instance == null) {
            instance = new EventBus();
        }
        return instance;
    }

    private EventBus() {

        pageHandlerRegistration = new ArrayList<HandlerRegistration>();
        pageHandlerManager = new HandlerManager(SOURCE_PAGE);

    }

    public <H extends EventHandler> HandlerRegistration addHandler(GwtEvent.Type<H> type, H handler) {
        HandlerRegistration handlerRegistration = null;

        handlerRegistration = pageHandlerManager.addHandler(type, handler);
        pageHandlerRegistration.add(handlerRegistration);
        return handlerRegistration;
    }

    public EventBus fireEvent(GwtEvent event) {
        if (pageHandlerManager.isEventHandled(event.getAssociatedType())) {
            pageHandlerManager.fireEvent(event);
        }

        return get();
    }

    public void unsubscribeAll() {
        for (HandlerRegistration handlerRegistration : pageHandlerRegistration) {
            handlerRegistration.removeHandler();
        }
        pageHandlerRegistration.clear();
    }

}