package com.client.event;

import com.google.gwt.event.shared.GwtEvent;


public class MuzekParamsEvent extends GwtEvent< MuzekParamsEventHandler> {
    public static Type<MuzekParamsEventHandler> TYPE = new Type<MuzekParamsEventHandler>();




    @Override
    public Type<MuzekParamsEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MuzekParamsEventHandler handler) {
        handler.onEvent(this);
    }
}