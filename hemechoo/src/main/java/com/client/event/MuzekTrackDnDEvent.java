package com.client.event;

import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.FBWidgetPlayListFriend;
import com.client.facebook.playlist.item.MuzekPlayListItem;
import com.client.facebook.playlist.item.MuzekPlayListItemFriend;
import com.google.gwt.event.shared.GwtEvent;


public class MuzekTrackDnDEvent extends GwtEvent< MuzekTrackDnDEventHandler> {
    public static Type<MuzekTrackDnDEventHandler> TYPE = new Type<MuzekTrackDnDEventHandler>();


    public static enum ACTION{
        MOUSE_DOWN,
        MOUSE_UP,
        MOUSE_OVER,

    }

    MuzekPlayListItem item;
    MuzekPlayListItemFriend itemFriend;
    FBWidgetPlayList fbWidgetPlayList;
    FBWidgetPlayListFriend fbWidgetPlayListFriend;
    ACTION action;

    public ACTION getAction() {
        return action;
    }

    public void setAction(ACTION action) {
        this.action = action;
    }

    public MuzekPlayListItem getItem() {
        return item;
    }

    public void setItem(MuzekPlayListItem item) {
        this.item = item;
    }

    public FBWidgetPlayList getFbWidgetPlayList() {
        return fbWidgetPlayList;
    }

    public void setFbWidgetPlayList(FBWidgetPlayList fbWidgetPlayList) {
        this.fbWidgetPlayList = fbWidgetPlayList;
    }

    public MuzekPlayListItemFriend getItemFriend() {
        return itemFriend;
    }

    public void setItemFriend(MuzekPlayListItemFriend itemFriend) {
        this.itemFriend = itemFriend;
    }

    public FBWidgetPlayListFriend getFbWidgetPlayListFriend() {
        return fbWidgetPlayListFriend;
    }

    public void setFbWidgetPlayListFriend(FBWidgetPlayListFriend fbWidgetPlayListFriend) {
        this.fbWidgetPlayListFriend = fbWidgetPlayListFriend;
    }

    @Override
    public Type<MuzekTrackDnDEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MuzekTrackDnDEventHandler handler) {
        handler.onEvent(this);
    }
}