package com.client.event;

import com.google.gwt.event.shared.EventHandler;


public interface MuzekTrackDnDEventHandler extends EventHandler {
    void onEvent(MuzekTrackDnDEvent event);
}