package com.client.event;

import com.google.gwt.event.shared.GwtEvent;


public class MuzekRadioEvent extends GwtEvent< MuzekRadioEventHandler> {
    public static Type<MuzekRadioEventHandler> TYPE = new Type<MuzekRadioEventHandler>();


    public static enum ACTION{
        MOUSE_DOWN,
        MOUSE_OVER,
        MOUSE_OUT,
        PLAY_FROM_TRACKLIST,

    }

    String radioId;
    ACTION action;

    public ACTION getAction() {
        return action;
    }

    public void setAction(ACTION action) {
        this.action = action;
    }

    public String getRadioId() {
        return radioId;
    }

    public void setRadioId(String radioId) {
        this.radioId = radioId;
    }

    @Override
    public Type<MuzekRadioEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MuzekRadioEventHandler handler) {
        handler.onEvent(this);
    }
}