package com.client.event;

import com.google.gwt.event.shared.EventHandler;


public interface MuzekPlayerPlayEventHandler extends EventHandler {
    void onEvent(MuzekPlayerPlayEvent event);
}