package com.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.media.client.Audio;


public class MuzekPlayerPlayEvent extends GwtEvent< MuzekPlayerPlayEventHandler> {
    public static Type<MuzekPlayerPlayEventHandler> TYPE = new Type<MuzekPlayerPlayEventHandler>();

    private Audio audio;


    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    @Override
    public Type<MuzekPlayerPlayEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MuzekPlayerPlayEventHandler handler) {
        handler.onEvent(this);
    }
}