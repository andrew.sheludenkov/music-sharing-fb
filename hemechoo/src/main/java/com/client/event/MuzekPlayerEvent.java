package com.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.media.client.Audio;
import com.shop.web.odto.MuzekAlbumOdto;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekPlayerEvent extends GwtEvent< MuzekPlayerEventHandler> {
    public static Type<MuzekPlayerEventHandler> TYPE = new Type<MuzekPlayerEventHandler>();


    public static enum ACTION{
        PLAY,
//        PLAY_NEXT,
        PAUSE,
        ENDED,
        REPEAT_ON,
        REPEAT_OFF,
        SHARE_TRACK,
        ATTACH_TRACK,
        CLOSE_ATTACH_TRACK,

        ADD_TRACK_TO_SEND_MESSAGE,
        REMOVE_TRACK_TO_SEND_MESSAGE,

        SHOW_PLAYLIST_OF_ALBUM,
        CLOSE_PLAY_LIST,
    }

//    private Audio audio;
    private ACTION action;
    private MuzekTrackOdto muzekTrackOdto;
    private MuzekAlbumOdto muzekAlbumOdto;

//    public Audio getAudio() {
//        return audio;
//    }
//
//    public void setAudio(Audio audio) {
//        this.audio = audio;
//    }

    public ACTION getAction() {
        return action;
    }

    public void setAction(ACTION action) {
        this.action = action;
    }

    public MuzekTrackOdto getMuzekTrackOdto() {
        return muzekTrackOdto;
    }

    public void setMuzekTrackOdto(MuzekTrackOdto muzekTrackOdto) {
        this.muzekTrackOdto = muzekTrackOdto;
    }

    public MuzekAlbumOdto getMuzekAlbumOdto() {
        return muzekAlbumOdto;
    }

    public void setMuzekAlbumOdto(MuzekAlbumOdto muzekAlbumOdto) {
        this.muzekAlbumOdto = muzekAlbumOdto;
    }

    @Override
    public Type<MuzekPlayerEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MuzekPlayerEventHandler handler) {
        handler.onEvent(this);
    }
}