package com.client.event;

/**
 * Created by Andrew Sheludenkov
 * Date: Feb 5, 2011
 */
abstract public class EventHandler<T> {


    abstract public void onEvent(T value);

}
