package com.client.event;

import com.google.gwt.event.shared.EventHandler;


public interface MuzekPlayerEventHandler extends EventHandler {
    void onEvent(MuzekPlayerEvent event);
}