package com.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.media.client.Audio;
import com.shop.web.odto.MuzekTrackOdto;


public class ProgressBarEvent extends GwtEvent< ProgressBarEventHandler> {
    public static Type<ProgressBarEventHandler> TYPE = new Type<ProgressBarEventHandler>();


    private Double volume;
    private Double progress;

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getProgress() {
        return progress;
    }

    public void setProgress(Double progress) {
        this.progress = progress;
    }

    @Override
    public Type<ProgressBarEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ProgressBarEventHandler handler) {
        handler.onEvent(this);
    }
}