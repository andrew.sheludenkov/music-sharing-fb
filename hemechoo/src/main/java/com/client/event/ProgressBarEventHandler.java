package com.client.event;

import com.google.gwt.event.shared.EventHandler;


public interface ProgressBarEventHandler extends EventHandler {
    void onEvent(ProgressBarEvent event);
}