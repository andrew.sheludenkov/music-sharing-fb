package com.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.shop.web.dto.HemeChooCommentDto;
import com.shop.web.dto.UserDto;

import java.util.List;

public interface MainHemechooUIServiceAsync {


    void createMessage(String name, String email, String subject, String text, AsyncCallback<Boolean> async);

    void addHemechooShop(String name, String email, String url, String phone, AsyncCallback<Boolean> async);


    void getComments(Integer offset, Integer limit, String storeStringId, AsyncCallback<List<HemeChooCommentDto>> async);
    void getImages(Integer offset, Integer limit, String storeStringId, AsyncCallback<List<HemeChooCommentDto>> async);
    void findUserByReferralCode(String referralCode, AsyncCallback<UserDto> async);
    void deactivateCoupon(String referralCode, AsyncCallback<UserDto> async);


    void updateCommentContent(Long id, String content, String pros, String cons, AsyncCallback<Void> async);
    void censorCommentContent(Long id, String content, String pros, String cons, AsyncCallback<Void> async);

    void publishComment(Long id, Boolean isPublished, AsyncCallback<Void> async);

    void getCssText(String storeStringId, AsyncCallback<String> async);

    void updateCssText(String storeStringId, String text, AsyncCallback<Void> async);

}


