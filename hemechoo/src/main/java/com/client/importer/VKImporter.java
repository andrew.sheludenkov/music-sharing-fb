package com.client.importer;

import com.client.facebook.feed.item.MuzekFeedItem;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.client.util.SearchUtils;
import com.client.util.VKImportProcessor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekFeedOdto;
import com.shop.web.odto.MuzekFeedResultOdto;

import java.util.*;


public class VKImporter extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, VKImporter> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    @UiField
    HTMLPanel container;
    @UiField
    Label label;
    @UiField
    FlowPanel tracksContainer;


    Boolean firstPageLoaded = false;
    Boolean nextPageLoadInProgress = false;
    Boolean haveMorePages = true;
    Integer currentPage = 0;
    Integer pageSize = 100;

    Integer previousTracksNumber = 0;

    private Map<String, MuzekFeedItem> feedItemsMap = new HashMap<String, MuzekFeedItem>();

    private Integer numberOfTrialsToProcess = 0;

    Set<String> receivedTRackNamesFromVK = new HashSet<String>();
    ArrayList<String> receivedTRackNamesFromVKlist = new ArrayList<String>();

    Integer totalItemsFound = 0;

    Timer scrollVkDownTimer = new Timer() {
        @Override
        public void run() {

            Window.scrollTo(0, Window.getScrollTop() + Window.getClientHeight());


        }
    };
    Timer processTracksTimer = new Timer() {
        @Override
        public void run() {

            processTracks();

        }
    };

    public VKImporter() {

        initWidget(ourUiBinder.createAndBindUi(this));

        //initWhenCreated();



    }

    public void initialize() {

//        currentPage = 0;
//        haveMorePages = true;
//        nextPageLoadInProgress = false;
//        firstPageLoaded = false;
//
//        StandaloneAsyncService.get().getMuzekFeed(currentPage, pageSize, new StandaloneCallback<MuzekFeedResultOdto>() {
//            @Override
//            public void onSuccess(MuzekFeedResultOdto result) {
//                //To change body of implemented methods use File | Settings | File Templates.
//
//
//                addMoreItems(result.getFeedDtoList());
//
//                firstPageLoaded = true;
//            }
//        });


        scrollVkDownTimer.scheduleRepeating(100);
        processTracksTimer.scheduleRepeating(3000);


    }

    public void processTracks() {


        //for (int i = 0; i < 100; i++){
        Integer currentNumber = importItems(true, tracksContainer);

        label.setText("Экспорт треков из ВК, всего найдено " + currentNumber + " треков");


        if (previousTracksNumber.equals(currentNumber)) {
            //label.setText("Экспортировано "+ currentNumber +"треков");

            processTracksTimer.cancel();
            scrollVkDownTimer.cancel();

            totalItemsFound = receivedTRackNamesFromVKlist.size();
            recognizeItems();
        }

        previousTracksNumber = currentNumber;
        //}

    }

    public void initWhenCreated() {


        Window.addWindowScrollHandler(new Window.ScrollHandler() {
            @Override
            public void onWindowScroll(Window.ScrollEvent scrollEvent) {

                if (firstPageLoaded && !nextPageLoadInProgress && haveMorePages) {

                    Integer scrollTop = scrollEvent.getScrollTop();
                    Integer containerHeight = container.getOffsetHeight();
                    Integer clientHeight = Window.getClientHeight();

                    if (containerHeight - scrollTop < clientHeight + 100) {
                        currentPage++;
                        nextPageLoadInProgress = true;

                        StandaloneAsyncService.get().getMuzekFeed(currentPage, pageSize, new StandaloneCallback<MuzekFeedResultOdto>() {
                            @Override
                            public void onSuccess(MuzekFeedResultOdto result) {

                                if (result.getFeedDtoList().length() > 0) {
                                    addMoreItems(result.getFeedDtoList());
                                } else {
                                    haveMorePages = false;
                                }

                                nextPageLoadInProgress = false;
                            }
                        });
                    }
                }
            }
        });
    }


    public void show(Boolean show) {
        this.setVisible(show);
    }


    private void addMoreItems(JsArray<MuzekFeedOdto> list) {


        feedItemsMap.clear();

        String currentUserFBid = ClientCache.get().getFBuserId();

        for (int i = 0; i < list.length(); i++) {
            JavaScriptObject object = list.get(i);

            MuzekFeedOdto item = (MuzekFeedOdto) object;

            try {

                Long.valueOf(item.getFbuserId());

                if (!item.getFbuserId().equals(currentUserFBid)) {
                    MuzekFeedItem feedItem = feedItemsMap.get(item.getFbuserId());

                    if (feedItem == null) {
                        feedItem = new MuzekFeedItem(null);
                        feedItem.initialize(item);


                        feedItemsMap.put(item.getFbuserId(), feedItem);
                    } else {
                        feedItem.initialize(item);
                    }
                }
            } catch (Throwable e) {

            }


        }


    }


    public Integer importItems(Boolean proceed, FlowPanel tracksContainer){

        if (proceed) {

            List<Element> elements = SearchUtils.findElementsForClass(RootPanel.get().getElement(), "audio_title_wrap");

            //NodeList elements = Document.get().getElementsByTagName("h2");


            for (int i = 0; i < elements.size(); i++) {

                NodeList<Element> audioPerformer = elements.get(i).getElementsByTagName("a");
                String performer = audioPerformer.getItem(0).getInnerHTML();
                String title = "";

                NodeList<Element> audioTitle = elements.get(i).getElementsByTagName("span");
                for (int j = 0; j < audioTitle.getLength(); j++) {
                    Element audioTitleItem = audioTitle.getItem(j);
                    if (audioTitleItem.getClassName().equals("audio_title_inner")) {
                        title = audioTitleItem.getInnerHTML();
                        break;
                    }
                }

                String trackName = performer + " - " + title;

                Integer size = receivedTRackNamesFromVK.size();
                receivedTRackNamesFromVK.add(trackName);
                if (receivedTRackNamesFromVK.size() > size){
                    receivedTRackNamesFromVKlist.add(trackName);
                    Label label = new Label(trackName);
                    tracksContainer.add(label);
                }



            }


            return elements.size();

        } else {
            //sendMessage("stop");



            return null;
        }


    }


    private void recognizeItems(){

        final String urlParams = Window.Location.getQueryString();

        String fbUserId = urlParams.split("=")[1];

        addTracksReceivedFromVK(fbUserId);
    }

    void addTracksReceivedFromVK(final String fbUserId){

        String tracks = "";

        label.setText("Экспортировано треков "+ (totalItemsFound-receivedTRackNamesFromVKlist.size()) +" из " + totalItemsFound);


        for (int i = 0; i < 8; i++) {
            if (receivedTRackNamesFromVKlist.size() > 0) {
                String name = receivedTRackNamesFromVKlist.get(receivedTRackNamesFromVKlist.size() - 1);
                tracks += tracks.length() > 0 ? "[]" + name : name;

                receivedTRackNamesFromVKlist.remove(receivedTRackNamesFromVKlist.size() - 1);
            }
        }

        StandaloneAsyncService.get().recognizeTracksFromVK(fbUserId, tracks, new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {


                if (receivedTRackNamesFromVKlist.size() > 0){
                    addTracksReceivedFromVK(fbUserId);
                }else {
                    label.setText("Экспорт завершён");

                }
            }
        });

    }

}