package com.client.importer;

import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Document;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;


public class VKPeopleImporter extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, VKPeopleImporter> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    @UiField
    HTMLPanel container;
    @UiField
    Label label;
    @UiField
    FlowPanel tracksContainer;




    public VKPeopleImporter() {

        initWidget(ourUiBinder.createAndBindUi(this));

        //initWhenCreated();



    }

    public void fetchUsers(String muzekUserFbId, String offset) {


        List<String> users = new ArrayList<String>();

        String content = Document.get().getFirstChild().getNodeValue();


        String bloOne[] = content.split("clear_fix");
        for (int j = 1; j < bloOne.length + 1; j++) {
            String name = "";
            String id = "";
            String imgurl = "";
            String city = "";
            String user = "";

            String bloOnePart = bloOne[j];

            if (bloOnePart != null) {

                String name1[] = bloOnePart.split("event");
                if (name1 != null && name1.length > 2) {
                    String name1str = name1[2];
                    name = name1str.split("</a>")[0];
                    name = name.replaceAll("\\)", "").replaceAll(";", "").replaceAll(";", "").replaceAll(">", "").replaceAll("\"", "");
                }

                //Window.alert(name);

                String id1[] = bloOnePart.split("over\\(this,");
                if (id1 != null && id1.length > 1) {
                    String id2str = id1[1];
                    id = id2str.split("\\)")[0];
                    id = id.trim();
                }

                //Window.alert(id);

                String url1[] = bloOnePart.split("src=");
                if (url1 != null && url1.length > 1) {
                    String url2str = url1[1];
                    imgurl = url2str.split("alt")[0];
                    imgurl = imgurl.trim().replaceAll("\"", "");
                }

                //Window.alert(imgurl);

                String city1[] = bloOnePart.split("labeled \">");
                if (city1 != null && city1.length > 1) {
                    String city2str = city1[1];
                    city = city2str.split("<")[0];
                    city = city.trim();
                }

                //Window.alert(city);

                user = id + "[]" + name + "[]" + city + "[]" + imgurl;
                users.add(user);
            }
        }


        sendUser(users, muzekUserFbId, offset);
    }

    private void sendUser(final List<String> users, final String muzekUserFbId, final String offset){

        if (users.size() > 0) {
            String user = users.get(0);
            users.remove(0);

            StandaloneAsyncService.get().sendVkUser(user, offset, muzekUserFbId, new StandaloneCallback<JavaScriptObject>() {
                @Override
                public void onSuccess(JavaScriptObject result) {
                    sendUser(users, muzekUserFbId, offset);
                }
            });
        }
    }



}