package com.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.shop.web.dto.CategoryDto;
import com.shop.web.dto.ShopDto;
import com.shop.web.dto.UserDto;
import com.shop.web.dto.UserResultDto;
import com.shop.web.dto.linkshare.LinkShareLogStatisticDto;
import com.shop.web.dto.linkshare.ProductGroupDto;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 19.02.13
 */
public interface LinkShareUIServiceAsync {

    void joinToProductGroup(Long productId, AsyncCallback<Void> callback);

    void getLoggedInUser(AsyncCallback<UserDto> callback);

    void getUserFriends(AsyncCallback<UserResultDto> callback);

    void getUserGroups(AsyncCallback<List<ProductGroupDto>> callback);

    void getAffiliateLinkActivities(AsyncCallback<LinkShareLogStatisticDto> callback);

    void getWidgetActivities(AsyncCallback<LinkShareLogStatisticDto> callback);

    void getTopCategoriesList(Integer from, Integer size, AsyncCallback<List<CategoryDto>> callback);

    void getChildCategories(Long parentId, AsyncCallback<List<CategoryDto>> callback);

    void getCategoryById(Long categoryId, AsyncCallback<CategoryDto> result);

    void getShopList(AsyncCallback<List<ShopDto>> callback);

    void getShop(long appMarketId, AsyncCallback<ShopDto> callback);
}
