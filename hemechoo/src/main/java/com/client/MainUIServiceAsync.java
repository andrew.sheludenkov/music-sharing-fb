package com.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.shop.web.dto.*;

import java.util.List;

public interface MainUIServiceAsync {



    void createUser(UserDto userDto, AsyncCallback<UserDto> async);

    void checkSignIn(String visitHash, AsyncCallback<UserDto> async);

    public void getProducts(String value, Integer from, Integer limit, Long categoryId, Long vendorId, String options, String priceRange, boolean orderPriceAsc, Boolean withCommissionOnly, AsyncCallback<ProductsResult> cb);

    public void getStandaloneShopProducts(String value, Integer from, Integer limit, Long categoryId, Long shopId, String options, String priceRange, boolean orderPriceAsc, Boolean withCommissionOnly, AsyncCallback<ProductsResult> cb);

    void getCategoriesBySearchResult(String searchString, AsyncCallback<List<CategoryDto>> async);

    void getActiveCategoriesTree(AsyncCallback<CategoryDto> async);

    void getActiveCategoriesTree(Long appStoreId, AsyncCallback<CategoryDto> async);

    void getProductPropertiesByCategory(Long categoryId, AsyncCallback<List<ProductPropertyDto>> async);

    void getSimpleProductsByCategory(Long categoryId, AsyncCallback<List<SimpleProductDto>> async);

    void updateUserPaypalEmail(Long userId, String email, AsyncCallback<Boolean> async);



    void getPriceRangeForCategory(Long categoryId, AsyncCallback<Double[]> async);



    void addFeedback(String username, String email, String text, Long productId, AsyncCallback<Boolean> async);

    void addSubscribtion(String name, String email, AsyncCallback<Void> async);


//    void getLoggedInUser(AsyncCallback<UserDto> async);


}


