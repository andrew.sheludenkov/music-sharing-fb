package com.client.exception;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;


public class NotLoggedInException extends Exception implements IsSerializable, Serializable{
}
