package com.client;


import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.shop.web.dto.*;

import java.util.List;

@RemoteServiceRelativePath("shop")
public interface MainUIService extends RemoteService {



    public UserDto createUser(UserDto userDto);

    public UserDto checkSignIn(String visitHash);

    public ProductsResult getProducts(String value, Integer from, Integer limit, Long categoryId, Long vendorId, String options, String priceRange, boolean orderPriceAsc, Boolean withCommissionOnly);

    ProductsResult getStandaloneShopProducts(String value, Integer from, Integer limit, Long categoryId, Long shopId, String options, String priceRange, boolean orderPriceAsc, Boolean withCommissionOnly);

    public List<CategoryDto> getCategoriesBySearchResult(String searchString);

    public CategoryDto getActiveCategoriesTree();

    public CategoryDto getActiveCategoriesTree(Long appStoreId);

    public List<ProductPropertyDto> getProductPropertiesByCategory(Long categoryId);

    public List<SimpleProductDto> getSimpleProductsByCategory(Long productId);

    public Boolean updateUserPaypalEmail(Long userId, String email);



    Double[] getPriceRangeForCategory(Long categoryId);



    public Boolean addFeedback(String username, String email, String text, Long productId);

    void addSubscribtion(String name, String email) throws Exception;


//    UserDto getLoggedInUser();
}
