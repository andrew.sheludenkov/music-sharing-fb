package com.client.facebook.feed;

import com.client.facebook.feed.item.MuzekFeedItem;
import com.client.facebook.player.FBWidgetPlayer;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekFeedOdto;
import com.shop.web.odto.MuzekFeedResultOdto;

import java.util.HashMap;
import java.util.Map;


public class FBWidgetFeed extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBWidgetFeed> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    @UiField
    HTMLPanel container;

    @UiField
    FlowPanel feedContainer;

    Boolean firstPageLoaded = false;
    Boolean nextPageLoadInProgress = false;
    Boolean haveMorePages = true;
    Integer currentPage = 0;
    Integer pageSize = 100;

    private Map<String, MuzekFeedItem> feedItemsMap = new HashMap<String, MuzekFeedItem>();

    FBWidgetPlayer fbWidgetPlayer;


    public FBWidgetFeed() {

        initWidget(ourUiBinder.createAndBindUi(this));

        //initWhenCreated();

    }

    public void initialize(FBWidgetPlayer fbWidgetPlayer) {
        this.fbWidgetPlayer = fbWidgetPlayer;

        currentPage = 0;
        haveMorePages = true;
        nextPageLoadInProgress = false;
        firstPageLoaded = false;

        StandaloneAsyncService.get().getMuzekFeed(currentPage, pageSize, new StandaloneCallback<MuzekFeedResultOdto>() {
            @Override
            public void onSuccess(MuzekFeedResultOdto result) {
                //To change body of implemented methods use File | Settings | File Templates.

                feedContainer.clear();

                addMoreItems(result.getFeedDtoList(), FBWidgetFeed.this.fbWidgetPlayer);

                firstPageLoaded = true;
            }
        });

    }

    public void initWhenCreated() {


        Window.addWindowScrollHandler(new Window.ScrollHandler() {
            @Override
            public void onWindowScroll(Window.ScrollEvent scrollEvent) {

                if (firstPageLoaded && !nextPageLoadInProgress && haveMorePages) {

                    Integer scrollTop = scrollEvent.getScrollTop();
                    Integer containerHeight = container.getOffsetHeight();
                    Integer clientHeight = Window.getClientHeight();

                    if (containerHeight - scrollTop < clientHeight + 100) {
                        currentPage++;
                        nextPageLoadInProgress = true;

                        StandaloneAsyncService.get().getMuzekFeed(currentPage, pageSize, new StandaloneCallback<MuzekFeedResultOdto>() {
                            @Override
                            public void onSuccess(MuzekFeedResultOdto result) {

                                if (result.getFeedDtoList().length() > 0) {
                                    addMoreItems(result.getFeedDtoList(), fbWidgetPlayer);
                                } else {
                                    haveMorePages = false;
                                }

                                nextPageLoadInProgress = false;
                            }
                        });
                    }
                }
            }
        });
    }


    public void show(Boolean show) {
        this.setVisible(show);
    }


    private void addMoreItems(JsArray<MuzekFeedOdto> list, FBWidgetPlayer fbWidgetPlayer) {


        feedItemsMap.clear();

        String currentUserFBid = ClientCache.get().getFBuserId();

        for (int i = 0; i < list.length(); i++) {
            JavaScriptObject object = list.get(i);

            MuzekFeedOdto item = (MuzekFeedOdto) object;

            try {

                Long.valueOf(item.getFbuserId());

                if (!item.getFbuserId().equals(currentUserFBid)) {
                    MuzekFeedItem feedItem = feedItemsMap.get(item.getFbuserId());

                    if (feedItem == null) {
                        feedItem = new MuzekFeedItem(fbWidgetPlayer);
                        feedItem.initialize(item);

                        feedContainer.add(feedItem);
                        feedItemsMap.put(item.getFbuserId(), feedItem);
                    } else {
                        feedItem.initialize(item);
                    }
                }
            } catch (Throwable e) {

            }


        }


    }


}