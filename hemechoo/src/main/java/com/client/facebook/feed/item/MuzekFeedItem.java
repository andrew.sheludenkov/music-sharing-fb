package com.client.facebook.feed.item;

import com.client.event.EventBus;
import com.client.event.MuzekTrackDnDEvent;
import com.client.facebook.player.FBWidgetPlayer;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.item.MuzekPlayListItemFeed;
import com.client.facebook.playlist.player.MuzekPlayer;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekFeedOdto;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekFeedItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekFeedItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    public HTMLPanel container;
    //    @UiField
//    Label trackName;
    @UiField
    FlowPanel playerContainer;
    @UiField
    Image image;
    @UiField
    Label userName;
    @UiField
    FlowPanel tracksContainer;
    @UiField
    Label date;
    @UiField
    Anchor more;
    @UiField
    Button sendMusic;

    Boolean initialized = false;


    MuzekPlayer player = new MuzekPlayer();

    FBWidgetPlayList fbWidgetPlayList;

    MuzekTrackOdto trackOdto;

    FBWidgetPlayer fbWidgetPlayer;

    MuzekFeedOdto feedItem;

    public MuzekFeedItem(FBWidgetPlayer fbWidgetPlayer) {

        this.fbWidgetPlayer = fbWidgetPlayer;

        initWidget(ourUiBinder.createAndBindUi(this));


        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                sendMusic.setVisible(true);
            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                sendMusic.setVisible(false);
            }
        }, MouseOutEvent.getType());

    }


    public void initialize(MuzekFeedOdto feedItem) {

        this.feedItem = feedItem;

        final String userTrackListId = feedItem.getFbuserId();
        String encodedUserId = ClientCache.get().encodeUserId(ClientCache.get().getFBuserId());
        String encodedFriendId = ClientCache.get().encodeUserId(userTrackListId);

        final String playlistUrl = "https://facebook.com/" + userTrackListId + "?mz=friend&mz_f=" + encodedFriendId + "&mz_ref=" + encodedUserId;

        if (!initialized) {
            String avatarUrl = "https://graph.facebook.com/v2.6/" + feedItem.getFbuserId() + "/picture?height=90&width=90";
            image.setUrl(avatarUrl);

            image.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });

            userName.setText(feedItem.getUserName());
            userName.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });
            more.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });

            date.setText(feedItem.getCreationDate());

            initialized = true;
        }

        if (tracksContainer.getWidgetCount() < 5) {
            MuzekPlayListItemFeed muzekPlayListItemFeed = new MuzekPlayListItemFeed();
            muzekPlayListItemFeed.initialize(feedItem.getTrack(), false);

            tracksContainer.add(muzekPlayListItemFeed);
        }

    }


    @UiHandler("sendMusic")
    void onSendMusicButton(ClickEvent clickEvent){
        fbWidgetPlayer.openSendMusicWindow(feedItem.getFbuserId(), feedItem.getUserName());
    }

}