package com.client.facebook.player;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekPlayerEventHandler;
import com.client.facebook.player.sharer.MuzekSharerImage;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekTrackOdto;


public class FBWidgetBigSharer extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBWidgetBigSharer> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);
    @UiField
    FlowPanel contentForFacebookButton;

    @UiField
    Label labelShareThisTrack;
    @UiField
    FlowPanel shareMusicBlock;
    @UiField
    HTMLPanel container;
    @UiField
    Image image;
    @UiField
    Label name;


    public FBWidgetBigSharer() {

        initWidget(ourUiBinder.createAndBindUi(this));


        EventBus.get().addHandler(MuzekPlayerEvent.TYPE, new MuzekPlayerEventHandler() {
            @Override
            public void onEvent(MuzekPlayerEvent event) {

                //try{


                MuzekTrackOdto muzekTrackOdto = event.getMuzekTrackOdto();

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PLAY)) {

                    if (muzekTrackOdto.getType().equals("youtube")) {

                        shareMusicBlock.setVisible(true);
                        showShareButton(muzekTrackOdto);

//                        String videoId = muzekTrackOdto.getId();
//                        String url = "https://i.ytimg.com/vi/"+videoId+"/hqdefault.jpg";
//                        image.setUrl(url);

                        name.setText(muzekTrackOdto.getTitle());

                    }else {
                        shareMusicBlock.setVisible(false);
                    }

                }
            }
        });
    }




    @Override
    protected void onLoad() {
        super.onLoad();    //To change body of overridden methods use File | Settings | File Templates.


    }


    private void showShareButton(MuzekTrackOdto muzekTrackOdto) {

        String videoId = muzekTrackOdto.getId();

        String link2 = "http://muzekbox.com/share?v=" + videoId + "&loc=" + ClientCache.get().getParams().getCountry();



        contentForFacebookButton.clear();
        link2 = link2.replaceAll("\\?", "-");
        link2 = link2.replaceAll("=", "--");
        link2 = link2.replaceAll("&", "---");
//        Frame frame = new Frame("http://localhost:8888/share_frame.html?v=" + link2);
        Frame frame = new Frame(ClientCache.get().hostUrl+"/share_frame_big.html?v=" + link2);
        DOM.setStyleAttribute(frame.getElement(), "border", "0");
        DOM.setStyleAttribute(frame.getElement(), "height", "32px");
        DOM.setStyleAttribute(frame.getElement(), "width", "120px");

        //addFacebookScript(contentForFacebookButton.getElement(), link);
        contentForFacebookButton.add(frame);
    }



}