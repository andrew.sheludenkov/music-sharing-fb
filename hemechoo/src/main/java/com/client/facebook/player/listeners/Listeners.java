package com.client.facebook.player.listeners;

import com.client.facebook.player.albums.item.MuzekAlbumItem;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekAlbumOdto;
import com.shop.web.odto.MuzekListenerOdto;
import com.shop.web.odto.MuzekListenersResultOdto;
import com.shop.web.odto.MuzekTrackOdto;


public class Listeners extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, Listeners> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;


    HandlerRegistration handler;

    Double max;

    public enum PROGRESS_TYPE {
        PROGRESS,
        VOLUME,
    }

    PROGRESS_TYPE progressType;

    FlowPanel externalContainer;

    public Listeners() {

        initWidget(ourUiBinder.createAndBindUi(this));

    }


    public void initialize(final MuzekTrackOdto track, final FlowPanel externalContainer) {
        this.externalContainer = externalContainer;

        Integer initialLimit = 100;

        StandaloneAsyncService.get().getTrackListeners(initialLimit, track.getId(), new StandaloneCallback<MuzekListenersResultOdto>() {
            @Override
            public void onSuccess(MuzekListenersResultOdto result) {
                JsArray<MuzekListenerOdto> list = result.getListenersDtoList();

                container.clear();

                if (list != null && list.length() > 0) {

                    for (int i = 0; i < list.length(); i++) {
                        if (i > 73) {
                            ListenerBlock moreBlock = new ListenerBlock();
                            moreBlock.initialize(null);
                            container.add(moreBlock);

                            moreBlock.getImage().addClickHandler(new ClickHandler() {
                                public void onClick(ClickEvent event) {
                                    StandaloneAsyncService.get().sendMuzekStat("listener getmore user:" + ClientCache.get().getFBuserId());
                                    getMoreListeners(track, externalContainer);
                                }
                            });
                            break;
                        }

                        MuzekListenerOdto listener = list.get(i);

                        ListenerBlock listenerBlock = new ListenerBlock();
                        listenerBlock.initialize(listener.getId());

                        container.add(listenerBlock);
                    }

                    externalContainer.setVisible(true);

                } else {
                    externalContainer.setVisible(false);
                }
            }
        });


    }

    public void getMoreListeners(final MuzekTrackOdto track, final FlowPanel externalContainer) {
        this.externalContainer = externalContainer;

        Integer limit = 270;

        StandaloneAsyncService.get().getTrackListeners(limit, track.getId(), new StandaloneCallback<MuzekListenersResultOdto>() {
            @Override
            public void onSuccess(MuzekListenersResultOdto result) {
                JsArray<MuzekListenerOdto> list = result.getListenersDtoList();

                container.clear();

                if (list != null && list.length() > 0) {

                    for (int i = 0; i < list.length(); i++) {


                        MuzekListenerOdto listener = list.get(i);

                        ListenerBlock listenerBlock = new ListenerBlock();
                        listenerBlock.initialize(listener.getId());

                        container.add(listenerBlock);
                    }

                    externalContainer.setVisible(true);

                } else {
                    externalContainer.setVisible(false);
                }
            }
        });


    }


}