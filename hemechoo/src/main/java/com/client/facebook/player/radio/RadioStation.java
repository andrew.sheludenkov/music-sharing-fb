package com.client.facebook.player.radio;

import com.client.event.EventBus;
import com.client.event.MuzekRadioEvent;
import com.client.event.MuzekRadioEventHandler;
import com.client.event.MuzekTrackDnDEvent;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;


public class RadioStation extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, RadioStation> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;
    @UiField
    Image image;
    @UiField
    Label caption;
    @UiField
    Image playImage;


    HandlerRegistration handler;

    Double max;

    String channelId;
    String imageUrl;
    String cannelCaption;

    public enum PROGRESS_TYPE {
        PROGRESS,
        VOLUME,
    }

    PROGRESS_TYPE progressType;


    public RadioStation() {

        initWidget(ourUiBinder.createAndBindUi(this));

        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {

                MuzekRadioEvent eventRadio = new MuzekRadioEvent();
                eventRadio.setRadioId(channelId);
                eventRadio.setAction(MuzekRadioEvent.ACTION.MOUSE_OVER);
                EventBus.get().fireEvent(eventRadio);
            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                MuzekRadioEvent eventRadio = new MuzekRadioEvent();
                eventRadio.setRadioId(channelId);
                eventRadio.setAction(MuzekRadioEvent.ACTION.MOUSE_OUT);
                EventBus.get().fireEvent(eventRadio);
            }
        }, MouseOutEvent.getType());

        container.addDomHandler(new MouseDownHandler() {
            @Override
            public void onMouseDown(MouseDownEvent event) {

                MuzekRadioEvent eventRadio = new MuzekRadioEvent();
                eventRadio.setRadioId(channelId);
                eventRadio.setAction(MuzekRadioEvent.ACTION.MOUSE_DOWN);
                EventBus.get().fireEvent(eventRadio);

            }
        }, MouseDownEvent.getType());


        EventBus.get().addHandler(MuzekRadioEvent.TYPE, new MuzekRadioEventHandler() {
            @Override
            public void onEvent(MuzekRadioEvent event) {
                if (event.getAction().equals(MuzekRadioEvent.ACTION.MOUSE_OVER)) {
                    if (event.getRadioId().equals(channelId)) {
                        playImage.setVisible(true);
                    }
// else {
//                        if (ClientCache.get().getCurrentRadioChannelId() == null){
//                            playImage.setVisible(false);
//                        }
//                        if (ClientCache.get().getCurrentRadioChannelId() != null && !ClientCache.get().getCurrentRadioChannelId().equals(channelId)){
//                            playImage.setVisible(false);
//                        }
//                    }
                }
                if (event.getAction().equals(MuzekRadioEvent.ACTION.MOUSE_OUT)) {

                    if (ClientCache.get().getCurrentRadioChannelId() == null) {
                        playImage.setVisible(false);
                    }
                    if (ClientCache.get().getCurrentRadioChannelId() != null && ClientCache.get().getCurrentRadioChannelId().equals(channelId)) {
                        playImage.setVisible(true);
                    } else {
                        playImage.setVisible(false);
                    }
                }
                if (event.getAction().equals(MuzekRadioEvent.ACTION.PLAY_FROM_TRACKLIST)) {

                    playImage.setVisible(false);

                }
            }
        });




        playImage.setUrl(ClientCache.getPathFromExtension("/img/radio/radio_play_1.png"));
    }

    public void initialize() {


    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        image.setUrl(ClientCache.getPathFromExtension( "/img/radio/" + imageUrl ));
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getCannelCaption() {
        return cannelCaption;
    }

    public void setCannelCaption(String cannelCaption) {
        this.cannelCaption = cannelCaption;
        caption.setText(cannelCaption);
    }
}