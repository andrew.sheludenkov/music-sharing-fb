package com.client.facebook.player.albums.item;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekPlayerEventHandler;
import com.client.facebook.player.FBYouTubePlayer;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekAlbumOdto;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekAlbumItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekAlbumItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;
    @UiField
    Label albumTitle;

    MuzekAlbumOdto album;
    Boolean selected = false;


    public MuzekAlbumItem() {

        initWidget(ourUiBinder.createAndBindUi(this));

        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {

                DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#4e69a2");
                DOM.setStyleAttribute(container.getElement(), "color", "white");

            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                if (!selected) {
                    DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#f6f7f8");
                    DOM.setStyleAttribute(container.getElement(), "color", "black");
                }
            }
        }, MouseOutEvent.getType());

        container.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                showPlaylistOfAlbum();
            }
        }, ClickEvent.getType());


        EventBus.get().addHandler(MuzekPlayerEvent.TYPE, new MuzekPlayerEventHandler() {
            @Override
            public void onEvent(MuzekPlayerEvent event) {


                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.SHOW_PLAYLIST_OF_ALBUM)) {

                    MuzekAlbumOdto receivedAlbum = event.getMuzekAlbumOdto();
                    if (receivedAlbum == null) {
                        setSelected(false);
                    } else {
                        if (receivedAlbum.getId().equals(album.getId())) {
                            setSelected(true);
                        } else {
                            setSelected(false);
                        }
                    }


                }

            }

        });

    }


    public void initialize(MuzekAlbumOdto album) {

        String albumName = album.getTitle();

        albumTitle.setText(albumName);

        this.album = album;
    }

    private void showPlaylistOfAlbum() {


        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setMuzekAlbumOdto(album);
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.SHOW_PLAYLIST_OF_ALBUM);
        EventBus.get().fireEvent(muzekPlayerEvent);
    }

    private void setSelected(Boolean value) {
        if (value) {
            selected = true;
            DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#4e69a2");
            DOM.setStyleAttribute(container.getElement(), "color", "white");
        } else {
            selected = false;
            DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#f6f7f8");
            DOM.setStyleAttribute(container.getElement(), "color", "black");
        }
    }

}