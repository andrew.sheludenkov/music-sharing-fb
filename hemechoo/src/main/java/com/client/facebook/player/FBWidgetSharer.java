package com.client.facebook.player;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekPlayerEventHandler;
import com.client.facebook.player.sharer.MuzekSharerImage;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.Image;


public class FBWidgetSharer extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBWidgetSharer> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    public FBWidgetSharer() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }


    @Override
    protected void onLoad() {
        super.onLoad();    //To change body of overridden methods use File | Settings | File Templates.


    }


    public void addAlltheShit() {

        String path = Window.Location.getQueryString();

        if (path.contains("muzekbox.com")) {

            StandaloneAsyncService.get().sendMuzekStat("share " + ClientCache.get().getFBuserId());

        }

        String videoId = path.split("share%3Fv%3D")[1].split("%26")[0];
        String url = "https://i.ytimg.com/vi/"+videoId+"/hqdefault.jpg";


        Element imageElement = getUserIdFromPage("img", "scaledImageFitWidth");

        if (imageElement == null){

            Element divElement = getUserIdFromPage("div", "unclickable");
            Node node1 = divElement.getChild(0);

            HTMLPanel htmlPanel = HTMLPanel.wrap((Element)node1);
            DOM.setAttribute(htmlPanel.getElement(), "style", "margin-left:158px");


            RootPanel.detachNow(htmlPanel);

            MuzekSharerImage image = new MuzekSharerImage();
            image.image.setUrl(url);

            htmlPanel.add(image);

        }

        //Window.alert(imageElement.getInnerHTML());

//        Image image = Image.wrap(imageElement);
//        RootPanel.detachNow(image);




        //image.setUrl(url);


    }

    Element getUserIdFromPage(String tagName, String name) {

        try {
            NodeList<Element> nodes1 = RootPanel.get().getElement().getElementsByTagName(tagName);
            for (int i = 0; i < nodes1.getLength(); i++) {
                Element node1 = nodes1.getItem(i);
                if (node1.getAttribute("class").contains(name)) {
                    return node1;
                }
            }
        } catch (Throwable e) {
            return null;
        }
        return null;
    }


}