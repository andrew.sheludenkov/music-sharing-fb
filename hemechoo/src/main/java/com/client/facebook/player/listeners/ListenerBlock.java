package com.client.facebook.player.listeners;

import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekListenerOdto;
import com.shop.web.odto.MuzekListenersResultOdto;
import com.shop.web.odto.MuzekTrackOdto;


public class ListenerBlock extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, ListenerBlock> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;
    @UiField
    Image image;


    HandlerRegistration handler;

    Double max;

    public enum PROGRESS_TYPE {
        PROGRESS,
        VOLUME,
    }

    PROGRESS_TYPE progressType;


    public ListenerBlock() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }


    public void initialize(final String friendId) {

        if (friendId != null) {

            String url = "https://graph.facebook.com/v2.6/" + friendId + "/picture?height=90&width=90";

            image.setUrl(url);

            image.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {

                    String encodedUserId = ClientCache.get().encodeUserId(ClientCache.get().getFBuserId());
                    String encodedFriendId = ClientCache.get().encodeUserId(friendId);


                    StandaloneAsyncService.get().sendMuzekStat("listener open user:"+ ClientCache.get().getFBuserId() + " friend:" + friendId);
                    Window.open("https://facebook.com/" + friendId + "?mz=friend&mz_f=" + encodedFriendId + "&mz_ref=" + encodedUserId, "_blank", "");

                }
            });

        } else {

            image.setUrl(ClientCache.getPathFromExtension("/img/mz/dots.png"));


        }

    }

    public Image getImage() {
        return image;
    }
}