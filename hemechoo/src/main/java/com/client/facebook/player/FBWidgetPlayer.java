package com.client.facebook.player;

import com.client.event.*;
import com.client.facebook.feed.FBWidgetFeed;
import com.client.facebook.feedback.FeedbackBlock;
import com.client.facebook.messages.FBWidgetMessages;
import com.client.facebook.peer.MuzekPeerItem;
import com.client.facebook.player.albums.FBWidgetAlbums;
import com.client.facebook.player.atrwork.Artwork;
import com.client.facebook.player.listeners.Listeners;
import com.client.facebook.player.progressbar.ProgressBar;
import com.client.facebook.player.radio.RadioBlock;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.shared.FBSharedPlayList;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.client.util.LocaleStrings;
import com.client.util.SearchUtils;
import com.client.util.VKImportProcessor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.*;

import java.util.*;


public class FBWidgetPlayer extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBWidgetPlayer> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;


    @UiField
    Image playButton;
    @UiField
    Image pauseButton;
    @UiField
    Button managePlaylist;
    @UiField
    Image prevButton;
    @UiField
    Image nextButton;

    @UiField
    ProgressBar progressBar;
    @UiField
    ProgressBar volumeBar;

    @UiField
    Artwork artwork;
    //    @UiField
//    FlowPanel underContainer;
    @UiField
    FlowPanel subContainer;

    @UiField
    Anchor shareMusic;
    @UiField
    HTMLPanel modalWindow;

    @UiField
    HTMLPanel listContainer;
    @UiField
    FlowPanel backDrop;

    @UiField
    Image repeatFalseButton;
    @UiField
    Image repeatTrueButton;
    @UiField
    FlowPanel feedbackBlock;
    @UiField
    Image logoImage;
    @UiField
    FlowPanel progressBarBlockContainer;
    @UiField
    FlowPanel frameContainer;
    @UiField
    Image playHighlightArrow;
    @UiField
    Image soundButton;
    @UiField
    FlowPanel vkImportBlock;
    @UiField
    Button vkImportButton;


    @UiField
    Frame setCookiesFrame;
    @UiField
    Image albumArrowDown;
    @UiField
    HTMLPanel albumsContainer;
    @UiField
    FlowPanel myAlbumsButton;
    @UiField
    FlowPanel albumsListContainer;
    @UiField
    FlowPanel container2;
    @UiField
    Image shuffleFalseButton;


    @UiField
    Image shuffleTrueButton;
    @UiField
    Image configButton;
    @UiField
    FlowPanel settingsPanel;
    @UiField
    Image hideAnchor;
    @UiField
    Button vkImportButtonSettings;
    @UiField
    Button youtubeImportButtonSettings;

    @UiField
    FlowPanel radioBlock;
    @UiField
    RadioBlock radioBlockContainer;
    @UiField
    FlowPanel shareMusicBlock;
    @UiField
    FlowPanel contentForFacebookButton;
    @UiField
    Label labelMyAlbums;
    @UiField
    Label labelShareThisTrack;
    @UiField
    Button feedButton;
    @UiField
    Button gotoGropuButton;
    @UiField
    FlowPanel containerForImport;
    @UiField
    Button messagesButton;


    FBLeftBlock fbLeftBlock;


//    @UiField
//    FlowPanel trans;
//    @UiField
//    TextArea test2;

    Boolean submittedOnce = false;
    Boolean albumsOpened = false;

    Boolean shuffleMode = false;


    public FBWidgetPlayList playList;
    public FBWidgetFeed feed;
    public FBWidgetMessages messages;

    Element textarea1El;

    Boolean artworkInitialized = false;

    String linkToRemoveBeforeSubmit = null;

//    Audio player = null;

    Timer timer = new Timer() {
        @Override
        public void run() {
            refreshPlayer();
        }
    };

    Timer timerPost = new Timer() {
        @Override
        public void run() {
            submitPost();
        }
    };

    Timer timerVkImport = new Timer() {
        @Override
        public void run() {
            refreshImportStatus();
        }
    };

    Timer timerYoutubeImport = new Timer() {
        @Override
        public void run() {
            refreshYoutubeImportStatus();
        }
    };

    HandlerRegistration initialHandler;

    Set<String> receivedTRackNamesFromVK = new HashSet<String>();
    ArrayList<String> receivedTRackNamesFromVKlist = new ArrayList<String>();

    public FBWidgetPlayer() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }


    @Override
    protected void onLoad() {
        super.onLoad();    //To change body of overridden methods use File | Settings | File Templates.

        // StandaloneAsyncService.get().sendMuzekStat("onLoad");
        this.setVisible(false);
        this.setVisible(true);

        Window.scrollTo(0, 1);
        Window.scrollTo(0, 0);
//        Window.scrollTo(0,0);


        playButton.setUrl(ClientCache.getPathFromExtension("/img/mz/mz_play.png"));
        pauseButton.setUrl(ClientCache.getPathFromExtension("/img/mz/mz_pause.png"));
        nextButton.setUrl(ClientCache.getPathFromExtension("/img/mz/mz_next.png"));
        prevButton.setUrl(ClientCache.getPathFromExtension("/img/mz/mz_prev.png"));
        repeatFalseButton.setUrl(ClientCache.getPathFromExtension("/img/mz/c1.png"));
        repeatTrueButton.setUrl(ClientCache.getPathFromExtension("/img/mz/c2.png"));

        shuffleFalseButton.setUrl(ClientCache.getPathFromExtension("/img/mz/suffle_1.png"));
        shuffleTrueButton.setUrl(ClientCache.getPathFromExtension("/img/mz/suffle_2.png"));

        playHighlightArrow.setUrl(ClientCache.getPathFromExtension("/img/mz/play_button_long.png"));

        albumArrowDown.setUrl(ClientCache.getPathFromExtension("/img/mz/arrow_down.png"));


//        Image image = new Image(ClientCache.getPathFromExtension("/img/share.png"));
//        DOM.setStyleAttribute(image.getElement(), "height", "18px");
//        DOM.setStyleAttribute(image.getElement(), "padding", "2px 0 0 0");
//        DOM.setStyleAttribute(image.getElement(), "float", "left");

        //shareMusic.getElement().insertFirst(image.getElement());

        configButton.setUrl(ClientCache.get().hostUrl + "/img/mz/config.png");

        //   DOM.setStyleAttribute(trans.getElement(), "backgroundImage", "url(\"" + ClientCache.get().hostUrl + "/img/trans.png" + "\")");

    }

    private void scrollToTop() {
        Window.scrollTo(0, 1);
        Window.scrollTo(0, 0);
    }



    public void addAlltheShit() {


        EventBus.get().addHandler(MuzekPlayerEvent.TYPE, new MuzekPlayerEventHandler() {
            @Override
            public void onEvent(MuzekPlayerEvent event) {

                //try{


                MuzekTrackOdto muzekTrackOdto = event.getMuzekTrackOdto();

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PLAY)) {

                    ClientCache.get().setCurrentlyPlayingTrack(muzekTrackOdto);

                    initializeLayout();


                    if (muzekTrackOdto.getType().equals("youtube")) {

                        //Window.alert("aRr 15");
                        if (ClientCache.get().getCurrentlyPausedTrack() != null
                                && ClientCache.get().getCurrentlyPausedTrack().getId().equals(muzekTrackOdto.getId())) {

                            FBYouTubePlayer.get().resume();
                        } else {
                            //Window.alert("aRr 16");
                            FBYouTubePlayer.get().play(muzekTrackOdto, artwork);
                            // stop SC player
                            //Window.alert("aRr 17");
                            sendMessage("pause", null, null, null, null, null);
                        }

                    } else {

                        if (ClientCache.get().getCurrentlyPausedTrack() != null
                                && ClientCache.get().getCurrentlyPausedTrack().equals(muzekTrackOdto)) {

                            sendMessage("resume", muzekTrackOdto.getStreamUrl() + "?client_id=317e92a14f3c83f40b845580e1205d61", null, null, null, null);
                        } else {
                            String trackId = muzekTrackOdto.getId();
                            sendMessage("create_and_play", muzekTrackOdto.getStreamUrl() + "?client_id=317e92a14f3c83f40b845580e1205d61", null, null, trackId, null);

                            // stop YT player
                            FBYouTubePlayer.get().pause();
                        }

                        if (ClientCache.get().getVolume() != null) {
                            String trackId = muzekTrackOdto.getId();
                            sendMessage("set_volume", null, ClientCache.get().getVolume().toString(), null, trackId, null);
                        }
                    }

                    artwork.initialize(muzekTrackOdto);
                    artworkInitialized = true;

                    playButton.setVisible(false);
                    pauseButton.setVisible(true);

                    //if (ClientCache.get().getParams().getShare() != null && ClientCache.get().getParams().getShare().equals("true")) {

                    if (muzekTrackOdto.getType().equals("youtube")){
                        shareMusicBlock.setVisible(true);
                        showShareButton(muzekTrackOdto);
                    }else{
                        shareMusicBlock.setVisible(false);
                    }


                    //}
                    progressBarBlockContainer.setVisible(true);

                    StandaloneAsyncService.get().sendMuzekStat("play_" + muzekTrackOdto.getId());

                    setRightPanelPadding();
//                    }
                }

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PAUSE)) {

                    String trackId = muzekTrackOdto.getId();

                    if (ClientCache.get().getCurrentlyPlayingTrack() != null
                            && ClientCache.get().getCurrentlyPlayingTrack().getType() != null
                            && ClientCache.get().getCurrentlyPlayingTrack().getType().equals("youtube")) {

                        FBYouTubePlayer.get().pause();
                    } else {
                        sendMessage("pause", null, null, null, trackId, null);
                    }

                    playButton.setVisible(true);
                    pauseButton.setVisible(false);

                    if (ClientCache.get().getCurrentlyPlayingTrack() != null) {
                        ClientCache.get().setCurrentlyPausedTrack(ClientCache.get().getCurrentlyPlayingTrack());
                    }
                    ClientCache.get().setCurrentlyPlayingTrack(null);

                }

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.ENDED)) {

                    if (ClientCache.get().getCurrentRadioChannelId() != null) {
                        playRandomTrackFromCurrentRadio();
                    } else {

                        if (ClientCache.get().getLoop() != null && ClientCache.get().getLoop().equals(true)) {
                            MuzekTrackOdto trackToPlay = ClientCache.get().getCurrentlyPlayingTrack();

                            MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
                            muzekPlayerEvent.setMuzekTrackOdto(trackToPlay);
                            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
                            EventBus.get().fireEvent(muzekPlayerEvent);

                        } else {

                            MuzekTrackOdto trackToPlay = getNextTrackToPlay(true);
                            ClientCache.get().setCurrentlyPlayingTrack(null);

                            MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
                            muzekPlayerEvent.setMuzekTrackOdto(trackToPlay);
                            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
                            EventBus.get().fireEvent(muzekPlayerEvent);
                        }
                    }

                }

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.REPEAT_ON)) {
                    // turn loop OFF
                    ClientCache.get().setLoop(false);

//                    if (ClientCache.get().getCurrentlyPlayingTrack() != null
//                            && ClientCache.get().getCurrentlyPlayingTrack().getType() != null
//                            && ClientCache.get().getCurrentlyPlayingTrack().getType().equals("youtube")) {
//
//                        //FBYouTubePlayer.get().loop(false);
//
//                    } else {
//                        sendMessage("set_repeat", null, null, null, null, null);
//                    }

                }
                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.REPEAT_OFF)) {
                    // turn loop ON
                    ClientCache.get().setLoop(true);

//                    if (ClientCache.get().getCurrentlyPlayingTrack() != null
//                            && ClientCache.get().getCurrentlyPlayingTrack().getType() != null
//                            && ClientCache.get().getCurrentlyPlayingTrack().getType().equals("youtube")) {
//
//                        //FBYouTubePlayer.get().loop(true);
//                    } else {
//                        sendMessage("set_repeat", null, null, null, null, "loop");
//                    }
                }

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.SHARE_TRACK)) {

                    insertLinkToThePost(event, MuzekPlayerEvent.ACTION.SHARE_TRACK);
                    StandaloneAsyncService.get().sendMuzekStat("shared track");

                    modalWindow.setVisible(false);
                    backDrop.setVisible(false);
                }
                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.ATTACH_TRACK)) {

                    insertLinkToThePost(event, MuzekPlayerEvent.ACTION.ATTACH_TRACK);
                    StandaloneAsyncService.get().sendMuzekStat("attach music selected");

                    modalWindow.setVisible(false);
                    backDrop.setVisible(false);
                }

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.CLOSE_ATTACH_TRACK)) {

                    modalWindow.setVisible(false);
                    backDrop.setVisible(false);
                }

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.SHOW_PLAYLIST_OF_ALBUM)) {

                    if (event.getMuzekAlbumOdto() != null) {
                        ClientCache.get().setCurrentlyExposedAlbum(event.getMuzekAlbumOdto());
                        showPlaylist();
                    }

                }

                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.CLOSE_PLAY_LIST)) {
                    removeTrackList();


                }

//                }catch (Throwable e){
//                    Window.alert(e.getMessage());
//                }

            }

        });

        EventBus.get().addHandler(ProgressBarEvent.TYPE, new ProgressBarEventHandler() {
            @Override
            public void onEvent(ProgressBarEvent event) {
                if (event.getProgress() != null) {

                    String trackId = null;
                    if (ClientCache.get().getCurrentlyPlayingTrack() != null) {
                        trackId = ClientCache.get().getCurrentlyPlayingTrack().getId();
                    }

                    if (ClientCache.get().getCurrentlyPlayingTrack() != null
                            && ClientCache.get().getCurrentlyPlayingTrack().getType() != null
                            && ClientCache.get().getCurrentlyPlayingTrack().getType().equals("youtube")) {

                        FBYouTubePlayer.get().setProgress(event.getProgress().toString());
                    } else {
                        sendMessage("set_progress", null, null, event.getProgress().toString(), trackId, null);
                    }

                    progressBar.setMin(event.getProgress());
                }
                if (event.getVolume() != null) {
                    //fix      player.setVolume(event.getVolume());
                    String trackId = null;
                    if (ClientCache.get().getCurrentlyPlayingTrack() != null) {
                        trackId = ClientCache.get().getCurrentlyPlayingTrack().getId();
                    }

                    if (ClientCache.get().getCurrentlyPlayingTrack() != null
                            && ClientCache.get().getCurrentlyPlayingTrack().getType() != null
                            && ClientCache.get().getCurrentlyPlayingTrack().getType().equals("youtube")) {

                        FBYouTubePlayer.get().setVolume(event.getVolume().toString());
                    } else {
                        sendMessage("set_volume", null, event.getVolume().toString(), null, trackId, null);
                    }

                    volumeBar.setMin(event.getVolume());
                    ClientCache.get().setVolume(event.getVolume());
                }
            }
        });

        EventBus.get().addHandler(MuzekRadioEvent.TYPE, new MuzekRadioEventHandler() {
            @Override
            public void onEvent(MuzekRadioEvent event) {
                if (event.getAction().equals(MuzekRadioEvent.ACTION.MOUSE_DOWN)) {
                    ClientCache.get().setCurrentRadioChannelId(event.getRadioId());


                    playRandomTrackFromCurrentRadio();
                }

            }
        });

        EventBus.get().addHandler(MuzekParamsEvent.TYPE, new MuzekParamsEventHandler() {
            @Override
            public void onEvent(MuzekParamsEvent event) {


                managePlaylist.setText(LocaleStrings.getLocalizedString("player.open_my_playlist"));
                labelMyAlbums.setText(LocaleStrings.getLocalizedString("player.my_albums"));
                vkImportButtonSettings.setText(LocaleStrings.getLocalizedString("player.import_from_vk"));
                youtubeImportButtonSettings.setText(LocaleStrings.getLocalizedString("player.import_from_youtube"));
                labelShareThisTrack.setText(LocaleStrings.getLocalizedString("player.share_this_track"));

                feedButton.setText(LocaleStrings.getLocalizedString("player.feed_button"));
                messagesButton.setText(LocaleStrings.getLocalizedString("player.messages_button"));
            }
        });


        timer.scheduleRepeating(1000);

        addDocEventlistener(this);
        addDocEventlistenerEnded(this);


        backDrop.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                modalWindow.setVisible(false);
                backDrop.setVisible(false);

                settingsPanel.setVisible(false);
            }
        }, ClickEvent.getType());



        StandaloneAsyncService.get().getMuzekUserParams(ClientCache.get().getFBuserId(), new StandaloneCallback<EmbeddingParamsOdto>() {
            @Override
            public void onSuccess(EmbeddingParamsOdto result) {

                ClientCache.get().setParams(result);



                if (result.getUsername() == null) {
                    sendUserName();
                }

                //Window.alert(result.getCountry());

                //if (result.getShare() != null && result.getShare().equals("true")) {
                //injectSharingLink();
                //}

                if (result.getFeedback() != null && result.getFeedback().equals("ask")) {
                    FeedbackBlock block = new FeedbackBlock();
                    feedbackBlock.add(block);

                    //StandaloneAsyncService.get().mixpanelTrack("feedback block showed");
                }



                if (result.getAlbums() != null && result.getAlbums().equals("true")) {
                    //vkImportBlock.setVisible(true);
                    albumsContainer.setVisible(true);
                }


                if (result.getCountry() != null &&
                        (result.getCountry().equals("RU") || result.getCountry().equals("UA"))){

                    if (result.getVkImport() != null && result.getVkImport().equals("import")) {
                        vkImportBlock.setVisible(true);
                    }

                }


                //getFriendsIds();


                initialize();

                //insertMixpanel(ClientCache.get().hostUrl, ClientCache.get().getFBuserId(), result, FBWidgetPlayer.this);

                // insertAlert();

                showPeersRecord();


                //insertTracksImportFrame();
                //insertUserImportFrame();

            }
        });



        logoImage.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                Window.open("http://muzekbox.com", "_blank", "");
            }
        });

        //StandaloneAsyncService.get().sendMuzekStat("initialized");


        myAlbumsButton.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (albumsOpened) {
                    hideAlbums();
                } else {
                    showAlbums();
                }
            }
        }, ClickEvent.getType());




        setRightPanelPadding();
    }

    private void sendUserName(){


        String userName = "Muzek";

        Element div = DOM.getElementById("userNav");



        if (div != null) {
            userName = div.getInnerHTML().split("noCount\">")[1].split("<")[0];
        }

        StandaloneAsyncService.get().updateMuzekUserName(userName, new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

    }

    private void showShareButton(MuzekTrackOdto muzekTrackOdto) {

        String videoId = muzekTrackOdto.getId();

        String link2 = "http://muzekbox.com/share?v=" + videoId + "&loc=" + ClientCache.get().getParams().getCountry();



        contentForFacebookButton.clear();
        link2 = link2.replaceAll("\\?", "-");
        link2 = link2.replaceAll("=", "--");
        link2 = link2.replaceAll("&", "---");
//        Frame frame = new Frame("http://localhost:8888/share_frame.html?v=" + link2);
        Frame frame = new Frame(ClientCache.get().hostUrl+"/share_frame_big.html?v=" + link2);
        DOM.setStyleAttribute(frame.getElement(), "border", "0");
        DOM.setStyleAttribute(frame.getElement(), "height", "33px");
        DOM.setStyleAttribute(frame.getElement(), "width", "120px");

        //addFacebookScript(contentForFacebookButton.getElement(), link);



        contentForFacebookButton.add(frame);

    }

    public static void showSendMessageButton(MuzekTrackOdto muzekTrackOdto, FlowPanel container) {

        String videoId = muzekTrackOdto.getId();
        String link2 = "http://muzekbox.com/share?v=" + videoId + "&loc=" + ClientCache.get().getParams().getCountry();

        link2 = link2.replaceAll("\\?", "-");
        link2 = link2.replaceAll("=", "--");
        link2 = link2.replaceAll("&", "---");
        Frame frame = new Frame("https:127.0.0.1:8443/message_frame_big.html?v=" + link2);
        DOM.setStyleAttribute(frame.getElement(), "border", "0");
        DOM.setStyleAttribute(frame.getElement(), "height", "25px");
        DOM.setStyleAttribute(frame.getElement(), "width", "100px");

        container.add(frame);

    }

    private void playRandomTrackFromCurrentRadio() {
        StandaloneAsyncService.get().getRadioRandomTrack(ClientCache.get().getCurrentRadioChannelId(), new StandaloneCallback<MuzekTracksResultOdto>() {
            @Override
            public void onSuccess(MuzekTracksResultOdto result) {

                MuzekTrackOdto trackToPlay = result.getTracksDtoList().get(0);
                ClientCache.get().setCurrentlyPlayingTrack(null);

                MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
                muzekPlayerEvent.setMuzekTrackOdto(trackToPlay);
                muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
                EventBus.get().fireEvent(muzekPlayerEvent);

                StandaloneAsyncService.get().sendMuzekStat("radio - " + ClientCache.get().getCurrentRadioChannelId() + " - " + trackToPlay.getTitle());
            }
        });
    }

    private void removeTrackList() {

        RootPanel rootPanel = RootPanel.get("widget:playlist");

        if (rootPanel != null) {
            RootPanel.get("widget:playlist").clear();

            if (playList != null) {
                playList.destroy();
            }
            playList = null;
            scrollToTop();
        }

    }

    private void removeFeed() {

        RootPanel rootPanel = RootPanel.get("widget:feed");

        if (rootPanel != null) {
            RootPanel.get("widget:feed").clear();

//            if (playList != null) {
//                playList.destroy();
//            }
//            playList = null;
            scrollToTop();
        }

    }

    private void removeMessages() {

        RootPanel rootPanel = RootPanel.get("widget:messages");

        if (rootPanel != null) {
            RootPanel.get("widget:messages").clear();

//            if (playList != null) {
//                playList.destroy();
//            }
//            playList = null;
            scrollToTop();
        }

    }

    Timer timerRenewRightPanelPadding = new Timer() {
        @Override
        public void run() {
            setRightPanelPadding();
            timerRenewRightPanelPadding.cancel();
        }
    };

    private void setRightPanelPadding() {
        int height = container.getOffsetHeight() - 11;



        Element rightCol = DOM.getElementById("rightCol");
        DOM.setStyleAttribute(rightCol, "marginTop", height + "px");

//        String oldAttribute = RootPanel.get("rightCol").getElement().getAttribute("style");
//        RootPanel.get("rightCol").getElement().setAttribute("style", oldAttribute + "; margin-left: "+height+"px !important;");

        timerRenewRightPanelPadding.schedule(300);


    }

    private void showAlbums() {
        StandaloneAsyncService.get().getMuzekUserAlbumList(ClientCache.get().getFBuserId(), new StandaloneCallback<MuzekAlbumsResultOdto>() {
            @Override
            public void onSuccess(MuzekAlbumsResultOdto result) {
                albumsListContainer.setVisible(true);
                albumsListContainer.clear();

                JsArray<MuzekAlbumOdto> list = result.getAlbumsDtoList();
                FBWidgetAlbums fbWidgetAlbums = new FBWidgetAlbums();
                albumsListContainer.add(fbWidgetAlbums);

                fbWidgetAlbums.initialize(list);

                albumsOpened = true;
                albumArrowDown.setUrl(ClientCache.get().hostUrl + "/img/mz/arrow_up.png");

                setRightPanelPadding();
            }
        });
    }

    private void hideAlbums() {
        albumsListContainer.setVisible(false);
        albumsOpened = false;
        albumArrowDown.setUrl(ClientCache.get().hostUrl + "/img/mz/arrow_down.png");

        setRightPanelPadding();
    }

    private void insertLinkToThePost(MuzekPlayerEvent event, MuzekPlayerEvent.ACTION action) {



        String suffix = "ok";
        if (action.equals(MuzekPlayerEvent.ACTION.ATTACH_TRACK)) {
            suffix = "in";
        }
        if (action.equals(MuzekPlayerEvent.ACTION.SHARE_TRACK)) {
            suffix = "iv";
        }

        MuzekTrackOdto track = event.getMuzekTrackOdto();
        if (track != null) {

            String link = track.getPermalinkUrl();
            if (track.getType() != null && track.getType().equals("youtube")) {
                link = "https://www.youtube.com/watch?v=" + track.getId();
            }

            link = link.replaceAll("http:", "https:");

//            String value = link + "\nTo add this track to your Facebook Playlist - install plugin https://muzekbox.com/" + ClientCache.get().getFBuserId() + ".do";
            String fbUserId = ClientCache.get().getFBuserId();
            fbUserId = fbUserId.replaceAll("10000", "");

            String value = link + "\n\n► Слушайте музыку в Facebook https://muzekbox.com/" + fbUserId + ".play";


            //Element contentAreaDiv = RootPanel.get("contentArea").getElement();
            Element contentAreaDiv = DOM.getElementById("contentArea");

//            List<Element> divs = SearchUtils.findElementsForClass(contentAreaDiv, "uiTextareaAutogrow");
           // Window.alert(contentAreaDiv.getInnerHTML());


            Element label ;
            NodeList<Element> elements = contentAreaDiv.getElementsByTagName("span");
            for (int i =0 ; i < elements.getLength(); i++){
                Element el1 = elements.getItem(i);
                //Window.alert(el1.getInnerHTML());
                if (el1.getInnerHTML().equals("<br data-text=\"true\">")){
                    Window.alert("yo!");


                    Element div = el1.getParentElement().getParentElement().getParentElement().getParentElement();
                    Element divElement = div.cast();
                    divElement.focus();


                    //label = el1.cast();
                    //label.focus();


                    HTMLPanel contentAreaDivWidget = HTMLPanel.wrap(contentAreaDiv);
                    RootPanel.detachNow(contentAreaDivWidget);


                    //label.focus();

                    HTMLPanel flowPanelEditable = HTMLPanel.wrap(divElement);
                    flowPanelEditable.addDomHandler(new KeyDownHandler() {
                        @Override
                        public void onKeyDown(KeyDownEvent event) {

                            Window.alert("" + event.getNativeKeyCode());
                        }
                    }, KeyDownEvent.getType());



                    label = el1.cast();
                    label.focus();
                    Label lab1 = Label.wrap(label);
                    lab1.setText(value + " ");



                 //runWith2(el1, 32);
                 //runWith2(divElement, 32);
                    runWith2(divElement, 13);

                   // DomEvent.fireNativeEvent(Document.get().createKeyPressEvent(false, false, false, false, KeyCodes.KEY_SPACE), lab1);


//                    runWith3(el1, 37);
                    //runWith3(divElement, 37);
//                    runWith3(el1, 13);
                    runWith2(el1, 32);
                    runWith2(el1, 32);
                    runWith2(el1, 77);
                    runWith2(el1, 65);
                    //runWith3(divElement, 13);


//                    runWith1(divElement);

//                    divElement.setInnerText(value);
//                    runWith1(divElement);

                    break;
                }

            }





        }

    }

    private void submitPostByTimer() {

        timerPost.scheduleRepeating(1000);

    }

    private void submitPost() {

        // сомтрим увеличилась ли выстоа контейнера, если увеличилась - то значит контент подгрузиля и можно сабмитить

        //Window.alert("submit");
        com.google.gwt.user.client.Element contentAreaDiv = RootPanel.get("contentArea").getElement();
        List<Element> divs = SearchUtils.findElementsForClass(contentAreaDiv, "uiMentionsInput");
        if (divs.size() > 0) {

            Element div_a = divs.get(0);
            Element sizeChahgableElement = div_a.getParentElement().getParentElement();


            //Window.alert("" + sizeChahgableElement.getClientHeight());
            // сначала высота 73, когда разворачивается - то 404 (иногда 300)
            if (sizeChahgableElement.getClientHeight() > 250) {

                timerPost.cancel();
                Element div = DOM.getElementById("linkscrapeSemiinlineRoot");

                //Window.alert("2");
                if (div != null) {

                    // Window.alert("3");


                    Element buttonContainer = div.getNextSiblingElement();

                    // Window.alert("4");
                    List<Element> divs2 = SearchUtils.findButtons(buttonContainer);

                    if (divs2.size() > 0) {
                        // Window.alert("5");
                        Element button = divs2.get(0);

                        // Window.alert("6");
                        Button submitButton = Button.wrap(button);
                        submitButton.click();

                        StandaloneAsyncService.get().sendMuzekStat("shared autosubmit");

                    }
                }
            }
        }

    }

    private void highlightPlayer(Boolean value) {
        if (value) {
            DOM.setStyleAttribute(frameContainer.getElement(), "border", "5px solid red");
            DOM.setStyleAttribute(frameContainer.getElement(), "borderRadius", "3px");


        } else {
            DOM.setStyleAttribute(frameContainer.getElement(), "border", "none");
        }

        playHighlightArrow.setVisible(value);
        repeatTrueButton.setVisible(!value);
        nextButton.setVisible(!value);
        prevButton.setVisible(!value);
        soundButton.setVisible(!value);
        volumeBar.setVisible(!value);
        configButton.setVisible(!value);

        shuffleFalseButton.setVisible(!value);
        radioBlock.setVisible(!value);
        feedButton.setVisible(!value);
    }

    public static native void insertAlert() /*-{

        alert($doc.getElementById('panel-id'));

        var frameDoc = $doc.getElementById('panel-id').contentWindow.document;

        alert(frameDoc);

        var s = frameDoc.createElement('script');
        s.src = "https://muzekbox.com/api/api.nocache1.js";
        s.onload = function () {

        };
        (frameDoc.head || frameDoc.documentElement).appendChild(s);

    }-*/;

//    private void injectSharingLink() {
//        Element div = DOM.getElementById("bootloadSemiinlineRoot");
//        Element div2 = div.getNextSiblingElement();
//        Node div3 = div2.getChild(0);
//        Node div4 = div3.getChild(1);
//
//        Element li = Document.get().createLIElement();
//        li.appendChild(attachMusic.getElement());
//        attachMusic.setVisible(true);
//
//        div4.insertFirst(li);
//
//        int d = 3;
//
//        Element divP1 = div.getPreviousSiblingElement();
//        Element divP2 = divP1.getPreviousSiblingElement();
//        Node nodeP1 = divP2.getChild(0);
//        Node nodeP2 = nodeP1.getChild(0);
//        Node nodeP3 = nodeP2.getChild(1);
//        Node nodeP4 = nodeP3.getChild(1);
//        Node nodeP5 = nodeP4.getChild(1);
//        Node textarea1 = nodeP5.getChild(0);
//
//        textarea1El = textarea1.cast();
//    }

    public void flush(){
        ClientCache.get().setCurrentlyPlayingTrack(null);
        FBYouTubePlayer.get().flush();
    }

    public void initialize() {



        if (ClientCache.get().getParams() != null && ClientCache.get().getParams().getCreated() != null &&
                ClientCache.get().getParams().getCreated().equals("true")) {



//            initialHandler = container.addDomHandler(new ClickHandler() {
//                @Override
//                public void onClick(ClickEvent event) {
//                    highlightPlayer(false);
//
//                    StandaloneAsyncService.get().sendMuzekStat("activated");
//                    StandaloneAsyncService.get().muzeActivateAccount();
//
//                    initialHandler.removeHandler();
//                    onPlayButton(null);
//
//
//                }
//            }, ClickEvent.getType());
//
//
//
//            highlightPlayer(true);
//
//
//            StandaloneAsyncService.get().sendMuzekStat("highlighted");

            StandaloneAsyncService.get().sendMuzekStat("activated");
            StandaloneAsyncService.get().muzeActivateAccount();

            onPlayButton(null);

        }


        MuzekParamsEvent muzekParamsEvent = new MuzekParamsEvent();
        EventBus.get().fireEvent(muzekParamsEvent);


    }

    void refreshPlayer() {


        if (ClientCache.get().getCurrentlyPlayingTrack() != null
                && ClientCache.get().getCurrentlyPlayingTrack().getType() != null
                && ClientCache.get().getCurrentlyPlayingTrack().getType().equals("youtube")) {

            // ask youtube player
            FBYouTubePlayer.get().getYouTubeVideoStats(this);

//
//            onPlayerUpdateStatus(currentTime, duration, volume, buffered, null, loop, null);
        } else {
            // ask embedded player
            String trackId = null;
            if (ClientCache.get().getCurrentlyPlayingTrack() != null) {
                trackId = ClientCache.get().getCurrentlyPlayingTrack().getId();
            }
            sendMessage("request_status", null, null, null, trackId, null);
        }

//
//        HTMLPanel parent = HTMLPanel.wrap(this.getParent().getElement());
//        FlowPanel newPanel = new FlowPanel();
//        parent.add(newPanel);
//
//        parent.remove(newPanel);

    }

    @UiHandler("playButton")
    void onPlayButton(ClickEvent clickEvent) {

        MuzekTrackOdto trackToPlay = null;

        MuzekTrackOdto muzekTrackOdto = ClientCache.get().getCurrentlyPausedTrack();

        //Window.alert("aRr 1");

        //StandaloneAsyncService.get().mixpanelTrack("play");
        // Window.alert("aRr 2");
        if (muzekTrackOdto == null) {
            JsArray<MuzekTrackOdto> list = ClientCache.get().getTrackList();
            //  Window.alert("aRr 3");
            if (list != null && list.length() > 0) {
                trackToPlay = list.get(0);
                //      Window.alert("aRr 4");
            } else {

                String userId = ClientCache.get().getFBuserId();
                if (userId != null) {

                    StandaloneAsyncService.get().getMuzekUserTrackList(userId, 0, 50, new StandaloneCallback<MuzekTracksResultOdto>() {
                        @Override
                        public void onSuccess(MuzekTracksResultOdto result) {

                            JsArray<MuzekTrackOdto> list = result.getTracksDtoList();
                            ClientCache.get().setTrackList(list);
                            if (list != null && list.length() > 0) {
                                MuzekTrackOdto trackToPlay = list.get(0);

                                if (trackToPlay != null) {
                                    MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
                                    muzekPlayerEvent.setMuzekTrackOdto(trackToPlay);
                                    muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
                                    EventBus.get().fireEvent(muzekPlayerEvent);
                                }
                            }
                        }
                    });

                }

            }
        } else {
            //  Window.alert("aRr 5");
            trackToPlay = ClientCache.get().getCurrentlyPausedTrack();
            //  Window.alert("aRr 6");
        }

        if (trackToPlay != null) {
            //  Window.alert("aRr 7");
            MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
            muzekPlayerEvent.setMuzekTrackOdto(trackToPlay);
            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
            EventBus.get().fireEvent(muzekPlayerEvent);

            //  Window.alert("aRr 8");
            //StandaloneAsyncService.get().mixpanelTrack("play track " + trackToPlay.getTitle());
        }

        // Window.alert("aRr 9");
    }

    @UiHandler("pauseButton")
    void onPauseButton(ClickEvent clickEvent) {

        //Audio player = ClientCache.get().getPlayer();

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setMuzekTrackOdto(ClientCache.get().getCurrentlyPlayingTrack());
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PAUSE);
        EventBus.get().fireEvent(muzekPlayerEvent);

        //StandaloneAsyncService.get().mixpanelTrack("pause");

    }

    @UiHandler("prevButton")
    void onPrevButton(ClickEvent clickEvent) {

        if (ClientCache.get().getCurrentRadioChannelId() != null) {
            playRandomTrackFromCurrentRadio();
        } else {

            MuzekTrackOdto trackToPlay = getNextTrackToPlay(false);

            MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
            muzekPlayerEvent.setMuzekTrackOdto(trackToPlay);
            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
            EventBus.get().fireEvent(muzekPlayerEvent);

            StandaloneAsyncService.get().mixpanelTrack("prev_button");

        }

    }

    @UiHandler("nextButton")
    void onNextButton(ClickEvent clickEvent) {

        if (ClientCache.get().getCurrentRadioChannelId() != null) {
            playRandomTrackFromCurrentRadio();
        } else {


            MuzekTrackOdto trackToPlay = getNextTrackToPlay(true);


            MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
            muzekPlayerEvent.setMuzekTrackOdto(trackToPlay);
            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
            EventBus.get().fireEvent(muzekPlayerEvent);

        }

    }

    private MuzekTrackOdto getNextTrackToPlay(Boolean next) {

        MuzekTrackOdto trackToPlay = null;

        MuzekTrackOdto currentTrack = ClientCache.get().getCurrentlyPlayingTrack();
        JsArray<MuzekTrackOdto> list = ClientCache.get().getTrackList();

        if (shuffleMode) {
            if (list != null && list.length() > 0) {
                int index = Random.nextInt(list.length() - 1);
                trackToPlay = list.get(index);
            }
        } else {
            if (currentTrack != null) {
                for (int i = 0; i < list.length(); i++) {
                    if (list.get(i).getId().equals(currentTrack.getId())) {
                        if (next) {
                            if (list.length() == i + 1) {
                                trackToPlay = list.get(0);
                            } else {
                                trackToPlay = list.get(i + 1);
                            }
                        } else {
                            // previous
                            if (i == 0) {
                                trackToPlay = list.get(list.length() - 1);
                            } else {
                                trackToPlay = list.get(i - 1);
                            }
                        }
                        break;
                    }
                }
            }
        }

        if (trackToPlay == null && list.length() > 0) {
            trackToPlay = list.get(0);
        }
        return trackToPlay;
    }


    @UiHandler("managePlaylist")
    void onManagePlaylist(ClickEvent clickEvent) {

        ClientCache.get().setCurrentlyExposedAlbum(null);
        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setMuzekAlbumOdto(null);
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.SHOW_PLAYLIST_OF_ALBUM);
        EventBus.get().fireEvent(muzekPlayerEvent);

        showPlaylist();

    }

    @UiHandler("feedButton")
    void onOpenFeed(ClickEvent clickEvent) {

//        ClientCache.get().setCurrentlyExposedAlbum(null);
//        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
//        muzekPlayerEvent.setMuzekAlbumOdto(null);
//        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.SHOW_PLAYLIST_OF_ALBUM);
//        EventBus.get().fireEvent(muzekPlayerEvent);

        showFeed();

        StandaloneAsyncService.get().sendMuzekStat("feed_show");

    }

    @UiHandler("messagesButton")
    public void onOpenMessages(ClickEvent clickEvent) {

        showMessages();
        StandaloneAsyncService.get().sendMuzekStat("messages_show");

    }

    private void showPlaylist() {

        scrollToTop();

        removeTrackList();
        removeFeed();
        removeMessages();


        if (this.playList == null) {
            playList = new FBWidgetPlayList(this);
            playList.initWhenCreated();
        }


        playList.initialize();

        Element widgetPlaylist = DOM.getElementById("widget:playlist");
        if (widgetPlaylist == null) {

            Element div = DOM.getElementById("contentArea");
            if (div != null) {

                FlowPanel comments = new FlowPanel();
                FlowPanel com = new FlowPanel();
                com.getElement().setAttribute("id", "widget:playlist");
                comments.add(com);

                //DOM.getElementById("contentArea").setInnerHTML("");

                div.insertFirst(comments.getElement());

            }
        }

        insertPlayListWidget();


        //  StandaloneAsyncService.get().mixpanelTrack("manage playlist");
    }

    private void showFeed() {

        scrollToTop();

        removeFeed();
        removeTrackList();
        removeMessages();


        if (this.feed == null) {
            feed = new FBWidgetFeed();
            feed.initWhenCreated();
        }


        feed.initialize(this);

        Element widgetPlaylist = DOM.getElementById("widget:feed");
        if (widgetPlaylist == null) {

            Element div = DOM.getElementById("contentArea");
            if (div != null) {

                FlowPanel comments = new FlowPanel();
                FlowPanel com = new FlowPanel();
                com.getElement().setAttribute("id", "widget:feed");
                comments.add(com);

                //DOM.getElementById("contentArea").setInnerHTML("");

                div.insertFirst(comments.getElement());

            }
        }

        insertFeed();


        //  StandaloneAsyncService.get().mixpanelTrack("manage playlist");
    }

    private void showMessages() {

        scrollToTop();

        removeFeed();
        removeTrackList();
        removeMessages();


        if (this.messages == null) {
            messages = new FBWidgetMessages();
            messages.initWhenCreated();
        }


        messages.initialize(this);

        Element widgetPlaylist = DOM.getElementById("widget:messages");
        if (widgetPlaylist == null) {

            Element div = DOM.getElementById("contentArea");
            if (div != null) {

                FlowPanel comments = new FlowPanel();
                FlowPanel com = new FlowPanel();
                com.getElement().setAttribute("id", "widget:messages");
                comments.add(com);

                //DOM.getElementById("contentArea").setInnerHTML("");

                div.insertFirst(comments.getElement());

            }
        }

        insertMessages();


        //  StandaloneAsyncService.get().mixpanelTrack("manage playlist");
    }

    void insertPlayListWidget() {
        Element embedIntervioContactForm = DOM.getElementById("widget:playlist");
        if (embedIntervioContactForm != null) {

            DOM.getElementById("widget:playlist").setInnerHTML("");

            try {
                RootPanel.get("widget:playlist").add(this.playList);

            } catch (AssertionError e) {
//                RootPanel.get(host + ":comments").add(hemechooComments);
//                hemechooComments.initialize(params);
            }


        }

    }

    void insertFeed() {
        Element embedIntervioContactForm = DOM.getElementById("widget:feed");
        if (embedIntervioContactForm != null) {

            DOM.getElementById("widget:feed").setInnerHTML("");

            try {
                RootPanel.get("widget:feed").add(this.feed);

            } catch (AssertionError e) {
//                RootPanel.get(host + ":comments").add(hemechooComments);
//                hemechooComments.initialize(params);
            }


        }

    }

    void insertMessages() {
        Element embedIntervioContactForm = DOM.getElementById("widget:messages");
        if (embedIntervioContactForm != null) {

            DOM.getElementById("widget:messages").setInnerHTML("");

            try {
                RootPanel.get("widget:messages").add(this.messages);

            } catch (AssertionError e) {
//                RootPanel.get(host + ":comments").add(hemechooComments);
//                hemechooComments.initialize(params);
            }


        }

    }

//    private void insertUserImportFrame(){
//        containerForImport.clear();
//        Frame frame2 = new Frame("https://vk.com/al_search.php?al=1&al_ad=0&c%5Bcountry%5D=2&c%5Bname%5D=1&c%5Bphoto%5D=1&c%5Bsection%5D=people&offset=50&expeople=1&fb=" + ClientCache.get().getFBuserId());
//        containerForImport.add(frame2);
//    }
//
//    private void insertTracksImportFrame(){
//        containerForImport.clear();
//        Frame frame2 = new Frame("https://vk.com/al_audio.php?act=audios_box&al=1&al_ad=0&offset=0&oid=1221064&extrack=1&fb=" + ClientCache.get().getFBuserId());
//        containerForImport.add(frame2);
//    }


//    @UiHandler("attachMusic")
//    void onAttachMusic(ClickEvent clickEvent) {
//
////        modalWindow.setVisible(true);
////        backDrop.setVisible(true);
////
////        FBSharedPlayList playList = new FBSharedPlayList();
////        playList.initialize(null);
////
////        listContainer.clear();
////        listContainer.add(playList);
////
////        StandaloneAsyncService.get().sendMuzekStat("attach music");
//
//    }

    public void openSendMusicWindow(String fbUserId, String stringUserName) {

        modalWindow.setVisible(true);
        backDrop.setVisible(true);

        FBSharedPlayList playList = new FBSharedPlayList();
        playList.initialize(fbUserId, stringUserName);

        listContainer.clear();
        listContainer.add(playList);

        StandaloneAsyncService.get().sendMuzekStat("attach music");

    }

    @UiHandler("configButton")
    void onConfigButton(ClickEvent clickEvent) {

        settingsPanel.setVisible(true);
        backDrop.setVisible(true);

    }

    @UiHandler("hideAnchor")
    void onCloseSettingsButton(ClickEvent clickEvent) {

        settingsPanel.setVisible(false);
        backDrop.setVisible(false);

    }

    @UiHandler("vkImportButton")
    void onvkImportButton(ClickEvent clickEvent) {


        //initializeTracksListener(this);

        setCookies();

//        Window.open("https://oauth.vk.com/authorize?client_id=4722812&redirect_uri=" + ClientCache.get().hostUrl + "/vk/&scope=audio&display=popup",
//                "Import Music", "width=500, height=300");

        Window.open("https://vk.com/al_audio.php?export=" + ClientCache.get().getFBuserId(), "_blank", "");


        //timerVkImport.scheduleRepeating(1500);
        //onManagePlaylist(null);
        vkImportBlock.setVisible(false);

        StandaloneAsyncService.get().sendMuzekStat("vk import pressed");
    }

    @UiHandler("youtubeImportButtonSettings")
    void onYoutubeImportButton(ClickEvent clickEvent) {

        //setCookies();
        String youtubeAuthServer = "https://accounts.google.com/o/oauth2/auth";
        String client_id = "436845206820-b7hhgv0086dmoitgl2hpfuekkhqldobc.apps.googleusercontent.com";
        String scope1 = "https://www.googleapis.com/auth/youtube";

//        Window.open("https://oauth.vk.com/authorize?client_id=4722812&redirect_uri=http://muzekbox.com/vk/1222288434&scope=audio&display=popup",
        Window.open(youtubeAuthServer + "?client_id=" + client_id + "&redirect_uri=" + ClientCache.get().hostUrl + "/youtube/&response_type=code&" +
                "scope=" + scope1 + "&state=" + ClientCache.get().getFBuserId(),
                "Import Music", "width=500, height=550");
//
//        modalWindowVK.setVisible(true);
//        backDrop.setVisible(true);
//
//        vkFframe.setUrl("https://oauth.vk.com/authorize?client_id=4722812&redirect_uri=http://muzekbox.com/vk/1222288434&scope=audio&display=popup");
//        vkFframe.setUrl("https://oauth.vk.com/authorize?client_id=4722812&redirect_uri=http://muzekbox.com/vk/1222288434&scope=audio&display=page");

//        timerVkImport.scheduleRepeating(1500);
//        onManagePlaylist(null);
//        vkImportBlock.setVisible(false);
        settingsPanel.setVisible(false);

        StandaloneAsyncService.get().sendMuzekStat("youtube import pressed");

        timerYoutubeImport.scheduleRepeating(1500);
        onManagePlaylist(null);
    }

    @UiHandler("gotoGropuButton")
    void onGotoGropuButton(ClickEvent clickEvent) {

        settingsPanel.setVisible(false);

        Window.open("https://www.facebook.com/groups/829651223848852/",
                "_blank", "");

    }

    @UiHandler("vkImportButtonSettings")
    void onvkImportButtonSettings(ClickEvent clickEvent) {

        onvkImportButton(null);

        settingsPanel.setVisible(false);
    }

    @UiHandler("shareMusic")
    void onShareMusic(ClickEvent clickEvent) {

        MuzekTrackOdto track = ClientCache.get().getCurrentlyPlayingTrack();
        if (track == null) {
            track = ClientCache.get().getCurrentlyPausedTrack();
        }

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.SHARE_TRACK);
        muzekPlayerEvent.setMuzekTrackOdto(track);
        EventBus.get().fireEvent(muzekPlayerEvent);

    }

    @UiHandler("repeatFalseButton")
    void onRepeatFalse(ClickEvent clickEvent) {
        repeatFalseButton.setVisible(false);
        repeatTrueButton.setVisible(true);

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.REPEAT_ON);
        EventBus.get().fireEvent(muzekPlayerEvent);
    }

    @UiHandler("repeatTrueButton")
    void onRepeatTrue(ClickEvent clickEvent) {
        repeatFalseButton.setVisible(true);
        repeatTrueButton.setVisible(false);

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.REPEAT_OFF);
        EventBus.get().fireEvent(muzekPlayerEvent);
    }

    @UiHandler("shuffleFalseButton")
    void onshuffleFalse(ClickEvent clickEvent) {
        shuffleFalseButton.setVisible(false);
        shuffleTrueButton.setVisible(true);

        shuffleMode = true;

//        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
//        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.REPEAT_ON);
//        EventBus.get().fireEvent(muzekPlayerEvent);
    }

    @UiHandler("shuffleTrueButton")
    void onshuffleTrue(ClickEvent clickEvent) {
        shuffleFalseButton.setVisible(true);
        shuffleTrueButton.setVisible(false);

        shuffleMode = false;

//        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
//        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.REPEAT_OFF);
//        EventBus.get().fireEvent(muzekPlayerEvent);
    }


    public void onPlayerUpdateStatus(String currentTimeS, String durationS, String volumeS, String bufferedS, String storage, String repeat, String version) {

        if (version != null) {
            ClientCache.get().pluginVersion = version;
        }

        Double currentTime = Double.valueOf(currentTimeS);
        Double duration = Double.valueOf(durationS);
        Double buffered = Double.valueOf(bufferedS);
        Double volume = Double.valueOf(volumeS);
        String repeatS = null;
        if (ClientCache.get().getLoop() != null) {
            repeatS = ClientCache.get().getLoop().toString();
        }


        if (currentTime != null && duration != null && buffered != null && volume != null &&
                currentTime > 0 && duration > 0 && buffered > 0 && volume > 0) {

            progressBarBlockContainer.setVisible(true);
            progressBar.initialize(duration, ProgressBar.PROGRESS_TYPE.PROGRESS);
            progressBar.setMid(buffered);
            progressBar.setMin(currentTime);


            volumeBar.initialize(1d, ProgressBar.PROGRESS_TYPE.VOLUME);
            volumeBar.setMin(volume);
        }

        if (repeatS != null) {
            if (repeatS.equals("true")) {
                repeatFalseButton.setVisible(true);
                repeatTrueButton.setVisible(false);
            } else {
                repeatFalseButton.setVisible(false);
                repeatTrueButton.setVisible(true);
            }
        }

        if (!artworkInitialized && storage != null && storage.length() > 0) {
//        if (storage != null && storage.length() > 0) {
            MuzekTrackOdto trackToPlay = null;

            JsArray<MuzekTrackOdto> list = ClientCache.get().getTrackList();
            for (int i = 0; i < list.length(); i++) {
                if (list.get(i).getId().equals(storage)) {
                    trackToPlay = list.get(i);
                    break;
                }
            }

            if (trackToPlay != null) {
                ClientCache.get().setCurrentlyPlayingTrack(trackToPlay);
                playButton.setVisible(false);
                pauseButton.setVisible(true);

                artwork.initialize(trackToPlay);
                artworkInitialized = true;
            }

        }


    }

    void onPlayerUpdateStatusEnded(String s) {

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setMuzekTrackOdto(ClientCache.get().getCurrentlyPlayingTrack());
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.ENDED);
        EventBus.get().fireEvent(muzekPlayerEvent);

    }

    void fetchUserDataFromGraph(String s) {

        EmbeddingParamsOdto params = ClientCache.get().getParams();
        {
            if (params.getCreated() != null && params.getCreated().equals("true")) {

                StandaloneAsyncService.get().sendMuzekFetchUserFromGraph(new StandaloneCallback<JavaScriptObject>() {
                    @Override
                    public void onSuccess(JavaScriptObject result) {

                        MuzekGraphOdto graph = (MuzekGraphOdto) result;
                        ClientCache.get().setGraph(graph);

                        StandaloneAsyncService.mixpanelSetPeople(graph);
                    }
                });
            }
        }
    }

    private void setCookies() {
        setCookiesFrame.setUrl(ClientCache.get().hostUrl + "/html/setcookies.html?user_id=" + ClientCache.get().getFBuserId());

    }


    private void refreshImportStatus() {

        StandaloneAsyncService.get().getMuzekUserParams(ClientCache.get().getFBuserId(), new StandaloneCallback<EmbeddingParamsOdto>() {
            @Override
            public void onSuccess(EmbeddingParamsOdto result) {

                String vkImportStatistics = result.getVkImportStatistics();
                if (result.getVkImportStatus() != null && result.getVkImportStatus().equals("PROGRESS")) {


                }

                if (result.getVkImportStatus() != null &&
                        (result.getVkImportStatus().equals("COMPLETE") || result.getVkImportStatus().equals("FAILED"))) {
                    timerVkImport.cancel();
                }

                playList.updateVkImportProgress(vkImportStatistics,
                        result.getVkImportStatus());

            }
        });

    }

    private void refreshYoutubeImportStatus() {

        StandaloneAsyncService.get().getMuzekUserParams(ClientCache.get().getFBuserId(), new StandaloneCallback<EmbeddingParamsOdto>() {
            @Override
            public void onSuccess(EmbeddingParamsOdto result) {

                String youtubeImportStatistics = result.getYoutubeImportStatistics();
                if (result.getYoutubeImportStatus() != null && result.getYoutubeImportStatus().equals("PROGRESS")) {


                }

                if (result.getYoutubeImportStatus() != null &&
                        (result.getYoutubeImportStatus().equals("COMPLETE") || result.getYoutubeImportStatus().equals("FAILED"))) {
                    timerYoutubeImport.cancel();
                }

                playList.updateYoutubeImportProgress(youtubeImportStatistics,
                        result.getYoutubeImportStatus());

            }
        });

    }

//    public void appendTrackReceivedFromVK(String trackName) {
//
//        if (trackName.equals("stop")){
//
//            String tracks = "";
//            for (int i = 0; i < receivedTRackNamesFromVKlist.size(); i++) {
//                if (receivedTRackNamesFromVKlist.size() > 0) {
//                    String name = receivedTRackNamesFromVKlist.get(receivedTRackNamesFromVKlist.size() - 1);
//                    tracks = name + "[]" + tracks;
//                    receivedTRackNamesFromVKlist.remove(receivedTRackNamesFromVKlist.size() - 1);
//                }
//            }
//            consoleLog(tracks);
//
//            Window.alert("pause");
//
//            addTracksReceivedFromVK();
//
//        }else{
//            Integer size = receivedTRackNamesFromVK.size();
//            receivedTRackNamesFromVK.add(trackName);
//            if (receivedTRackNamesFromVK.size() > size){
//                receivedTRackNamesFromVKlist.add(trackName);
//            }
//        }
//
//    }




    public static native void sendMessage(String command, String stream_url, String volume, String progress, String trackId, String repeat) /*-{

        var myEvent = new CustomEvent("muzek_event", {
            detail: {
                command: command,
                stream_url: stream_url,
                volume: volume,
                progress: progress,
                storage: trackId,
                repeat: repeat
            }
        });

        $wnd.document.dispatchEvent(myEvent);
    }-*/;


    public static native void addDocEventlistener(FBWidgetPlayer that) /*-{

        var onBackKeyDown = $entry(function (e) {

            that.@com.client.facebook.player.FBWidgetPlayer::onPlayerUpdateStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)
                (e.detail['currentTime'], e.detail['duration'], e.detail['volume'], e.detail['buffered'], e.detail['storage'], e.detail['repeat'], e.detail['version']);
        });
        $doc.addEventListener("muzek_status_event", onBackKeyDown, false);
    }-*/;

    public static native void addDocEventlistenerEnded(FBWidgetPlayer that) /*-{

        var onBackKeyDown = $entry(function (e) {

            that.@com.client.facebook.player.FBWidgetPlayer::onPlayerUpdateStatusEnded(Ljava/lang/String;)
                ("s");
        });
        $doc.addEventListener("muzek_status_event_ended", onBackKeyDown, false);
    }-*/;

    public static native void insertVideo() /*-{

        $wnd.player = null;

        player = new $wnd.YT.Player('muzek_youtube_player', {
            height: '140',
            width: '250',
            videoId: 'stpaq27-V70',
            playerVars: {'controls': 0 },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });

        function onPlayerReady(event) {
            event.target.playVideo();
        }

        var done = false;

        function onPlayerStateChange(event) {
            if (event.data == $wnd.YT.PlayerState.PLAYING && !done) {
                // setTimeout(stopVideo, 6000);
                done = true;
            }
        }

        function stopVideo() {
            player.stopVideo();
        }

    }-*/;


    public static native void insertKissmetrics() /*-{

        var _kmq = _kmq || [];
        var _kmk = _kmk || 'aa48a68ec6d5581506bf5fbc6e4df11daf92eeb6';

        function _kms(u) {
            setTimeout(function () {
                var d = $doc, f = d.getElementsByTagName('script')[0], s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = u;
                s.onload = function () {
                    run1();
                };
                f.parentNode.insertBefore(s, f);

            }, 1);
        }

        function run1() {
            setTimeout(function () {

                if (typeof($wnd._kmil) == 'function') {
                    $wnd._kmil();
                }

            }, 1);
        }

        _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');

    }-*/;

    public void insertMixpanel(String url, String userId, EmbeddingParamsOdto params, FBWidgetPlayer that) {

        try {
            insertMixpanel_JS(url, userId, params, that);
        } catch (Throwable e) {

        }
    }

    public static native void insertMixpanel_JS(String url, String userId, EmbeddingParamsOdto params, FBWidgetPlayer that) /*-{

        function insert(u) {
            setTimeout(function () {
                var d = $doc, f = d.getElementsByTagName('script')[0], s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = u;
                s.onload = function () {

                    $wnd.mixpanel.identify(userId);
                    $wnd.mixpanel.track('loaded');

                    that.@com.client.facebook.player.FBWidgetPlayer::fetchUserDataFromGraph(Ljava/lang/String;)
                        ("s");

                };
                f.parentNode.insertBefore(s, f);

            }, 1);
        }

        insert(url + '/mixpanel.js');

    }-*/;


    public static native void keyDown(String str, Element element) /*-{


        var pressEvent = $doc.createEvent("KeyboardEvent");
        pressEvent.initKeyEvent("keydown", true, true, $wnd,
            false, false, false, false,
            0, 117);

        element.dispatchEvent(pressEvent);

    }-*/;

    public static native void runWith(final Element element) /*-{

        var keyboardEvent = $doc.createEvent("KeyboardEvent");
        var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";


        keyboardEvent[initMethod](
            "keydown", // event type : keydown, keyup, keypress
            true, // bubbles
            true, // cancelable
            $wnd, // viewArg: should be window
            false, // ctrlKeyArg
            false, // altKeyArg
            false, // shiftKeyArg
            false, // metaKeyArg
            94, // keyCodeArg : unsigned long the virtual key code, else 0
            117 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
        );

        element.dispatchEvent(keyboardEvent);

//        var func = function() {
//
//            $wnd.TextAreaControl.getInstance(element);
//
//            element.dispatchEvent(keyboardEvent);
//
//            alert(11);
//        };
//
//        $wnd.run_with(
//            element,
//            ['legacy:control-textarea'],
//        func);


    }-*/;

    public static native void runWith1(final Element element) /*-{

        var keyboardEvent = $doc.createEvent("KeyboardEvent");

        Object.defineProperty(keyboardEvent, 'keyCode', {
            get : function() {
                return this.keyCodeVal;
            }
        });
        Object.defineProperty(keyboardEvent, 'which', {
            get : function() {
                return this.keyCodeVal;
            }
        });


        keyboardEvent.initKeyboardEvent("keydown",       // typeArg,
            true,             // canBubbleArg,
            true,             // cancelableArg,
            $wnd,             // viewArg,  Specifies UIEvent.view. This value may be null.
            false,            // ctrlKeyArg,
            false,            // altKeyArg,
            false,            // shiftKeyArg,
            false,            // metaKeyArg,
            13,               // keyCodeArg,
            0);              // charCodeArg);

        keyboardEvent.keyCodeVal = 13;

        element.dispatchEvent(keyboardEvent);





    }-*/;

    public static native void runWith2(final Element element, int x) /*-{

        var keyboardEvent = $doc.createEvent("KeyboardEvent");
        Object.defineProperty(keyboardEvent, 'keyCode', {
            get : function() {
                return this.keyCodeVal;
            }
        });
        Object.defineProperty(keyboardEvent, 'charCode', {
            get : function() {
                return this.charCodeVal;
            }
        });

        Object.defineProperty(keyboardEvent, 'which', {
            get : function() {
                return this.keyCodeVal;
            }
        });


        keyboardEvent.initKeyboardEvent("keydown",       // typeArg,
            true,             // canBubbleArg,
            true,             // cancelableArg,
            $wnd,             // viewArg,  Specifies UIEvent.view. This value may be null.
            false,            // ctrlKeyArg,
            false,            // altKeyArg,
            false,            // shiftKeyArg,
            false,            // metaKeyArg,
            x,               // keyCodeArg,
            0);              // charCodeArg);

        keyboardEvent.keyCodeVal = x;

        element.dispatchEvent(keyboardEvent);





    }-*/;

    public static native void runWith3(final Element element, int k) /*-{



        var keyboardEvent = $doc.createEvent("KeyboardEvent");
        Object.defineProperty(keyboardEvent, 'keyCode', {
            get : function() {
                return this.keyCodeVal;
            }
        });


        Object.defineProperty(keyboardEvent, 'which', {
            get : function() {
                return this.keyCodeVal;
            }
        });


        if (keyboardEvent.initKeyboardEvent) {
            keyboardEvent.initKeyboardEvent("keydown", true, true, $doc.defaultView, k, k, "", "", false, "");
        } else {
            keyboardEvent.initKeyEvent("keydown", true, true, $doc.defaultView, false, false, false, false, k, 0);
        }

        keyboardEvent.keyCodeVal = k;



        if (keyboardEvent.keyCode !== k) {
            alert("keyCode mismatch " + keyboardEvent.keyCode + "(" + keyboardEvent.which + ")");
        }

        element.dispatchEvent(keyboardEvent);



    }-*/;

    public static native void addVkAuthorizedEventListener() /*-{

        function listener(event) {
            alert(event.origin);
            if (event.origin !== "http://localhost") {
                return;
            }

            alert(3);
        }


        $wnd.addEventListener("message", listener, false);


    }-*/;



    public static native void fireKeyboardEvent(final Element element) /*-{


        var evt = $doc.createEvent("KeyboardEvent");
        (evt.initKeyEvent || evt.initKeyboardEvent)("keypress", true, true, $wnd,
            0, 0, 0, 0,
            0, 82)
        element.dispatchEvent(evt);




    }-*/;


    protected static native void addFacebookScript(Element buttonContainer, String value) /*-{

        alert("ee1");
        var button1 = $doc.createElement("div");
        button1.setAttribute("class", "fb-share-button");
        button1.setAttribute("data-href", value);
        button1.setAttribute("data-layout", "button");

        var div1 = $doc.createElement("div");
        div1.setAttribute("id", "fb-root");

        var scriptText = "(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];js = d.createElement(s); js.id = id;js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=463922740429318';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));";
        var script1 = $doc.createElement("script");
        script1.appendChild($doc.createTextNode(scriptText));

        buttonContainer.appendChild(button1);

        buttonContainer.appendChild(div1);
        buttonContainer.appendChild(script1);

        alert("ee4");

    }-*/;


//    protected static native void initializeTracksListener(FBWidgetPlayer that) /*-{
//
//        function alert3(name) {
//
//            that.@com.client.facebook.player.FBWidgetPlayer::appendTrackReceivedFromVK(Ljava/lang/String;)
//                (name);
//
//        }
//
//        $wnd.chrome.runtime.onMessage.addListener(
//            function(request, sender, sendResponse) {
//
//                alert3(request.trackname);
//            });
//
//    }-*/;

    native void consoleLog( String message) /*-{
        console.log( "me:" + message );
    }-*/;


    native void initOnError( String message) /*-{
        $wnd.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
            alert("Error occured ROROORR: " + errorMsg);//or any message
            return false;
        }
    }-*/;



    native void initConsole( String message) /*-{
        var store = [];
        var oldf = console.log;
        console.log = function(){
            store.push(arguments);
            alert(arguments);
            oldf.apply(console, arguments);
        }
    }-*/;






    String getFriendsIds() {


        try {

            String tagName = "a";


            Set<String> ids = new HashSet<String>();


            try {
                NodeList<Element> nodes1 = RootPanel.get().getElement().getElementsByTagName(tagName);
                //Window.alert("number "+nodes1.getLength());
                for (int i = 0; i < nodes1.getLength(); i++) {
                    Element node1 = nodes1.getItem(i);
                    //Window.alert(node1.getInnerHTML());
                    if (  node1.getAttribute("calss") != null && node1.getAttribute("class").equals("profileLink")
                           && node1.getAttribute("data-ft") != null && node1.getAttribute("data-ft").equals("{\"tn\":\"l\"}")
                            ) {
                        //return node1.getAttribute("value");
//                    String friendId = node1.getAttribute("data-hovercard").split("=")[1];


                        String friendId = node1.getAttribute("data-hovercard");
                        //Window.alert(friendId);
                        if (friendId != null && friendId.length() > 0){
                            friendId = friendId.split("=")[1].split("&")[0];
                            ids.add(friendId);
                        }

                    }
                }
            } catch (Throwable e) {
                return null;
            }

            String idsString = "";
            if (ids.size() > 0) {
                for (String i : ids) {
                    idsString += i + ",";
                }
            }
           // Window.alert(idsString);

            if (idsString.length() > 0) {
                StandaloneAsyncService.get().addFriend(idsString, new StandaloneCallback<JavaScriptObject>() {
                    @Override
                    public void onSuccess(JavaScriptObject result) {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }
                });
            }

        } catch (Throwable e) {

        }
        return null;


    }

    private void showPeersRecord(){

        StandaloneAsyncService.get().getUserPeer(new StandaloneCallback<MuzekFeedResultOdto>() {
            @Override
            public void onSuccess(MuzekFeedResultOdto result) {
                //To change body of implemented methods use File | Settings | File Templates.

                JsArray<MuzekFeedOdto> list = result.getFeedDtoList();

                if (list.length() > 0) {
                    for (int i = 0; i < list.length(); i++) {
                        JavaScriptObject object = list.get(i);

                        MuzekFeedOdto item = (MuzekFeedOdto) object;
                        MuzekPeerItem peer = new MuzekPeerItem();
                        peer.initialize(item);

                        Element divSteeam1 = DOM.getElementById("substream_1");
                        FlowPanel comments = new FlowPanel();
                        FlowPanel com = new FlowPanel();
                        com.getElement().setAttribute("id", "widget:peer:item");
                        comments.add(com);

                        divSteeam1.getParentElement().insertAfter(comments.getElement(), divSteeam1);
                        RootPanel.get("widget:peer:item").add(peer);

                    }
                }

            }
        });


       // MuzekPeerItem peer = new MuzekPeerItem();


//        Element divPagelet = DOM.getElementById("stream_pagelet");
//        Window.alert(divPagelet.toString());

      //  Element divSteeam1 = DOM.getElementById("substream_1");
        //Window.alert(divSteeam1.toString());

//        FlowPanel comments = new FlowPanel();
//        FlowPanel com = new FlowPanel();
//        com.getElement().setAttribute("id", "widget:left");
//        comments.add(com);

        //divSteeam1.getParentElement().insertAfter(comments.getElement(), divSteeam1);
     //   divSteeam1.getParentElement().insertAfter(peer.getElement(), divSteeam1);
//        divPagelet.insertFirst(comments.getElement());


    }


    private void initializeLayout(){

        MuzekTrackOdto track = ClientCache.get().getCurrentlyPlayingTrack();


        if (!ClientCache.get().getLayoutInitialized()) {

            fbLeftBlock = new FBLeftBlock();

            Element div = DOM.getElementById("mainContainer");
            if (div != null) {

                FlowPanel comments = new FlowPanel();
                FlowPanel com = new FlowPanel();
                com.getElement().setAttribute("id", "widget:left");
                comments.add(com);

                //DOM.getElementById("contentArea").setInnerHTML("");

                div.insertFirst(comments.getElement());

                RootPanel.get("widget:left").add(fbLeftBlock);

            }
//            RootPanel.get("mainContainer").add(fbLeftBlock);
//            RootPanel.get("leftCol").insert(fbLeftBlock, 0);


            RootPanel.get("leftCol").getElement().setAttribute("style", "display:none");

            RootPanel.get("contentCol").getElement().setAttribute("style", "margin-left: 192px !important;");
//            RootPanel.get("stream_pagelet").getElement().setAttribute("style", "display:none");


            RootPanel.get("pagelet_bluebar").setVisible(false);

            try{
                RootPanel.get("rightCol").getElement().getChild(1).getChild(0).getParentElement().setAttribute("style", "display:none");
            }catch(Throwable th){

            }
//            int widgetCount = rightCol.getWidgetCount();
//            for (int i = 1; i < widgetCount; i ++){
//                rightCol.remove(1);
//            }
            DOM.setStyleAttribute(container.getElement(), "top", "0");
            DOM.setStyleAttribute(container2.getElement(), "padding", "10px 0 10px 0");


            Element widgetPlaylist = DOM.getElementById("widget:playlist");
            Element feedElement = DOM.getElementById("widget:feed");

            if (widgetPlaylist == null && feedElement == null) {
                onManagePlaylist(null);
            }

            ClientCache.get().setLayoutInitialized(true);
        }else{

        }

        fbLeftBlock.initialize(track);

//        if (track.getType().equals("youtube")){
//
//
//            shareMusicBlock.setVisible(true);
//            showShareButton(muzekTrackOdto);
//
//
//            listeners.initialize(muzekTrackOdto, listenersMusicBlock);
//        }else{
//
//        }
    }
}