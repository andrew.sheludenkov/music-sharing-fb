package com.client.facebook.player.albums;

import com.client.facebook.player.albums.item.MuzekAlbumItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekAlbumOdto;


public class FBWidgetAlbums extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBWidgetAlbums> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    @UiField
    HTMLPanel container;


    public FBWidgetAlbums() {

        initWidget(ourUiBinder.createAndBindUi(this));

        //initWhenCreated();

    }

    public void initialize(JsArray<MuzekAlbumOdto> list) {

        container.clear();

        if (list != null && list.length() > 0) {

            for (int i = 0; i < list.length(); i++) {
                MuzekAlbumOdto album = list.get(i);

                MuzekAlbumItem muzekAlbumItem = new MuzekAlbumItem();
                muzekAlbumItem.initialize(album);

                container.add(muzekAlbumItem);

            }

        }

    }


}