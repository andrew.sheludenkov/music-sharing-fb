package com.client.facebook.player;

import com.client.facebook.player.listeners.Listeners;
import com.client.facebook.player.sharer.MuzekSharerImage;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekTrackOdto;


public class FBLeftBlock extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBLeftBlock> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);
    @UiField
    Listeners listeners;
    @UiField
    FlowPanel listenersMusicBlock;


    public FBLeftBlock() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }


    @Override
    protected void onLoad() {
        super.onLoad();    //To change body of overridden methods use File | Settings | File Templates.


    }


    public void initialize(final MuzekTrackOdto track) {


        listeners.initialize(track, listenersMusicBlock);

    }




}