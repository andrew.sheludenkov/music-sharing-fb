package com.client.facebook.player.radio;

import com.client.event.EventBus;
import com.client.event.MuzekParamsEvent;
import com.client.event.MuzekParamsEventHandler;
import com.client.util.LocaleStrings;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;


public class RadioBlock extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, RadioBlock> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;
    @UiField
    Label radioChannelsSeparator;


    HandlerRegistration handler;

    Double max;

    public enum PROGRESS_TYPE {
        PROGRESS,
        VOLUME,
    }

    PROGRESS_TYPE progressType;


    public RadioBlock() {

        initWidget(ourUiBinder.createAndBindUi(this));


        initialize();
    }

    public void initialize() {


        EventBus.get().addHandler(MuzekParamsEvent.TYPE, new MuzekParamsEventHandler() {
            @Override
            public void onEvent(MuzekParamsEvent event) {


                radioChannelsSeparator.setText(LocaleStrings.getLocalizedString("player.radio_channels"));

            }
        });


    }


}