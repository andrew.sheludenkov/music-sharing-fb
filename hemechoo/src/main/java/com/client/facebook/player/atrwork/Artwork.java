package com.client.facebook.player.atrwork;

import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekTrackOdto;


public class Artwork extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, Artwork> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;

    @UiField
    Image soundcloudImg;
    @UiField
    Image artwork;
    @UiField
    Anchor author;
    @UiField
    Anchor title;
    @UiField
    FlowPanel youtubeFrameContainer;
    @UiField
    HTMLPanel soundcloudContainer;
    @UiField
    HTMLPanel youtubeContainer;

    HandlerRegistration handler;

    Double max;

    public enum PROGRESS_TYPE {
        PROGRESS,
        VOLUME,
    }

    PROGRESS_TYPE progressType;


    public Artwork() {

        initWidget(ourUiBinder.createAndBindUi(this));

        soundcloudImg.setUrl(ClientCache.get().hostUrl + "/img/powered_by_black-ee7e351d64511ecea75c6c17ca30064f.png");

        soundcloudImg.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                Window.open("https://soundcloud.com", "_blank", "");
            }
        });


        youtubeFrameContainer.getElement().setInnerHTML("<div id='muzek_youtube_player'></div>");

    }

    public void reInitializeYouTubeContainer(){
        youtubeFrameContainer.clear();
        youtubeFrameContainer.getElement().setInnerHTML("<div id='muzek_youtube_player'></div>");
    }


    public void initialize(final MuzekTrackOdto track) {



        if (track.getType().equals("youtube")) {



            container.setVisible(true);
            soundcloudContainer.setVisible(false);
            youtubeContainer.setVisible(true);

        } else {

            if (track.getArtworkUrl() != null && track.getArtworkUrl().length() > 0) {
                artwork.setUrl(track.getArtworkUrl());
                artwork.setVisible(true);
                //artwork.setVisible(true);
            } else {
                artwork.setUrl("");
                artwork.setVisible(false);
            }


            String titleStr = track.getTitle();
            if (titleStr.length() > 60) {
                titleStr = titleStr.substring(0, 60);
            }

            title.setText(titleStr);
            title.setHref(track.getPermalinkUrl());

            if (track.getUser() != null && track.getUser().getUserName() != null){
                author.setText(track.getUser().getUserName());
            }
            if (track.getUser() != null && track.getUser().getPermalink() != null){
                author.setHref(track.getUser().getPermalink());
            }

            if (handler != null) {
                handler.removeHandler();
            }

            handler = artwork.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    Window.open(track.getUser().getPermalink(), "_blank", "");
                }
            });

            container.setVisible(true);
            soundcloudContainer.setVisible(true);
            youtubeContainer.setVisible(false);
        }




    }




}