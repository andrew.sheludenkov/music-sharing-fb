package com.client.facebook.player.sharer;

import com.client.facebook.player.albums.item.MuzekAlbumItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekAlbumOdto;


public class MuzekSharerImage extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekSharerImage> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    @UiField
    HTMLPanel container;
    @UiField
    public Image image;


    public MuzekSharerImage() {

        initWidget(ourUiBinder.createAndBindUi(this));

        //initWhenCreated();

    }



}