package com.client.facebook.player;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.facebook.player.atrwork.Artwork;
import com.client.util.ClientCache;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekTrackOdto;

public class FBYouTubePlayer {


    private static FBYouTubePlayer player;
    Widget widgetPlayer;


    public static FBYouTubePlayer get() {
        if (player == null) {
            player = new FBYouTubePlayer();
        }
        return player;
    }

    public FBYouTubePlayer() {
        initializeListener(this);
    }

    public void flush(){
        playerInitialized = false;
    }


    MuzekTrackOdto muzekTrackOdto;
    Artwork artwork;

    Boolean playerInitialized = false;

    void play(MuzekTrackOdto muzekTrackOdto, Artwork artwork) {


        this.muzekTrackOdto = muzekTrackOdto;
        this.artwork = artwork;


        if (!playerInitialized) {
            stop();
            insertVideoEvent(muzekTrackOdto.getId());
            playerInitialized = true;
        } else {
            loadVideoById(muzekTrackOdto.getId());
        }
    }

    void resume() {
        if (playerInitialized) {
            playEvent();
        }
    }

    void stop() {
        if (playerInitialized) {
            stopEvent();
            playerInitialized = false;
        }
        artwork.reInitializeYouTubeContainer();
    }

    public void getYouTubeVideoStats(Widget widgetPlayer) {
        this.widgetPlayer = widgetPlayer;
        getYouTubeVideoStatsEvent();

    }

    String getCurrentTime() {
//        if (playerInitialized){
//            String time = getCurrentTime(jsPlayer);
//            return time;
//        }
        return "0";
    }

    String getDuration() {
//        if (playerInitialized){
//            String time = getDuration(jsPlayer);
//            return time;
//        }
        return "0";
    }

    String getVolume() {
//        if (playerInitialized){
//            String volume = getVolume(jsPlayer);
//            Double volumeDouble = Double.valueOf(volume) / 100d;
//            return volumeDouble.toString();
//        }
        return "0";
    }

    void setVolume(String volume) {
        Double volumeDouble = Double.valueOf(volume) * 100;
        if (playerInitialized) {
            setPlayerVolumeEvent(volumeDouble.toString());
        }
    }

    void setProgress(String progress) {
        if (playerInitialized) {
            setPlayerProgressEvent(progress);
        }
    }

    void pause() {
        if (playerInitialized) {
            pauseEvent();
        }
    }

    void loadVideoById(String videoId) {
        if (playerInitialized) {
            loadVideoByIdEvent(videoId);
        }
    }

//    void loop(Boolean value){
//        if (jsPlayer != null){
//            setLoop(jsPlayer, value);
//        }
//    }
//
//    String getVideoUrl(){
//        if (jsPlayer != null){
//            String time = getVideoUrl(jsPlayer);
//            return time;
//        }
//        return null;
//    }

    String getBuffered() {
//        if (jsPlayer != null){
//            String bufferedFraction = getVideoLoadedFraction(jsPlayer);
//            Float fraction = Float.valueOf(bufferedFraction);
//
//            String durationStr = getDuration();
//            Float duration = Float.valueOf(durationStr);
//
//            return String.valueOf(fraction * duration);
//        }
        return null;
    }


    void onPlayerEnded(String s) {

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setMuzekTrackOdto(ClientCache.get().getCurrentlyPlayingTrack());
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.ENDED);
        EventBus.get().fireEvent(muzekPlayerEvent);

    }

    void onPlayerStatUpdated(String volumeS, String duration, String currentTime) {

        Double volume = Double.valueOf(volumeS);
        volume = volume / 100d;

        if (widgetPlayer instanceof FBWidgetPlayer){
            ((FBWidgetPlayer)widgetPlayer).onPlayerUpdateStatus(currentTime, duration, volume.toString(), duration, null, null, null);
        }
        if (widgetPlayer instanceof FBWidgetPlayerFriend){
            ((FBWidgetPlayerFriend)widgetPlayer).onPlayerUpdateStatus(currentTime, duration, volume.toString(), duration, null, null, null);
        }

    }


    public static native void insertVideoEvent(String trackId) /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'INSERT_VIDEO', object: trackId }, '*');
    }-*/;

    public static native void playEvent() /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'PLAY'}, '*');
    }-*/;

    public static native void stopEvent() /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'STOP'}, '*');
    }-*/;

    public static native void setPlayerVolumeEvent(String volume) /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'SET_VOLUME', object: volume }, '*');
    }-*/;

    public static native void setPlayerProgressEvent(String progress) /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'SET_PROGRESS', object: progress }, '*');
    }-*/;

    public static native void pauseEvent() /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'PAUSE', object: 'object' }, '*');
    }-*/;

    public static native JavaScriptObject loadVideoByIdEvent(String trackId) /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'LOAD_VIDEO_BY_ID', object: trackId }, '*');
    }-*/;

    public static native void getYouTubeVideoStatsEvent() /*-{
        window.postMessage({ type: 'MUZEKBOX_YOUTUBE_EVENT', action: 'GET_STATS' }, '*');
    }-*/;

    public static native void initializeListener(FBYouTubePlayer that) /*-{


        window.addEventListener('message', function (event) {
            if (event.data.type && (event.data.type == 'MUZEKBOX_YOUTUBE_EVENT_CALLBACK')) {

                if (event.data.action == 'GET_STATS_CALLBACK') {

                    that.@com.client.facebook.player.FBYouTubePlayer::onPlayerStatUpdated(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)
                        (event.data.volume, event.data.duration, event.data.currentTime);


                }

                if (event.data.action == 'STATE_ENDED_CALLBACK') {

                    that.@com.client.facebook.player.FBYouTubePlayer::onPlayerEnded(Ljava/lang/String;)
                        ("s");

                }
            }
        });


    }-*/;

    public static native JavaScriptObject insertVideo(String trackId, FBYouTubePlayer that) /*-{

        var muzek_yt_player = null;

        muzek_yt_player = new $wnd.YT.Player('muzek_youtube_player', {
            height: '144',
            width: '256',
            videoId: trackId,
            playerVars: {'controls': 0 },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onStateChange
            }
        });

        function onPlayerReady(event) {
            event.target.playVideo();
        }

        function onStateChange(event) {

            if (event.data == $wnd.YT.PlayerState.ENDED) {
                that.@com.client.facebook.player.FBYouTubePlayer::onPlayerEnded(Ljava/lang/String;)
                    ("s");
            }
        }

        return muzek_yt_player;

    }-*/;

    public static native JavaScriptObject loadVideoById(JavaScriptObject jsPlayer, String trackId) /*-{

        jsPlayer.loadVideoById(trackId, 5, "large")

        return jsPlayer;

    }-*/;


    public static native void stop(JavaScriptObject jsPlayer) /*-{
        jsPlayer.stopVideo();
        jsPlayer.destroy();
    }-*/;

    public static native void play(JavaScriptObject jsPlayer) /*-{
        jsPlayer.playVideo();
    }-*/;

    public static native String getCurrentTime(JavaScriptObject jsPlayer) /*-{
        return jsPlayer.getCurrentTime();
    }-*/;

    public static native String getDuration(JavaScriptObject jsPlayer) /*-{
        return jsPlayer.getDuration();
    }-*/;

    public static native String getVolume(JavaScriptObject jsPlayer) /*-{
        return jsPlayer.getVolume();
    }-*/;

    public static native void setPlayerVolume(JavaScriptObject jsPlayer, String volume) /*-{
        jsPlayer.setVolume(volume);
    }-*/;

    public static native void setPlayerProgress(JavaScriptObject jsPlayer, String progress) /*-{
        jsPlayer.seekTo(progress, true);
    }-*/;

    public static native String getVideoUrl(JavaScriptObject jsPlayer) /*-{
        return jsPlayer.getVideoUrl();
    }-*/;

    public static native String getVideoLoadedFraction(JavaScriptObject jsPlayer) /*-{
        return jsPlayer.getVideoLoadedFraction();
    }-*/;

    public static native void pause(JavaScriptObject jsPlayer) /*-{
        jsPlayer.pauseVideo();
    }-*/;

    public static native void setLoop(JavaScriptObject jsPlayer, Boolean value) /*-{
        jsPlayer.setLoop(value);
        alert(value);
    }-*/;


}