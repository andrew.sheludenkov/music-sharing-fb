package com.client.facebook.player.progressbar;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekPlayerEventHandler;
import com.client.event.ProgressBarEvent;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.media.client.Audio;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekTrackOdto;
import com.shop.web.odto.MuzekTracksResultOdto;


public class ProgressBar extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, ProgressBar> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;


    @UiField
    FlowPanel bar1;
    @UiField
    FlowPanel bar2;
    @UiField
    FlowPanel bar3;
    @UiField
    FlowPanel cover;

    Double max;

    public enum PROGRESS_TYPE{
        PROGRESS,
        VOLUME,
        VK_IMPORT,
    }
    PROGRESS_TYPE progressType;


    public ProgressBar() {

        initWidget(ourUiBinder.createAndBindUi(this));

        cover.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                int left = event.getRelativeX(cover.getElement());
                int width = cover.getOffsetWidth();

                double value = left * max / width;

                ProgressBarEvent progressBarEvent = new ProgressBarEvent();

                switch (progressType){
                    case PROGRESS:
                        progressBarEvent.setProgress(value);
                        break;
                    case VOLUME:
                        progressBarEvent.setVolume(value);
                        break;
                }
                EventBus.get().fireEvent(progressBarEvent);
            }
        }, ClickEvent.getType());

    }


    public void initialize(Double max, PROGRESS_TYPE progressType){
        this.max = max;
        this.progressType = progressType;
    }

    public void setMid(Double mid){
        this.max = max;

        Double midPercent = 100d * mid / max;

        bar2.setWidth("" + midPercent + "%");

//        DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#fff");
    }

    public void setMin(Double min){
        this.max = max;

        Double minPercent = 100d * min / max;

        bar1.setWidth("" + minPercent + "%");

//        DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#fff");
    }





}