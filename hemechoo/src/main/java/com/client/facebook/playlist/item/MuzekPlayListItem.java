package com.client.facebook.playlist.item;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekTrackDnDEvent;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.player.MuzekPlayer;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekPlayListItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekPlayListItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    public HTMLPanel container;
//    @UiField
//    Label trackName;
    @UiField
    FlowPanel playerContainer;


    MuzekPlayer player = new MuzekPlayer();

    FBWidgetPlayList fbWidgetPlayList;

    MuzekTrackOdto trackOdto;

    public MuzekPlayListItem() {

        initWidget(ourUiBinder.createAndBindUi(this));

        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#eee");
                player.showAddAnchor(true);

                MuzekTrackDnDEvent eventDnD = new MuzekTrackDnDEvent();
                eventDnD.setItem(MuzekPlayListItem.this);
                eventDnD.setFbWidgetPlayList(fbWidgetPlayList);
                eventDnD.setAction(MuzekTrackDnDEvent.ACTION.MOUSE_OVER);
                EventBus.get().fireEvent(eventDnD);
            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#fff");
                player.showAddAnchor(false);
            }
        }, MouseOutEvent.getType());

        container.addDomHandler(new MouseDownHandler() {
            @Override
            public void onMouseDown(MouseDownEvent event) {

                MuzekTrackDnDEvent eventDnD = new MuzekTrackDnDEvent();
                eventDnD.setItem(MuzekPlayListItem.this);
                eventDnD.setFbWidgetPlayList(fbWidgetPlayList);
                eventDnD.setAction(MuzekTrackDnDEvent.ACTION.MOUSE_DOWN);
                EventBus.get().fireEvent(eventDnD);

                DOM.setStyleAttribute(container.getElement(), "cursor", "move");

            }
        }, MouseDownEvent.getType());

        container.addDomHandler(new MouseUpHandler() {
            @Override
            public void onMouseUp(MouseUpEvent event) {

                MuzekTrackDnDEvent eventDnD = new MuzekTrackDnDEvent();
                eventDnD.setItem(MuzekPlayListItem.this);
                eventDnD.setFbWidgetPlayList(fbWidgetPlayList);
                eventDnD.setAction(MuzekTrackDnDEvent.ACTION.MOUSE_UP);
                EventBus.get().fireEvent(eventDnD);

                DOM.setStyleAttribute(container.getElement(), "cursor", "auto");

            }
        }, MouseUpEvent.getType());

    }


    public void initialize(MuzekTrackOdto trackOdto, FBWidgetPlayList.LIST_TYPE listType, Boolean currentlyPlaying,
                           FBWidgetPlayList fbWidgetPlayList){

        this.trackOdto = trackOdto;
        this.fbWidgetPlayList = fbWidgetPlayList;

        String trackName = trackOdto.getTitle();

//        this.trackName.setText(trackName);



        String streamUrl = trackOdto.getStreamUrl();
        String title = trackOdto.getTitle();

        if (streamUrl != null && title != null){
            player.initialize(trackOdto, listType, this, currentlyPlaying);
        }

        playerContainer.add(player);

    }


    public String getSortField() {
        return trackOdto.getIdentifier();
    }

    public MuzekTrackOdto getTrackOdto() {
        return trackOdto;
    }

    public void setTrackOdto(MuzekTrackOdto trackOdto) {
        this.trackOdto = trackOdto;
    }
}