package com.client.facebook.playlist.item;

import com.client.event.EventBus;
import com.client.event.MuzekTrackDnDEvent;
import com.client.facebook.playlist.FBWidgetPlayListFriend;
import com.client.facebook.playlist.player.MuzekPlayerFeed;
import com.client.facebook.playlist.player.MuzekPlayerFriend;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekPlayListItemFeed extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekPlayListItemFeed> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    public HTMLPanel container;
//    @UiField
//    Label trackName;
    @UiField
    FlowPanel playerContainer;


    MuzekPlayerFeed player = new MuzekPlayerFeed();

    //FBWidgetPlayListFriend fbWidgetPlayList;

    MuzekTrackOdto trackOdto;

    public MuzekPlayListItemFeed() {

        initWidget(ourUiBinder.createAndBindUi(this));


        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#eee");
                player.showAddAnchor(true);

            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#fff");
                player.showAddAnchor(false);
            }
        }, MouseOutEvent.getType());


    }


    public void initialize(MuzekTrackOdto trackOdto, Boolean currentlyPlaying){

        this.trackOdto = trackOdto;
        //this.fbWidgetPlayList = fbWidgetPlayList;

        String trackName = trackOdto.getTitle();

//        this.trackName.setText(trackName);



        String streamUrl = trackOdto.getStreamUrl();
        String title = trackOdto.getTitle();

        if (streamUrl != null && title != null){
            player.initialize(trackOdto, this, currentlyPlaying);
        }

        playerContainer.add(player);

    }


    public String getSortField() {
        return trackOdto.getIdentifier();
    }

    public MuzekTrackOdto getTrackOdto() {
        return trackOdto;
    }

    public void setTrackOdto(MuzekTrackOdto trackOdto) {
        this.trackOdto = trackOdto;
    }

    public void play(){
        player.play();
    }
}