package com.client.facebook.playlist;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekTrackDnDEvent;
import com.client.event.MuzekTrackDnDEventHandler;
import com.client.facebook.player.progressbar.ProgressBar;
import com.client.facebook.playlist.item.MuzekPlayListItem;
import com.client.facebook.playlist.item.MuzekPlayListItemFriend;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.media.client.Audio;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekTrackOdto;
import com.shop.web.odto.MuzekTracksResultOdto;


public class FBWidgetPlayListFriend extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBWidgetPlayListFriend> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    public enum LIST_TYPE {
        PLAY_LIST,

        SEARCH_LIST
    }

    public enum RESPONSE_TYPE {
        YOUTUBE,
        SOUNDCLOUD
    }

    @UiField
    HTMLPanel container;
    @UiField
    TextBox searchString;
    @UiField
    Image searchButton;

    @UiField
    Image hideAnchor;
    @UiField
    FlowPanel tracksContainerYT;
    @UiField
    FlowPanel tracksContainerSC;
    @UiField
    Label resultsLabelYt;
    @UiField
    Label resultsLabelSc;
    @UiField
    FlowPanel savedListContainer;
    @UiField
    Image progress;
    @UiField
    Anchor moreFromYouTube;
    @UiField
    FlowPanel tracksContainerYTNext;
    @UiField
    ProgressBar progressBar;
    @UiField
    Label importCompleteMsg;
    @UiField
    FlowPanel importFromVkContainer;

    @UiField
    Label importProgressMsgYoutube;
    @UiField
    FlowPanel importFromYoutubeContainer;

    Audio player = null;

    Boolean receivedFromYoutube = false;
    Boolean receivedFromSoundcloud = false;

    String nextPageToken = null;

    Boolean progressBarInitialized = false;
    String importProgressStat = "";

    private Integer currentPage = 0;
    private Boolean firstPageLoaded = false;
    private Boolean nextPageLoadInProgress = false;
    private Boolean haveMorePages = true;
    private Boolean searchResult = false;


    MuzekPlayListItem item1;
    MuzekPlayListItem item2;

    Boolean justReordered = false;

    FBWidgetPlayListFriend.LIST_TYPE listType;


    public FBWidgetPlayListFriend() {

        initWidget(ourUiBinder.createAndBindUi(this));

        //initWhenCreated();

    }

    public void initWhenCreated() {
        searchString.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                onSearchButton(null);
            }
        });

//        searchString.addKeyPressHandler(new KeyPressHandler() {
//            @Override
//            public void onKeyPress(KeyPressEvent event) {
//                String text = searchString.getText();
//                if (text.length() > 0){
//                    DOM.setStyleAttribute(container.getElement(), "color", "#eee");
//                }else{
//
//                }
//            }
//        });

        searchString.getElement().setPropertyString("placeholder", "Искать новую музыку");

        if (ClientCache.get().getBrowser().equals(ClientCache.BROWSER.FF)) {
            searchString.setHeight("24px");
        }

        progressBar.initialize(1d, ProgressBar.PROGRESS_TYPE.VK_IMPORT);


        Window.addWindowScrollHandler(new Window.ScrollHandler() {
            @Override
            public void onWindowScroll(Window.ScrollEvent scrollEvent) {

                if (firstPageLoaded && !nextPageLoadInProgress && haveMorePages
                        && !searchResult
                    //&& container.getParent().getParent().isVisible()
                        ) {

                    Integer scrollTop = scrollEvent.getScrollTop();
                    Integer containerHeight = container.getOffsetHeight();
                    Integer clientHeight = Window.getClientHeight();

                    if (containerHeight - scrollTop < clientHeight + 100) {
                        currentPage++;

                        String userId = ClientCache.get().getFBFriend();
                        if (userId != null) {

                            nextPageLoadInProgress = true;

                            StandaloneAsyncService.get().getMuzekUserTrackList(userId, currentPage, ClientCache.get().getTracksPerPage(), new StandaloneCallback<MuzekTracksResultOdto>() {
                                @Override
                                public void onSuccess(MuzekTracksResultOdto result) {
                                    //Window.alert("load");

                                    JsArray<MuzekTrackOdto> list = result.getTracksDtoList();
                                    if (list.length() > 0) {
                                        addItemsToList(result, currentPage);
                                    } else {
                                        haveMorePages = false;
                                    }

                                    nextPageLoadInProgress = false;

                                }
                            });

                        }

                    }
                }


            }
        });

        savedListContainer.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                item1 = null;
                item2 = null;
            }
        }, MouseOutEvent.getType());

        EventBus.get().addHandler(MuzekTrackDnDEvent.TYPE, new MuzekTrackDnDEventHandler() {
            @Override
            public void onEvent(MuzekTrackDnDEvent event) {

                if (!searchResult) {



                    if (event.getAction() != null && event.getAction().equals(MuzekTrackDnDEvent.ACTION.MOUSE_OVER)
                            && event.getFbWidgetPlayList().equals(FBWidgetPlayListFriend.this)) {

                        if (item1 != null) {
                            //DOM.setStyleAttribute(event.getItem().container.getElement(), "backgroundColor", "red");
                            item2 = event.getItem();

                            if (item2 != null && item1 != item2) {
                                int index1 = savedListContainer.getWidgetIndex(item1);
                                item1.removeFromParent();

                                int index2 = savedListContainer.getWidgetIndex(item2);


                                console("index1 = " + index1);
                                console("index2 = " + index2);

                                if (index1 > index2) {
                                    savedListContainer.insert(item1, index2);
                                } else {
                                    savedListContainer.insert(item1, index2 + 1);
                                }

                                item2 = null;

                                justReordered = true;


                            }
                        }

                    }
                }

            }
        });

    }


    public void initialize() {


        refreshTrackList();

    }

    public void show(Boolean show) {
        this.setVisible(show);
    }

    private void refreshTrackList() {
        currentPage = 0;
        firstPageLoaded = false;
        haveMorePages = true;
        searchResult = false;

        importFromVkContainer.setVisible(false);
        importFromYoutubeContainer.setVisible(false);

        savedListContainer.setVisible(false);
        savedListContainer.clear();

        tracksContainerYT.clear();
        tracksContainerSC.clear();

        tracksContainerYT.setVisible(false);
        tracksContainerSC.setVisible(false);

        resultsLabelSc.setVisible(false);
        resultsLabelYt.setVisible(false);

        tracksContainerYTNext.setVisible(false);

        item1 = null;
        item2 = null;


        String userId = ClientCache.get().getFBFriend();
        if (userId != null) {

            StandaloneAsyncService.get().getMuzekUserTrackList(userId, 0, ClientCache.get().getTracksPerPage(), new StandaloneCallback<MuzekTracksResultOdto>() {
                @Override
                public void onSuccess(MuzekTracksResultOdto result) {

                    addItemsToList(result, 0);

                    firstPageLoaded = true;

                }
            });

        }
    }

    private void addItemsToList(MuzekTracksResultOdto result, int page) {

        if (page == 0) {
            ClientCache.get().setTrackList((JsArray<MuzekTrackOdto>) JavaScriptObject.createArray().cast());
        }

        JsArray<MuzekTrackOdto> list = result.getTracksDtoList();
        //buildList(list, LIST_TYPE.PLAY_LIST);

        MuzekTrackOdto currentlyPlayingTrack = ClientCache.get().getCurrentlyPlayingTrack();

        for (int i = 0; i < list.length(); i++) {
            JavaScriptObject object = list.get(i);

            ClientCache.get().getTrackList().push((MuzekTrackOdto) object);

            MuzekTrackOdto muzekTrackOdto = (MuzekTrackOdto) object;
            if (muzekTrackOdto.getTitle() != null && muzekTrackOdto.getStreamUrl() != null &&
                    muzekTrackOdto.getTitle().length() > 0 && muzekTrackOdto.getStreamUrl().length() > 0) {

                Boolean currentlyPlaying = false;
                if (currentlyPlayingTrack != null && currentlyPlayingTrack.getId().equals(muzekTrackOdto.getId())) {
                    ClientCache.get().setCurrentlyPlayingTrack(muzekTrackOdto);
                    currentlyPlaying = true;
                }

                MuzekPlayListItemFriend item = new MuzekPlayListItemFriend();
                item.initialize(muzekTrackOdto, LIST_TYPE.SEARCH_LIST, currentlyPlaying, this);


                savedListContainer.add(item);
                savedListContainer.setVisible(true);

            }
        }

        if (list.length() < ClientCache.get().getTracksPerPage()) {
            haveMorePages = false;
        }
    }

    private void buildList(JsArray<MuzekTrackOdto> list, LIST_TYPE listType, RESPONSE_TYPE responseType) {

        if (responseType.equals(RESPONSE_TYPE.SOUNDCLOUD)) {
            receivedFromSoundcloud = true;
        }
        if (responseType.equals(RESPONSE_TYPE.YOUTUBE)) {
            receivedFromYoutube = true;
        }

        MuzekTrackOdto currentlyPlayingTrack = ClientCache.get().getCurrentlyPlayingTrack();

        for (int i = 0; i < list.length(); i++) {
            JavaScriptObject object = list.get(i);

            ClientCache.get().getTrackList().push((MuzekTrackOdto) object);

            MuzekTrackOdto muzekTrackOdto = (MuzekTrackOdto) object;
            if (muzekTrackOdto.getTitle() != null && muzekTrackOdto.getStreamUrl() != null &&
                    muzekTrackOdto.getTitle().length() > 0 && muzekTrackOdto.getStreamUrl().length() > 0) {

                Boolean currentlyPlaying = false;
                if (currentlyPlayingTrack != null && currentlyPlayingTrack.getId().equals(muzekTrackOdto.getId())) {
                    ClientCache.get().setCurrentlyPlayingTrack(muzekTrackOdto);
                    currentlyPlaying = true;
                }

                MuzekPlayListItemFriend item = new MuzekPlayListItemFriend();
                item.initialize(muzekTrackOdto, listType, currentlyPlaying, this);

                if (muzekTrackOdto.getType().equals("youtube")) {
                    tracksContainerYT.add(item);
//                    tracksContainerYT.setVisible(true);
//                    resultsLabelYt.setVisible(true);

                } else {
                    tracksContainerSC.add(item);
//                    tracksContainerSC.setVisible(true);
//                    resultsLabelSc.setVisible(true);
                }

            }
        }

        //if (receivedFromSoundcloud && receivedFromYoutube) {
        showSearchResult();
        //}
    }

    @UiHandler("searchButton")
    void onSearchButton(ClickEvent clickEvent) {
        searchResult = true;

        nextPageToken = null;

        //progress.setVisible(true);

        receivedFromYoutube = false;
        receivedFromSoundcloud = false;


        savedListContainer.setVisible(false);
//
//        tracksContainerYT.clear();
//        tracksContainerSC.clear();

//        resultsLabelSc.setVisible(false);
//        resultsLabelYt.setVisible(false);

        ClientCache.get().setTrackList((JsArray<MuzekTrackOdto>) JavaScriptObject.createArray().cast());

        StandaloneAsyncService.get().getMuzekSearch(new StandaloneCallback<JsArray>() {
            @Override
            public void onSuccess(JsArray result) {

                tracksContainerSC.clear();
                buildList(result, LIST_TYPE.SEARCH_LIST, RESPONSE_TYPE.SOUNDCLOUD);


            }
        }, searchString.getText());

        StandaloneAsyncService.get().getMuzekSearchYT(new StandaloneCallback<MuzekTracksResultOdto>() {
            @Override
            public void onSuccess(MuzekTracksResultOdto result) {

                JsArray<MuzekTrackOdto> list = result.getYoutubeTracksDtoList();
                nextPageToken = result.getNextPageToken();

                tracksContainerYT.clear();
                buildList(list, LIST_TYPE.SEARCH_LIST, RESPONSE_TYPE.YOUTUBE);

            }
        }, searchString.getText(), nextPageToken);


        StandaloneAsyncService.get().sendMuzekStat("search " + searchString.getText());

        StandaloneAsyncService.get().mixpanelTrack("search");
//        StandaloneAsyncService.get().mixpanelTrack("search string = " + searchString.getText());

        ClientCache.get().setCurrentlyExposedAlbum(null);
        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setMuzekAlbumOdto(null);
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.SHOW_PLAYLIST_OF_ALBUM);
        EventBus.get().fireEvent(muzekPlayerEvent);

    }

    @UiHandler("moreFromYouTube")
    void onMoreFromYouTubeButton(ClickEvent clickEvent) {


        StandaloneAsyncService.get().getMuzekSearchYT(new StandaloneCallback<MuzekTracksResultOdto>() {
            @Override
            public void onSuccess(MuzekTracksResultOdto result) {

                JsArray<MuzekTrackOdto> list = result.getYoutubeTracksDtoList();
                nextPageToken = result.getNextPageToken();

                buildList(list, LIST_TYPE.SEARCH_LIST, RESPONSE_TYPE.YOUTUBE);

            }
        }, searchString.getText(), nextPageToken);


        StandaloneAsyncService.get().sendMuzekStat("search more" + searchString.getText());

    }

    private void showSearchResult() {
        tracksContainerYT.setVisible(true);
        resultsLabelYt.setVisible(true);

        tracksContainerSC.setVisible(true);
        resultsLabelSc.setVisible(true);

        tracksContainerYTNext.setVisible(true);

        progress.setVisible(false);
    }

    @UiHandler("hideAnchor")
    void onHideAnchor(ClickEvent clickEvent) {

        //
//        show(false);
//
//        Window.scrollTo(0, 1);
//        Window.scrollTo(0, 0);

        // Window.alert("1a");


        //StandaloneAsyncService.get().mixpanelTrack("close");

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.CLOSE_PLAY_LIST);
        EventBus.get().fireEvent(muzekPlayerEvent);

        // Window.alert("1b");


    }


    public void updateVkImportProgress(String statistics, String status) {

        importFromVkContainer.setVisible(true);
        if (importProgressStat.equals(statistics)) {
            return;
        }

        Double total = Double.valueOf(statistics.split(":")[0]);
        Double imported = Double.valueOf(statistics.split(":")[1]);
        Double failed = Double.valueOf(statistics.split(":")[2]);

        importCompleteMsg.setText("Import from VK, " + imported + " of " + total);

        //Window.alert(""+ ((imported + failed ) / total));
        progressBar.setMin((imported + failed) / total);
        importProgressStat = statistics;

        if (status != null && (status.equals("COMPLETE") || status.equals("FAILED"))) {
            importCompleteMsg.setText("Import Complete, " + imported + " of " + total);
            //progressBar.setVisible(false);
            refreshTrackList();
        }

    }

    public void updateYoutubeImportProgress(String statistics, String status) {


        importFromYoutubeContainer.setVisible(true);
        if (importProgressStat.equals(statistics)) {
            return;
        }

        String total = statistics;

        importProgressMsgYoutube.setText("Import from Youtube in progress, " + total + " tracks imported...");

        importProgressStat = statistics;

        if (status != null && (status.equals("COMPLETE") || status.equals("FAILED"))) {
            importProgressMsgYoutube.setText("Import Complete, " + total + " tracks imported");
            //progressBar.setVisible(false);
            refreshTrackList();
        }

    }


    public void destroy() {
        savedListContainer.clear();
        item1 = null;
        item2 = null;
    }

    public static native void console(String text)
/*-{
    console.log(text);
}-*/;

}