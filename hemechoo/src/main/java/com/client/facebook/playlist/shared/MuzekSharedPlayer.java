package com.client.facebook.playlist.shared;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekPlayerEventHandler;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.item.MuzekPlayListItem;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekSharedPlayer extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekSharedPlayer> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;

    @UiField
    Label trackName;
    @UiField
    Image artwork;
    @UiField
    Button buttonSelect;
    @UiField
    Label labelSelected;
//    @UiField
//    Image playButton;
//    @UiField
//    Image pauseButton;
//    @UiField
//    Anchor addAnchor;
//    @UiField
//    Anchor removeAnchor;
//    @UiField
//    Image avatar;
//    @UiField
//    HTMLPanel audio;

    //public Audio player = null;

    public MuzekTrackOdto trackDto;

    FBSharedPlayList.LIST_TYPE listType;
    MuzekSharedPlayListItem muzekPlayListItem;


    public MuzekSharedPlayer() {

        initWidget(ourUiBinder.createAndBindUi(this));

        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                if (!labelSelected.isVisible()) {
                    buttonSelect.setVisible(true);
                }
            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                if (!labelSelected.isVisible()) {
                    buttonSelect.setVisible(false);
                }
            }
        }, MouseOutEvent.getType());

//        player = ClientCache.get().getPlayer();
        //player = Audio.createIfSupported();

    }


    public void initialize(MuzekTrackOdto trackDto, FBSharedPlayList.LIST_TYPE listType, MuzekSharedPlayListItem muzekPlayListItem, Boolean currentlyPlaying) {

        this.trackDto = trackDto;
        //audio.add(player);
        this.listType = listType;
        this.muzekPlayListItem = muzekPlayListItem;

        if (trackDto.getArtworkUrl() != null) {
            artwork.setUrl(trackDto.getArtworkUrl());
        }

//        if (currentlyPlaying){
//            playButton.setVisible(false);
//            pauseButton.setVisible(true);
//        }


        EventBus.get().addHandler(MuzekPlayerEvent.TYPE, new MuzekPlayerEventHandler() {
            @Override
            public void onEvent(MuzekPlayerEvent event) {
                //Audio player = event.getAudio();
                MuzekTrackOdto track = event.getMuzekTrackOdto();
                if (track != null) {

                    if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PAUSE)) {
                        if (track.equals(MuzekSharedPlayer.this.trackDto)) {
//                        playButton.setVisible(true);
//                        pauseButton.setVisible(false);
                        }
                    }
//                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PLAY)) {
//                    if (track.equals(MuzekSharedPlayer.this.trackDto)) {
//                        playButton.setVisible(false);
//                        pauseButton.setVisible(true);
//                    } else {
//                        playButton.setVisible(true);
//                        pauseButton.setVisible(false);
//                    }
//                }
//                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.ENDED)) {
//                    if (track.equals(MuzekSharedPlayer.this.trackDto)) {
//                        playButton.setVisible(true);
//                        pauseButton.setVisible(false);
//                    }
//                }
                    if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PLAY)) {
                        MuzekTrackOdto trackToPlay = event.getMuzekTrackOdto();
                        if (trackToPlay.equals(MuzekSharedPlayer.this.trackDto)) {
                            // onPlayButton(null);

//                        playButton.setVisible(false);
//                        pauseButton.setVisible(true);
                        }
                    }
                }

            }
        });

        if (trackDto.getTitle() != null) {
            this.trackName.setText(trackDto.getTitle());
        }


//        if (trackDto.getArtworkUrl() != null) {
//            avatar.setUrl(trackDto.getArtworkUrl());
//        }
//        playButton.setUrl(ClientCache.get().hostUrl + "/img/1.png");
//        pauseButton.setUrl(ClientCache.get().hostUrl + "/img/2.png");

    }


//    public void showAddAnchor(Boolean show) {
//        switch (listType) {
//            case SEARCH_LIST:
//                addAnchor.setVisible(show);
//                break;
//            case PLAY_LIST:
//                removeAnchor.setVisible(show);
//                break;
//        }
//    }


//    @UiHandler("addAnchor")
//    void onaddAnchor(ClickEvent clickEvent) {
//
//        String userId = ClientCache.get().getFBuserId();
//        String trackId = trackDto.getId();
//        String trackName = trackDto.getTitle();
//
//        StandaloneAsyncService.get().addTrackToPlaylist(userId, trackDto, new StandaloneCallback<JavaScriptObject>() {
//            @Override
//            public void onSuccess(JavaScriptObject result) {
//                //To change body of implemented methods use File | Settings | File Templates.
//            }
//        });
//
//    }
//
//    @UiHandler("removeAnchor")
//    void onRemoveAnchor(ClickEvent clickEvent) {
//
//        String userId = ClientCache.get().getFBuserId();
//        String trackId = trackDto.getId();
//        String trackName = trackDto.getTitle();
//
//        StandaloneAsyncService.get().removeTrackToPlaylist(userId, trackId, new StandaloneCallback<JavaScriptObject>() {
//            @Override
//            public void onSuccess(JavaScriptObject result) {
//                //To change body of implemented methods use File | Settings | File Templates.
//            }
//        });
//
//        muzekPlayListItem.setVisible(false);
//
//
//    }


    public MuzekTrackOdto getTrackDto() {
        return trackDto;
    }

    public void setTrackDto(MuzekTrackOdto trackDto) {
        this.trackDto = trackDto;
    }


    public void onTracksSelect() {

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();

        if (!labelSelected.isVisible()) {
            buttonSelect.setVisible(false);
            labelSelected.setVisible(true);
            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.ADD_TRACK_TO_SEND_MESSAGE);
        }else{
            buttonSelect.setVisible(true);
            labelSelected.setVisible(false);
            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.REMOVE_TRACK_TO_SEND_MESSAGE);
        }

        muzekPlayerEvent.setMuzekTrackOdto(trackDto);
        EventBus.get().fireEvent(muzekPlayerEvent);
    }
}