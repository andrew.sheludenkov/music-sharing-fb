package com.client.facebook.playlist.shared;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekPlayerEventHandler;
import com.client.facebook.playlist.item.MuzekPlayListItem;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.media.client.Audio;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.EmbeddingParamsOdto;
import com.shop.web.odto.MuzekFeedOdto;
import com.shop.web.odto.MuzekTrackOdto;
import com.shop.web.odto.MuzekTracksResultOdto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FBSharedPlayList extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBSharedPlayList> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    public enum LIST_TYPE {
        PLAY_LIST,
        SEARCH_LIST
    }

    @UiField
    HTMLPanel container;
    @UiField
    TextBox searchString;
    @UiField
    Image searchButton;


    @UiField
    FlowPanel tracksContainerYT;
    @UiField
    FlowPanel tracksContainerSC;
    @UiField
    Label resultsLabelYt;
    @UiField
    Label resultsLabelSc;
    @UiField
    FlowPanel savedListContainer;
    @UiField
    Image hideAnchor;
    @UiField
    Image image;
    @UiField
    Label userName;
    @UiField
    Button sendMusic;

    Audio player = null;

    String fbUserId;
    String stringUserName;

    HashMap<String, MuzekTrackOdto> trackMapToSend = new HashMap<String, MuzekTrackOdto>();

    public FBSharedPlayList() {

        initWidget(ourUiBinder.createAndBindUi(this));

        searchString.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                onSearchButton(null);
            }
        });

        if (ClientCache.get().getBrowser().equals(ClientCache.BROWSER.FF)){
            searchString.setHeight("24px");
        }
    }


    public void initialize(String fbUserId, String stringUserName) {

        this.fbUserId = fbUserId;
        this.stringUserName = stringUserName;

        refreshTrackList();

        final String userTrackListId = fbUserId;
        String encodedUserId = ClientCache.get().encodeUserId(ClientCache.get().getFBuserId());
        String encodedFriendId = ClientCache.get().encodeUserId(userTrackListId);

        final String playlistUrl = "https://facebook.com/" + userTrackListId + "?mz=friend&mz_f=" + encodedFriendId + "&mz_ref=" + encodedUserId;


        String avatarUrl = "https://graph.facebook.com/v2.6/" + fbUserId + "/picture?height=90&width=90";
        image.setUrl(avatarUrl);

        image.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                Window.open(playlistUrl, "_blank", "");
            }
        });

        userName.setText(stringUserName);
        userName.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                Window.open(playlistUrl, "_blank", "");
            }
        });

        EventBus.get().addHandler(MuzekPlayerEvent.TYPE, new MuzekPlayerEventHandler() {
            @Override
            public void onEvent(MuzekPlayerEvent event) {
                if (event.getAction().equals(MuzekPlayerEvent.ACTION.ADD_TRACK_TO_SEND_MESSAGE)){
                    trackMapToSend.put(event.getMuzekTrackOdto().getId(), event.getMuzekTrackOdto());

                }
                if (event.getAction().equals(MuzekPlayerEvent.ACTION.REMOVE_TRACK_TO_SEND_MESSAGE)){
                    trackMapToSend.remove(event.getMuzekTrackOdto().getId());

                }
            }
        });

    }

    public void show(Boolean show) {
        this.setVisible(show);
    }

    private void refreshTrackList() {

        savedListContainer.setVisible(false);
        savedListContainer.clear();

        tracksContainerYT.clear();
        tracksContainerSC.clear();

        tracksContainerYT.setVisible(false);
        tracksContainerSC.setVisible(false);

        resultsLabelSc.setVisible(false);
        resultsLabelYt.setVisible(false);



        String userId = ClientCache.get().getFBuserId();
        if (userId != null) {

            StandaloneAsyncService.get().getMuzekUserTrackList(userId, 0, 300, new StandaloneCallback<MuzekTracksResultOdto>() {
                @Override
                public void onSuccess(MuzekTracksResultOdto result) {

                    ClientCache.get().setTrackList((JsArray<MuzekTrackOdto>) JavaScriptObject.createArray().cast());

                    JsArray<MuzekTrackOdto> list = result.getTracksDtoList();
                    //buildList(list, LIST_TYPE.PLAY_LIST);

                    MuzekTrackOdto currentlyPlayingTrack = ClientCache.get().getCurrentlyPlayingTrack();

                    for (int i = 0; i < list.length(); i++) {
                        JavaScriptObject object = list.get(i);

                        ClientCache.get().getTrackList().push((MuzekTrackOdto) object);

                        MuzekTrackOdto muzekTrackOdto = (MuzekTrackOdto) object;
                        if (muzekTrackOdto.getTitle() != null && muzekTrackOdto.getStreamUrl() != null &&
                                muzekTrackOdto.getTitle().length() > 0 && muzekTrackOdto.getStreamUrl().length() > 0) {

                            Boolean currentlyPlaying = false;
                            if (currentlyPlayingTrack != null && currentlyPlayingTrack.getId().equals(muzekTrackOdto.getId())) {
                                ClientCache.get().setCurrentlyPlayingTrack(muzekTrackOdto);
                                currentlyPlaying = true;
                            }

                            MuzekSharedPlayListItem item = new MuzekSharedPlayListItem();
                            item.initialize(muzekTrackOdto, LIST_TYPE.PLAY_LIST, currentlyPlaying);


                            savedListContainer.add(item);
                            savedListContainer.setVisible(true);

                        }
                    }


                }
            });

        }
    }

    private void buildList(JsArray<MuzekTrackOdto> list, LIST_TYPE listType) {


        MuzekTrackOdto currentlyPlayingTrack = ClientCache.get().getCurrentlyPlayingTrack();

        for (int i = 0; i < list.length(); i++) {
            JavaScriptObject object = list.get(i);

            ClientCache.get().getTrackList().push((MuzekTrackOdto) object);

            MuzekTrackOdto muzekTrackOdto = (MuzekTrackOdto) object;
            if (muzekTrackOdto.getTitle() != null && muzekTrackOdto.getStreamUrl() != null &&
                    muzekTrackOdto.getTitle().length() > 0 && muzekTrackOdto.getStreamUrl().length() > 0) {

                Boolean currentlyPlaying = false;
                if (currentlyPlayingTrack != null && currentlyPlayingTrack.getId().equals(muzekTrackOdto.getId())) {
                    ClientCache.get().setCurrentlyPlayingTrack(muzekTrackOdto);
                    currentlyPlaying = true;
                }

                MuzekSharedPlayListItem item = new MuzekSharedPlayListItem();
                item.initialize(muzekTrackOdto, listType, currentlyPlaying);

                if (muzekTrackOdto.getType().equals("youtube")) {
                    tracksContainerYT.add(item);
                    tracksContainerYT.setVisible(true);
                    resultsLabelYt.setVisible(true);
                } else {
                    tracksContainerSC.add(item);
                    tracksContainerSC.setVisible(true);
                    resultsLabelSc.setVisible(true);
                }

            }
        }
    }

    @UiHandler("searchButton")
    void onSearchButton(ClickEvent clickEvent) {

        savedListContainer.setVisible(false);

        tracksContainerYT.clear();
        tracksContainerSC.clear();

        resultsLabelSc.setVisible(false);
        resultsLabelYt.setVisible(false);

        ClientCache.get().setTrackList((JsArray<MuzekTrackOdto>) JavaScriptObject.createArray().cast());

        StandaloneAsyncService.get().getMuzekSearch(new StandaloneCallback<JsArray>() {
            @Override
            public void onSuccess(JsArray result) {

                tracksContainerSC.clear();
                buildList(result, LIST_TYPE.SEARCH_LIST);

            }
        }, searchString.getText());

        StandaloneAsyncService.get().getMuzekSearchYT(new StandaloneCallback<MuzekTracksResultOdto>() {
            @Override
            public void onSuccess(MuzekTracksResultOdto result) {

                JsArray<MuzekTrackOdto> list = result.getYoutubeTracksDtoList();

                tracksContainerYT.clear();
                buildList(list, LIST_TYPE.SEARCH_LIST);

            }
        }, searchString.getText(), null);


    }


    @UiHandler("hideAnchor")
    void onCloseButton(ClickEvent clickEvent) {

        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.CLOSE_ATTACH_TRACK);
        EventBus.get().fireEvent(muzekPlayerEvent);

    }

    @UiHandler("sendMusic")
    void onSendMusic(ClickEvent event){
        sendMusic();

        onCloseButton(null);
    }

    private void sendMusic(){
        if (trackMapToSend.size() > 0){
            String key = (String)trackMapToSend.keySet().toArray()[0];
            MuzekTrackOdto item = trackMapToSend.get(key);

            trackMapToSend.remove(key);

            StandaloneAsyncService.get().sendMuzekMessage(fbUserId,
                    stringUserName,
                    item.getTitle(),
                    item.getType(),
                    item.getId(),
                    new StandaloneCallback<EmbeddingParamsOdto>() {
                        @Override
                        public void onSuccess(EmbeddingParamsOdto result) {
                            sendMusic();
                        }
                    });
        }
    }

}