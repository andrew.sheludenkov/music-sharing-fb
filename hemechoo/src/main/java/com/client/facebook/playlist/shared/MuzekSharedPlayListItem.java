package com.client.facebook.playlist.shared;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.player.MuzekPlayer;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekSharedPlayListItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekSharedPlayListItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;
//    @UiField
//    Label trackName;
    @UiField
    FlowPanel playerContainer;


    MuzekSharedPlayer player = new MuzekSharedPlayer();



    public MuzekSharedPlayListItem() {

        initWidget(ourUiBinder.createAndBindUi(this));

        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#eee");
//                player.showAddAnchor(true);
            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                DOM.setStyleAttribute(container.getElement(), "backgroundColor", "#fff");
//                player.showAddAnchor(false);
            }
        }, MouseOutEvent.getType());

        container.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                player.onTracksSelect();





            }
        }, ClickEvent.getType());


    }


    public void initialize(MuzekTrackOdto trackOdto, FBSharedPlayList.LIST_TYPE listType, Boolean currentlyPlaying){

        String trackName = trackOdto.getTitle();

//        this.trackName.setText(trackName);



        String streamUrl = trackOdto.getStreamUrl();
        String title = trackOdto.getTitle();

        if (streamUrl != null && title != null){
            player.initialize(trackOdto, listType, this, currentlyPlaying);
        }

        playerContainer.add(player);

    }





}