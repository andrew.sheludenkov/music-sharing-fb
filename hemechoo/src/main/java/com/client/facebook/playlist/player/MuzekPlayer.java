package com.client.facebook.playlist.player;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.event.MuzekPlayerEventHandler;
import com.client.event.MuzekRadioEvent;
import com.client.facebook.player.FBWidgetPlayer;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.item.MuzekPlayListItem;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekPlayer extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekPlayer> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel container;

    @UiField
    Label trackName;
    @UiField
    Image playButton;
    @UiField
    Image pauseButton;
    @UiField
    Anchor addAnchor;
    @UiField
    Anchor removeAnchor;
    @UiField
    Label addedLabel;
    @UiField
    FlowPanel sendButtonContainer;
//    @UiField
//    Image avatar;
//    @UiField
//    HTMLPanel audio;

    //public Audio player = null;

    public MuzekTrackOdto trackDto;

    FBWidgetPlayList.LIST_TYPE listType;
    MuzekPlayListItem muzekPlayListItem;
    Boolean sendButtonShowed = false;

    Boolean justAdded = false;

    public MuzekPlayer() {

        initWidget(ourUiBinder.createAndBindUi(this));

//        player = ClientCache.get().getPlayer();
        //player = Audio.createIfSupported();

    }


    public void initialize(final MuzekTrackOdto trackDto, FBWidgetPlayList.LIST_TYPE listType, MuzekPlayListItem muzekPlayListItem, Boolean currentlyPlaying) {

        this.trackDto = trackDto;
        //audio.add(player);
        this.listType = listType;
        this.muzekPlayListItem = muzekPlayListItem;

        if (currentlyPlaying) {
            playButton.setVisible(false);
            pauseButton.setVisible(true);
        }


        EventBus.get().addHandler(MuzekPlayerEvent.TYPE, new MuzekPlayerEventHandler() {
            @Override
            public void onEvent(MuzekPlayerEvent event) {
                //Audio player = event.getAudio();
                MuzekTrackOdto track = event.getMuzekTrackOdto();
                if (track != null) {


                    if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PAUSE)) {
                        if (track.getId().equals(MuzekPlayer.this.trackDto.getId())) {
                            playButton.setVisible(true);
                            pauseButton.setVisible(false);
                        }
                    }
                    if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PLAY)) {
                        if (track.getId().equals(MuzekPlayer.this.trackDto.getId())) {
                            playButton.setVisible(false);
                            pauseButton.setVisible(true);

//                            if (!sendButtonShowed){
//                                sendButtonShowed = true;
//                                FBWidgetPlayer.showSendMessageButton(trackDto, sendButtonContainer);
//                            }
                        } else {
                            playButton.setVisible(true);
                            pauseButton.setVisible(false);
                        }



                    }
                    if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.ENDED)) {
                        if (track.getId().equals(MuzekPlayer.this.trackDto.getId())) {
                            playButton.setVisible(true);
                            pauseButton.setVisible(false);
                        }
                    }
                    // Window.alert("aRr 10" + event.getAction().toString());
//                if (event.getAction() != null && event.getAction().equals(MuzekPlayerEvent.ACTION.PLAY_NEXT)) {
//                  //  Window.alert("aRr 11");
//                    MuzekTrackOdto trackToPlay = event.getMuzekTrackOdto();
//
//                    if (track == null){
//                    Window.alert("track");
//                    }
//                    if (MuzekPlayer.this.trackDto == null){
//                    Window.alert("MuzekPlayer.this.trackDto");
//                    }
//
//
//                   // if (track != null && MuzekPlayer.this.trackDto != null){
//
//                    if (track.getId().equals(MuzekPlayer.this.trackDto.getId())) {
//                      //  Window.alert("aRr 13");
//                        onPlayButton(null);
//
////                        playButton.setVisible(false);
////                        pauseButton.setVisible(true);
//                    }
//                   // }
//                }

                }
            }
        });

        if (trackDto.getTitle() != null) {
            this.trackName.setText(trackDto.getTitle());
        }


//        if (trackDto.getArtworkUrl() != null) {
//            avatar.setUrl(trackDto.getArtworkUrl());
//        }
        playButton.setUrl(ClientCache.get().hostUrl + "/img/mz/mz_play.png");
        pauseButton.setUrl(ClientCache.get().hostUrl + "/img/mz/mz_pause.png");

    }


    @UiHandler("playButton")
    void onPlayButton(ClickEvent clickEvent) {

//
//        if (ClientCache.get().getSourceElement() != null) {
//            if (player != null) {
//                if (player.getCurrentSrc() != null) {
//                    player.removeSource(ClientCache.get().getSourceElement());
//
//                    player.
//                }
//            }
//        }


        if (this.trackDto != null) {

//            if (player == null) {
//
//                String streamUrl = this.trackDto.getStreamUrl();
//                //streamUrl = streamUrl.replaceAll("http", "https");
//
//                player = Audio.createIfSupported();
//                player.addEndedHandler(new EndedHandler() {
//                    @Override
//                    public void onEnded(EndedEvent event) {
//                        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
//                        muzekPlayerEvent.setAudio(player);
//                        muzekPlayerEvent.setMuzekTrackOdto(MuzekPlayer.this.trackDto);
//                        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.ENDED);
//                        EventBus.get().fireEvent(muzekPlayerEvent);
//                    }
//                });
//                //SourceElement sourceElement = player.addSource(streamUrl + "?client_id=317e92a14f3c83f40b845580e1205d61", AudioElement.TYPE_MP3);
////            ClientCache.get().setSourceElement(sourceElement);
//
////            player.play();
//            }

            ClientCache.get().setCurrentRadioChannelId(null);
            MuzekRadioEvent eventRadio = new MuzekRadioEvent();
            eventRadio.setAction(MuzekRadioEvent.ACTION.PLAY_FROM_TRACKLIST);
            EventBus.get().fireEvent(eventRadio);


            MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
            //muzekPlayerEvent.setAudio(player);
            muzekPlayerEvent.setMuzekTrackOdto(this.trackDto);
            muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PLAY);
            EventBus.get().fireEvent(muzekPlayerEvent);

//            playButton.setVisible(false);
//            pauseButton.setVisible(true);
        }


//        MuzekPlayerPlayEvent muzekPlayerPlayEvent = new MuzekPlayerPlayEvent();
//        muzekPlayerEvent.setAudio(player);
//        EventBus.get().fireEvent(muzekPlayerEvent);

        //StandaloneAsyncService.get().mixpanelTrack("play in playlist");

        // Window.alert("aRr 14");
    }

    @UiHandler("pauseButton")
    void onPauseButton(ClickEvent clickEvent) {


        MuzekPlayerEvent muzekPlayerEvent = new MuzekPlayerEvent();
        //muzekPlayerEvent.setAudio(player);
        muzekPlayerEvent.setMuzekTrackOdto(this.trackDto);
        muzekPlayerEvent.setAction(MuzekPlayerEvent.ACTION.PAUSE);
        EventBus.get().fireEvent(muzekPlayerEvent);

//        playButton.setVisible(true);
//        pauseButton.setVisible(false);

        StandaloneAsyncService.get().mixpanelTrack("pause in playlist");

    }


    public void showAddAnchor(Boolean show) {

        if (!justAdded) {
            switch (listType) {
                case SEARCH_LIST:
                    addAnchor.setVisible(show);
                    break;
                case PLAY_LIST:
                    removeAnchor.setVisible(show);
                    break;
            }
        }
    }


    @UiHandler("addAnchor")
    void onaddAnchor(ClickEvent clickEvent) {

        String userId = ClientCache.get().getFBuserId();
        String trackId = trackDto.getId();
        String trackName = trackDto.getTitle();

        StandaloneAsyncService.get().addTrackToPlaylist(userId, trackDto, new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        addedLabel.setVisible(true);
        addAnchor.setVisible(false);
        removeAnchor.setVisible(false);

        justAdded = true;

        StandaloneAsyncService.get().mixpanelTrack("add to playlist");
    }

    @UiHandler("removeAnchor")
    void onRemoveAnchor(ClickEvent clickEvent) {

        String userId = ClientCache.get().getFBuserId();
        String trackId = trackDto.getId();
        String trackName = trackDto.getTitle();

        StandaloneAsyncService.get().removeTrackToPlaylist(userId, trackId, new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        muzekPlayListItem.setVisible(false);


        StandaloneAsyncService.get().mixpanelTrack("remove from playlist");
    }


}