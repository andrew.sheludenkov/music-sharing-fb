package com.client.facebook.feedback;


import com.client.MainIntervioUIService;
import com.client.MainIntervioUIServiceAsync;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;


public class FeedbackBlock extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    private final MainIntervioUIServiceAsync serviceAsync = GWT
            .create(MainIntervioUIService.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, FeedbackBlock> {
    }

    @UiField
    FlowPanel block;

    @UiField
    Button button;
    @UiField
    HTMLPanel additionalFields;

    @UiField
    TextArea plus;

    @UiField
    StarsBlock stars;
    @UiField
    Label rateThisApp;
    @UiField
    Label tankForFeedback;
    @UiField
    Button cancel;


    private String productId;

    private static FeedbackBlock searchBlock;

    FlowPanel container;

    public static FeedbackBlock get() {
        if (searchBlock == null) {
            searchBlock = new FeedbackBlock();
        }
        return searchBlock;
    }


    public void setFormWidth(String params){
        block.setWidth(params);


    }

    public FeedbackBlock() {
        initWidget(uiBinder.createAndBindUi(this));

        plus.getElement().setPropertyString("placeholder", "Please write your feedback here");

        plus.addKeyPressHandler(new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                String text = plus.getText();
                if (text.length() > 800){
                    plus.setText(text.substring(0, 800));
                }
            }
        });

        stars.initialize(this);

    }

    public void initialize() {



    }



    @UiHandler("button")
    void onRemove(ClickEvent clickEvent) {

        String text = plus.getText();
        String starsNumber = stars.getStars().toString();

        StandaloneAsyncService.get().muzekPostFeedback(text, starsNumber, new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        feedbackReceived();
        StandaloneAsyncService.mixpanelTrack("feedback sent");

    }

    @UiHandler("cancel")
    void onCancel(ClickEvent clickEvent) {

        this.setVisible(false);
        StandaloneAsyncService.get().muzekPostFeedback("", "cancelled", new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        StandaloneAsyncService.mixpanelTrack("feedback cancelled");

    }

    public void feedbackReceived(){
        rateThisApp.setVisible(false);
        tankForFeedback.setVisible(true);
        additionalFields.setVisible(false);
        button.setVisible(false);
    }


    public static native String getURL() /*-{
        return $wnd.location.href;
    }-*/;

}