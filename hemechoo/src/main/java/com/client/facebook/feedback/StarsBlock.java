package com.client.facebook.feedback;


import com.client.MainIntervioUIService;
import com.client.MainIntervioUIServiceAsync;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;


public class StarsBlock extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    private final MainIntervioUIServiceAsync serviceAsync = GWT
            .create(MainIntervioUIService.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, StarsBlock> {
    }



    private String productId;

    private static StarsBlock searchBlock;

    FlowPanel container;
    @UiField
    Image star1;
    @UiField
    Image star2;
    @UiField
    Image star3;
    @UiField
    Image star4;
    @UiField
    Image star5;

    HandlerRegistration hr1;
    HandlerRegistration hr2;
    HandlerRegistration hr3;
    HandlerRegistration hr4;
    HandlerRegistration hr5;

    HandlerRegistration hrc1;
    HandlerRegistration hrc2;
    HandlerRegistration hrc3;
    HandlerRegistration hrc4;
    HandlerRegistration hrc5;

    Integer value = 0;
    FeedbackBlock feedbackBlock;

    public static StarsBlock get() {
        if (searchBlock == null) {
            searchBlock = new StarsBlock();
        }
        return searchBlock;
    }


    public StarsBlock() {
        initWidget(uiBinder.createAndBindUi(this));

        hr1 = star1.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(1);
            }
        }, MouseOverEvent.getType());
        hr2 = star2.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(2);
            }
        }, MouseOverEvent.getType());
        hr3 = star3.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(3);
            }
        }, MouseOverEvent.getType());
        hr4 = star4.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(4);
            }
        }, MouseOverEvent.getType());
        hr5 = star5.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(5);
            }
        }, MouseOverEvent.getType());



        hrc1 = star1.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                openEditor();
                StandaloneAsyncService.mixpanelTrack("stars selected 1");
            }
        },ClickEvent.getType());
        hrc2 = star2.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                openEditor();
                StandaloneAsyncService.mixpanelTrack("stars selected 2");
            }
        },ClickEvent.getType());
        hrc3 = star3.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                openEditor();
                StandaloneAsyncService.mixpanelTrack("stars selected 3");
            }
        },ClickEvent.getType());
        hrc4 = star4.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sendToMarket();
                StandaloneAsyncService.mixpanelTrack("stars selected 4");
            }
        },ClickEvent.getType());
        hrc5 = star5.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sendToMarket();
                StandaloneAsyncService.mixpanelTrack("stars selected 5");
            }
        },ClickEvent.getType());


    }

    private void sendToMarket() {
        StandaloneAsyncService.get().muzekPostFeedback("", value.toString(), new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        Window.open("https://chrome.google.com/webstore/detail/adblock-plus/ebpjgnfppnecnnaffbmfocapphedapfp/reviews", "_blank", "");

        feedbackBlock.feedbackReceived();
        removeHandlers();


    }

    private void removeHandlers(){
        hr1.removeHandler();
        hr2.removeHandler();
        hr3.removeHandler();
        hr4.removeHandler();
        hr5.removeHandler();

        hrc1.removeHandler();
        hrc2.removeHandler();
        hrc3.removeHandler();
        hrc4.removeHandler();
        hrc5.removeHandler();
    }

    private void openEditor(){

        feedbackBlock.plus.setVisible(true);
        feedbackBlock.plus.setFocus(true);
        feedbackBlock.button.setVisible(true);

        removeHandlers();

    }

    private void star(int number){
        String img1 = "https://muzekbox.com/img/star1.png";
        String img2 = "https://muzekbox.com/img/star2.png";
        star1.setUrl(number < 1 ? img1 : img2);
        star2.setUrl(number < 2 ? img1 : img2);
        star3.setUrl(number < 3 ? img1 : img2);
        star4.setUrl(number < 4 ? img1 : img2);
        star5.setUrl(number < 5 ? img1 : img2);

        value = number;

    }

    public Integer getValue(){
        return value;
    }

    public void initialize(FeedbackBlock feedbackBlock) {
        this.feedbackBlock = feedbackBlock;

    }

    public Integer getStars(){
        return value;
    }

    public static native String getURL() /*-{
        return $wnd.location.href;
    }-*/;

}