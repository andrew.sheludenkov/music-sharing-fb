package com.client.facebook.messages.item;

import com.client.facebook.player.FBWidgetPlayer;
import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.item.MuzekPlayListItemFeed;
import com.client.facebook.playlist.item.MuzekPlayListItemMessage;
import com.client.facebook.playlist.player.MuzekPlayer;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekFeedOdto;
import com.shop.web.odto.MuzekMessagesOdto;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekMessageItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekMessageItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    public HTMLPanel container;
    //    @UiField
//    Label trackName;
    @UiField
    FlowPanel playerContainer;
    @UiField
    Image image;
    @UiField
    Label userName;
    @UiField
    FlowPanel tracksContainer;
    @UiField
    Label date;

    @UiField
    Button sendMusic;
    @UiField
    FlowPanel nameBlockRecepient;
    @UiField
    FlowPanel nameBlockSender;
    @UiField
    Label dateSender;
    @UiField
    Label userNameSender;

    Boolean initialized = false;


    MuzekPlayer player = new MuzekPlayer();

    FBWidgetPlayList fbWidgetPlayList;

    MuzekTrackOdto trackOdto;

    FBWidgetPlayer fbWidgetPlayer;

    MuzekMessagesOdto item;

    public MuzekMessageItem(FBWidgetPlayer fbWidgetPlayer) {

        this.fbWidgetPlayer = fbWidgetPlayer;

        initWidget(ourUiBinder.createAndBindUi(this));


        container.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                sendMusic.setVisible(true);
            }
        }, MouseOverEvent.getType());


        container.addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                sendMusic.setVisible(false);
            }
        }, MouseOutEvent.getType());

    }


    public void initialize(MuzekMessagesOdto item) {

        this.item = item;
        String userId = "";
        String userMessageName = "";

        String currentUserFBid = ClientCache.get().getFBuserId();
        if (item.getSenderId().equals(currentUserFBid)) {
            // current user is sender of this message
            userId = item.getRecipientId();
            userMessageName = item.getRecipientName();

            nameBlockSender.setVisible(true);
            nameBlockRecepient.setVisible(false);
        } else{
            userId = item.getSenderId();
            userMessageName = item.getSenderName();

            nameBlockSender.setVisible(false);
            nameBlockRecepient.setVisible(true);
        }


        final String userTrackListId = userId;
        String encodedUserId = ClientCache.get().encodeUserId(ClientCache.get().getFBuserId());
        String encodedFriendId = ClientCache.get().encodeUserId(userTrackListId);

        final String playlistUrl = "https://facebook.com/" + userTrackListId + "?mz=friend&mz_f=" + encodedFriendId + "&mz_ref=" + encodedUserId;

        if (!initialized) {
            String avatarUrl = "https://graph.facebook.com/v2.6/" + userId + "/picture?height=90&width=90";
            image.setUrl(avatarUrl);
            image.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });

            userName.setText(userMessageName);
            userName.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });
            userNameSender.setText(userMessageName);
            userNameSender.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });


            date.setText(item.getCreationDate());
            dateSender.setText(item.getCreationDate());

            initialized = true;
        }

        //if (tracksContainer.getWidgetCount() < 5) {
            MuzekPlayListItemMessage muzekPlayListItemMessage = new MuzekPlayListItemMessage();
        muzekPlayListItemMessage.initialize(item.getTrack(), false, item);

            tracksContainer.add(muzekPlayListItemMessage);
        //}

    }


    @UiHandler("sendMusic")
    void onSendMusicButton(ClickEvent clickEvent){

        String currentUserFBid = ClientCache.get().getFBuserId();
        if (item.getSenderId().equals(currentUserFBid)) {
            fbWidgetPlayer.openSendMusicWindow(item.getRecipientId(), item.getRecipientName());
        }else{
            fbWidgetPlayer.openSendMusicWindow(item.getSenderId(), item.getSenderName());
        }

    }

}