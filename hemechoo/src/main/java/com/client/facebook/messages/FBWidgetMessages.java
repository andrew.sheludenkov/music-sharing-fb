package com.client.facebook.messages;

import com.client.event.EventBus;
import com.client.event.MuzekPlayerEvent;
import com.client.facebook.feed.item.MuzekFeedItem;
import com.client.facebook.messages.item.MuzekMessageItem;
import com.client.facebook.people.FBWidgetPeople;
import com.client.facebook.player.FBWidgetPlayer;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientCache;
import com.client.util.LocaleStrings;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.*;

import java.util.HashMap;
import java.util.Map;


public class FBWidgetMessages extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, FBWidgetMessages> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    @UiField
    HTMLPanel container;

    @UiField
    FlowPanel feedContainer;
    @UiField
    TextBox searchString;
    @UiField
    Image searchButton;
    @UiField
    FlowPanel notFoundMessage;

    Boolean firstPageLoaded = false;
    Boolean nextPageLoadInProgress = false;
    Boolean haveMorePages = true;
    Integer currentPage = 0;
    Integer pageSize = 100;

    private Map<String, MuzekMessageItem> feedItemsMapSender = new HashMap<String, MuzekMessageItem>();
    private Map<String, MuzekMessageItem> feedItemsMapRecipient = new HashMap<String, MuzekMessageItem>();

    FBWidgetPlayer fbWidgetPlayer;
    FBWidgetPeople fbWidgetPeople;


    public FBWidgetMessages() {

        initWidget(ourUiBinder.createAndBindUi(this));

        //initWhenCreated();

    }

    public void initialize(FBWidgetPlayer fbWidgetPlayer) {
        this.fbWidgetPlayer = fbWidgetPlayer;

        currentPage = 0;
        haveMorePages = true;
        nextPageLoadInProgress = false;
        firstPageLoaded = false;

        StandaloneAsyncService.get().getMuzekMessages(currentPage, pageSize, new StandaloneCallback<MuzekMessagesResultOdto>() {
            @Override
            public void onSuccess(MuzekMessagesResultOdto result) {
                //To change body of implemented methods use File | Settings | File Templates.

                feedContainer.clear();

                if (result.getMessagesDtoList().length() > 0) {
                    addMoreItems(result.getMessagesDtoList(), FBWidgetMessages.this.fbWidgetPlayer);

                    feedContainer.setVisible(true);
                    notFoundMessage.setVisible(false);
                } else {
                    feedContainer.setVisible(false);
                    notFoundMessage.setVisible(true);
                }


                firstPageLoaded = true;
            }
        });

        searchString.getElement().setPropertyString("placeholder", "Искать людей");


        try{
            searchString.getElement().setPropertyString("placeholder", LocaleStrings.getLocalizedString("track_list.search_for_people"));
        }catch(Throwable e){

        }

    }

    public void initWhenCreated() {


        Window.addWindowScrollHandler(new Window.ScrollHandler() {
            @Override
            public void onWindowScroll(Window.ScrollEvent scrollEvent) {

                if (firstPageLoaded && !nextPageLoadInProgress && haveMorePages) {

                    Integer scrollTop = scrollEvent.getScrollTop();
                    Integer containerHeight = container.getOffsetHeight();
                    Integer clientHeight = Window.getClientHeight();

                    if (containerHeight - scrollTop < clientHeight + 100) {
                        currentPage++;
                        nextPageLoadInProgress = true;

                        StandaloneAsyncService.get().getMuzekMessages(currentPage, pageSize, new StandaloneCallback<MuzekMessagesResultOdto>() {
                            @Override
                            public void onSuccess(MuzekMessagesResultOdto result) {

                                if (result.getMessagesDtoList().length() > 0) {
                                    addMoreItems(result.getMessagesDtoList(), fbWidgetPlayer);


                                } else {
                                    haveMorePages = false;

                                }

                                nextPageLoadInProgress = false;
                            }
                        });
                    }
                }
            }
        });

        searchString.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                onSearchButton(null);
            }
        });
    }


    public void show(Boolean show) {
        this.setVisible(show);
    }


    private void addMoreItems(JsArray<MuzekMessagesOdto> list, FBWidgetPlayer fbWidgetPlayer) {


        feedItemsMapSender.clear();
        feedItemsMapRecipient.clear();

        String currentUserFBid = ClientCache.get().getFBuserId();

        for (int i = 0; i < list.length(); i++) {
            JavaScriptObject object = list.get(i);

            MuzekMessagesOdto item = (MuzekMessagesOdto) object;

            try {

                Long.valueOf(item.getSenderId());
                Long.valueOf(item.getRecipientId());

                if (item.getSenderId().equals(currentUserFBid)) {
                    MuzekMessageItem feedItem = feedItemsMapSender.get(item.getRecipientId());

                    if (feedItem == null) {
                        feedItem = new MuzekMessageItem(fbWidgetPlayer);
                        feedItem.initialize(item);

                        feedContainer.add(feedItem);

                        feedItemsMapSender.put(item.getRecipientId(), feedItem);
                    } else {
                        feedItem.initialize(item);
                    }
                }else{
                    MuzekMessageItem feedItem = feedItemsMapRecipient.get(item.getSenderId());

                    if (feedItem == null) {
                        feedItem = new MuzekMessageItem(fbWidgetPlayer);
                        feedItem.initialize(item);

                        feedContainer.add(feedItem);

                        feedItemsMapRecipient.put(item.getSenderId(), feedItem);
                    } else {
                        feedItem.initialize(item);
                    }
                }
            } catch (Throwable e) {

            }


        }


    }

    @UiHandler("searchButton")
    void onSearchButton(ClickEvent clickEvent) {

        feedContainer.clear();
        feedContainer.setVisible(true);
        notFoundMessage.setVisible(false);

        if (fbWidgetPeople == null){
            fbWidgetPeople = new FBWidgetPeople();
        }

        feedContainer.add(fbWidgetPeople);
        fbWidgetPeople.initialize(fbWidgetPlayer, searchString.getValue());

    }


}