package com.client.facebook.peer;

import com.client.facebook.playlist.FBWidgetPlayList;
import com.client.facebook.playlist.item.MuzekPlayListItemFeed;
import com.client.facebook.playlist.player.MuzekPlayer;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.MuzekFeedOdto;
import com.shop.web.odto.MuzekTrackOdto;


public class MuzekPeerItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, MuzekPeerItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    public HTMLPanel container;
    //    @UiField
//    Label trackName;
    @UiField
    FlowPanel playerContainer;
    @UiField
    Image image;
    @UiField
    Label userName;
    @UiField
    FlowPanel tracksContainer;
    @UiField
    Label date;
    @UiField
    Anchor more;
    @UiField
    Image thumbnail;
    @UiField
    FlowPanel previewContainer;
    @UiField
    Label videoLabel;
    @UiField
    FlowPanel playButton;

    Boolean initialized = false;


    MuzekPlayer player = new MuzekPlayer();

    FBWidgetPlayList fbWidgetPlayList;

    MuzekTrackOdto trackOdto;

    MuzekPlayListItemFeed currentTrack = null;

    public MuzekPeerItem() {

        initWidget(ourUiBinder.createAndBindUi(this));

        thumbnail.addDomHandler(new MouseDownHandler() {
            @Override
            public void onMouseDown(MouseDownEvent event) {
                currentTrack.play();
            }
        }, MouseDownEvent.getType());
        videoLabel.addDomHandler(new MouseDownHandler() {
            @Override
            public void onMouseDown(MouseDownEvent event) {
                currentTrack.play();
            }
        }, MouseDownEvent.getType());
        playButton.addDomHandler(new MouseDownHandler() {
            @Override
            public void onMouseDown(MouseDownEvent event) {
                currentTrack.play();
            }
        }, MouseDownEvent.getType());


    }


    public void initialize(MuzekFeedOdto feedItem) {

        final String userTrackListId = feedItem.getFbuserId();
        String encodedUserId = ClientCache.get().encodeUserId(ClientCache.get().getFBuserId());
        String encodedFriendId = ClientCache.get().encodeUserId(userTrackListId);

        final String playlistUrl = "https://facebook.com/" + userTrackListId + "?mz=friend&mz_f=" + encodedFriendId + "&mz_ref=" + encodedUserId;

        if (!initialized) {
            String avatarUrl = "https://graph.facebook.com/v2.6/" + feedItem.getFbuserId() + "/picture?height=90&width=90";
            image.setUrl(avatarUrl);

            image.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });

            userName.setText(feedItem.getUserName());
            userName.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });
            more.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    StandaloneAsyncService.get().sendMuzekStat("feed open playlist:"+ ClientCache.get().getFBuserId() + " friend:" + userTrackListId);
                    Window.open(playlistUrl, "_blank", "");
                }
            });

            //date.setText(feedItem.getCreationDate());

            if (feedItem.getTrackSource() != null && feedItem.getTrackSource().equals("YOUTUBE")){
                previewContainer.setVisible(true);
                String previewUrl = "https://i.ytimg.com/vi/"+ feedItem.getTrackId() +"/hqdefault.jpg";
                thumbnail.setUrl(previewUrl);

                videoLabel.setText(feedItem.getTrackName());
            }

            initialized = true;
        }

        if (tracksContainer.getWidgetCount() < 5) {
            MuzekPlayListItemFeed muzekPlayListItemFeed = new MuzekPlayListItemFeed();
            muzekPlayListItemFeed.initialize(feedItem.getTrack(), false);

            tracksContainer.add(muzekPlayListItemFeed);
            currentTrack = muzekPlayListItemFeed;
        }

    }


}