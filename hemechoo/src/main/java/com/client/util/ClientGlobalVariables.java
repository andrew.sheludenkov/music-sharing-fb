package com.client.util;

import com.client.MainUIService;
import com.client.MainUIServiceAsync;
import com.google.gwt.core.client.GWT;
import com.shop.web.dto.UserDto;
import com.shop.web.dto.VendorDto;


public class ClientGlobalVariables {

    private final MainUIServiceAsync serviceAsync = GWT
            .create(MainUIService.class);

    private static ClientGlobalVariables instance;

    private UserDto loggedInUser;
    private Float currencyValue;
    private VendorDto.CURRENCY currency;


    public static ClientGlobalVariables getInstance() {
        if (instance == null) {
            instance = new ClientGlobalVariables();
        }

        return instance;
    }

    public UserDto getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(UserDto loggedInUser) {

        this.loggedInUser = loggedInUser;


    }




    public void setCurrencyValue(Float currencyValue) {
        this.currencyValue = currencyValue;
    }

    public void setCurrencyValue(String currencyValueStr) {
        this.currencyValue = Float.valueOf(currencyValueStr);
    }

    public VendorDto.CURRENCY getCurrency() {
        return currency;
    }

    public void setCurrency(VendorDto.CURRENCY currency) {
        this.currency = currency;
    }

    public VendorDto.CURRENCY setCurrency(String value) {

        if (value.equals(VendorDto.CURRENCY.USD.toString())) {
            this.currency = VendorDto.CURRENCY.USD;
        }
        if (value.equals(VendorDto.CURRENCY.GRN.toString())) {
            this.currency = VendorDto.CURRENCY.GRN;
        }

        return this.currency;

    }

    public Float getCurrencyValue() {
        return currencyValue;
    }




}
