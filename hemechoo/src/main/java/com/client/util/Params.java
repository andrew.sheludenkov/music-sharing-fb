package com.client.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 14.01.12
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
public class Params {

    public static String PAGE = "page";
    public static String ITEM = "item";

    public static String PAGE_SEARCH = "search";
    public static String PAGE_COMMENTS = "comments";
    public static String PAGE_PROMO = "promo";
    public static String PAGE_LOGIN = "login";
    public static String PAGE_USER = "user";
    public static String PAGE_MODER = "moder";
    public static String PAGE_CSS = "css";
    public static String PAGE_IMAGES = "images";
    public static String PAGE_VENDOR = "vendors";
    public static String PAGE_ASSIGNED_PRICES = "assigned_prices";
    public static String PAGE_FEEDBACK = "feedback";

    public static String PAGE_DISCOUNTS = "discounts";
    public static String PAGE_HISTORY = "history";
    public static String PAGE_NEWS = "news";

    public static String delimiter = "=";

    public static String encodeParams(Map<String, String> map) {

        StringBuffer string = new StringBuffer();

        for (String key : map.keySet()) {
            String value = map.get(key);
            string.append(string.length() > 0 ? "&" + key + delimiter + value : key + delimiter + value);
        }

        return string.toString();
    }

    public static Map<String, String> fetchParams(String token) {
        Map<String, String> params = new HashMap<String, String>();

        String splittedParams[] = token.split("\\&");

        if (splittedParams.length > 1) {
            for (String splittedParam : splittedParams) {
                String pair[] = splittedParam.split(delimiter);
                params.put(pair[0], pair[1]);
            }
        }

        return params;
    }

}
