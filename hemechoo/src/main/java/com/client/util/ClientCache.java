package com.client.util;

import com.client.facebook.playlist.FBWidgetPlayList;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.SourceElement;
import com.google.gwt.http.client.URL;
import com.google.gwt.media.client.Audio;
import com.shop.web.dto.UserDto;
import com.shop.web.odto.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 09.01.12
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
public class ClientCache {

    public enum BROWSER{
        CHROME,
        FF,
        IE
    }

    public UserDto userDto;
    public UserOdto userOdto;
    private static ClientCache cache;
    public static String visitHash = null;
    private String currentRadioChannelId = null;
    private Boolean layoutInitialized = false;

    EmbeddingParamsOdto params;

//    Audio player = null;
    SourceElement sourceElement = null;

    String FBuserId = null;
    String FBFriend = null;

    JsArray<MuzekTrackOdto> trackList = null;


    MuzekTrackOdto currentlyPlayingTrack;
    MuzekTrackOdto currentlyPausedTrack;
    MuzekAlbumOdto currentlyExposedAlbum;
    MuzekGraphOdto graph;

    Double volume;
    Boolean loop;
    String storage;
    public String pluginVersion;


    public static ClientCache get() {
        if (cache == null) {
            cache = new ClientCache();
        }
        return cache;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public UserOdto getUserOdto() {
        return userOdto;
    }

    public void setUserOdto(UserOdto userOdto) {
        this.userOdto = userOdto;
    }

    public static String getVisitHash() {
        return visitHash;
    }

    public static void setVisitHash(String visitHash) {
        ClientCache.visitHash = visitHash;
    }

    public EmbeddingParamsOdto getParams() {
        return params;
    }

    public void setParams(EmbeddingParamsOdto params) {
        this.params = params;
    }

//    public Audio getPlayer() {
//        return player;
//    }
//
//    public void setPlayer(Audio player) {
//        this.player = player;
//    }

    public SourceElement getSourceElement() {
        return sourceElement;
    }

    public void setSourceElement(SourceElement sourceElement) {
        this.sourceElement = sourceElement;
    }


    public String getFBuserId() {
        return FBuserId;
    }

    public void setFBuserId(String FBuserId) {
        this.FBuserId = FBuserId;
    }

    public String getFBFriend() {
        return FBFriend;
    }

    public void setFBFriend(String FBFriend) {
        this.FBFriend = FBFriend;
    }

    public JsArray<MuzekTrackOdto> getTrackList() {
        return trackList;
    }

    public void setTrackList(JsArray<MuzekTrackOdto> trackList) {
        this.trackList = trackList;
    }



    public MuzekTrackOdto getCurrentlyPlayingTrack() {
        return currentlyPlayingTrack;
    }

    public void setCurrentlyPlayingTrack(MuzekTrackOdto currentlyPlayingTrack) {
        this.currentlyPlayingTrack = currentlyPlayingTrack;
    }

    public MuzekTrackOdto getCurrentlyPausedTrack() {
        return currentlyPausedTrack;
    }

    public void setCurrentlyPausedTrack(MuzekTrackOdto currentlyPausedTrack) {
        this.currentlyPausedTrack = currentlyPausedTrack;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public Boolean getLoop() {
        return loop;
    }

    public void setLoop(Boolean loop) {
        this.loop = loop;
    }

    public MuzekGraphOdto getGraph() {
        return graph;
    }

    public void setGraph(MuzekGraphOdto graph) {
        this.graph = graph;
    }

    public MuzekAlbumOdto getCurrentlyExposedAlbum() {
        return currentlyExposedAlbum;
    }

    public void setCurrentlyExposedAlbum(MuzekAlbumOdto currentlyExposedAlbum) {
        this.currentlyExposedAlbum = currentlyExposedAlbum;
    }

    public static native String getBrowserName() /*-{
        return navigator.userAgent.toLowerCase();
    }-*/;

    public BROWSER getBrowser(){
        String browserName = getBrowserName();
        if (browserName != null && browserName.length() > 0){
            if (browserName.toLowerCase().contains("firefox")){
                return BROWSER.FF;
            }
            if (browserName.toLowerCase().contains("msie")){
                return BROWSER.IE;
            }
        }

        return  BROWSER.CHROME;
    }


    public String getCurrentRadioChannelId() {
        return currentRadioChannelId;
    }

    public void setCurrentRadioChannelId(String currentRadioChannelId) {
        this.currentRadioChannelId = currentRadioChannelId;
    }

    public Integer getTracksPerPage() {
        return tracksPerPage;
    }

    private Integer tracksPerPage = 100;

    public String encodeUserId(String userId){

        String value = "error_parameter_is_not_integer";
        try {
            value = ((Long) (Long.valueOf(userId) * 13)).toString();
            value = ClientBase64.toBase64(value.getBytes());
            //value = btoa(value);
        } catch (Throwable e) {

        }
        return value;
    }

    public String decodeUserId(String userId){
        String value = new String (ClientBase64.fromBase64(userId));
        Long intValue =  Long.valueOf(value) / 13;
        value =  String.valueOf(intValue);
        return value;
    }

    public Boolean getLayoutInitialized() {
        return layoutInitialized;
    }

    public void setLayoutInitialized(Boolean layoutInitialized) {
        this.layoutInitialized = layoutInitialized;
    }



//    public String hostUrl = "http://linqs.co";
    //public String hostUrl = "http://127.0.0.1:8080/hemechoo";
//    public String hostUrl = "http://127.0.0.1:8888";
//    public String hostUrl = "http://10.0.2.2:8080";




//    public String hostUrl = "https://muzekbox.com";
    public String hostUrl = "http://localhost:8888";


//    public String hostUrl = "http://127.0.0.1:8080";
//    public String hostUrl = "https://127.0.0.1:8443";

//    public String hostUrl = "https://localhost:8443";
//    public String hostUrl = "https://192.168.1.69:8443";

 //   public String hostUrl = "http://embedstore.com";
//    public String hostUrl = "http://citymart.lg.ua";



//    public String hostUrl = "https://hemechoo.com";






    public static native String getPathFromExtension(String path) /*-{

        try{
            return chrome.extension.getURL(path);
        }catch (e){
            return "https://127.0.0.1:8443/" + path;
        }





    }-*/;

}

