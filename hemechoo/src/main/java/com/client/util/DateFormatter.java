package com.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;

public class DateFormatter extends DateTimeFormat {

	protected DateFormatter(String pattern) {
		super(pattern);
	}
	
	public static DateFormatter getInstance(String pattern) {
        DateFormatter d = new DateFormatter(pattern);
		return d;
	}
}
