package com.client.util;

import com.client.util.cleaners.ClientUtilCleaner590;
import com.shop.web.dto.UserDto;
import com.shop.web.odto.UserOdto;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 09.01.12
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
public class ClientUtil {


    private static ClientUtil cache;


    public static ClientUtil get() {
        if (cache == null) {
            cache = new ClientUtil();
        }
        return cache;
    }

    public String cleanProductName(String string){

        return string.replaceAll("\n", "").replaceAll("\\s+", " ");


//        String url = getURL();
//        if (url.split("/").length > 2){
//            String host = url.split("/")[2];
//            //if (host.equals("590.ua")){
//            if (host.equals("localhost:8888")){
//                return ClientUtilCleaner590.get().cleanProductName(string);
//            }
//        }
//
//        return string;
    }

    public static String formatDate(Date dt, String pattern) {
        int timezoneOffset = 0;

        DateFormatter format = DateFormatter.getInstance(pattern);
        // zulu timezone
        return format.format(dt, com.google.gwt.i18n.client.TimeZone.createTimeZone(timezoneOffset));
        //return format.format(dt);
    }

    public static String formatDate(Date dt, String pattern, int timeZonePffset) {

        DateFormatter format = DateFormatter.getInstance(pattern);
        // zulu timezone
        return format.format(dt, com.google.gwt.i18n.client.TimeZone.createTimeZone(timeZonePffset));
        //return format.format(dt);
    }

    public static Date parseDate(String date, String pattern) {
        DateFormatter format = DateFormatter.getInstance(pattern);
        return format.parseStrict(date);
    }

    public static native String getURL() /*-{
        return $wnd.location.href;
    }-*/;


}

