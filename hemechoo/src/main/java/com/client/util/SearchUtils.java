package com.client.util;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;


import java.util.ArrayList;
import java.util.List;


public class SearchUtils {

    public static List findElementsForClass(Element element, String className) {
        ArrayList result = new ArrayList();
        recElementsForClass(result, element, className);
        return result;
    }

    private static void recElementsForClass(ArrayList res, Element element, String className) {
        String c;

        if (element == null) {
            return;
        }

        c = DOM.getElementProperty(element, "className");

        if (c != null) {
            String[] p = c.split(" ");

            for (int x = 0; x < p.length; x++) {
                if (p[x].equals(className)) {
                    res.add(element);
                }
            }
        }

        for (int i = 0; i < DOM.getChildCount(element); i++) {
            Element child = DOM.getChild(element, i);
            recElementsForClass(res, child, className);
        }
    }

    public static List findElementsForTitle(Element element, String className) {
        ArrayList result = new ArrayList();
        recElementsForTitle(result, element, className);
        return result;
    }

    private static void recElementsForTitle(ArrayList res, Element element, String className) {
        String c;

        if (element == null) {
            return;
        }

        c = DOM.getElementProperty(element, "data-text");

        if (c != null) {

            Window.alert("Loo - " +  element.toString() );

                if (c.equals(className)) {
                    res.add(element);
                }

        }

        for (int i = 0; i < DOM.getChildCount(element); i++) {
            Element child = DOM.getChild(element, i);
            recElementsForTitle(res, child, className);
        }
    }



    // find buttons ===============================================================================

    public static List findButtons(com.google.gwt.dom.client.Element element) {
        ArrayList result = new ArrayList();
        recFindButtons(result, element);
        return result;
    }
    private static void recFindButtons(ArrayList res, com.google.gwt.dom.client.Element element) {
        String c;

        if (element == null) {
            return;
        }

        c = DOM.getElementProperty(element, "type");

        if (c != null) {
            String[] p = c.split(" ");

            for (int x = 0; x < p.length; x++) {
                if (p[x].equals("submit")) {
                    res.add(element);
                }
            }
        }

        for (int i = 0; i < DOM.getChildCount(element); i++) {
            com.google.gwt.dom.client.Element child = DOM.getChild(element, i);
            recFindButtons(res, child);
        }
    }

}
