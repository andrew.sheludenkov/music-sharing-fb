package com.client.util;

import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 09.01.12
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
public class VKImportProcessor {


    private static VKImportProcessor cache;



    public static VKImportProcessor get() {
        if (cache == null) {
            cache = new VKImportProcessor();
        }
        return cache;
    }




    protected static native void sendMessage(String message) /*-{

        $wnd.chrome.runtime.sendMessage({trackname: message}, function(response) {

        });

    }-*/;


//    public void importUsersList(){
//
//        String offset = "";
//
//        StandaloneAsyncService.get().getMuzekVkUser(offset, new StandaloneCallback<JavaScriptObject>() {
//            @Override
//            public void onSuccess(JavaScriptObject result) {
//                Window.alert(result.toString());
//            }
//        });
//    }

}

