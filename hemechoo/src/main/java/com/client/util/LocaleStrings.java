package com.client.util;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.SourceElement;
import com.shop.web.dto.UserDto;
import com.shop.web.odto.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 09.01.12
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
public class LocaleStrings {





    private static Map<String, String> localString_player_open_my_playlist;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Плейлист");
        aMap.put("UA", "Плейлист");
        aMap.put("DEFAULT", "My Playlist");
        localString_player_open_my_playlist = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> localString_player_radio_channels;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Радиоканалы");
        aMap.put("UA", "Радиоканалы");
        aMap.put("DEFAULT", "Radio Stations");
        localString_player_radio_channels = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> localString_player_my_albums;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Мои альбомы");
        aMap.put("UA", "Мои альбомы");
        aMap.put("DEFAULT", "My Albums");
        localString_player_my_albums = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> localString_import_from_vk;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Импортировать музыку из ВК");
        aMap.put("UA", "Импортировать музыку из ВК");
        aMap.put("DEFAULT", "Import Music from VK");
        localString_import_from_vk = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> localString_import_from_youtube;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Импортировать музыку из YouTube");
        aMap.put("UA", "Импортировать музыку из YouTube");
        aMap.put("DEFAULT", "Import Music from YouTube");
        localString_import_from_youtube = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> localString_share_this_track;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Поделитесь этим треком с друзьями");
        aMap.put("UA", "Поделитесь этим треком с друзьями");
        aMap.put("DEFAULT", "Share this Track with Friends");
        localString_share_this_track = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> feed_button;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Что слушают");
        aMap.put("UA", "Что слушают");
        aMap.put("DEFAULT", "Feed");
        feed_button = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> messages_button;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Сообщения");
        aMap.put("UA", "Сообщения");
        aMap.put("DEFAULT", "Messages");
        messages_button = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> search_for_new_music;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Искать новую музыку");
        aMap.put("UA", "Искать новую музыку");
        aMap.put("DEFAULT", "Search for new Music");
        search_for_new_music = Collections.unmodifiableMap(aMap);
    }
    private static Map<String, String> search_for_people;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("RU", "Искать людей");
        aMap.put("UA", "Искать людей");
        aMap.put("DEFAULT", "Search for people");
        search_for_people = Collections.unmodifiableMap(aMap);
    }

    private static Map<String, Map<String, String>> localString;
    static {
        Map<String, Map<String, String>> aMap = new HashMap<String, Map<String, String>>();
        aMap.put("player.open_my_playlist", localString_player_open_my_playlist);
        aMap.put("player.radio_channels", localString_player_radio_channels);
        aMap.put("player.my_albums", localString_player_my_albums);
        aMap.put("player.import_from_vk", localString_import_from_vk);
        aMap.put("player.import_from_youtube", localString_import_from_youtube);
        aMap.put("player.share_this_track", localString_share_this_track);
        aMap.put("player.feed_button", feed_button);
        aMap.put("player.messages_button", messages_button);
        aMap.put("track_list.search_for_new_music", search_for_new_music);
        aMap.put("track_list.search_for_people", search_for_people);


        localString = Collections.unmodifiableMap(aMap);
    }


    public static String getLocalizedString(String id){

        Map <String, String> map = localString.get(id);

        if (ClientCache.get().getParams() == null){
            return map.get("DEFAULT");
        }
        if (ClientCache.get().getParams().getCountry() == null){
            return map.get("DEFAULT");
        }
        if (map.get(ClientCache.get().getParams().getCountry()) == null){
            return map.get("DEFAULT");
        }

        return map.get(ClientCache.get().getParams().getCountry());

    }



}

