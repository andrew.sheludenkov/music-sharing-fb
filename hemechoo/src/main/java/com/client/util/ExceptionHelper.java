package com.client.util;

import com.client.exception.NotLoggedInException;
import com.google.gwt.user.client.History;

import java.util.HashMap;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 24.04.12
 */
public class ExceptionHelper {

    public static void handleException(Throwable ex){
        if(ex instanceof NotLoggedInException){
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(Params.PAGE, Params.PAGE_LOGIN);
            map.put(Params.ITEM, "none");

            History.newItem(Params.encodeParams(map));
        }else {
            new Throwable(ex);
            //Window.alert(ex.getMessage());
        }
    }
}
