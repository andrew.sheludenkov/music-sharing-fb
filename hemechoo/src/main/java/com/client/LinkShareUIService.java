package com.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import com.shop.web.dto.CategoryDto;
import com.shop.web.dto.ShopDto;
import com.shop.web.dto.UserDto;
import com.shop.web.dto.UserResultDto;
import com.shop.web.dto.linkshare.LinkShareLogStatisticDto;
import com.shop.web.dto.linkshare.ProductGroupDto;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 19.02.13
 */
@RemoteServiceRelativePath("asynch")
public interface LinkShareUIService extends RemoteService {

    public void joinToProductGroup(Long productId);

    public UserDto getLoggedInUser();

    public UserResultDto getUserFriends();

    List<ProductGroupDto> getUserGroups();

    LinkShareLogStatisticDto getAffiliateLinkActivities();

    LinkShareLogStatisticDto getWidgetActivities();

    List<CategoryDto> getTopCategoriesList(Integer from, Integer size);

    List<CategoryDto> getChildCategories(Long parentId);

    CategoryDto getCategoryById(Long categoryId);

    List<ShopDto> getShopList();

    ShopDto getShop(long appMarketId);
}
