package com.client;


import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.shop.web.dto.HemeChooCommentDto;
import com.shop.web.dto.UserDto;

import java.util.List;

@RemoteServiceRelativePath("hemechoo")
public interface MainHemechooUIService extends RemoteService {


    public Boolean createMessage(String name, String email, String subject, String text);

    public Boolean addHemechooShop(String name, String email, String url, String phone);

    public List<HemeChooCommentDto> getComments(Integer offset, Integer limit, String storeStringId);
    public List<HemeChooCommentDto> getImages(Integer offset, Integer limit, String storeStringId);

    public UserDto findUserByReferralCode(String referralCode);

    public UserDto deactivateCoupon(String referralCode) ;

    public void updateCommentContent(Long id, String content, String pros, String cons);
    public void censorCommentContent(Long id, String content, String pros, String cons);

    public void publishComment(Long id, Boolean isPublished);

    public String getCssText(String storeStringId);

    public void updateCssText(String storeStringId, String text);


//    UserDto getLoggedInUser();
}
