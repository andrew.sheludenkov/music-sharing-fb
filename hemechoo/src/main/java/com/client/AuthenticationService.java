package com.client;

import com.client.exception.NotLoggedInException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.shop.web.dto.UserDto;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 24.04.12
 */
@RemoteServiceRelativePath("login")
public interface AuthenticationService extends RemoteService {

    UserDto loginUser(String username, String password) throws NotLoggedInException;

    UserDto loginUserFast(String fastHash) throws NotLoggedInException;

    void logoutUser();

    void saveUser(UserDto userDto) throws NotLoggedInException;

    UserDto getLoggedInUser() throws NotLoggedInException;
}
