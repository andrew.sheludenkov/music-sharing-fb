package com.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.shop.web.dto.UserDto;

public interface AuthenticationServiceAsync {
    void loginUser(String username, String password, AsyncCallback<UserDto> async);

    void loginUserFast(String fastHash, AsyncCallback<UserDto> async);

    void logoutUser(AsyncCallback<Void> async);

    void saveUser(UserDto userDto, AsyncCallback<Void> async);

    void getLoggedInUser(AsyncCallback<UserDto> async);
}
