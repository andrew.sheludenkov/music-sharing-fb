package com.client.service;

public abstract class StandaloneCallback<T> {

	public abstract void onSuccess(T result);

    public void onError(Throwable result){}

    public void onFailure(){}

}