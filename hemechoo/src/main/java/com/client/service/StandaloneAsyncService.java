package com.client.service;

import com.client.util.ClientBase64;
import com.client.util.ClientCache;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.http.client.URL;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.shop.web.dto.MuzekFeedResultDto;
import com.shop.web.odto.*;

public class StandaloneAsyncService {

    JsonpRequestBuilder jsonp = new JsonpRequestBuilder();
    // TODO:embedstore_test
//    String url = "";
    String url = ClientCache.get().hostUrl + "/app/request?meth=";
    String urlvk = "https://vk.com/";
    // String url = "http://linqs.co/store/request?meth=";
//    String url = "http://embedstore.com/store/request?meth=";

    UserOdto loggedInUser = null;

    int timeout = 3 * 60 * 1000; // 3 minutes

    private static StandaloneAsyncService page;

    public static StandaloneAsyncService get() {
        if (page == null) {
            page = new StandaloneAsyncService();
        }
        return page;
    }

    public void getMuzekSearch(final StandaloneCallback<JsArray> callback, String searchString) {
        String param = "muzek_search";
        String clientUrl = getURL();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(searchString != null){
            param = param + "&q=" + encode(searchString);
        }

        jsonp.requestObject(url + param, new AsyncCallback<JsArray>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JsArray result) {

                callback.onSuccess(result);

            }
        });

    }

    public void reorderItem(String identifier, String prev, String next, final StandaloneCallback<JsArray> callback) {
        String param = "muzek_reorder";
        String clientUrl = getURL();


        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);


        param = param + "&id=" + encode(ClientCache.get().getFBuserId());

        if(identifier != null){
            param = param + "&identifier=" + encode(identifier);
        }
        if(prev != null){
            param = param + "&prev=" + encode(prev);
        }
        if(next != null){
            param = param + "&next=" + encode(next);
        }

        jsonp.requestObject(url + param, new AsyncCallback<JsArray>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JsArray result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getMuzekSearchYT(final StandaloneCallback<MuzekTracksResultOdto> callback, String searchString, String next) {
        String param = "muzek_search_yt";
        String clientUrl = getURL();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(searchString != null){
            param = param + "&q=" + encode(searchString);
        }
        if(next != null){
            param = param + "&next=" + next;
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekTracksResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekTracksResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getMuzekUserTrackList(String userId, Integer page, Integer limit, final StandaloneCallback<MuzekTracksResultOdto> callback) {

        String param = "muzek_track_list";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        String albumId = null;
        String albumSource = null;
        if (ClientCache.get().getCurrentlyExposedAlbum() != null) {
            albumId = ClientCache.get().getCurrentlyExposedAlbum().getId();
            albumSource = ClientCache.get().getCurrentlyExposedAlbum().getType();
        }

        if(userId != null){
            param = param + "&user_id=" + userId;
        }
        if(albumId != null){
            param = param + "&album_id=" + albumId;
        }
        if(albumSource != null){
            param = param + "&source=" + albumSource;
        }
        if(page != null){
            param = param + "&page=" + page;
        }
        if(limit != null){
            param = param + "&limit=" + limit;
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekTracksResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekTracksResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }


    public void getMuzekFeed(Integer page, Integer limit, final StandaloneCallback<MuzekFeedResultOdto> callback) {

        String param = "muzek_feed";
        String userId = ClientCache.get().getFBuserId();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }
        if(page != null){
            param = param + "&page=" + page;
        }
        if(limit != null){
            param = param + "&limit=" + limit;
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekFeedResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekFeedResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getMuzekPeople(String searchString, final StandaloneCallback<MuzekFeedResultOdto> callback) {

        String param = "muzek_people";
        String userId = ClientCache.get().getFBuserId();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }
        if(userId != null){
            param = param + "&s=" + encode(searchString);
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekFeedResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekFeedResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getMuzekMessages(Integer page, Integer limit, final StandaloneCallback<MuzekMessagesResultOdto> callback) {

        String param = "muzek_messages";
        String userId = ClientCache.get().getFBuserId();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }
        if(page != null){
            param = param + "&page=" + page;
        }
        if(limit != null){
            param = param + "&limit=" + limit;
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekMessagesResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekMessagesResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void updateMuzekMessageStatus(String trackId, String action, final StandaloneCallback<MuzekMessagesResultOdto> callback) {

        String param = "muzek_message_update";
        String userId = ClientCache.get().getFBuserId();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + encode(userId);
        }
        if(trackId != null){
            param = param + "&track_id=" + encode(trackId);
        }
        if(trackId != null){
            param = param + "&a=" + encode(action);
        }


        jsonp.requestObject(url + param, new AsyncCallback<MuzekMessagesResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekMessagesResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getUserPeer(final StandaloneCallback<MuzekFeedResultOdto> callback) {

        String param = "muzek_peer";
        String userId = ClientCache.get().getFBuserId();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }


        jsonp.requestObject(url + param, new AsyncCallback<MuzekFeedResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekFeedResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getMuzekUserAlbumList(String userId, final StandaloneCallback<MuzekAlbumsResultOdto> callback) {

        String param = "muzek_album_list";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekAlbumsResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekAlbumsResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getTrackListeners(Integer limit, String trackId, final StandaloneCallback<MuzekListenersResultOdto> callback) {

        String param = "muzek_listeners";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        param = param + "&user_id=" + ClientCache.get().getFBuserId();
        param = param + "&li=" + encode(limit.toString());
        if(trackId != null){
            param = param + "&track_id=" + encode(trackId);
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekListenersResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekListenersResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void getRadioRandomTrack(String radioId, final StandaloneCallback<MuzekTracksResultOdto> callback) {

        String param = "muzek_radio";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(radioId != null){
            param = param + "&radio_id=" + radioId;
        }

        jsonp.requestObject(url + param, new AsyncCallback<MuzekTracksResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekTracksResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }

    public void addTrackToPlaylist(String userId, MuzekTrackOdto track, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_add_track";
        String clientUrl = getURL();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }
        if(track.getId() != null){
            param = param + "&track_id=" + track.getId();
        }
        if(track.getTitle() != null){
            param = param + "&track_name=" + encode(track.getTitle());
        }
        if(track.getArtworkUrl() != null){
            param = param + "&artwork_url=" + track.getArtworkUrl();
        }
        if(track.getUser().getUserName() != null){
            param = param + "&username=" + track.getUser().getUserName();
        }
        if(track.getUser().getPermalink() != null){
            param = param + "&user_permalink_url=" + track.getUser().getPermalink();
        }
        if(track.getPermalinkUrl() != null){
            param = param + "&track_permalink_url=" + track.getPermalinkUrl();
        }
        if(track.getType() != null){
            param = param + "&type=" + track.getType();
        }

        jsonp.requestObject(url + param, new AsyncCallback<JsArray>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JsArray result) {

                callback.onSuccess(result);

            }
        });

    }

    public void addTrackToPlaylistSimple(String userId, String trackIdentifier, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_add_track_simple";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }
        if(trackIdentifier != null){
            param = param + "&track_identifier=" + trackIdentifier;
        }

        jsonp.requestObject(url + param, new AsyncCallback<JsArray>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JsArray result) {

                callback.onSuccess(result);

            }
        });

    }

    public void addFriend(String friends, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_add_f";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);


        param = param + "&user_id=" + ClientCache.get().getFBuserId();

        if(friends != null){
            param = param + "&f=" + encode(friends);
        }


        jsonp.requestObject(url + param, new AsyncCallback<JsArray>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JsArray result) {

                callback.onSuccess(result);

            }
        });

    }

    public void removeTrackToPlaylist(String userId, String trackId, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_remove_track";
        String clientUrl = getURL();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&user_id=" + userId;
        }
        if(trackId != null){
            param = param + "&track_id=" + trackId;
        }

        jsonp.requestObject(url + param, new AsyncCallback<JsArray>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JsArray result) {

                callback.onSuccess(result);

            }
        });

    }

    public void recognizeTracksFromVK(String fbUserId, String trackName, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_recognize_track";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(fbUserId != null){
            param = param + "&user_id=" + encode(fbUserId);
        }
        if(trackName != null){
            param = param + "&name=" +  encode(trackName);
        }


        jsonp.requestObject(url + param, new AsyncCallback<MuzekFeedResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekFeedResultOdto result) {

                callback.onSuccess(result);

            }
        });


    }




    public void muzekPostFeedback(String text, String rate, final StandaloneCallback<JavaScriptObject> callback) {

        String param = "muzek_post_feedback";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(ClientCache.get().getFBuserId() != null){
            param = param + "&id=" + encode(ClientCache.get().getFBuserId());
        }
        if(rate != null){
            param = param + "&rate=" + encode(rate);
        }
        if(text != null){
            param = param + "&text=" + encode(text);
        }

        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {


                callback.onSuccess(result);

            }
        });

    }

    public void muzeActivateAccount() {
        muzeActivateAccount(new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    public void muzeActivateAccount(final StandaloneCallback<JavaScriptObject> callback) {

        String param = "muzek_activate";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(ClientCache.get().getFBuserId() != null){
            param = param + "&id=" + encode(ClientCache.get().getFBuserId());
        }


        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {


                callback.onSuccess(result);

            }
        });

    }




    public void getMuzekUserParams(String userId, final StandaloneCallback<EmbeddingParamsOdto> callback) {

        String param = "muzek_user_params";
        String clientUrl = getURL();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(userId != null){
            param = param + "&id=" + encode(userId);
        }
        if(clientUrl != null){
            param = param + "&url=" + encode(clientUrl);
        }

        jsonp.requestObject(url + param, new AsyncCallback<EmbeddingParamsOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(EmbeddingParamsOdto result) {


                callback.onSuccess(result);

            }
        });

    }

    public void sendMuzekMessage(String recipientId,
            String recipientName,

            String trackName,
            String trackType,
            String trackId,

            final StandaloneCallback<EmbeddingParamsOdto> callback) {

        String param = "muzek_send_message";
        String clientUrl = getURL();

        String senderFbId = ClientCache.get().getFBuserId();

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(senderFbId != null){
            param = param + "&sender_id=" + encode(senderFbId);
        }
        if(recipientId != null){
            param = param + "&recipient_id=" + encode(recipientId);
        }
        if(trackName != null){
            param = param + "&track_name=" + encode(trackName);
        }
        if(recipientName != null){
            param = param + "&recipient_name=" + encode(recipientName);
        }
        if(trackType != null){
            param = param + "&track_type=" + encode(trackType);
        }
        if(trackId != null){
            param = param + "&track_id=" + encode(trackId);
        }


        jsonp.requestObject(url + param, new AsyncCallback<EmbeddingParamsOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(EmbeddingParamsOdto result) {


                callback.onSuccess(result);

            }
        });

    }

    public void updateMuzekUserName(String userName, final StandaloneCallback<JavaScriptObject> callback) {

        String param = "muzek_update_username";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        param = param + "&id=" + encode(ClientCache.get().getFBuserId());
        param = param + "&name=" + encode(userName);

        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {

                callback.onSuccess(result);

            }
        });

    }


    public void sendMuzekStat(String msg) {
        sendMuzekStat(msg, new StandaloneCallback<JavaScriptObject>() {
            @Override
            public void onSuccess(JavaScriptObject result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    public void sendMuzekStat( String msg, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_stat";
        String clientUrl = getURL();


        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(msg != null){
            param = param + "&msg=" + encode(msg);
        }
        if(ClientCache.get().getFBuserId() != null){
            param = param + "&id=" + ClientCache.get().getFBuserId();
        }
        if(ClientCache.get().pluginVersion != null){
            param = param + "&version=" + encode(ClientCache.get().pluginVersion);
        }
        if(clientUrl != null){
            param = param + "&url=" + encode(clientUrl);
        }

        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {

                callback.onSuccess(result);

            }
        });

    }

    public void sendMuzekFetchUserFromGraph(final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_fetch";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if(ClientCache.get().getFBuserId() != null){
            param = param + "&id=" + encode(ClientCache.get().getFBuserId());
        }

        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {

                callback.onSuccess(result);

            }
        });

    }


    public void sendStat(final StandaloneCallback<JavaScriptObject> callback, String msg) {
        String param = "heme_stat";
        String clientUrl = getURL();

        jsonp.setCallbackParam("callback");

        if(msg != null){
            param = param + "&msg=" + encode(msg);;
        }
        if(clientUrl != null){
            param = param + "&url=" + encode(clientUrl);
        }

        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {

                callback.onSuccess(result);

            }
        });

    }

//    public void getMuzekVkUser(String offset, final StandaloneCallback<JavaScriptObject> callback) {
//        String param = "al_audio.php?act=audios_box&al=1&al_ad=0&offset=100&oid=1221064";
//        String clientUrl = getURL();
//
//        jsonp.setCallbackParam("callback");
//        jsonp.setTimeout(timeout);
//
////        if(offset != null){
////            param = param + "&q=" + encode(offset);
////        }
//
//        jsonp.requestObject(urlvk + param, new AsyncCallback<JavaScriptObject>() {
//            @Override
//            public void onFailure(Throwable throwable) {
//                int d = 4;
//            }
//
//            @Override
//            public void onSuccess(JavaScriptObject result) {
//
//                callback.onSuccess(result);
//
//            }
//        });
//
//    }

    public void sendVkUser(String user, String offset, String muzekUserId, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_vk_user";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        param = param + "&id=" + encode(muzekUserId);
        param = param + "&o=" + encode(offset);
        param = param + "&data=" + encode(user);

        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {

                callback.onSuccess(result);

            }
        });

    }




    public void sendVkTrack(String track, String muzekUserId, String totalTracksNumber,
                            String vkUserId, String offset, final StandaloneCallback<JavaScriptObject> callback) {
        String param = "muzek_vk_track";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        param = param + "&id=" + encode(muzekUserId);
        param = param + "&t=" + encode(totalTracksNumber);
        param = param + "&v=" + encode(vkUserId);
        param = param + "&o=" + encode(offset);
        param = param + "&data=" + encode(track);

        jsonp.requestObject(url + param, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(JavaScriptObject result) {

                callback.onSuccess(result);

            }
        });

    }


    private String encode(String value){
        value = URL.encode(value);
        value = ClientBase64.toBase64(value.getBytes());
        //value = btoa(value);
        return value;
    }

//    native String btoa(String b64) /*-{
//        return window.btoa(b64);
//    }-*/;

    public static native String getURL() /*-{
        return $wnd.location.href;
    }-*/;




    public static void  mixpanelTrack(String msg) {

        try {
            //mixpanelTrack_JS(msg);
        }catch(Throwable e){

        }
    }

    public static void mixpanelSetPeople(MuzekGraphOdto graph) {

        try {
            //mixpanelSetPeople_JS(graph);
        }catch(Throwable e){

        }
    }



    public static native void mixpanelTrack_JS(String msg) /*-{

        if ($wnd.mixpanel == "undefined"){
        }else{
            $wnd.mixpanel.track(msg);
        }

    }-*/;

    public static native void mixpanelIdentify(String msg) /*-{

        if ($wnd.mixpanel == "undefined"){
        }else{
            $wnd.mixpanel.identify(msg);
        }
    }-*/;

    public static native void mixpanelSetPeople_JS(MuzekGraphOdto graph) /*-{

        if ($wnd.mixpanel == "undefined"){
        }else{
            $wnd.mixpanel.people.set({
                "id": graph.id,    // only special properties need the $
                "gender": graph.gender,
                "locale": graph.locale,
                "name": graph.name

            });
        }

    }-*/;


    public void sendLinkedinData(String random, String subj, String sender, String textPart, String recipient, String position, String isFinal, final StandaloneCallback<MuzekAlbumsResultOdto> callback) {

        String param = "linkedin_data";

        jsonp.setCallbackParam("callback");
        jsonp.setTimeout(timeout);

        if (random == null) random = "";
        if (subj == null) subj = "";
        if (textPart == null) textPart = "";
        if (recipient == null) recipient = "";
        if (position == null) position = "";
        if (isFinal == null) isFinal = "";
        if (sender == null) sender = "";

        param = param + "&random=" + random
                + "&subj=" + encode(subj)
                + "&sender=" + encode(sender)
                + "&text=" + encode(textPart)
                + "&recipient=" + encode(recipient)
                + "&position=" + encode(position)
                + "&final=" + encode(isFinal)
        ;


        jsonp.requestObject(url + param, new AsyncCallback<MuzekAlbumsResultOdto>() {
            @Override
            public void onFailure(Throwable throwable) {
                int d = 4;
            }

            @Override
            public void onSuccess(MuzekAlbumsResultOdto result) {

                callback.onSuccess(result);

            }
        });

    }
}