package com.client.linkshare;

import com.shop.web.dto.UserDto;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 06.02.13
 */
public class LinkShareLoginHolder {
    private UserDto userDto;

    private static LinkShareLoginHolder loginHolder;

    public static LinkShareLoginHolder getInstance(){
        if(loginHolder == null){
            loginHolder = new LinkShareLoginHolder();
        }

        return loginHolder;
    }

    public static int getLoggedInUserId(){
        return loginHolder != null && loginHolder.getUserDto() != null ? loginHolder.getUserDto().getId().intValue() : 0;
    }

    public static boolean isLoggedIn(){
        return loginHolder != null && loginHolder.getUserDto() != null;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }
}
