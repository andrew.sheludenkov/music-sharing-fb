package com.client.linkshare;

import com.client.facebook.player.FBWidgetPlayer;
import com.client.facebook.player.FBWidgetPlayerFriend;
import com.client.facebook.player.FBWidgetSharer;
import com.client.importer.VKImporter;
import com.client.importer.VKPeopleImporter;
import com.client.importer.VKTracksImporter;
import com.client.service.StandaloneAsyncService;
import com.client.util.ClientCache;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;



public class HemechooInitializer {
    private static HemechooInitializer linkShareInitializer;

    boolean hasStandalonePage = false;


    public static HemechooInitializer getInstance() {
        if (linkShareInitializer == null) {
            linkShareInitializer = new HemechooInitializer();
        }

        return linkShareInitializer;
    }

    Timer playerRefresherTimer = new Timer() {
        @Override
        public void run() {
            Element playerElement = DOM.getElementById("widget:player");
            if (playerElement == null){
                insertPlayerWidget();
            }
        }
    };




    public boolean initialize() {


        String url = Window.Location.getHostName();
        final String urlParams = Window.Location.getQueryString();
        String path = Window.Location.getPath();


        if (url.contains("facebook") || path.contains("Facebook")) {


            if (path.contains("sharer/sharer.php")) {
                String userId = getUserIdFromPage("input", "xhpc_targetid");
                ClientCache.get().setFBuserId(userId);

                insertSharerWidget();

            } else {

                String userId = getUserIdFromPage("pagelet_bluebar", "img", "profile_pic_header_");

                ClientCache.get().setFBuserId(userId);

                try {
                    if (urlParams != null && urlParams.length() > 0) {
                        String[] splittedParams = urlParams.replaceAll("\\?", "").split("&");
                        for (String splittedParam : splittedParams) {
                            String[] param = splittedParam.split("=");
                            if (param[0].equals("radio_user_id")) {
                                //Window.alert(param[1]);
                                ClientCache.get().setFBuserId(param[1]);
                                break;
                            }
                        }

                    }
                } catch (Throwable e) {

                }

                insertPlayerWidget();


                addYoutubeScript();

                playerRefresherTimer.scheduleRepeating(1000);
            }

        }

        String queryString = Window.Location.getQueryString();
        if (url.contains("vk.com")){

            if (path.equals("/al_search.php") && queryString.contains("expeople")){

                String muzekUserFbId = queryString.split("fb")[1].split("=")[1];
                String offset = queryString.split("offset")[1].split("=")[1].split("\\&")[0];
                processVKPeople(muzekUserFbId, offset);
            }

            if (path.equals("/al_audio.php") && queryString.contains("extrack")){

                String muzekUserFbId = queryString.split("fb")[1].split("=")[1];
                String vkUserId = queryString.split("oid")[1].split("=")[1].split("\\&")[0];
                String offset = queryString.split("offset")[1].split("=")[1].split("\\&")[0];
                processVKTracks(muzekUserFbId, vkUserId, offset);
            }


            if (path.equals("/al_audio.php") && queryString.contains("export")){

                processVK();
            }
        }


        return hasStandalonePage;
    }


    private void insertPlayerWidget() {



        String path = Window.Location.getPath();
        String queryString = Window.Location.getQueryString();

//        String url = Window.Location.getHostName();
        if (path.equals("") || path.equals("/") || path.equals("/home.php")
//                || path.equals("/?ref=logo")
//                || path.equals("/?sk=nf")
                || path.contains("Facebook.html")
                || path.contains("Facebook1.htm")
                || path.contains("Facebook2.htm")) {

            Element div = DOM.getElementById("rightCol");
            Element divContainer = DOM.getElementById("widget:player");

            if (div != null && divContainer == null) {

                FBWidgetPlayer player = new FBWidgetPlayer();
                player.getElement().setAttribute("id", "widget:player");

                try {

                    RootPanel.get("rightCol").insert(player, 0);

                    player.setVisible(true);


                    player.addAlltheShit();
                    player.flush();


                } catch (Throwable e) {
                    StandaloneAsyncService.get().sendMuzekStat("throwable error");
                }

            }


        }else if (queryString.contains("mz=friend")
//                || path.equals("/?ref=logo")
//                || path.equals("/?sk=nf")
                || path.contains("FacebookUserPage")) {

            String friendId = Window.Location.getParameter("mz_f");
            friendId = ClientCache.get().decodeUserId(friendId);
            ClientCache.get().setFBFriend(friendId);

            String referrerId = Window.Location.getParameter("mz_ref");
            referrerId = ClientCache.get().decodeUserId(referrerId);
            ClientCache.get().setFBuserId(referrerId);


            Element element = getLeftColumn();
            Element trackListContainer = getTrackListContainer();
            element.setAttribute("id", "widget:player");


            FBWidgetPlayerFriend player = new FBWidgetPlayerFriend();
            player.setTrackListContainer(trackListContainer);


            try {
                RootPanel.get("widget:player").insert(player, 0);

                player.setVisible(true);


                player.addAlltheShit();
                player.flush();


            } catch (Throwable e) {
                StandaloneAsyncService.get().sendMuzekStat("throwable error");
            }


        } else{
            Element div = DOM.getElementById("contentCol");
            if (div != null) {
                FlowPanel stub = new FlowPanel();
                stub.getElement().setAttribute("id", "widget:player");

                div.appendChild(stub.getElement());
            }
        }

    }

    private void processVK(){

//        Window.alert("processing VK");
//        Window.scrollTo(0, Window.getScrollTop() + Window.getClientHeight());

        VKImporter vkImporter = new VKImporter();
        RootPanel.get().add(vkImporter);
        vkImporter.initialize();


    }

    private void processVKPeople(String muzekUserFbId, String offset){

        VKPeopleImporter vkImporter = new VKPeopleImporter();
        RootPanel.get().add(vkImporter);
        vkImporter.fetchUsers(muzekUserFbId, offset);
    }


    private void processVKTracks(String muzekUserFbId, String vkUserId, String offset){

        VKTracksImporter vkImporter = new VKTracksImporter();
        RootPanel.get().add(vkImporter);
        vkImporter.fetchTracks(muzekUserFbId, vkUserId, offset);
    }


    private void insertSharerWidget() {


        Element div = DOM.getElementById("pageTitle");
        Element divContainer = DOM.getElementById("widget:player");

        if (div != null && divContainer == null) {

            FBWidgetSharer sharer = new FBWidgetSharer();
            sharer.getElement().setAttribute("id", "widget:player");

            try {
                RootPanel.get("pageTitle").insert(sharer, 0);

                sharer.setVisible(false);


                sharer.addAlltheShit();

                //fixWidget();

                // StandaloneAsyncService.get().sendMuzekStat("embedded 1");
                //player.initialize();
            } catch (Throwable e) {
                StandaloneAsyncService.get().sendMuzekStat("throwable error");
            }

            //   StandaloneAsyncService.get().sendMuzekStat("embedded 2");

        }


    }


    /**
     * Load CSS file from url
     */
    public static void loadCss(String url) {
        LinkElement link = Document.get().createLinkElement();
        link.setRel("stylesheet");
        link.setType("text/css");
        link.setHref(url);
        nativeAttachToHead(link);
        //DOM.getElementById("app:comments").appendChild(link);
    }

    /**
     * Attach element to head
     */
    protected static native void nativeAttachToHead(JavaScriptObject scriptElement) /*-{
        $doc.getElementsByTagName("head")[0].appendChild(scriptElement);
    }-*/;

    String getUserIdFromPage(String containerId, String selectorTag, String prefix) {

        try {
            NodeList<Element> nodes1 = DOM.getElementById(containerId).getElementsByTagName(selectorTag);
            for (int i = 0; i < nodes1.getLength(); i++) {
                Element node1 = nodes1.getItem(i);
                if (node1.getId().startsWith(prefix)) {
                    String nodeId = node1.getId();
                    nodeId = nodeId.replaceAll(prefix, "");
                    return nodeId;
                }
            }
        } catch (Throwable e) {
            return null;
        }
        return null;
    }

    String getUserIdFromPage(String tagName, String name) {

        try {
            NodeList<Element> nodes1 = RootPanel.get().getElement().getElementsByTagName(tagName);
            for (int i = 0; i < nodes1.getLength(); i++) {
                Element node1 = nodes1.getItem(i);
                if (node1.getAttribute("name").equals(name)) {
                    return node1.getAttribute("value");
                }
            }
        } catch (Throwable e) {
            return null;
        }
        return null;
    }



    String getUserNameFromLinkedinPage(String containerId, String selectorTag) {

        try {
            NodeList<Element> nodes1 = DOM.getElementById(containerId).getElementsByTagName(selectorTag);
            for (int i = 0; i < nodes1.getLength(); i++) {
                Element node1 = nodes1.getItem(i);
                if (node1.getAttribute("class").equals("profile-photo")) {
                    String name = node1.getAttribute("alt");
                    //Window.alert(name);
                    return name;
                }
            }
        } catch (Throwable e) {
            return null;
        }
        return null;
    }

    Element getLeftColumn() {

        try {
            Element element = DOM.getElementById("timeline_tab_content");
            Node node = element.getChild(0).getChild(0).getChild(0).getChild(1);

            return node.cast();

        } catch (Throwable e) {
            return null;
        }

    }

    Element getTrackListContainer() {

        try {
            Element element = DOM.getElementById("timeline_tab_content");
            Node node = element.getChild(0).getChild(0).getChild(0).getChild(0);

            return node.cast();

        } catch (Throwable e) {
            return null;
        }

    }


    protected static native void addYoutubeScript() /*-{
        var tag = $doc.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = $doc.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    }-*/;


}
