package com.client;

import com.client.linkshare.HemechooInitializer;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class MainHemechooUI implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {


        HemechooInitializer.getInstance().initialize();


    }


}
