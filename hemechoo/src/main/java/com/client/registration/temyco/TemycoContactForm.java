package com.client.registration.temyco;

import com.client.MainIntervioUIService;
import com.client.MainIntervioUIServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;


public class TemycoContactForm extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, TemycoContactForm> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    private final MainIntervioUIServiceAsync serviceAsync = GWT
            .create(MainIntervioUIService.class);


    @UiField
    FormPanel formPanel;
    @UiField
    SubmitButton button;
//    @UiField
//    TextBox name;

    @UiField
    TextBox email;
    @UiField
    FlowPanel successLabel;

//    @UiField
//    TextBox subject;
    @UiField
    TextArea thetext;
//    @UiField
//    TextBox name;
//    @UiField
//    PasswordTextBox password;

    public TemycoContactForm() {

        initWidget(ourUiBinder.createAndBindUi(this));


//        if (ClientCache.get().userDto != null){
////            if (ClientCache.get().userDto.getFullName() != null){
////                name.setText(ClientCache.get().userDto.getFullName());
////            }
////            if (ClientCache.get().userDto.getPhone() != null){
////                phone.setText(ClientCache.get().userDto.getPhone());
////            }
//              if (ClientCache.get().userDto.getEmail() != null){
//                email.setText(ClientCache.get().userDto.getFullName());
//              }
//        }

    }





    public void initialize() {


    }

    @UiHandler("button")
    void onButton(ClickEvent clickEvent) {



        serviceAsync.createMessage("name",
                email.getText(),
                "quote request",
                thetext.getText(),
                new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable throwable) {
                int s = 4;
            }

            @Override
            public void onSuccess(Boolean userDto) {
                formPanel.setVisible(false);
                successLabel.setVisible(true);
            }
        });


    }


}