package com.client.registration.hemechoo.admin.promo;

import com.client.AuthenticationService;
import com.client.AuthenticationServiceAsync;
import com.client.MainHemechooUIService;
import com.client.MainHemechooUIServiceAsync;
import com.client.registration.hemechoo.admin.comments.components.CommentWidget;
import com.client.registration.hemechoo.admin.events.PagerEventHandler;
import com.client.registration.hemechoo.admin.pager.Pager;
import com.client.registration.hemechoo.admin.promo.user.UserPromoItem;
import com.client.util.ClientGlobalVariables;
import com.client.util.ExceptionHelper;
import com.client.util.Params;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.shop.web.dto.HemeChooCommentDto;
import com.shop.web.dto.UserDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PromoPage extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, PromoPage> {
    }

    private final MainHemechooUIServiceAsync serviceAsync = GWT
            .create(MainHemechooUIService.class);

    private static AuthenticationServiceAsync authenticationService = GWT.create(AuthenticationService.class);

    @UiField
    HTMLPanel thePage;
    @UiField
    FlowPanel container;

    @UiField
    Button buttonCheck;
    @UiField
    TextBox promo;
    @UiField
    Label noFound;


    private static PromoPage page;
    private final Integer PRODUCTS_PER_PAGE = 30;

    public static PromoPage get() {
        if (page == null) {
            page = new PromoPage();
        }
        return page;
    }

    public PromoPage() {

        initWidget(uiBinder.createAndBindUi(this));


    }


    public void initialize() {

        noFound.setVisible(false);

//
//        UserDto userDto = ClientGlobalVariables.getInstance().getLoggedInUser();
//        if (userDto == null) {
//            authenticationService.getLoggedInUser(new AsyncCallback<UserDto>() {
//                @Override
//                public void onFailure(Throwable caught) {
//                    ExceptionHelper.handleException(caught);
//                }
//
//                @Override
//                public void onSuccess(UserDto result) {
//                    ClientGlobalVariables.getInstance().setLoggedInUser(result);
//
//                    find(0, PRODUCTS_PER_PAGE);
//
//                    pager.initialize(10000, PRODUCTS_PER_PAGE);
//                    pager.setEventHandler(new PagerEventHandler() {
//                        @Override
//                        public void onEvent(int offset, int limit, int page) {
//                            find(offset, limit);
//                        }
//                    });
//                }
//            });
//        }else {
//            find(0, PRODUCTS_PER_PAGE);
//
//            pager.initialize(10000, PRODUCTS_PER_PAGE);
//            pager.setEventHandler(new PagerEventHandler() {
//                @Override
//                public void onEvent(int offset, int limit, int page) {
//                    find(offset, limit);
//                }
//            });
//        }


    }

    private void find(int offset, int limit) {

        UserDto user = ClientGlobalVariables.getInstance().getLoggedInUser();
        String storeStringId = user.getCarColor();


        serviceAsync.getComments(offset, limit, storeStringId, new AsyncCallback<List<HemeChooCommentDto>>() {
            @Override
            public void onFailure(Throwable caught) {
                int a = 3;
            }

            @Override
            public void onSuccess(List<HemeChooCommentDto> result) {

                Window.scrollTo(0, 0);

                if (result != null) {
                    container.clear();

                    for (HemeChooCommentDto item : result) {
                        CommentWidget commentWidget = new CommentWidget();
                        commentWidget.initialize(item);

                        container.add(commentWidget);
                    }
                }

            }
        });
    }

    @UiHandler("buttonCheck")
    void onCheck(ClickEvent clickEvent) {

        noFound.setVisible(false);
        container.clear();

        String referralCode = promo.getText();

        serviceAsync.findUserByReferralCode(referralCode, new AsyncCallback<UserDto>() {
            @Override
            public void onFailure(Throwable caught) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onSuccess(UserDto user) {
                if (user == null) {
                    noFound.setVisible(true);
                } else {
                    UserPromoItem userPromoItem = new UserPromoItem();
                    userPromoItem.initialize(user);
                    container.add(userPromoItem);
                }
            }
        });
    }

}