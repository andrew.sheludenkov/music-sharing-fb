package com.client.registration.hemechoo.admin.moder.components;

import com.client.MainHemechooUIService;
import com.client.MainHemechooUIServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.shop.web.dto.DiscountDto;
import com.shop.web.dto.HemeChooCommentDto;

public class ModerImageWidget extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    private final MainHemechooUIServiceAsync serviceAsync = GWT
            .create(MainHemechooUIService.class);



    @UiField
    HTMLPanel theComponent;



    @UiField
    Label status;

    @UiField
    Button hide;
    @UiField
    Image image;
    @UiField
    Label productName;

    private DiscountDto discountDto;

    HemeChooCommentDto comment;

    interface ProgramPageUiBinder extends UiBinder<Widget, ModerImageWidget> {
    }


    public ModerImageWidget() {
        initWidget(uiBinder.createAndBindUi(this));
    }


    public void initialize(HemeChooCommentDto comment) {

        this.comment = comment;
       image.setUrl(comment.getSmall());
        productName.setText(comment.getProductName());

    }



    @UiHandler("hide")
    void onUpdate(ClickEvent clickEvent) {

        serviceAsync.censorCommentContent(comment.getId(), null, null, null, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onSuccess(Void result) {
                status.setVisible(true);
                hide.setVisible(false);
            }
        });


    }




}
