package com.client.registration.hemechoo;

import com.client.registration.hemechoo.feedback.FeedbackBlock;
import com.client.registration.hemechoo.item.CommentItem;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientBase64;
import com.client.util.ClientUtil;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.http.client.URL;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.HemeChooCommentOdto;
import com.shop.web.odto.HemechooCommentsResultOdto;

import java.util.ArrayList;
import java.util.List;


public class HemechooComments extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, HemechooComments> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    FlowPanel container;
    @UiField
    FeedbackBlock feedback;
    @UiField
    Anchor showComments;
    @UiField
    FlowPanel showCommentsContainer;

    Boolean hide = false;

    List<CommentItem> commentsList = new ArrayList<CommentItem>();

    public HemechooComments() {

        initWidget(ourUiBinder.createAndBindUi(this));

        feedback.initialize(null, container);

    }

    public void setFormWidth(String params) {
        feedback.setFormWidth(params);
    }

    public void initialize() {

//        container.setVisible(false);
//
//        // get comments by product id
//        if (Window.Location.getPath() != null) {
//            String arr[] = Window.Location.getPath().split("/");
//            if (arr.length > 2) {
//
//                if (Window.Location.getHost().contains("citymart") && (arr[1].equals("short") || arr[1].equals("product"))) {
//                    String productId = Window.Location.getPath().split("/")[2];
//
//                    getCommentsById(productId);
//
//                    return;
//                }
//            }
//        }
//
//        // get comments by product name
//        NodeList<Element> list = Document.get().getElementsByTagName("h1");
//
//        Element element = null;
//
//        if ((Window.Location.getQueryString().contains("kosholka.com.ua") || Window.Location.getHost().contains("kosholka.com.ua"))
//                && list.getLength() > 1) {
//            element = list.getItem(1);
//
//        } else if (Window.Location.getQueryString().contains("magazyaka.com.ua") || Window.Location.getHost().contains("magazyaka.com.ua")) {
//            list = Document.get().getElementsByTagName("h2");
//            if (list.getLength() > 1) {
//                element = list.getItem(1);
//            }
//        } else if (list != null && list.getLength() > 0) {
//            element = list.getItem(0);
//        }
//
//        if (element != null) {
//            //String html = element.getInnerHTML();
//            String text = element.getInnerText().trim();
//            text = ClientUtil.get().cleanProductName(text);
//            text = URL.encode(text);
//            //text = btoa(text);
//            text = ClientBase64.toBase64(text.getBytes());
//
//            StandaloneAsyncService.get().getComments(new StandaloneCallback<HemechooCommentsResultOdto>() {
//                @Override
//                public void onSuccess(HemechooCommentsResultOdto result) {
//                    createComments(result);
//                    hideDiv(result);
//                }
//            }, URL.encode(text));
//        }


    }

    private void getCommentsById(String productId) {
//        StandaloneAsyncService.get().getCommentsById(new StandaloneCallback<HemechooCommentsResultOdto>() {
//            @Override
//            public void onSuccess(HemechooCommentsResultOdto result) {
//                createComments(result);
//            }
//        }, productId);
    }

    private void createComments(HemechooCommentsResultOdto result) {
        JsArray<HemeChooCommentOdto> list = result.getCommentsList();

        Boolean hideComment = false;

        for (int i = 0, resultSize = list.length(); i < resultSize; i++) {

            if (hide && i >= 3) {
                hideComment = true;
            }

            HemeChooCommentOdto comment = list.get(i);

            CommentItem commentItem = new CommentItem();
            container.add(commentItem);
            commentsList.add(commentItem);
            commentItem.initialize(comment, hideComment);

            container.setVisible(true);

        }
        showCommentsContainer.setVisible(hideComment);
    }

    private void hideDiv(HemechooCommentsResultOdto result) {
        if (result.getHideDivId() != null) {
            Element element = Document.get().getElementById(result.getHideDivId());
            if (element != null) {
                element.setAttribute("hidden", "true");
                element.setAttribute("style", "display: none;");

                //DOM.setStyleAttribute(element, "hidden", "true");

            }
        }
    }


//    native String btoa(String b64) /*-{
//        return window.btoa(b64);
//    }-*/;

    public void setHide(String hideStr) {
        if (hideStr != null && hideStr.equals("yes")) {
            hide = true;
        }
    }

    @UiHandler("showComments")
    void onShowComments(ClickEvent clickEvent) {

        showCommentsContainer.setVisible(false);
        for (CommentItem item : commentsList) {
            item.showComment(true);
        }

    }





}