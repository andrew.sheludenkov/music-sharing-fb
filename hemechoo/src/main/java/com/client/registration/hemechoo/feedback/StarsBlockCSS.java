package com.client.registration.hemechoo.feedback;


import com.client.MainIntervioUIService;
import com.client.MainIntervioUIServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;


public class StarsBlockCSS extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    private final MainIntervioUIServiceAsync serviceAsync = GWT
            .create(MainIntervioUIService.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, StarsBlockCSS> {
    }



    private String productId;

    private static StarsBlockCSS searchBlock;

    FlowPanel container;
    @UiField
    Image star1;
    @UiField
    Image star2;
    @UiField
    Image star3;
    @UiField
    Image star4;
    @UiField
    Image star5;

    Integer value = 0;

    public static StarsBlockCSS get() {
        if (searchBlock == null) {
            searchBlock = new StarsBlockCSS();
        }
        return searchBlock;
    }


    public StarsBlockCSS() {
        initWidget(uiBinder.createAndBindUi(this));

        star1.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(1);
            }
        }, MouseOverEvent.getType());
        star2.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(2);
            }
        }, MouseOverEvent.getType());
        star3.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(3);
            }
        }, MouseOverEvent.getType());
        star4.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(4);
            }
        }, MouseOverEvent.getType());
        star5.addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                star(5);
            }
        }, MouseOverEvent.getType());
    }


    private void star(int number){
        String img1 = "http://hemechoo.com/img/star1.png";
        String img2 = "http://hemechoo.com/img/star2.png";
        star1.setUrl(number < 1 ? img1 : img2);
        star2.setUrl(number < 2 ? img1 : img2);
        star3.setUrl(number < 3 ? img1 : img2);
        star4.setUrl(number < 4 ? img1 : img2);
        star5.setUrl(number < 5 ? img1 : img2);

        value = number;

    }

    public Integer getValue(){
        return value;
    }

    public void initialize(String productId, FlowPanel container) {

        this.productId = productId;
        this.container = container;

    }

    public Integer getStars(){
        return value;
    }

    public static native String getURL() /*-{
        return $wnd.location.href;
    }-*/;

}