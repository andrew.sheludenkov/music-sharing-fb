package com.client.registration.hemechoo.feedback;


import com.client.MainIntervioUIService;
import com.client.MainIntervioUIServiceAsync;
import com.client.registration.hemechoo.feedback.signin.SignInBlock;
import com.client.registration.hemechoo.item.CommentItemCSS;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;


public class FeedbackBlockCSS extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    private final MainIntervioUIServiceAsync serviceAsync = GWT
            .create(MainIntervioUIService.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, FeedbackBlockCSS> {
    }

    @UiField
    FlowPanel block;
    @UiField
    TextBox username;
    @UiField
    TextBox email;
    @UiField
    TextArea text1;
    @UiField
    Button button;
    @UiField
    HTMLPanel additionalFields;
    @UiField
    Anchor plusMinus;
    @UiField
    TextArea plus;
    @UiField
    TextArea minus;
    @UiField
    StarsBlockCSS stars;
    @UiField
    Label login;
    @UiField
    SignInBlock signinBlock;

    private String productId;

    private static FeedbackBlockCSS searchBlock;

    FlowPanel container;

    public static FeedbackBlockCSS get() {
        if (searchBlock == null) {
            searchBlock = new FeedbackBlockCSS();
        }
        return searchBlock;
    }


    public void setFormWidth(String params){
        block.setWidth(params);
    }

    public FeedbackBlockCSS() {
        initWidget(uiBinder.createAndBindUi(this));

        plusMinus.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (additionalFields.isVisible()){
                    additionalFields.setVisible(false);
                    plusMinus.setText("Указать плюсы и минусы модели");
                }else{
                    additionalFields.setVisible(true);
                    plusMinus.setText("Скрыть дополнительные поля");
                }
            }
        }, ClickEvent.getType());

    }

    public void initialize(String productId, FlowPanel container) {

        this.productId = productId;
        this.container = container;

    }



    @UiHandler("button")
    void onRemove(ClickEvent clickEvent) {

//        String url = getURL();
//        String dataItem = null;
//        String dataUser = null;
//        String starsNumber = stars.getStars().toString();
//
//        if (Window.Location.getQueryString().contains("allbiz.me") || Window.Location.getHost().contains("allbiz.me")){
//            Element div = DOM.getElementById("app:comments");
//            dataItem = div.getAttribute("data-item");
//            dataUser = div.getAttribute("data-user");
//        }
//
//        if (text1.getText().length() > 0) {
//
//            StandaloneAsyncService.get().createComment(username.getText(), email.getText(), text1.getText(), plus.getText(), minus.getText(), productId, url,
//                    starsNumber, dataItem, dataUser,
//                    new StandaloneCallback<JavaScriptObject>() {
//                        @Override
//                        public void onSuccess(JavaScriptObject result) {
//
//                            if (container != null) {
//
//                                CommentItemCSS commentItem = new CommentItemCSS();
//                                container.add(commentItem);
//                                commentItem.initialize(username.getText(), text1.getText(), plus.getText(), minus.getText());
//
//                                username.setText("");
//                                email.setText("");
//                                text1.setText("");
//
//                                plus.setText("");
//                                minus.setText("");
//
//                                container.setVisible(true);
//                            }
//
//                        }
//                    });
//        }

    }

//    private String getProductId() {
//        String url = getURL();
//        String productId = url.split("/")[4];
//        return productId;
//    }


    @UiHandler("login")
    void onLogin(ClickEvent clickEvent) {
        signinBlock.setVisible(true);
    }


    public static native String getURL() /*-{
        return $wnd.location.href;
    }-*/;

}