package com.client.registration.hemechoo.images;

import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.odto.HemeChooCommentOdto;


public class ImageItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, ImageItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel mainContainer;

    @UiField
    Image image;

    private HemeChooCommentOdto comment;
    ImagesContainer imagesContainer;

    public ImageItem() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }

    public void initialize(final HemeChooCommentOdto comment, final ImagesContainer imagesContainer) {

        image.setUrl(comment.getSmall());
        this.imagesContainer = imagesContainer;

        DOM.setStyleAttribute(image.getElement(), "margin", "0 0 0 0");

        image.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                imagesContainer.showModalWindow(comment);

                StandaloneAsyncService.get().sendStat(new StandaloneCallback<JavaScriptObject>() {
                    @Override
                    public void onSuccess(JavaScriptObject result) {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }
                }, "image clicked");
            }
        }, ClickEvent.getType());
    }



}