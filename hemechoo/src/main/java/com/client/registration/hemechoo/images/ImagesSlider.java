package com.client.registration.hemechoo.images;

import com.client.registration.hemechoo.item.CommentItemCSS;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.HemeChooCommentOdto;

import java.util.ArrayList;
import java.util.List;


public class ImagesSlider extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, ImagesSlider> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    private Boolean initialized = false;

    private HemeChooCommentOdto comment;


    List<Image> images = new ArrayList<Image>();
    @UiField
    HTMLPanel mainContainer;
    @UiField
    FlowPanel imagesContainer;
    @UiField
    Anchor left;
    @UiField
    Anchor right;
    @UiField
    Button hideButton;

    Integer index = 0;

    String loginCookie;
    Boolean showHideButton = false;


    List<HemeChooCommentOdto> comments = new ArrayList<HemeChooCommentOdto>();



    public ImagesSlider() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }

    public void initialize(List<HemeChooCommentOdto> comments, HemeChooCommentOdto comment) {

        this.comment = comment;
        if (!initialized) {
            this.comments = comments;

            loginCookie = Cookies.getCookie("loginHash");
            if (loginCookie != null && loginCookie.length() > 0) {
                showHideButton = true;
                hideButton.setVisible(true);
            }

            for (HemeChooCommentOdto hemeChooCommentOdto : comments) {

                Image image = new Image(hemeChooCommentOdto.getBig());
                DOM.setStyleAttribute(image.getElement(), "maxHeight", "530px");
                image.setVisible(false);
                images.add(image);
                imagesContainer.add(image);

                image.addDomHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        onRight(null);
                    }
                }, ClickEvent.getType());
            }
            initialized = true;
        }

        for (int i = 0; i < images.size(); i++) {
            Image image = images.get(i);
            image.setVisible(false);
            if (image.getUrl().equals(comment.getBig())) {
                image.setVisible(true);
                index = i;
            }
        }

    }

    void showImage(Integer index) {

        for (int i = 0; i < images.size(); i++) {
            Image image = images.get(i);
            image.setVisible(false);
            if (i == index) {
                image.setVisible(true);
            }
        }

    }

    @UiHandler("left")
    void onLeft(ClickEvent clickEvent) {

        index--;
        if (index < 0) {
            index = images.size() - 1;
        }
        showImage(index);
    }

    @UiHandler("right")
    void onRight(ClickEvent clickEvent) {

        index++;
        if (index > images.size() - 1) {
            index = 0;
        }
        showImage(index);
    }

    @UiHandler("hideButton")
    void onHideImaget(ClickEvent clickEvent) {
//
//        if (showHideButton) {
//
//            StandaloneAsyncService.get().hideComments(new StandaloneCallback<JavaScriptObject>() {
//                @Override
//                public void onSuccess(JavaScriptObject result) {
//                    int d = 1;
//                }
//            }, comments.get(index).getId(), comments.get(index).getBig(), loginCookie);
//
//        }
    }

}