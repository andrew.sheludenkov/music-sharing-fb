package com.client.registration.hemechoo.form;

import com.client.MainHemechooUIService;
import com.client.MainHemechooUIServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;



public class HemechooContactForm extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, HemechooContactForm> {
    }

    private final MainHemechooUIServiceAsync serviceAsync = GWT
            .create(MainHemechooUIService.class);

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);
    @UiField
    FlowPanel tab1;
    @UiField
    FlowPanel tab2;
    @UiField
    TextBox username;
    @UiField
    Button submitButton;
    @UiField
    FlowPanel active1;
    @UiField
    FlowPanel active2;
    @UiField
    FlowPanel tab3;
    @UiField
    FlowPanel tab4;
    @UiField
    FlowPanel guide2;
    @UiField
    FlowPanel guide1;
    @UiField
    TextArea textarea1;
    @UiField
    TextArea textarea2;
    @UiField
    TextBox email;
    @UiField
    TextBox phone;
    @UiField
    TextBox url;
    @UiField
    HTMLPanel form;
    @UiField
    FlowPanel greetings;


    public HemechooContactForm() {

        initWidget(ourUiBinder.createAndBindUi(this));

        tab2.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                active2.setVisible(true);
                active1.setVisible(false);
                guide1.setVisible(false);
                guide2.setVisible(true);
            }
        }, ClickEvent.getType());

        tab3.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                active2.setVisible(false);
                active1.setVisible(true);
                guide1.setVisible(true);
                guide2.setVisible(false);
            }
        }, ClickEvent.getType());

        textarea1.setText("<script type=\"text/javascript\" language=\"javascript\" src=\"http://hemechoo.com/api/api.nocache.js\"></script>");
        textarea2.setText("<script type=\"text/javascript\" language=\"javascript\" src=\"http://hemechoo.com/api/api.nocache.js\"></script>\n" +
                "        <div id=\"app:comments\"></div>");

    }





    public void initialize() {


    }

    @UiHandler("submitButton")
    void onTab1(ClickEvent clickEvent) {

        submitButton.setText("Отправляем ...");
        submitButton.setEnabled(false);

        serviceAsync.addHemechooShop(username.getText(), email.getText(), url.getText(), phone.getText(), new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onSuccess(Boolean result) {
                form.setVisible(false);
                greetings.setVisible(true);
            }
        });



    }


}