package com.client.registration.hemechoo.admin;

import com.client.AuthenticationService;
import com.client.AuthenticationServiceAsync;
import com.client.MainHemechooUIService;
import com.client.MainHemechooUIServiceAsync;
import com.client.registration.hemechoo.admin.login.LoginPage;
import com.client.util.ClientGlobalVariables;
import com.client.util.Params;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.shop.web.dto.UserDto;

import java.util.HashMap;


public class HemechooAdmin extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, HemechooAdmin> {
    }

    private final MainHemechooUIServiceAsync serviceAsync = GWT
            .create(MainHemechooUIService.class);

    private static AuthenticationServiceAsync authenticationService = GWT.create(AuthenticationService.class);

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    FlowPanel container;
    @UiField
    Anchor comments;
    @UiField
    Anchor user;
    @UiField
    HTMLPanel navBar;


    public HemechooAdmin() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }


    public void initialize() {

        final ClientGlobalVariables globalVariables = ClientGlobalVariables.getInstance();

        if (globalVariables.getLoggedInUser() == null) {
            authenticationService.getLoggedInUser(new AsyncCallback<UserDto>() {
                @Override
                public void onFailure(Throwable caught) {
                    //ExceptionHelper.handleException(caught);
                }

                @Override
                public void onSuccess(UserDto result) {
                    globalVariables.setLoggedInUser(result);
                    //updateUser(result);
                }
            });

        }

        navBar.setVisible(false);

    }

    public void setContent(Widget widget){
        container.clear();
        container.add(widget);

        navBar.setVisible(!(widget instanceof LoginPage));
    }

    @UiHandler("promo")
    void onPromo(ClickEvent clickEvent) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Params.PAGE, Params.PAGE_PROMO);
        map.put(Params.ITEM, "none");

        History.newItem(Params.encodeParams(map));
    }

    @UiHandler("comments")
    void onComments(ClickEvent clickEvent) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Params.PAGE, Params.PAGE_COMMENTS);
        map.put(Params.ITEM, "none");

        History.newItem(Params.encodeParams(map));
    }

    @UiHandler("user")
    void onUser(ClickEvent clickEvent) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Params.PAGE, Params.PAGE_USER);
        map.put(Params.ITEM, "none");

        History.newItem(Params.encodeParams(map));
    }

    @UiHandler("css")
    void onCss(ClickEvent clickEvent) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Params.PAGE, Params.PAGE_CSS);
        map.put(Params.ITEM, "none");

        History.newItem(Params.encodeParams(map));
    }
}