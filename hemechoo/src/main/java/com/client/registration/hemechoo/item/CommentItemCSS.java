package com.client.registration.hemechoo.item;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.HemeChooCommentOdto;


public class CommentItemCSS extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, CommentItemCSS> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);



    @UiField
    Label content;
    @UiField
    Label name;
    @UiField
    Label date;
    @UiField
    Label cons;
    @UiField
    Label pros;
    @UiField
    FlowPanel prosConsContainer;
    @UiField
    FlowPanel container;
    @UiField
    HTMLPanel mainContainer;


    private HemeChooCommentOdto comment;

    public CommentItemCSS() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }

    public void initialize(HemeChooCommentOdto comment, Boolean hide) {

        mainContainer.setVisible(!hide);

        prosConsContainer.setVisible(false);

        this.comment = comment;

        String contentText = comment.getContent();

        if (comment.getHtml() != null && comment.getHtml().equals("yes")){
            content.getElement().setInnerHTML(contentText);
        } else {
            content.setText(contentText);
        }

        String nameTxt = comment.getName();
        if (nameTxt != null) {
            name.setText(nameTxt);
        }

        String dateTxt = comment.getDate();
        if (dateTxt != null){
            date.setText(dateTxt);
        }

        String prosTxt = comment.getPros();
        if (prosTxt != null){
            if (comment.getHtml() != null && comment.getHtml().equals("yes")){
                pros.getElement().setInnerHTML("Плюсы: " + prosTxt);
                prosConsContainer.setVisible(true);
            } else {
                pros.setText("Плюсы: " + prosTxt);
                prosConsContainer.setVisible(true);
            }

        }

        String consTxt = comment.getCons();
        if (consTxt != null){
            if (comment.getHtml() != null && comment.getHtml().equals("yes")){
                cons.getElement().setInnerHTML("Минусы: " + consTxt);
                prosConsContainer.setVisible(true);
            } else {
                cons.setText("Минусы: " + consTxt);
                prosConsContainer.setVisible(true);
            }

        }

        if (comment.getSub() != null && comment.getSub().equals("SUB")){
            DOM.setStyleAttribute(container.getElement(), "paddingLeft", "30px");
        }

    }

    public void initialize(String nameTxt, String contentText, String plus, String minus) {

        prosConsContainer.setVisible(false);
        content.setText(contentText);
        if (nameTxt != null) {
            name.setText(nameTxt);
        }
        if (plus != null && plus.length() > 0){
            pros.setText("Плюсы: " + plus);
            prosConsContainer.setVisible(true);
        }
        if (minus != null && minus.length() > 0){
            cons.setText("Минусы: " + minus);
            prosConsContainer.setVisible(true);
        }

    }

    public void showComment(Boolean show){
        mainContainer.setVisible(show);
    }

}