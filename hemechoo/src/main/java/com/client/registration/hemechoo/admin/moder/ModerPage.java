package com.client.registration.hemechoo.admin.moder;

import com.client.AuthenticationService;
import com.client.AuthenticationServiceAsync;
import com.client.MainHemechooUIService;
import com.client.MainHemechooUIServiceAsync;

import com.client.registration.hemechoo.admin.moder.components.ModerImageWidget;
import com.client.util.ClientGlobalVariables;
import com.client.util.ExceptionHelper;
import com.client.util.Params;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.shop.web.dto.HemeChooCommentDto;
import com.shop.web.dto.UserDto;

import java.util.List;
import java.util.Map;

public class ModerPage extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, ModerPage> {
    }

    private final MainHemechooUIServiceAsync serviceAsync = GWT
            .create(MainHemechooUIService.class);

    private static AuthenticationServiceAsync authenticationService = GWT.create(AuthenticationService.class);

    @UiField
    HTMLPanel thePage;
    @UiField
    FlowPanel container;


    private static ModerPage page;
    private final Integer IMAGES_PER_PAGE = 300;

    public static ModerPage get() {
        if (page == null) {
            page = new ModerPage();
        }
        return page;
    }

    public ModerPage() {

        initWidget(uiBinder.createAndBindUi(this));


    }


    public void initialize(Map<String, String> map) {

        final String page = map.get(Params.ITEM);


        UserDto userDto = ClientGlobalVariables.getInstance().getLoggedInUser();
        if (userDto == null) {
            authenticationService.getLoggedInUser(new AsyncCallback<UserDto>() {
                @Override
                public void onFailure(Throwable caught) {
                    ExceptionHelper.handleException(caught);
                }

                @Override
                public void onSuccess(UserDto result) {
                    ClientGlobalVariables.getInstance().setLoggedInUser(result);

                    find(Integer.valueOf(page) * IMAGES_PER_PAGE, IMAGES_PER_PAGE);

                }
            });
        } else {
            find(Integer.valueOf(page) * IMAGES_PER_PAGE, IMAGES_PER_PAGE);

        }

    }

    private void find(int offset, int limit) {

        UserDto user = ClientGlobalVariables.getInstance().getLoggedInUser();
        String storeStringId = user.getCarColor();
        if (user.getUsername().equals("moder_hemechoo")) {

            serviceAsync.getImages(offset, limit, storeStringId, new AsyncCallback<List<HemeChooCommentDto>>() {
                @Override
                public void onFailure(Throwable caught) {
                    int a = 3;
                }

                @Override
                public void onSuccess(List<HemeChooCommentDto> result) {

                    Window.scrollTo(0, 0);

                    if (result != null) {
                        container.clear();

                        for (HemeChooCommentDto item : result) {
                            ModerImageWidget commentWidget = new ModerImageWidget();
                            commentWidget.initialize(item);

                            container.add(commentWidget);
                        }
                    }

                }
            });
        }
    }


}