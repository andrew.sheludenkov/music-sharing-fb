package com.client.registration.hemechoo.admin.login;

import com.client.AuthenticationService;
import com.client.AuthenticationServiceAsync;
import com.client.util.ClientGlobalVariables;
import com.client.util.Params;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.shop.web.dto.UserDto;

import java.util.HashMap;



public class LoginPage extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, LoginPage> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    private static AuthenticationServiceAsync authenticationService = GWT.create(AuthenticationService.class);

    @UiField
    TextBox login;
    @UiField
    PasswordTextBox password;
    @UiField
    Button okBtn;
    @UiField
    FlowPanel alert;
    @UiField
    FormPanel formPanel;

    public LoginPage() {

        initWidget(ourUiBinder.createAndBindUi(this));

    }

    @Override
    protected void onLoad() {
        super.onLoad();
        alert.setVisible(false);
    }

    public void initialize() {


    }

    private void submit() {
        authenticationService.loginUser(login.getText(), password.getText(), new AsyncCallback<UserDto>() {
            @Override
            public void onFailure(Throwable caught) {
                alert.setVisible(true);
            }

            @Override
            public void onSuccess(UserDto result) {
                Cookies.setCookie("loginHash", result.getLoginHash());
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(Params.PAGE, Params.PAGE_COMMENTS);
                map.put(Params.ITEM, "none");

                ClientGlobalVariables.getInstance().setLoggedInUser(result);

                History.newItem(Params.encodeParams(map));
            }
        });
    }

    @UiHandler("okBtn")
    void onOkbutton(ClickEvent clickEvent) {
        submit();
    }
}