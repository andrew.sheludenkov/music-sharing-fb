package com.client.registration.hemechoo.images;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.HemeChooCommentOdto;

import java.util.ArrayList;
import java.util.List;


public class ImagesContainer extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, ImagesContainer> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);

    @UiField
    HTMLPanel mainContainer;
    @UiField
    FlowPanel container2;
    @UiField
    FlowPanel backDrop;
    @UiField
    HTMLPanel modalWindow;
    @UiField
    ImagesSlider imagesSlider;
    @UiField
    Anchor cross;


    private List<HemeChooCommentOdto> comments = new ArrayList<HemeChooCommentOdto>();

    public ImagesContainer() {

        initWidget(ourUiBinder.createAndBindUi(this));

        backDrop.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                modalWindow.setVisible(false);
                backDrop.setVisible(false);
            }
        }, ClickEvent.getType());

        cross.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                modalWindow.setVisible(false);
                backDrop.setVisible(false);
            }
        }, ClickEvent.getType());

    }

    public void addImageItem(HemeChooCommentOdto comment) {

        ImageItem imageItem = new ImageItem();
        imageItem.initialize(comment, this);

        container2.add(imageItem);
        comments.add(comment);

    }

    public void showModalWindow(HemeChooCommentOdto comment){
        modalWindow.setVisible(true);
        backDrop.setVisible(true);
        imagesSlider.initialize(comments, comment);
    }


}