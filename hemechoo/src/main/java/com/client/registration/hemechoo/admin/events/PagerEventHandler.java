package com.client.registration.hemechoo.admin.events;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 15.01.12
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */

abstract public class PagerEventHandler {


    abstract public void onEvent(int offset, int limit, int page);


}
