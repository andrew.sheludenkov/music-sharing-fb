package com.client.registration.hemechoo.feedback.signin;


import com.client.MainIntervioUIService;
import com.client.MainIntervioUIServiceAsync;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.UserOdto;


public class SignInBlock extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    private final MainIntervioUIServiceAsync serviceAsync = GWT
            .create(MainIntervioUIService.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, SignInBlock> {
    }



    private String productId;

    private static SignInBlock searchBlock;

    FlowPanel container;

    @UiField
    SubmitButton okBtn;
    @UiField
    TextBox login;
    @UiField
    PasswordTextBox password;
    @UiField
    HTMLPanel mainContainer;

    Integer value = 0;

    public static SignInBlock get() {
        if (searchBlock == null) {
            searchBlock = new SignInBlock();
        }
        return searchBlock;
    }


    public SignInBlock() {
        initWidget(uiBinder.createAndBindUi(this));

     }



    public Integer getValue(){
        return value;
    }

    public void initialize(String productId, FlowPanel container) {

        this.productId = productId;
        this.container = container;

    }

    public Integer getStars(){
        return value;
    }



    @UiHandler("cancel")
    void onCancel(ClickEvent clickEvent) {
        mainContainer.getParent().setVisible(false);
    }

    @UiHandler("okBtn")
    void onLogin(ClickEvent clickEvent) {

//
//        String loginStr = login.getText();
//        String passwordStr = password.getText();
//
//        StandaloneAsyncService.get().userSignIn(new StandaloneCallback<UserOdto>() {
//            @Override
//            public void onSuccess(UserOdto user) {
//
//               if (user != null){
//                   String loginHash = user.getLoginHash();
//                   if (loginHash != null){
////                       Cookies.setCookie("loginHash", loginHash);
//                       String host = getURL().replaceAll("http://", "").split("/")[0];
//                       Cookies.setCookie("loginHash", loginHash, null, host, "/", false);
//                   }
//               }
//
//                mainContainer.getParent().setVisible(false);
//            }
//        }, loginStr, passwordStr);

    }




    public static native String getURL() /*-{
        return $wnd.location.href;
    }-*/;

}