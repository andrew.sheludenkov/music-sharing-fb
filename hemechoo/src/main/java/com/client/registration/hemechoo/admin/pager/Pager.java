package com.client.registration.hemechoo.admin.pager;

import com.client.registration.hemechoo.admin.events.PagerEventHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class Pager extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, Pager> {
    }


    public int page = 0;
    public int total;
    public int limit;

    private PagerEventHandler eventHandler;
    @UiField
    HTMLPanel container;

    private final int MAX_NUM_PAGES = 9;
    private int NUM_PAGES = 9;

    public Pager() {
        initWidget(uiBinder.createAndBindUi(this));

    }



    public void initialize(int total, int limit) {

        this.total = total;
        this.limit = limit;

        NUM_PAGES = Math.min((int) (Math.floor(total / limit) + 1), MAX_NUM_PAGES);

        //page = 0;
        rebuildPager();
    }

    public void reset() {
        page = 0;
        rebuildPager();
    }

    private void rebuildPager() {
        Anchor prev = new Anchor("← Сюда");
        prev.addStyleName("prev");
        prev.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (eventHandler != null) {
                    if (page > 0) {
                        page--;
                    }
                    eventHandler.onEvent(page * Pager.this.limit, Pager.this.limit, Pager.this.page);
                    rebuildPager();
                }
            }
        });

        Anchor next = new Anchor("Туда →");
        next.addStyleName("next");
        next.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (eventHandler != null) {
                    if (page < total / limit) {
                        page++;
                    }
                    eventHandler.onEvent(page * Pager.this.limit, Pager.this.limit, Pager.this.page);
                    rebuildPager();
                }
            }
        });


        container.clear();

        container.add(prev);

        int i = page;
        i = i - NUM_PAGES / 2;
        if (i < 0) {
            i = 0;
        }
        if (limit > 0) {
            if (i + NUM_PAGES > total / limit) {
                i = total / limit - NUM_PAGES + 1;
            }
        }


        for (int k = 0; k < NUM_PAGES; k++) {
            final int localPage = i + k;

            Anchor pgAnchor = new Anchor((i + k + 1) + "");
            if (i + k == page) {
                pgAnchor.setStyleName("active");
            }

            container.add(pgAnchor);
            pgAnchor.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    if (eventHandler != null) {
                        eventHandler.onEvent(localPage * Pager.this.limit, Pager.this.limit, localPage);
                        page = localPage;
                        rebuildPager();
                    }
                }
            });
        }

        container.add(next);
    }

    public PagerEventHandler getEventHandler() {
        return eventHandler;
    }

    public void setEventHandler(PagerEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }
}