package com.client.registration.hemechoo;

import com.client.registration.hemechoo.feedback.FeedbackBlockCSS;
import com.client.registration.hemechoo.images.ImagesContainer;
import com.client.registration.hemechoo.item.CommentItemCSS;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.client.util.ClientBase64;
import com.client.util.ClientCache;
import com.client.util.ClientUtil;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.http.client.URL;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.EmbeddingParamsOdto;
import com.shop.web.odto.HemeChooCommentOdto;
import com.shop.web.odto.HemechooCommentsResultOdto;

import java.util.ArrayList;
import java.util.List;


public class HemechooCommentsCSS extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, HemechooCommentsCSS> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);


    @UiField
    FlowPanel container;
    @UiField
    FeedbackBlockCSS feedback;
    @UiField
    Anchor showComments;
    @UiField
    FlowPanel showCommentsContainer;
    @UiField
    FlowPanel imagesContainer;
    @UiField
    ImagesContainer imageContainer;
    @UiField
    HTMLPanel hemeContent;

    Boolean hide = false;
    Boolean showCommentsBool = false;

    List<CommentItemCSS> commentsList = new ArrayList<CommentItemCSS>();

    public HemechooCommentsCSS() {

        initWidget(ourUiBinder.createAndBindUi(this));

        feedback.initialize(null, container);

    }

    public void setFormWidth(String params) {
        feedback.setFormWidth(params);
        hemeContent.setWidth(params);
    }

    public void initialize(EmbeddingParamsOdto params) {
//
//        String showComments = params.getComments();
//        if (showComments.equals("true")) {
//            showCommentsBool = true;
//            feedback.setVisible(true);
//        }
//        String imagesBlockBidth = params.getImagesWidth();
//        if (imagesBlockBidth != null) {
//            imageContainer.setWidth(imagesBlockBidth);
//        }
//
//        container.setVisible(false);
//        imagesContainer.setVisible(false);
//
//
//        // get comments by product id
//        if (Window.Location.getPath() != null) {
//            String arr[] = Window.Location.getPath().split("/");
//            if (arr.length > 2) {
//
//                if (Window.Location.getHost().contains("citymart") && (arr[1].equals("short") || arr[1].equals("product"))) {
//                    String productId = Window.Location.getPath().split("/")[2];
//
//                    getCommentsById(productId);
//
//                    return;
//                }
//            }
//        }
//
//        // get comments by product name
//        NodeList<Element> list = Document.get().getElementsByTagName("h1");
//
//        Element element = null;
//
//        if ((Window.Location.getQueryString().contains("kosholka.com.ua") || Window.Location.getHost().contains("kosholka.com.ua"))
//                && list.getLength() > 1) {
//            element = list.getItem(1);
//
//        }else if ((Window.Location.getQueryString().contains("evotex.kh.ua") || Window.Location.getHost().contains("evotex.kh.ua"))
//                && list.getLength() > 1) {
//            element = list.getItem(1);
//
//        } else if ((Window.Location.getQueryString().contains("ukr-shop.net") || Window.Location.getHost().contains("ukr-shop.net") ||
//                Window.Location.getQueryString().contains("kaz-shop.net") || Window.Location.getHost().contains("kaz-shop.net"))
//                && list.getLength() > 1) {
//            element = list.getItem(1);
//
//        } else if (Window.Location.getQueryString().contains("magazyaka.com.ua") || Window.Location.getHost().contains("magazyaka.com.ua")) {
//            list = Document.get().getElementsByTagName("h2");
//            if (list.getLength() > 1) {
//                element = list.getItem(1);
//            }
//        } else if (list != null && list.getLength() > 0) {
//            element = list.getItem(0);
//        }
//
//        if (element != null) {
//            //String html = element.getInnerHTML();
//            String text = element.getInnerText().trim();
//            text = ClientUtil.get().cleanProductName(text);
//            text = URL.encode(text);
//
//            text = ClientBase64.toBase64(text.getBytes());
//            //text = btoa(text);
//
//            if (showCommentsBool) {
//                StandaloneAsyncService.get().getComments(new StandaloneCallback<HemechooCommentsResultOdto>() {
//                    @Override
//                    public void onSuccess(HemechooCommentsResultOdto result) {
//                        createComments(result);
//                        hideDiv(result);
//                    }
//                }, URL.encode(text));
//            } else {
//                StandaloneAsyncService.get().getImages(new StandaloneCallback<HemechooCommentsResultOdto>() {
//                    @Override
//                    public void onSuccess(HemechooCommentsResultOdto result) {
//                        createComments(result);
//                        hideDiv(result);
//                    }
//                }, URL.encode(text));
//            }
//        }


    }

    private void getCommentsById(String productId) {
//        StandaloneAsyncService.get().getCommentsById(new StandaloneCallback<HemechooCommentsResultOdto>() {
//            @Override
//            public void onSuccess(HemechooCommentsResultOdto result) {
//                createComments(result);
//            }
//        }, productId);
    }

    private void createComments(HemechooCommentsResultOdto result) {

        String showImages = ClientCache.get().getParams().getImage();

        JsArray<HemeChooCommentOdto> list = result.getCommentsList();

        Boolean hideComment = false;

        for (int i = 0, resultSize = list.length(); i < resultSize; i++) {

            if (hide && i >= 3) {
                hideComment = true;
            }

            HemeChooCommentOdto comment = list.get(i);

            if (comment.getTyle().equals("text")) {
                CommentItemCSS commentItem = new CommentItemCSS();
                container.add(commentItem);
                commentsList.add(commentItem);
                commentItem.initialize(comment, hideComment);

                container.setVisible(showCommentsBool);
            } else {
                if (showImages.equals("true")) {
                    imagesContainer.setVisible(true);
                    imageContainer.addImageItem(comment);
                }
            }

        }
        showCommentsContainer.setVisible(hideComment);
    }

    private void hideDiv(HemechooCommentsResultOdto result) {
        if (result.getHideDivId() != null) {
            Element element = Document.get().getElementById(result.getHideDivId());
            if (element != null) {
                element.setAttribute("hidden", "true");
                element.setAttribute("style", "display: none;");

                //DOM.setStyleAttribute(element, "hidden", "true");

            }
        }
    }


//    native String btoa(String b64) /*-{
//        return window.btoa(b64);
//    }-*/;


    public void setHide(String hideStr) {
        if (hideStr != null && hideStr.equals("yes")) {
            hide = true;
        }
    }

    @UiHandler("showComments")
    void onShowComments(ClickEvent clickEvent) {

        showCommentsContainer.setVisible(false);
        for (CommentItemCSS item : commentsList) {
            item.showComment(true);
        }

    }


}