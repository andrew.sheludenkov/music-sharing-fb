package com.client.registration.hemechoo.item;

import com.client.MainIntervioUIService;
import com.client.MainIntervioUIServiceAsync;
import com.client.service.StandaloneAsyncService;
import com.client.service.StandaloneCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.shop.web.odto.HemeChooCommentOdto;
import com.shop.web.odto.HemechooCommentsResultOdto;


public class CommentItem extends Composite {
    interface LoginPageUiBinder extends UiBinder<Widget, CommentItem> {
    }

    private static LoginPageUiBinder ourUiBinder = GWT.create(LoginPageUiBinder.class);



    @UiField
    Label content;
    @UiField
    Label name;
    @UiField
    Label date;
    @UiField
    Label cons;
    @UiField
    Label pros;
    @UiField
    FlowPanel prosConsContainer;
    @UiField
    FlowPanel container;
    @UiField
    HTMLPanel mainContainer;


    private HemeChooCommentOdto comment;

    public CommentItem() {

        initWidget(ourUiBinder.createAndBindUi(this));


    }

    public void initialize(HemeChooCommentOdto comment, Boolean hide) {

        mainContainer.setVisible(!hide);

        prosConsContainer.setVisible(false);

        this.comment = comment;

        String contentText = comment.getContent();
        content.setText(contentText);

        String nameTxt = comment.getName();
        if (nameTxt != null) {
            name.setText(nameTxt);
        }

        String dateTxt = comment.getDate();
        if (dateTxt != null){
            date.setText(dateTxt);
        }

        String prosTxt = comment.getPros();
        if (prosTxt != null){
            pros.setText("Плюсы: " + prosTxt);
            prosConsContainer.setVisible(true);
        }

        String consTxt = comment.getCons();
        if (consTxt != null){
            cons.setText("Минусы: " + consTxt);
            prosConsContainer.setVisible(true);
        }

        if (comment.getSub() != null && comment.getSub().equals("SUB")){
            DOM.setStyleAttribute(container.getElement(), "paddingLeft", "30px");
        }

    }

    public void initialize(String nameTxt, String contentText, String plus, String minus) {

        prosConsContainer.setVisible(false);
        content.setText(contentText);
        if (nameTxt != null) {
            name.setText(nameTxt);
        }
        if (plus != null && plus.length() > 0){
            pros.setText("Плюсы: " + plus);
            prosConsContainer.setVisible(true);
        }
        if (minus != null && minus.length() > 0){
            cons.setText("Минусы: " + minus);
            prosConsContainer.setVisible(true);
        }

    }

    public void showComment(Boolean show){
        mainContainer.setVisible(show);
    }

}