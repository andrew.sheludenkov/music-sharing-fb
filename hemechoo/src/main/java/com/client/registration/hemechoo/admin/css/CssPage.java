package com.client.registration.hemechoo.admin.css;

import com.client.AuthenticationService;
import com.client.AuthenticationServiceAsync;
import com.client.MainHemechooUIService;
import com.client.MainHemechooUIServiceAsync;
import com.client.registration.hemechoo.admin.comments.components.CommentWidget;
import com.client.registration.hemechoo.admin.events.PagerEventHandler;
import com.client.registration.hemechoo.admin.pager.Pager;
import com.client.util.ClientGlobalVariables;
import com.client.util.ExceptionHelper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.shop.web.dto.HemeChooCommentDto;
import com.shop.web.dto.UserDto;

import java.util.List;
import java.util.Map;

public class CssPage extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    interface ProgramPageUiBinder extends UiBinder<Widget, CssPage> {
    }

    private final MainHemechooUIServiceAsync serviceAsync = GWT
            .create(MainHemechooUIService.class);

    private static AuthenticationServiceAsync authenticationService = GWT.create(AuthenticationService.class);

    @UiField
    HTMLPanel thePage;
    @UiField
    FlowPanel container;

    @UiField
    TextArea text;
    @UiField
    Button btn;

    private static CssPage page;
    private final Integer PRODUCTS_PER_PAGE = 30;

    public static CssPage get() {
        if (page == null) {
            page = new CssPage();
        }
        return page;
    }

    public CssPage() {

        initWidget(uiBinder.createAndBindUi(this));


    }


    public void initialize(Map<String, String> map) {


        UserDto userDto = ClientGlobalVariables.getInstance().getLoggedInUser();
        if (userDto == null) {
            authenticationService.getLoggedInUser(new AsyncCallback<UserDto>() {
                @Override
                public void onFailure(Throwable caught) {
                    ExceptionHelper.handleException(caught);
                }

                @Override
                public void onSuccess(UserDto result) {
                    ClientGlobalVariables.getInstance().setLoggedInUser(result);

                    getCss();
                }
            });
        }else {
            getCss();
        }


    }

    private void getCss() {

        UserDto user = ClientGlobalVariables.getInstance().getLoggedInUser();
        String storeStringId = user.getCarColor();

        serviceAsync.getCssText(storeStringId, new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onSuccess(String result) {
                text.setText(result);
            }
        });
    }


    @UiHandler("btn")
    void onButton(ClickEvent clickEvent) {

        UserDto user = ClientGlobalVariables.getInstance().getLoggedInUser();
        String storeStringId = user.getCarColor();

        String cssString = text.getText();

        serviceAsync.updateCssText(storeStringId, cssString, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onSuccess(Void result) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

}