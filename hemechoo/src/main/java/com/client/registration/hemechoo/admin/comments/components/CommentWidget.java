package com.client.registration.hemechoo.admin.comments.components;

import com.client.MainHemechooUIService;
import com.client.MainHemechooUIServiceAsync;
import com.client.MainUIService;
import com.client.MainUIServiceAsync;
import com.client.util.ClientUtil;
import com.client.util.ExceptionHelper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.shop.web.dto.ActivityDto;
import com.shop.web.dto.DiscountDto;
import com.shop.web.dto.HemeChooCommentDto;

public class CommentWidget extends Composite {

    private static ProgramPageUiBinder uiBinder = GWT
            .create(ProgramPageUiBinder.class);

    private final MainHemechooUIServiceAsync serviceAsync = GWT
            .create(MainHemechooUIService.class);

    @UiField
    InlineHTML captionCategory;

    @UiField
    InlineHTML captionProduct;
    @UiField
    InlineHTML captionType;
    @UiField
    Button remove;
    @UiField
    HTMLPanel theComponent;
    @UiField
    InlineHTML description;

    @UiField
    InlineHTML user;
    @UiField
    InlineHTML date;
    @UiField
    Label status;
    @UiField
    InlineHTML cons;
    @UiField
    InlineHTML pros;
    @UiField
    Button edit;
    @UiField
    TextArea descriptionEdit;
    @UiField
    TextArea prosEdit;
    @UiField
    TextArea consEdit;
    @UiField
    Button update;
    @UiField
    Anchor url;
    @UiField
    InlineHTML email;

    private DiscountDto discountDto;

    HemeChooCommentDto comment;

    interface ProgramPageUiBinder extends UiBinder<Widget, CommentWidget> {
    }


    public CommentWidget() {
        initWidget(uiBinder.createAndBindUi(this));
    }


    public void initialize(HemeChooCommentDto comment) {

        this.comment = comment;

        date.setText(ClientUtil.formatDate(comment.getCreationDate(), "d MMM yyyy"));


            user.setText(comment.getName());

        email.setText(comment.getEmail());

       // activityType.setText(activityDto.getActivityType());

        description.setText(comment.getContent());
        descriptionEdit.setText(comment.getContent());

        pros.setText(comment.getPros());
        prosEdit.setText(comment.getPros());

        cons.setText(comment.getCons());
        consEdit.setText(comment.getCons());

        url.setHref(comment.getUrl());
        url.setText(comment.getUrl());


    }


    @UiHandler("edit")
    void onEdit(ClickEvent clickEvent) {

        description.setVisible(false);
        descriptionEdit.setVisible(true);

        pros.setVisible(false);
        prosEdit.setVisible(true);

        cons.setVisible(false);
        consEdit.setVisible(true);

        edit.setVisible(false);
        update.setVisible(true);

    }

    @UiHandler("update")
    void onUpdate(ClickEvent clickEvent) {

        serviceAsync.updateCommentContent(comment.getId(), descriptionEdit.getText(), prosEdit.getText(), consEdit.getText(), new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onSuccess(Void result) {
                description.setText(descriptionEdit.getText());
                pros.setText(prosEdit.getText());
                cons.setText(consEdit.getText());

                description.setVisible(true);
                descriptionEdit.setVisible(false);

                pros.setVisible(true);
                prosEdit.setVisible(false);

                cons.setVisible(true);
                consEdit.setVisible(false);

                edit.setVisible(true);
                update.setVisible(false);
            }
        });


    }

    @UiHandler("remove")
    void onRemove(ClickEvent clickEvent) {
//        serviceAsync.removeDiscount(discountDto.getId(), new AsyncCallback<Boolean>() {
//            @Override
//            public void onFailure(Throwable caught) {
//                ExceptionHelper.handleException(caught);
//            }
//
//            @Override
//            public void onSuccess(Boolean result) {
//                theComponent.setVisible(false);
//            }
//        });
    }


}
