package com.server;

import com.shop.web.service.PeersProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
//@RequestMapping(value = "/files/prices")
public class PeersProcessingController implements org.springframework.web.servlet.mvc.Controller {


    PeersProcessorService peersProcessorService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {



       peersProcessorService.processAllUsers();


//        RequestDispatcher view = request.getRequestDispatcher("/BetaList.html");
        RequestDispatcher view = request.getRequestDispatcher("/MuzekPageRu.html");
        // don't add your web-app name to the path

        view.forward(request, response);

        return null;
    }


    @Autowired
    public void setPeersProcessorService(PeersProcessorService peersProcessorService) {
        this.peersProcessorService = peersProcessorService;
    }
}
