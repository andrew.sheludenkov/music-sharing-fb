package com.server.util;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: AS
 */
public class P3PFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        addCacheHeaders(servletRequest, servletResponse);
        filterChain.doFilter(servletRequest, servletResponse);

    }

    private void addCacheHeaders(ServletRequest request, ServletResponse response)
		throws IOException, ServletException {

		HttpServletResponse sr = (HttpServletResponse) response;
//		sr.setHeader("P3P", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
        sr.setHeader("P3P", "CP=\"This is not P3P yee\"");
	}

    @Override
    public void destroy() {

    }
}
