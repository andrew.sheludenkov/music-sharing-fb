package com.server.util;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: AS
 *
 */
public class CacheFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        addCacheHeaders(servletRequest, servletResponse);
        filterChain.doFilter(servletRequest, servletResponse);

    }

    private void addCacheHeaders(ServletRequest request, ServletResponse response)
		throws IOException, ServletException {

		HttpServletResponse sr = (HttpServletResponse) response;
		sr.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        sr.setHeader("Pragma", "no-cache");
        sr.setHeader("Expires", "0");


	}

    @Override
    public void destroy() {

    }
}
