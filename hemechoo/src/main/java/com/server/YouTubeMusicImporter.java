package com.server;

import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.MuzekAlbumsImpl;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import com.shop.model.persistence.impl.UserImpl;
import com.shop.model.service.MuzekAlbumsService;
import com.shop.model.service.MuzekTrackListService;
import com.shop.model.service.UserModelService;
import com.shop.web.dto.MuzekTrackDto;
import com.shop.web.dto.MuzekTrackUserDto;
import com.shop.web.dto.MuzekYoutubeChannelDto;
import com.shop.web.dto.MuzekYoutubeTracklistDto;
import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.util.Duration;
import org.jboss.netty.handler.codec.http.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shell
 * Date: 6/20/15
 * Time: 10:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class YouTubeMusicImporter implements Runnable {


    String accessToken;
    String userFBid;
    UserModelService userModelService;
    MuzekAlbumsService muzekAlbumsService;
    MuzekTrackListService muzekTrackListService;

    public String host = "www.googleapis.com";
    public String uri_stub = "/method/audio.get?&access_token=";
    //    public String uri_stub_albums = "/method/audio.getAlbums?&v=5.34&access_token=";
    public String uri_stub_albums = "/method/audio.getAlbums?&access_token=";
    public String uri;

    String requestResult;
    String fetchedAlbums;

    class ParsedResult{
        private Map<String, MuzekTrackDto> value;
        private String nextPageToken;

        Map<String, MuzekTrackDto> getValue() {
            return value;
        }

        void setValue(Map<String, MuzekTrackDto> value) {
            this.value = value;
        }

        String getNextPageToken() {
            return nextPageToken;
        }

        void setNextPageToken(String nextPageToken) {
            this.nextPageToken = nextPageToken;
        }
    }

    @Override
    public void run() {

        int totalNumberOfImportedTracks = 0;

        User user = userModelService.findUserByMuzekFBid(userFBid);

        try {

            String subscriptionsSourceString = getSubscriptions();
            List<MuzekYoutubeChannelDto> channels = deserializeSubscriptions(subscriptionsSourceString);

            for (MuzekYoutubeChannelDto channel : channels) {
//            String videosFromChannel = searchVideosInChannel("UCvk_qhWVhrfcWDEJt-XEcRQ");
                String videosFromChannel = searchVideosInChannel(channel.getId(), null);
                ParsedResult result = deserializeTracks(videosFromChannel);
                Map<String, MuzekTrackDto> allTracksInChannel = result.getValue();
                String nextPageToken = result.getNextPageToken();
                int numberOfpages = 0;
                while (nextPageToken != null || numberOfpages > 10) {
                    numberOfpages++;

                    videosFromChannel = searchVideosInChannel(channel.getId(), nextPageToken);
                    result = deserializeTracks(videosFromChannel);
                    allTracksInChannel.putAll(result.getValue());
                    nextPageToken = result.getNextPageToken();
                }


                // String playLists = getPlaylists("UCvk_qhWVhrfcWDEJt-XEcRQ");
                String playListsSourceString = getPlaylists(channel.getId());
                List<MuzekYoutubeTracklistDto> trackList = deserializeTracksList(playListsSourceString);

                for (MuzekYoutubeTracklistDto muzekYoutubeTracklistDto : trackList) {
                    // String playListItems = getPlayListItems("PLMkhcBvk648u0kMSPrSOH6l6PVvBaXHzG");
                    String tracksSourceString = getPlayListItems(muzekYoutubeTracklistDto.getId());
                    List<MuzekTrackDto> tracks = deserializePlaylistTracks(tracksSourceString);

                    List<MuzekTrackDto> tracksForRecord = new ArrayList<MuzekTrackDto>();
                    for (MuzekTrackDto track : tracks) {
                        MuzekTrackDto fetchedTrack = allTracksInChannel.get(track.getId());
                        if (fetchedTrack != null) {
                            track.setArtwork_url(fetchedTrack.getArtwork_url());
                            tracksForRecord.add(track);
                        }
                    }
                    if (tracksForRecord.size() > 0) {
                        muzekAlbumsService.create(MuzekAlbumsImpl.ALBUM_SOURCE.YOUTUBE, user.getIdentifier(), user.getMuzekFBid(),
                                null, null, null, muzekYoutubeTracklistDto.getId(), muzekYoutubeTracklistDto.getTitle());


                        for (MuzekTrackDto track : tracksForRecord) {

                            try {
                                muzekTrackListService.create(user.getIdentifier(), user.getMuzekFBid(), track.getId(),
                                        MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE,
                                        track.getTitle(),
                                        track.getArtwork_url(),
                                        track.getPermalink_url(),
                                        track.getUser().getUsername(), track.getUser().getPermalink_url(),
                                        null, null, null, null,
                                        MuzekTrackListImpl.VK_IMPORT_STATUS.IMPORTED,
                                        muzekYoutubeTracklistDto.getId());

                                totalNumberOfImportedTracks++;
                                String stat = String.valueOf(totalNumberOfImportedTracks);

                                userModelService.updateYoutubeImportStatus(user.getIdentifier(), stat, UserImpl.USER_TRACK_VK_IMPORT_STATUS.PROGRESS);
                            } catch (Throwable e) {

                            }

                        }


                    }

                }

            }

            userModelService.updateYoutubeImportStatus(user.getIdentifier(), null, UserImpl.USER_TRACK_VK_IMPORT_STATUS.COMPLETE);

        } catch (Exception e) {
            userModelService.updateYoutubeImportStatus(user.getIdentifier(), null, UserImpl.USER_TRACK_VK_IMPORT_STATUS.FAILED);
        }
    }


    public String searchVideosInChannel(String channelId, String nextPageToken) {
        String uri = "/youtube/v3/search?part=snippet&maxResults=50&type=video&videoCategoryId=10&q=&channelId=" + channelId + "&access_token=" + accessToken;
        if (nextPageToken != null){
            uri+= "&pageToken=" + nextPageToken;
        }
        return sendRequestHTTPS(host, uri);
    }

    public String getPlaylists(String channelId) {
        return sendRequestHTTPS(host, "/youtube/v3/playlists?maxResults=50&part=snippet&channelId=" + channelId + "&access_token=" + accessToken);
    }

    public String getPlayListItems(String playlistId) {
        return sendRequestHTTPS(host, "/youtube/v3/playlistItems?maxResults=50&part=snippet&playlistId=" + playlistId + "&access_token=" + accessToken);
    }

    public String getSubscriptions() {
        return sendRequestHTTPS(host, "/youtube/v3/subscriptions?maxResults=50&part=snippet&mine=true&access_token=" + accessToken);
    }

    public List<MuzekYoutubeChannelDto> deserializeSubscriptions(String sourceString) {
        List<MuzekYoutubeChannelDto> list = new ArrayList<MuzekYoutubeChannelDto>();

        Object obj = JSONValue.parse(sourceString);
        JSONArray items = (JSONArray) ((JSONObject) obj).get("items");
        String nextPageToken = (String) ((JSONObject) obj).get("nextPageToken");

        //ArrayList<MuzekTrackDto> dtoList = new ArrayList<MuzekTrackDto>();
        for (int i = 0; i < items.size(); i++) {
            MuzekYoutubeChannelDto channel = new MuzekYoutubeChannelDto();


            JSONObject jsonObject = (JSONObject) items.get(i);
            channel.setTitle((String) ((JSONObject) (jsonObject.get("snippet"))).get("title"));
            channel.setId((String) (((JSONObject) ((JSONObject) (jsonObject.get("snippet"))).get("resourceId")).get("channelId")));

            list.add(channel);
        }

        return list;
    }

    public List<MuzekYoutubeTracklistDto> deserializeTracksList(String sourceString) {
        List<MuzekYoutubeTracklistDto> list = new ArrayList<MuzekYoutubeTracklistDto>();

        Object obj = JSONValue.parse(sourceString);
        JSONArray items = (JSONArray) ((JSONObject) obj).get("items");
        String nextPageToken = (String) ((JSONObject) obj).get("nextPageToken");

        //ArrayList<MuzekTrackDto> dtoList = new ArrayList<MuzekTrackDto>();
        for (int i = 0; i < items.size(); i++) {
            MuzekYoutubeTracklistDto trackList = new MuzekYoutubeTracklistDto();


            JSONObject jsonObject = (JSONObject) items.get(i);
            trackList.setTitle((String) ((JSONObject) (jsonObject.get("snippet"))).get("title"));
            trackList.setId((String) ((jsonObject.get("id"))));

            list.add(trackList);
        }

        return list;
    }

    public ParsedResult deserializeTracks(String sourceString) {

        ParsedResult result = new ParsedResult();

        Map<String, MuzekTrackDto> map = new HashMap<String, MuzekTrackDto>();

        Object obj = JSONValue.parse(sourceString);
        JSONArray items = (JSONArray) ((JSONObject) obj).get("items");
        String nextPageToken = (String) ((JSONObject) obj).get("nextPageToken");

        //ArrayList<MuzekTrackDto> dtoList = new ArrayList<MuzekTrackDto>();
        for (int i = 0; i < items.size(); i++) {
            MuzekTrackDto trackDto = new MuzekTrackDto();
            trackDto.setType("youtube");
            trackDto.setStream_url("youtube");

            JSONObject jsonObject = (JSONObject) items.get(i);
            trackDto.setTitle((String) ((JSONObject) (jsonObject.get("snippet"))).get("title"));
            trackDto.setId((String) ((JSONObject) (jsonObject.get("id"))).get("videoId"));
            trackDto.setArtwork_url((String) ((JSONObject) ((JSONObject) ((JSONObject) (jsonObject.get("snippet"))).get("thumbnails")).get("default")).get("url"));

            MuzekTrackUserDto user = new MuzekTrackUserDto();
            user.setUsername((String) ((JSONObject) (jsonObject.get("snippet"))).get("channelTitle"));
            trackDto.setUser(user);


            map.put(trackDto.getId(), trackDto);
        }

        result.setValue(map);
        result.setNextPageToken(nextPageToken);

        return result;
    }

    public List<MuzekTrackDto> deserializePlaylistTracks(String sourceString) {
        List<MuzekTrackDto> list = new ArrayList<MuzekTrackDto>();

        Object obj = JSONValue.parse(sourceString);
        JSONArray items = (JSONArray) ((JSONObject) obj).get("items");
        String nextPageToken = (String) ((JSONObject) obj).get("nextPageToken");

        //ArrayList<MuzekTrackDto> dtoList = new ArrayList<MuzekTrackDto>();
        for (int i = 0; i < items.size(); i++) {
            MuzekTrackDto trackDto = new MuzekTrackDto();
            trackDto.setType("youtube");
            trackDto.setStream_url("youtube");

            JSONObject jsonObject = (JSONObject) items.get(i);
            trackDto.setTitle((String) ((JSONObject) (jsonObject.get("snippet"))).get("title"));
//            trackDto.setId((String) ((JSONObject) (jsonObject.get("id"))).get("videoId"));
            trackDto.setId((String) (((JSONObject) ((JSONObject) (jsonObject.get("snippet"))).get("resourceId")).get("videoId")));

            MuzekTrackUserDto user = new MuzekTrackUserDto();
            user.setUsername((String) ((JSONObject) (jsonObject.get("snippet"))).get("channelTitle"));
            trackDto.setUser(user);

            list.add(trackDto);
        }

        return list;
    }

    public String fetchTracksFromVK() {
        return sendRequestHTTPS(host, uri);
    }

    public String fetchTracksFromVK(String captcha_sid, String captcha_key) {
        String addParams = "&captcha_sid=" + captcha_sid + "&captcha_key=" + captcha_key;
        return sendRequestHTTPS(host, uri + addParams);
    }

    public String fetchAlbumsFromVK() {
        String uri = uri_stub_albums + accessToken;
        String fetchedAlbumsStr = sendRequestHTTPS(host, uri);
        System.out.println("fetched albums - " + fetchedAlbumsStr);
        return fetchedAlbumsStr;
    }

    public String fetchTracksFromVKByAlbum(Long albumId) {
        return sendRequestHTTPS(host, uri + "&album_id=" + albumId);
    }

    private MuzekTrackDto getTrackFromYoutube(String searchString) {
        if (searchString != null) {
            searchString = URLEncoder.encode(searchString);
        }

        String requestString = "/youtube/v3/search?key=AIzaSyA2FmomPU_dYP7B82qYGwVgjnK6u2aCxFE&part=snippet&type=video&videoCategoryId=10&q=" + searchString;
        String requestResult = sendRequestHTTPS("www.googleapis.com", requestString);

        Object obj = JSONValue.parse(requestResult);
        JSONArray items = (JSONArray) ((JSONObject) obj).get("items");
        String nextPageToken = (String) ((JSONObject) obj).get("nextPageToken");

        //ArrayList<MuzekTrackDto> dtoList = new ArrayList<MuzekTrackDto>();
        for (int i = 0; i < items.size(); i++) {
            MuzekTrackDto trackDto = new MuzekTrackDto();
            trackDto.setType("youtube");
            trackDto.setStream_url("youtube");

            JSONObject jsonObject = (JSONObject) items.get(i);
            trackDto.setTitle((String) ((JSONObject) (jsonObject.get("snippet"))).get("title"));
            trackDto.setId((String) ((JSONObject) (jsonObject.get("id"))).get("videoId"));
            trackDto.setArtwork_url((String) ((JSONObject) ((JSONObject) ((JSONObject) (jsonObject.get("snippet"))).get("thumbnails")).get("default")).get("url"));

            MuzekTrackUserDto user = new MuzekTrackUserDto();
            user.setUsername((String) ((JSONObject) (jsonObject.get("snippet"))).get("channelTitle"));
            trackDto.setUser(user);


            return trackDto;
        }
        return null;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        uri = uri_stub + accessToken;
    }


    public String sendRequestHTTPS(String host, String uri) {

        try {
            return sendRequestHTTPSpure(host, uri);
        } catch (Throwable e1) {
            e1.printStackTrace();
            try {
                return sendRequestHTTPSpure(host, uri);
            } catch (Throwable e2) {
                e2.printStackTrace();
                try {
                    return sendRequestHTTPSpure(host, uri);
                } catch (Throwable e3) {
                    e3.printStackTrace();
                }
            }
        }
        return null;
    }

    public String sendRequestHTTPSpure(String host, String uri) {

        Service<HttpRequest, HttpResponse> client = null;

        System.out.println("Client creating: " + Thread.currentThread().getName());
        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                        //  .hosts("78.108.83.148:3128")

                        //.hosts(proxyDbContainer.getProxyDb().getIp())
//                .hosts(host + ":80")
                .hosts(host + ":443")
                .tls(host)
                .retries(1)
                .hostConnectionLimit(1)
        );

        int readingTries = 0;
        String pageUri = uri;

        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, pageUri);   // 2
        request.setHeader(HttpHeaders.Names.HOST, host);
//        request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
//        request.setHeader(HttpHeaders.Names.CACHE_CONTROL, "max-age=0");
//        request.setHeader(HttpHeaders.Names.ACCEPT, "ext/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//        request.setHeader(HttpHeaders.Names.USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36");
//        request.setHeader(HttpHeaders.Names.ACCEPT_ENCODING, "gzip,deflate,sdch");
//        request.setHeader(HttpHeaders.Names.ACCEPT_LANGUAGE, "en-US,en;q=0.8,ru;q=0.6,uk;q=0.4");

        HttpResponse response = client.apply(request).apply();

        if (response.getStatus().getCode() == 200) {
            //byte[] array = response.getContent().array();
            String encoding = "utf8";//"cp1251";//EncodingDeterminer.getEncoding(array);
            String result = response.getContent().toString(Charset.forName("utf8"));

            return result;

        } else {
            System.out.println("==== Missing URI: " + pageUri);

        }
        return null;
    }

    public UserModelService getUserModelService() {
        return userModelService;
    }

    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }

    public MuzekTrackListService getMuzekTrackListService() {
        return muzekTrackListService;
    }

    public void setMuzekTrackListService(MuzekTrackListService muzekTrackListService) {
        this.muzekTrackListService = muzekTrackListService;
    }

    public void setUserFBid(String userFBid) {
        this.userFBid = userFBid;
    }

    public String getRequestResult() {
        return requestResult;
    }

    public void setRequestResult(String requestResult) {
        this.requestResult = requestResult;
    }

    public void setFetchedAlbums(String fetchedAlbums) {
        this.fetchedAlbums = fetchedAlbums;
    }

    public void setMuzekAlbumsService(MuzekAlbumsService muzekAlbumsService) {
        this.muzekAlbumsService = muzekAlbumsService;
    }
}
