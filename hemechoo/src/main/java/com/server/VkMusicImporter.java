package com.server;

import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.MuzekAlbumsImpl;
import com.shop.model.persistence.impl.MuzekQueryCacheImpl;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import com.shop.model.persistence.impl.UserImpl;
import com.shop.model.service.MuzekAlbumsService;
import com.shop.model.service.MuzekQueryCacheService;
import com.shop.model.service.MuzekTrackListService;
import com.shop.model.service.UserModelService;
import com.shop.web.dto.MuzekTrackDto;
import com.shop.web.dto.MuzekTrackUserDto;
import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.util.Duration;
import org.jboss.netty.handler.codec.http.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shell
 * Date: 6/20/15
 * Time: 10:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class VkMusicImporter implements Runnable {


    String accessToken;
    String userFBid;
    UserModelService userModelService;
    MuzekAlbumsService muzekAlbumsService;
    MuzekTrackListService muzekTrackListService;
    MuzekQueryCacheService muzekQueryCacheService;

    public String host = "api.vk.com";
    public String uri_stub = "/method/audio.get?&access_token=";
//    public String uri_stub_albums = "/method/audio.getAlbums?&v=5.34&access_token=";
    public String uri_stub_albums = "/method/audio.getAlbums?&access_token=";
    public String uri;

    String requestResult;
    String fetchedAlbums;

    @Override
    public void run() {

        Integer vk_total = 0;
        Integer vk_imported = 0;
        Integer vk_failed = 0;

        User user = userModelService.findUserByMuzekFBid(userFBid);

        System.out.println("requestResult 2 " + requestResult);

        Object obj = JSONValue.parse(requestResult);
        JSONArray items = (JSONArray) ((JSONObject) obj).get("response");

        vk_total = items.size();
        String stat = vk_total + ":" + vk_imported + ":" + vk_failed;
        userModelService.updateVkImportStatus(user.getIdentifier(), stat, fetchedAlbums, UserImpl.USER_TRACK_VK_IMPORT_STATUS.PROGRESS);

        for (int i = items.size() - 1; i >=0; i--) {
            JSONObject jsonObject = (JSONObject) items.get(i);
            Long vk_track_id = (Long)(jsonObject.get("aid"));
            String artist = (String)(jsonObject.get("artist"));
            String title = (String)(jsonObject.get("title"));
            Long duration = (Long)(jsonObject.get("duration"));

            String stringToSearch = artist + " " + title;
            MuzekTrackDto track = getTrackFromYoutube(stringToSearch, null);

            if (track != null){
                muzekTrackListService.create(user.getIdentifier(), user.getMuzekFBid(), track.getId(),
                        MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE,
                        track.getTitle(),
                        track.getArtwork_url(),
                        track.getPermalink_url(),
                        track.getUser().getUsername(), track.getUser().getPermalink_url(),
                        title, artist, duration, vk_track_id,
                        MuzekTrackListImpl.VK_IMPORT_STATUS.IMPORTED, null);
                vk_imported++;

//                if ( (vk_imported + vk_failed) > 300){
//                    break;
//                }
            }else{
                try{
                muzekTrackListService.createHidden(user.getIdentifier(), user.getMuzekFBid(),
                        MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE,
                        title, artist, duration, MuzekTrackListImpl.VK_IMPORT_STATUS.NOT_FOUND);
                }catch(Throwable e){
                    e.printStackTrace();
                }
                vk_failed++;
            }

            stat = vk_total + ":" + vk_imported + ":" + vk_failed;
            userModelService.updateVkImportStatus(user.getIdentifier(), stat, fetchedAlbums, UserImpl.USER_TRACK_VK_IMPORT_STATUS.PROGRESS);
        }

        stat = vk_total + ":" + vk_imported + ":" + vk_failed;
        userModelService.updateVkImportStatus(user.getIdentifier(), stat, fetchedAlbums, UserImpl.USER_TRACK_VK_IMPORT_STATUS.COMPLETE);

        userModelService.updateVkImportButtonVisibilitySetInvisible(user.getIdentifier());


        createAlbums(user);

        int d = 1;
    }

    private void createAlbums(User user) {
        Object objAlbums = JSONValue.parse(fetchedAlbums);
        JSONArray itemsAlbumsArray = (JSONArray)((JSONObject) objAlbums).get("response");


        //JSONArray itemsAlbums = (JSONArray) ((JSONObject) itemsAlbumsObject).get("items");

        for (int i = 1; i < itemsAlbumsArray.size(); i++) {
            JSONObject jsonObject = (JSONObject) itemsAlbumsArray.get(i);
            Long albumId = (Long)(jsonObject.get("album_id"));
            Long albumOwnerId = (Long)(jsonObject.get("owner_id"));
            String albumTitle = (String)(jsonObject.get("title"));

            muzekAlbumsService.create(MuzekAlbumsImpl.ALBUM_SOURCE.VK, user.getIdentifier(), user.getMuzekFBid(),
                    albumId, albumOwnerId, albumTitle, null, null);

            String tracksByAlbum = fetchTracksFromVKByAlbum(albumId);

            Object obj = JSONValue.parse(tracksByAlbum);
            JSONArray items = (JSONArray) ((JSONObject) obj).get("response");

            List<MuzekTrackList> tracks = new ArrayList<MuzekTrackList>();

            for (int j = 0; j < items.size(); j++) {
                JSONObject jsonObjectTrack = (JSONObject) items.get(j);
                Long vk_track_id = (Long)(jsonObjectTrack.get("aid"));

                MuzekTrackList trackWithId = new MuzekTrackListImpl();
                trackWithId.setVkTrackId(vk_track_id);
                trackWithId.setVkAlbumId(albumId);
                tracks.add(trackWithId);

            }

            muzekTrackListService.setAlbumId(tracks, user.getIdentifier());

        }
    }

    public String fetchTracksFromVK() {
        return sendRequestHTTPS(host, uri);
    }

    public String fetchTracksFromVK(String captcha_sid, String captcha_key) {
        String addParams = "&captcha_sid=" + captcha_sid + "&captcha_key=" + captcha_key;
        return sendRequestHTTPS(host, uri + addParams);
    }

    public String fetchAlbumsFromVK() {
        String uri = uri_stub_albums + accessToken;
        String fetchedAlbumsStr =  sendRequestHTTPS(host, uri);
        System.out.println("fetched albums - " + fetchedAlbumsStr);
        return fetchedAlbumsStr;
    }

    public String fetchTracksFromVKByAlbum(Long albumId) {
        return sendRequestHTTPS(host, uri + "&album_id=" + albumId);
    }

    public  MuzekTrackDto getTrackFromYoutube(String searchString, String userId) {
        String originalSearchString = searchString;


        String requestResult = muzekQueryCacheService.findRequest(originalSearchString, userId);

        if (requestResult == null){

            if (searchString != null) {
                searchString = URLEncoder.encode(searchString);
            }

            String requestString = "/youtube/v3/search?key=AIzaSyA2FmomPU_dYP7B82qYGwVgjnK6u2aCxFE&part=snippet&type=video&videoCategoryId=10&q=" + searchString;
            requestResult = sendRequestHTTPS("www.googleapis.com", requestString);

            try{
                muzekQueryCacheService.create(originalSearchString, requestResult, MuzekQueryCacheImpl.QUERY_TYPE.YOU_TUBE_IMPORT);
            }catch (Throwable e){

            }
        }

        Object obj = JSONValue.parse(requestResult);
        JSONArray items = (JSONArray) ((JSONObject) obj).get("items");
        String nextPageToken = (String) ((JSONObject) obj).get("nextPageToken");

        //ArrayList<MuzekTrackDto> dtoList = new ArrayList<MuzekTrackDto>();
        for (int i = 0; i < items.size(); i++) {
            MuzekTrackDto trackDto = new MuzekTrackDto();
            trackDto.setType("youtube");
            trackDto.setStream_url("youtube");

            JSONObject jsonObject = (JSONObject) items.get(i);
            trackDto.setTitle((String) ((JSONObject) (jsonObject.get("snippet"))).get("title"));
            trackDto.setId((String) ((JSONObject) (jsonObject.get("id"))).get("videoId"));
            trackDto.setArtwork_url((String) ((JSONObject) ((JSONObject) ((JSONObject) (jsonObject.get("snippet"))).get("thumbnails")).get("default")).get("url"));

            MuzekTrackUserDto user = new MuzekTrackUserDto();
            user.setUsername((String) ((JSONObject) (jsonObject.get("snippet"))).get("channelTitle"));
            trackDto.setUser(user);


            return trackDto;
        }
        return null;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        uri = uri_stub + accessToken;
    }


    public String sendRequestHTTPS(String host, String uri) {

        try{
            return sendRequestHTTPSpure(host, uri);
        }catch(Throwable e1){
            e1.printStackTrace();
            try{
                return sendRequestHTTPSpure(host, uri);
            }catch(Throwable e2){
                e2.printStackTrace();
                try{
                    return sendRequestHTTPSpure(host, uri);
                }catch(Throwable e3){
                    e3.printStackTrace();
                }
            }
        }
        return null;
    }

    public String sendRequestHTTPSpure(String host, String uri) {

        Service<HttpRequest, HttpResponse> client = null;

        System.out.println("Client creating: " + Thread.currentThread().getName());
        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                        //  .hosts("78.108.83.148:3128")

                        //.hosts(proxyDbContainer.getProxyDb().getIp())
//                .hosts(host + ":80")
                .hosts(host + ":443")
                .tls(host)
                .retries(1)
                .hostConnectionLimit(1)
        );

        int readingTries = 0;
        String pageUri = uri;

        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, pageUri);   // 2
        request.setHeader(HttpHeaders.Names.HOST, host);
//        request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
//        request.setHeader(HttpHeaders.Names.CACHE_CONTROL, "max-age=0");
//        request.setHeader(HttpHeaders.Names.ACCEPT, "ext/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//        request.setHeader(HttpHeaders.Names.USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36");
//        request.setHeader(HttpHeaders.Names.ACCEPT_ENCODING, "gzip,deflate,sdch");
//        request.setHeader(HttpHeaders.Names.ACCEPT_LANGUAGE, "en-US,en;q=0.8,ru;q=0.6,uk;q=0.4");

        HttpResponse response = client.apply(request).apply();

        if (response.getStatus().getCode() == 200) {
            //byte[] array = response.getContent().array();
            String encoding = "utf8";//"cp1251";//EncodingDeterminer.getEncoding(array);
            String result = response.getContent().toString(Charset.forName("utf8"));

            return result;

        } else {
            System.out.println("==== Missing URI: " + pageUri);

        }
        return null;
    }

    public UserModelService getUserModelService() {
        return userModelService;
    }

    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }

    public MuzekTrackListService getMuzekTrackListService() {
        return muzekTrackListService;
    }

    public void setMuzekTrackListService(MuzekTrackListService muzekTrackListService) {
        this.muzekTrackListService = muzekTrackListService;
    }

    public void setUserFBid(String userFBid) {
        this.userFBid = userFBid;
    }

    public String getRequestResult() {
        return requestResult;
    }

    public void setRequestResult(String requestResult) {
        this.requestResult = requestResult;
    }

    public void setFetchedAlbums(String fetchedAlbums) {
        this.fetchedAlbums = fetchedAlbums;
    }

    public void setMuzekAlbumsService(MuzekAlbumsService muzekAlbumsService) {
        this.muzekAlbumsService = muzekAlbumsService;
    }

    public void setMuzekQueryCacheService(MuzekQueryCacheService muzekQueryCacheService) {
        this.muzekQueryCacheService = muzekQueryCacheService;
    }
}
