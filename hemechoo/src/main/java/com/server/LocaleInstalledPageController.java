package com.server;

import com.shop.web.service.LocatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
//@RequestMapping(value = "/files/prices")
public class LocaleInstalledPageController implements org.springframework.web.servlet.mvc.Controller {


    LocatorService locatorService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {


        String ip = request.getRemoteAddr();
        String country = locatorService.getCountry(ip);

        RequestDispatcher view = request.getRequestDispatcher("/installedEn.html");

        if (country == null){
            view.forward(request, response);
            return null;
        }

        if (country.equals("UA") || country.equals("RU")){
            view = request.getRequestDispatcher("/installedRu.html");
            view.forward(request, response);
            return null;
        }

        view.forward(request, response);
        return null;
    }


    @Autowired
    public void setLocatorService(LocatorService locatorService) {
        this.locatorService = locatorService;
    }
}
