package com.server;

import com.client.util.ClientCache;
import com.shop.model.form.CaptchaModel;
import com.shop.model.service.ActivityLogModelService;
import com.shop.model.service.MuzekAlbumsService;
import com.shop.model.service.MuzekTrackListService;
import com.shop.model.service.UserModelService;
import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.util.Duration;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.handler.codec.http.*;
import org.jboss.netty.util.CharsetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Controller
//@RequestMapping(value = "/files/prices")
public class YouTubeImporterController implements org.springframework.web.servlet.mvc.Controller {

    UserModelService userModelService;
    MuzekTrackListService muzekTrackListService;
    MuzekAlbumsService muzekAlbumsService;
    ActivityLogModelService activityLogModelService;


    String CLIENT_ID = "436845206820-b7hhgv0086dmoitgl2hpfuekkhqldobc.apps.googleusercontent.com";
    String CLIENT_SECRET = "KTmJYPkjvi-15S1n4swgZ8vd";
    //    String REDIRECT_URI = "http://localhost:8888/vk/";
//    String REDIRECT_URI = "http://localhost:8888/vk/";
//    String REDIRECT_URI = "http://muzekbox.com/vk/";
    String REDIRECT_URI = ClientCache.get().hostUrl + "/youtube/";
    String HOST = "oauth.vk.com";

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {

            RequestDispatcher view = request.getRequestDispatcher("/html/youtubeauthorized.html");

            String userId = request.getParameter("state");
            String code = request.getParameter("code");

            activityLogModelService.createHemechooLog("MUZEK_vk_import_1", userId, null, null, null, null);

            Map<String, String> params = new HashMap<String, String>();
            params.put("client_id", CLIENT_ID);
            params.put("client_secret", CLIENT_SECRET);
            params.put("code", code);
            params.put("redirect_uri", REDIRECT_URI);
            params.put("grant_type", "authorization_code");

//            String requestString = "/o/oauth2/token?" +
//                    "client_id=" + CLIENT_ID + "&" +
//                    "client_secret=" + CLIENT_SECRET + "&" +
//                    "code=" + code + "&" +
//                    "redirect_uri=" + REDIRECT_URI + "&" +
//                    "grant_type=authorization_code";
//
//            System.out.println(requestString);

            String requestResult = null;

            try {
                requestResult = sendRequestHTTPS_POST("accounts.google.com", "/o/oauth2/token", params);
            } catch (Throwable e1) {
                e1.printStackTrace();
                System.out.println("sleep_1");
                Thread.sleep(1000);
                try {
                    requestResult = sendRequestHTTPS_POST("accounts.google.com", "/o/oauth2/token", params);
                } catch (Throwable e2) {

                    e2.printStackTrace();
                    System.out.println("sleep_2");
                    Thread.sleep(1000);
                    try {
                        requestResult = sendRequestHTTPS_POST("accounts.google.com", "/o/oauth2/token", params);
                    } catch (Throwable e3) {
                        e3.printStackTrace();
                        System.out.println("sleep_3");
                        Thread.sleep(1000);
                        try {
                            requestResult = sendRequestHTTPS_POST("accounts.google.com", "/o/oauth2/token", params);
                        } catch (Throwable e4) {
                            e3.printStackTrace();
                        }
                    }
                }

            }

            if (requestResult != null && requestResult.contains("access_token")) {
                String access_token = requestResult.split("\"")[3];

                System.out.println("access_token " + access_token);
                activityLogModelService.createHemechooLog("MUZEK_youtube_import_2_access_token", userId, null, null, null, null);

                YouTubeMusicImporter youtubeMusicImporter = new YouTubeMusicImporter();

                youtubeMusicImporter.setAccessToken(access_token);
                youtubeMusicImporter.setMuzekTrackListService(muzekTrackListService);
                youtubeMusicImporter.setMuzekAlbumsService(muzekAlbumsService);
                youtubeMusicImporter.setUserModelService(userModelService);
                youtubeMusicImporter.setUserFBid(userId);


                    //String fetchedTracks = youtubeMusicImporter.getPlaylists();
                   // youtubeMusicImporter.setRequestResult(fetchedTracks);
//
//                    System.out.println("fetched tracks 1 " + fetchedTracks);
//
//                    String fetchedAlbums = youtubeMusicImporter.fetchAlbumsFromVK();
//                    youtubeMusicImporter.setFetchedAlbums(fetchedAlbums);

                Thread thread = new Thread(youtubeMusicImporter);
                thread.start();

            } else {
                view = request.getRequestDispatcher("/html/vkerror.html");
                view.forward(request, response);
                return null;
            }


            view = request.getRequestDispatcher("/html/youtubeauthorized.html");
            view.forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    String sendRequestHTTPS(String host, String uri) {

        Service<HttpRequest, HttpResponse> client = null;

        System.out.println("Client creating: " + Thread.currentThread().getName());
        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                .hosts(host + ":443")
                .tls(host)
                .retries(3)
                .hostConnectionLimit(1)
        );

        String pageUri = uri;

        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, pageUri);   // 2
        request.setHeader(HttpHeaders.Names.HOST, host);

        HttpResponse response = client.apply(request).apply();

        if (response.getStatus().getCode() == 200) {
            String result = response.getContent().toString(Charset.forName("utf8"));
            return result;
        } else {
            System.out.println("==== Missing URI: " + pageUri);
        }
        return null;
    }

    String sendRequestHTTPS_POST(String host, String uri, Map<String, String> map) {

        String result = null;
        Service<HttpRequest, HttpResponse> client = null;

        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                .hosts(host + ":443")
                .tls(host)
                .retries(3)
                .hostConnectionLimit(1)
        );

        int readingTries = 0;


        try {

            String data = "";
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                data += "&" + key + "=" + URLEncoder.encode(value == null ? "" : value, "UTF-8");
            }

            String pageUri = uri;

            HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, pageUri);   // 2

            request.setHeader(HttpHeaders.Names.HOST, host);
            request.setHeader(HttpHeaders.Names.CONTENT_TYPE, "application/x-www-form-urlencoded");

            ChannelBuffer buffer = ChannelBuffers.copiedBuffer(data, CharsetUtil.UTF_8);
            //ChannelBuffer buffer = ChannelBuffers.copiedBuffer(data, Charset.defaultCharset());
            request.addHeader(HttpHeaders.Names.CONTENT_LENGTH, buffer.readableBytes());
            request.setContent(buffer);


            HttpResponse response = client.apply(request).apply();


            byte[] array = response.getContent().array();
            String encoding = "UTF-8";//EncodingDeterminer.getEncoding(array);
            try {

                result = new String(array, encoding);

                //System.out.println(result);

            } catch (UnsupportedEncodingException ex) {

                ex.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        } catch (Exception e1) {
            //productModelService.updateProductHemechooStatus(productId, ProductImpl.ROZETKA_SCAN_STATUS.ERROR);
            e1.printStackTrace();
        }
        return result;

    }


    @Autowired
    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }

    @Autowired
    public void setMuzekTrackListService(MuzekTrackListService muzekTrackListService) {
        this.muzekTrackListService = muzekTrackListService;
    }

    @Autowired
    public void setActivityLogModelService(ActivityLogModelService activityLogModelService) {
        this.activityLogModelService = activityLogModelService;
    }

    @Autowired
    public void setMuzekAlbumsService(MuzekAlbumsService muzekAlbumsService) {
        this.muzekAlbumsService = muzekAlbumsService;
    }
}
