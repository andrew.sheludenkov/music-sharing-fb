package com.server;

import com.client.util.ClientCache;
import com.shop.model.service.ActivityLogModelService;
import com.shop.model.service.MuzekAlbumsService;
import com.shop.model.service.MuzekTrackListService;
import com.shop.model.service.UserModelService;
import com.shop.web.service.LocatorService;
import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.util.Duration;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.handler.codec.http.*;
import org.jboss.netty.util.CharsetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;


@Controller
//@RequestMapping(value = "/files/prices")
public class LocaleLandingController implements org.springframework.web.servlet.mvc.Controller {


    LocatorService locatorService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {


        String ip = request.getRemoteAddr();
        String country = locatorService.getCountry(ip);

        RequestDispatcher view = request.getRequestDispatcher("/MuzekPageEn.html");

        if (country == null){
            view.forward(request, response);
            return null;
        }

        if (country.equals("UA") || country.equals("RU")){
            view = request.getRequestDispatcher("/MuzekPageRu.html");
            view.forward(request, response);
            return null;
        }

        view.forward(request, response);
        return null;
    }


    @Autowired
    public void setLocatorService(LocatorService locatorService) {
        this.locatorService = locatorService;
    }
}
