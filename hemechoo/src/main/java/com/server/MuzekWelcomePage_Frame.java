package com.server;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
//@RequestMapping(value = "/files/prices")
public class MuzekWelcomePage_Frame implements org.springframework.web.servlet.mvc.Controller {


    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String uti = request.getRequestURI();

//        RequestDispatcher view = request.getRequestDispatcher("/BetaList.html");
        RequestDispatcher view = request.getRequestDispatcher("/MuzekPageRu.html");
        // don't add your web-app name to the path

        view.forward(request, response);

        return null;
    }


}
