package com.server;

import com.client.util.ClientCache;
import com.shop.model.form.CaptchaModel;
import com.shop.model.service.*;
import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.util.Duration;
import org.jboss.netty.handler.codec.http.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.Charset;


@Controller
//@RequestMapping(value = "/files/prices")
public class VkImporterController implements org.springframework.web.servlet.mvc.Controller {

    UserModelService userModelService;
    MuzekTrackListService muzekTrackListService;
    MuzekAlbumsService muzekAlbumsService;
    ActivityLogModelService activityLogModelService;
    MuzekQueryCacheService muzekQueryCacheService;


    String APP_ID = "4722812";
    String APP_SECRET = "vT6dOyYH40xmKApz8R2n";
    //    String REDIRECT_URI = "http://localhost:8888/vk/";
//    String REDIRECT_URI = "http://localhost:8888/vk/";
//    String REDIRECT_URI = "http://muzekbox.com/vk/";
    String REDIRECT_URI = ClientCache.get().hostUrl + "/vk/";
    String HOST = "oauth.vk.com";

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {

            RequestDispatcher view = request.getRequestDispatcher("/html/vkauthorized.html");

            String userId = getUserId(request);
            String captcha_text = request.getParameter("captcha_text");

            if (captcha_text != null && captcha_text.length() > 0) {

                activityLogModelService.createHemechooLog("MUZEK_vk_import_2_captcha", userId, null, null, null, null);

                String access_token = request.getParameter("access_token");
                String captcha_sid = request.getParameter("captcha_sid");

                System.out.println("access_token from captcha page" + access_token);

                VkMusicImporter vkMusicImporter = new VkMusicImporter();

                vkMusicImporter.setAccessToken(access_token);
                vkMusicImporter.setMuzekTrackListService(muzekTrackListService);
                vkMusicImporter.setMuzekAlbumsService(muzekAlbumsService);
                vkMusicImporter.setUserModelService(userModelService);
                vkMusicImporter.setMuzekQueryCacheService(muzekQueryCacheService);
                vkMusicImporter.setUserFBid(userId);


                String fetchedTracks = vkMusicImporter.fetchTracksFromVK(captcha_sid, captcha_text);
                vkMusicImporter.setRequestResult(fetchedTracks);
                System.out.println("fetched tracks 2 " + fetchedTracks);

                if (fetchedTracks.contains("error_code\":14")) {
//                if (fetchedTracks.contains("Captcha needed")) {
                    System.out.println("error_code_14");
                    //fetchedTracks = "{\"error\":{\"error_code\":14,\"error_msg\":\"Captcha needed\",\"request_params\":[{\"key\":\"oauth\",\"value\":\"1\"},{\"key\":\"method\",\"value\":\"captcha.force\"},{\"key\":\"uids\",\"value\":\"66748\"},{\"key\":\"access_token\",\"value\":\"b9b5151856dcc745d785a6b604295d30888a827a37763198888d8b7f5271a4d8a049fefbaeed791b2882\"}],\"captcha_sid\":\"239633676097\",\"captcha_img\":\"http:\\/\\/api.vk.com\\/captcha.php?sid=239633676097&s=1\"}} ";
                    return enterCaptcha(fetchedTracks, access_token, view, request);
                }

                String fetchedAlbums = vkMusicImporter.fetchAlbumsFromVK();

                vkMusicImporter.setFetchedAlbums(fetchedAlbums);

                Thread thread = new Thread(vkMusicImporter);
                thread.start();
            } else {

                activityLogModelService.createHemechooLog("MUZEK_vk_import_1", userId, null, null, null, null);

                String code = request.getParameter("code");


                String requestString = "/access_token/?" +
                        "client_id=" + APP_ID + "&" +
                        "client_secret=" + APP_SECRET + "&" +
                        "code=" + code + "&" +
                        "redirect_uri=" + REDIRECT_URI;

                System.out.println(requestString);

                String requestResult = null;

                try {
                    requestResult = sendRequestHTTPS("oauth.vk.com", requestString);
                } catch (Throwable e1) {
                    e1.printStackTrace();
                    System.out.println("sleep_1");
                    Thread.sleep(1000);
                    try {
                        requestResult = sendRequestHTTPS("oauth.vk.com", requestString);
                    } catch (Throwable e2) {

                        e2.printStackTrace();
                        System.out.println("sleep_2");
                        Thread.sleep(1000);
                        try {
                            requestResult = sendRequestHTTPS("oauth.vk.com", requestString);
                        } catch (Throwable e3) {
                            e3.printStackTrace();
                            System.out.println("sleep_3");
                            Thread.sleep(1000);
                            try {
                                requestResult = sendRequestHTTPS("oauth.vk.com", requestString);
                            } catch (Throwable e4) {
                                e3.printStackTrace();
                            }
                        }
                    }
                }


                if (requestResult != null && requestResult.contains("access_token")) {
                    String access_token = requestResult.split("\"")[3];

                    System.out.println("access_token " + access_token);
                    activityLogModelService.createHemechooLog("MUZEK_vk_import_2_access_token", userId, null, null, null, null);

                    VkMusicImporter vkMusicImporter = new VkMusicImporter();

                    vkMusicImporter.setAccessToken(access_token);
                    vkMusicImporter.setMuzekTrackListService(muzekTrackListService);
                    vkMusicImporter.setMuzekAlbumsService(muzekAlbumsService);
                    vkMusicImporter.setUserModelService(userModelService);
                    vkMusicImporter.setUserFBid(userId);


                    String fetchedTracks = vkMusicImporter.fetchTracksFromVK();
                    vkMusicImporter.setRequestResult(fetchedTracks);

                    System.out.println("fetched tracks 1 " + fetchedTracks);

//                    if (fetchedTracks.contains("error")) {
//                    if (fetchedTracks.contains("Captcha needed")) {
                    if (fetchedTracks.contains("error_code\":14")) {
                        System.out.println("error_code_14_1");
                        //fetchedTracks = "{\"error\":{\"error_code\":14,\"error_msg\":\"Captcha needed\",\"request_params\":[{\"key\":\"oauth\",\"value\":\"1\"},{\"key\":\"method\",\"value\":\"captcha.force\"},{\"key\":\"uids\",\"value\":\"66748\"},{\"key\":\"access_token\",\"value\":\"b9b5151856dcc745d785a6b604295d30888a827a37763198888d8b7f5271a4d8a049fefbaeed791b2882\"}],\"captcha_sid\":\"239633676097\",\"captcha_img\":\"http:\\/\\/api.vk.com\\/captcha.php?sid=239633676097&s=1\"}} ";
                        return enterCaptcha(fetchedTracks, access_token, view, request);
                    }

                    String fetchedAlbums = vkMusicImporter.fetchAlbumsFromVK();
                    vkMusicImporter.setFetchedAlbums(fetchedAlbums);

                    Thread thread = new Thread(vkMusicImporter);
                    thread.start();

                } else {
                    view = request.getRequestDispatcher("/html/vkerror.html");
                    view.forward(request, response);
                    return null;
                }

            }

            view = request.getRequestDispatcher("/html/vkauthorized.html");
            view.forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private ModelAndView enterCaptcha(String value, String access_token, RequestDispatcher view, HttpServletRequest request) {

        try {
            request.setCharacterEncoding("UTF-8");

            System.out.println("value1=" + value);

            String wor = value.split("captcha_sid\":\"")[1];
            wor = wor.split("\"")[0];

            String captcha_sid = wor;
            String url = ClientCache.get().hostUrl + "/vk/?captcha_sid=" + captcha_sid + "&access_token=" + access_token;

            CaptchaModel captchaModel = new CaptchaModel();
            captchaModel.setAccessToken(access_token);
            captchaModel.setImgUrl("https://api.vk.com/captcha.php?sid=" + captcha_sid);
            captchaModel.setAccessToken(access_token);
            captchaModel.setCaptchaSid(captcha_sid);
            captchaModel.setPostUrl(url);


            return new ModelAndView("captcha", "model", captchaModel);


        } catch (Throwable e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }


    String sendRequestHTTPS(String host, String uri) {

        Service<HttpRequest, HttpResponse> client = null;

        System.out.println("Client creating: " + Thread.currentThread().getName());
        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                .hosts(host + ":443")
                .tls(host)
                .retries(3)
                .hostConnectionLimit(1)
        );

        String pageUri = uri;

        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, pageUri);   // 2
        request.setHeader(HttpHeaders.Names.HOST, host);

        HttpResponse response = client.apply(request).apply();

        if (response.getStatus().getCode() == 200) {
            String result = response.getContent().toString(Charset.forName("utf8"));
            return result;
        } else {
            System.out.println("==== Missing URI: " + pageUri);
        }
        return null;
    }


    private String getUserId(HttpServletRequest request) {

        javax.servlet.http.Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (javax.servlet.http.Cookie cookie : cookies) {
                if (cookie.getName().equals("muzek_user_id")) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    @Autowired
    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }

    @Autowired
    public void setMuzekTrackListService(MuzekTrackListService muzekTrackListService) {
        this.muzekTrackListService = muzekTrackListService;
    }

    @Autowired
    public void setActivityLogModelService(ActivityLogModelService activityLogModelService) {
        this.activityLogModelService = activityLogModelService;
    }

    @Autowired
    public void setMuzekAlbumsService(MuzekAlbumsService muzekAlbumsService) {
        this.muzekAlbumsService = muzekAlbumsService;
    }

    @Autowired
    public void setMuzekQueryCacheService(MuzekQueryCacheService muzekQueryCacheService) {
        this.muzekQueryCacheService = muzekQueryCacheService;
    }
}
