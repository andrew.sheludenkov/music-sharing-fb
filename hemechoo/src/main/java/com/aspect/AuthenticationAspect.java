package com.aspect;

import com.client.exception.NotLoggedInException;
import com.shop.model.auth.AuthenticatedUserHolder;
import com.shop.model.persistence.User;
import com.shop.model.service.UserModelService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Aspect
public class AuthenticationAspect {
    private AuthenticatedUserHolder authenticatedUserHolder;
    private UserModelService userModelService;
    private static final String loginCookieName = "loginHash";

    private HttpServletRequest request;

    public void checkAuthentication(JoinPoint joinPoint) throws NotLoggedInException {
        if (joinPoint != null && joinPoint.getSignature().getName().equalsIgnoreCase("handleRequest")) {
            return;
        }

        if (!authenticatedUserHolder.isLoggedIn()) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals(loginCookieName)) {
                    User user = userModelService.findUserByLoginHash(cookie.getValue());
                    if (user != null) {
                        authenticatedUserHolder.loginUser(user);
                        return;
                    }
                }
            }
            throw new NotLoggedInException();
        }
    }


    public void checkAuthentication() {
        if (!authenticatedUserHolder.isLoggedIn()) {
            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals(loginCookieName)) {
                        User user = userModelService.findUserByLoginHash(cookie.getValue());
                        if (user != null) {
                            authenticatedUserHolder.loginUser(user);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void initRequest(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
    }

    public void setAuthenticatedUserHolder(AuthenticatedUserHolder authenticatedUserHolder) {
        this.authenticatedUserHolder = authenticatedUserHolder;
    }

    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }
}
