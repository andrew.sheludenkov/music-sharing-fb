package com.shop.web;

import com.shop.web.pages.FindPage;
import com.shop.web.pages.MuzekPage;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.request.http.WebResponse;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.format.datetime.DateFormatter;

import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 22.01.13
 */
public class WicketApplication extends WebApplication {

    public WicketApplication() {
    }

    @Override
    protected void init() {
        super.init();
        getComponentInstantiationListeners().add(new SpringComponentInjector(this));


        mountPage("/find", FindPage.class);


    }

    @Override
    public Class<? extends Page> getHomePage() {
//        return MainPage.class;
        return MuzekPage.class;
    }

    @Override
    protected WebResponse newWebResponse(WebRequest webRequest, HttpServletResponse httpServletResponse) {

        //Fri, 20 Jul 2012 10:04:29GMT
        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date date = calendar.getTime();
        DateFormatter format = new DateFormatter("EEE, dd MMM yyyy HH:mm:ss zzz");
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        format.setTimeZone(timeZone);
        Locale locale = new Locale("en");
        String dateString = format.print(date, locale);

        httpServletResponse.setHeader("Last-Modified", dateString);

        return super.newWebResponse(webRequest, httpServletResponse);
    }
}
