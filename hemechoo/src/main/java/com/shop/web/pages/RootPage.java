package com.shop.web.pages;

import com.shop.web.service.SimpleService;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 22.01.13
 */
public class RootPage extends WebPage {
    protected static final String META_DESCRIPTION_PATTERN = "<meta name=\"description\" content=\"{0}\"/>";
    protected static final String META_KEYWORDS_PATTERN = "<meta name=\"keywords\" content=\"{0}\"/>";
    protected static final String TITLE_PATTERN = "<title>{0}</title>";

    @SpringBean
    protected SimpleService simpleService;


    protected Panel leftBlock = new EmptyPanel("leftBlock");

    public RootPage() {

    }

    @Override
    protected void onModelChanged() {
        super.onModelChanged();
        removeAll();
        init();
    }

    protected void init() {

        leftBlock.setVisible(false);
        add(leftBlock);



        // title and meta
//        add(new Label("title", getPageTitle()));
//
//        WebComponent keywords = new WebComponent("metaKeywords");
//        keywords.add(new AttributeAppender("content", getMetaKeywords()));
//        add(keywords);
//
//        WebComponent description = new WebComponent("metaDescription");
//        description.add(new AttributeAppender("content", getMetaDescription()));
//        add(description);
//
//        // twitter
//        WebComponent twitterUrl = new WebComponent("twitterUrl");
//        twitterUrl.add(new AttributeAppender("content", getTwitterUrl()));
//        add(twitterUrl);
//
//        WebComponent twitterTitle = new WebComponent("twitterTitle");
//        twitterTitle.add(new AttributeAppender("content", getTwitterTitle()));
//        add(twitterTitle);
//
//        WebComponent twitterDescription = new WebComponent("twitterDescription");
//        twitterDescription.add(new AttributeAppender("content", getTwitterDescription()));
//        add(twitterDescription);
//
//        WebComponent twitterImage = new WebComponent("twitterImage");
//        twitterImage.add(new AttributeAppender("content", getTwitterImage()));
//        add(twitterImage);
//
//        ((WebResponse) getRequestCycle().getResponse()).addCookie(new Cookie("affiliate", ""));
    }

//    @Override
//    public void renderHead(IHeaderResponse response) {
//
//
//        response.renderString(getPageTitle());
//        response.renderString(getMetaDescription());
//        response.renderString(getMetaKeywords());
//        super.renderHead(response);
//    }

    protected Map<String, String> getBreadcrumbs(){
        LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        String path = getRequest().getContextPath();
        result.put("Главная", StringUtils.hasLength(path) ? path : "/");
        return result;
    }

    protected String getMetaDescription() {
        return "Affiliate Links";
    }

    protected String getMetaKeywords() {
        return "Affiliate Links, affiliate program";
    }

    public String getPageTitle() {
        return "Affiliate Links";
    }

    // twitter
    public String getTwitterUrl() {
        return "";
    }

    public String getTwitterTitle() {
        return "Easy affiliate links";
    }

    public String getTwitterDescription() {
        return "Linqs.co start monetize";
    }

    public String getTwitterImage() {
        return "";
    }
}