package com.shop.web.pages;

import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.service.MuzekTrackListService;
import com.shop.web.service.SimpleService;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.markup.html.basic.Label;

import javax.servlet.http.HttpServletRequest;


public class MuzekPage extends WebPage {

    @SpringBean
    protected SimpleService simpleService;

    @SpringBean
    protected MuzekTrackListService muzekTrackListService;


    public MuzekPage() {
        init();
    }

    @Override
    protected void onModelChanged() {
        this.setEscapeModelStrings(false);
        super.onModelChanged();
        removeAll();
        init();

    }

    protected void init() {


        HttpServletRequest request = (HttpServletRequest)
                getRequest().getContainerRequest();

//        String img = request.getParameter("img");
        String videoId = request.getParameter("v");
        String country = request.getParameter("loc");
//        String trackTitle = request.getParameter("t");


        MuzekTrackList track = muzekTrackListService.findTrackByTrackId(videoId);
        String img = track.getArtworkUrl().replaceAll("default", "hqdefault");


        String trackTitle = track.getName();


        WebComponent meta0 = new WebComponent("meta0");
        meta0.add(new AttributeAppender("content", trackTitle));
        add(meta0);

        WebComponent meta1 = new WebComponent("meta1");
        meta1.add(new AttributeAppender("content", img));
        add(meta1);

        WebComponent meta2 = new WebComponent("meta2");
        meta2.add(new AttributeAppender("content", "https://www.youtube.com/embed/" + videoId));
        add(meta2);

        WebComponent meta3 = new WebComponent("meta3");
        meta3.add(new AttributeAppender("content", "https://www.youtube.com/embed/" + videoId));
        add(meta3);

        WebComponent meta4 = new WebComponent("meta4");
        meta4.add(new AttributeAppender("content", "http://www.youtube.com/v/" + videoId));
        add(meta4);

        WebComponent meta5 = new WebComponent("meta5");
        meta5.add(new AttributeAppender("content", "https://www.youtube.com/v/" + videoId));
        add(meta5);


        WebComponent meta6 = new WebComponent("meta6");

        if (country == null) {
            meta6.add(new AttributeAppender("content", "► Установите расширение MuzekBox для вашего браузера, чтобы слушать музыку на странице Facebook, http://muzekbox.com"));
            add(new Label("p1", "Установите расширение для браузера и слушайте музыку на своей Facebook странице"));
            add(new Label("p2", "1. Легко ищите новую музыку"));
            add(new Label("p3", "2. Слушайте музыку прямо в Facebook"));
            add(new Label("p4", "3. Делитесь музыкой с друзьями"));
            add(new Label("header1", "Музыка в Facebook"));
            add(new Label("a0", "Установить на Chrome"));
            add(new Label("a1", "Установить на Opera"));
            add(new Label("a2", "Установить на Firefox"));
            add(new Label("a3", "Установить на Safari"));
        } else if (country.equals("UA") || country.equals("RU")) {
            meta6.add(new AttributeAppender("content", "► Установите расширение MuzekBox для вашего браузера, чтобы слушать музыку на странице Facebook, http://muzekbox.com"));
            add(new Label("p1", "Установите расширение для браузера и слушайте музыку на своей Facebook странице"));
            add(new Label("p2", "1. Легко ищите новую музыку"));
            add(new Label("p3", "2. Слушайте музыку прямо в Facebook"));
            add(new Label("p4", "3. Делитесь музыкой с друзьями"));
            add(new Label("header1", "Музыка в Facebook"));
            add(new Label("a0", "Установить на Chrome"));
            add(new Label("a1", "Установить на Opera"));
            add(new Label("a2", "Установить на Firefox"));
            add(new Label("a3", "Установить на Safari"));

        } else {
            meta6.add(new AttributeAppender("content", "► Install extension MuzekBox for your web-browser to start listen to music on Facebook page, http://muzekbox.com"));
            add(new Label("p1", "Install extension for web-browser and listen to music on your Facebook page"));
            add(new Label("p2", "1. Easily find new music"));
            add(new Label("p3", "2. Listen to music directly on the Facebook"));
            add(new Label("p4", "3. Share music with friends"));
            add(new Label("header1", "Music on Facebook"));
            add(new Label("a0", "Install on Chrome"));
            add(new Label("a1", "Install on Opera"));
            add(new Label("a2", "Install on Firefox"));
            add(new Label("a3", "Install on Safari"));
        }

        add(meta6);


    }


}
