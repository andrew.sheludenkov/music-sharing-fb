package com.shop.web.service;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 22.05.12
 */
public class SimpleService {
    private Integer currentPage = 1;

    public String getSimpleMassage(){
        return "Simple message";
    }

    public String getPagedMessage(Integer pageNumber){
        return "You are on " + pageNumber.toString() + " page";
    }

    public Integer getCurrentPage(){
        return currentPage;
    }

    public Integer nextPage(){
        currentPage++;
        return currentPage;
    }

    public Integer previousPage(){
        if(currentPage > 1){
            currentPage--;
        }

        return currentPage;
    }
}
