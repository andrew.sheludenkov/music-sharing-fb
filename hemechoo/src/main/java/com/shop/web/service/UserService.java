package com.shop.web.service;


import com.shop.model.persistence.User;
import com.shop.model.service.UserModelService;


public class UserService {

    UserModelService userModelService;


    public User createUser(User user, String sessionId) {

        user.setSessionId(sessionId);
        userModelService.createUser(user);

        return user;
    }

//    public void removeProduct(Product product, String sessionId) {
//
//        UserOrder userOrder = userOrderModelService.findBySessionId(sessionId);
//        if (userOrder == null) {
//            userOrder = userOrderModelService.createOrder(sessionId);
//        }
//
//        List<ProductImpl> products = userOrder.getProducts();
//
//        if (products == null) {
//            products = new ArrayList<ProductImpl>();
//        }
//
//        Boolean contains = false;
//        for (ProductImpl product1 : products) {
//            if (product1.getIdentifier().equals(product.getIdentifier())) {
//                products.remove(product1);
//                break;
//            }
//        }
//
//        userOrderModelService.save(userOrder);
//
//    }

//    public List<Product> getProducts(String sessionId) {
//        List<Product> products = new ArrayList<Product>();
//
//        UserOrder userOrder = userOrderModelService.findBySessionId(sessionId);
//
//        if (userOrder != null) {
//            if (userOrder.getProducts() != null) {
//                for (ProductImpl product : userOrder.getProducts()) {
//                    products.add(product);
//                }
//            }
//        } else {
//            return products;
//        }
//
//        return products;
//    }



    public UserModelService getUserModelService() {
        return userModelService;
    }

    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }


}
