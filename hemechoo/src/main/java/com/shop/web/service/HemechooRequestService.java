package com.shop.web.service;


import com.server.VkMusicImporter;
import com.shop.model.persistence.*;
import com.shop.model.persistence.impl.*;
import com.shop.model.service.*;
import com.shop.web.dto.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.util.Duration;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.jboss.netty.handler.codec.http.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


@Controller
public class HemechooRequestService implements org.springframework.web.servlet.mvc.Controller {


    private GoogleSpreadsheetService googleSpreadsheetService;



    private MuzekFriendsService muzekFriendsService;
    private MuzekTrackListService muzekTrackListService;
    private MuzekAlbumsService muzekAlbumsService;
    private MuzekFeedService muzekFeedService;
    private MuzekPeerRecordService muzekPeerRecordService;
    private MuzekQueryCacheService muzekQueryCacheService;

    ActivityLogModelService activityLogModelService;
    UserModelService userModelService;
    MuzekVkTrackService muzekVkTrackService;
    MuzekVkUserService muzekVkUserService;
    LocatorService locatorService;
    MuzekMessageService muzekMessageService;


    public enum RECORD_STATUS {
        CONNECT_INVITATION,
        INITIAL_MAIL,
        RESPONSE
    }

    enum USER_ACTIVITY_LEVEL{
        DAILY,
        THREE_DAYS,
        SEVEN_DAYS,
        MORE_THAN_SEVEN_DAYS
    }

    String colorsArr[] = {"Gold",
            "Silver",
            "SpaceGrey",
            "Blue",
            "Purple",
            "pink",
            "Green",
            "RED",
            "Slate",
            "red",
            "black",
            "gold",
            "white",
            "grey"};

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String ip = request.getRemoteAddr();

        String callback = request.getParameter("callback");
        String meth = request.getParameter("meth");

        String browserName = request.getHeader("User-Agent");


        HemechooCommentsResultDto commnetsResultDto = new HemechooCommentsResultDto();
        commnetsResultDto.setResultsFound(0);

//        ProductsResult productsResult = mainStoreService.find("nord", 0, 10, null, null, null);
//        String serialized = toString(productsResult);
        //String serialized = "ha ha ha!";


        response.setContentType("text/plain;charset=UTF-8");

        if (meth != null && meth.equals("muzek_search")) {



            String searchString = decode(request, "q");



            if (searchString != null) {
                searchString = URLEncoder.encode(searchString);
            }

            String requestString = "/tracks.json?client_id=317e92a14f3c83f40b845580e1205d61&q=" + searchString;
            requestString = requestString + "&limit=10";
            String requestResult = sendRequest("api.soundcloud.com", requestString);

//          requestResult = "[\n" +
//                  "    {\"download_url\": null, \"key_signature\": \"\", \"user_favorite\": false, \"likes_count\": 6010, \"release\": \"\", \"attachments_uri\": \"https://api.soundcloud.com/tracks/115985816/attachments\", \"waveform_url\": \"https://w1.sndcdn.com/KmufKtKB678o_m.png\", \"purchase_url\": \"https://itunes.apple.com/fr/album/recharged/id701162375?wmgref=&affId=2051893&ign-mpt=uo%3D4\", \"video_url\": null, \"artwork_url\": \"https://i1.sndcdn.com/artworks-000060468826-filwdv-large.jpg\", \"comment_count\": 312, \"commentable\": true, \"description\": \"The new LINKIN PARK album RECHARGED is out October 29th.\\r\\nPre-Order RECHARGED on iTunes: http://smarturl.it/recharged\\r\\nPre-Order RECHARGED on CD/Vinyl: http://www.linkinpark.com\\r\\n\\r\\nBecome a fan of Dirtyphonics: www.facebook.com/dirtyphonics  \\r\\nFollow Dirtyphonics on Twitter: www.twitter.com/dirtyphonics \\r\\n Subscribe to Dirtyphonics' Chanel: www.youtube.com/dirtyphonics \\r\\nFollow Dirtyphonics on Instagram: www.instagram.com/dirtyphonics\\r\\n\\r\\nLINKIN PARK: http://www.linkinpark.com\\r\\nYouTube: http://www.youtube.com/linkinparktv\\r\\nLike on Facebook: http://www.facebook.com/linkinpark\\r\\nFollow on Twitter: http://www.twitter.com/linkinpark\\r\\n\\r\\n\", \"download_count\": 0, \"downloadable\": false, \"embeddable_by\": \"all\", \"favoritings_count\": 6010, \"genre\": \"Drum & Bass\", \"isrc\": \"\", \"label_id\": null, \"label_name\": \"Warner Bros Record\", \"license\": \"all-rights-reserved\", \"original_content_size\": 11741571, \"original_format\": \"mp3\", \"playback_count\": 225133, \"purchase_title\": null, \"release_day\": 29, \"release_month\": 10, \"release_year\": 2013, \"reposts_count\": 1600, \"state\": \"finished\", \"streamable\": true, \"tag_list\": \"\\\"Likin Park\\\" \\\"Drum & Bass\\\" \\\"Bass Music\\\" Drumstep Dubstep Electronic Edm Heavy Bass Rock Remix Recharged LP\", \"track_type\": \"remix\", \"user\": {\"avatar_url\": \"https://i1.sndcdn.com/avatars-000120175435-xnj5x7-large.jpg\", \"id\": 1589064, \"kind\": \"user\", \"permalink_url\": \"http://soundcloud.com/dirtyphonics\", \"uri\": \"https://api.soundcloud.com/users/1589064\", \"username\": \"Dirtyphonics\", \"permalink\": \"dirtyphonics\", \"last_modified\": \"2015/04/28 18:21:02 +0000\"}, \"bpm\": null, \"user_playback_count\": null, \"id\": 115985816, \"kind\": \"track\", \"created_at\": \"2013/10/18 20:34:12 +0000\", \"last_modified\": \"2015/06/08 10:53:13 +0000\", \"permalink\": \"lies-greed-misery-remix\", \"permalink_url\": \"https://soundcloud.com/dirtyphonics/lies-greed-misery-remix\", \"title\": \"Linkin Park - Lies Greed Misery (Dirtyphonics Remix)\", \"duration\": 291213, \"sharing\": \"public\", \"stream_url\": \"https://api.soundcloud.com/tracks/115985816/stream\", \"uri\": \"https://api.soundcloud.com/tracks/115985816\", \"user_id\": 1589064, \"policy\": \"ALLOW\"},\n" +
//                  "  ]";

//                       requestResult = "[\n" +
//                  //  "    {\"download_url\": null, \"key_signature\": \"\", \"user_favorite\": false, \"likes_count\": 6010, \"release\": \"\", \"attachments_uri\": \"https://api.soundcloud.com/tracks/115985816/attachments\", \"waveform_url\": \"https://w1.sndcdn.com/KmufKtKB678o_m.png\", \"purchase_url\": \"https://itunes.apple.com/fr/album/recharged/id701162375?wmgref=&affId=2051893&ign-mpt=uo%3D4\", \"video_url\": null, \"artwork_url\": \"https://i1.sndcdn.com/artworks-000060468826-filwdv-large.jpg\", \"comment_count\": 312, \"commentable\": true, \"description\": \"The new LINKIN PARK album RECHARGED is out October 29th.\\r\\nPre-Order RECHARGED on iTunes: http://smarturl.it/recharged\\r\\nPre-Order RECHARGED on CD/Vinyl: http://www.linkinpark.com\\r\\n\\r\\nBecome a fan of Dirtyphonics: www.facebook.com/dirtyphonics  \\r\\nFollow Dirtyphonics on Twitter: www.twitter.com/dirtyphonics\", \"download_count\": 0, \"downloadable\": false, \"embeddable_by\": \"all\", \"favoritings_count\": 6010, \"genre\": \"Drum & Bass\", \"isrc\": \"\", \"label_id\": null, \"label_name\": \"Warner Bros Record\"}\n" +
////                    "    {\"download_url\": null, \"key_signature\": \"\", \"user_favorite\": false, \"likes_count\": 6010, \"release\": \"\", \"attachments_uri\": \"https://api.soundcloud.com/tracks/115985816/attachments\", \"waveform_url\": \"https://w1.sndcdn.com/KmufKtKB678o_m.png\", \"purchase_url\": \"https://itunes.apple.com/fr/album/recharged/id701162375?wmgref=&affId=2051893&ign-mpt=uo%3D4\", \"video_url\": null, \"artwork_url\": \"https://i1.sndcdn.com/artworks-000060468826-filwdv-large.jpg\", \"comment_count\": 312, \"commentable\": true, \"description\": \"The new LINKIN PARK album RECHARGED is out October 29th.\\r\\nPre-Order RECHARGED on iTunes: http://smarturl.it/recharged\\r\\nPre-Order RECHARGED on CD/Vinyl: http://www.linkinpark.com\\r\\n\\r\\nBecome a fan of Dirtyphonics: www.facebook.com/dirtyphonics  \\r\\nFollow\", \"download_count\": 0, \"downloadable\": false, \"embeddable_by\": \"all\", \"favoritings_count\": 6010, \"genre\": \"Drum & Bass\", \"isrc\": \"\", \"label_id\": null, \"label_name\": \"Warner Bros Record\"}\n" +
//                    "    {\"download_url\": null, \"key_signature\": \"\", \"user_favorite\": false, \"likes_count\": 6010, \"release\": \"\", \"attachments_uri\": \"https://api.soundcloud.com/tracks/115985816/attachments\", \"waveform_url\": \"https://w1.sndcdn.com/KmufKtKB678o_m.png\", \"purchase_url\": \"https://itunes.apple.com/fr/album/recharged/id701162375?wmgref=&affId=2051893&ign-mpt=uo%3D4\", \"video_url\": null, \"artwork_url\": \"https://i1.sndcdn.com/artworks-000060468826-filwdv-large.jpg\", \"comment_count\": 312, \"commentable\": true, \"description\": \"The new LINKIN PARK album RECHARGED is out October 29th.\\r\\nPre-Order RECHARGED on iTunes: http://smarturl.it/recharged\\r\\nPre-Order RECHARGED on CD/Vinyl: http://www.linkinpark.com\\r\\n\\r\\nBecome a fan of Dirtyphonics: www.facebook.com/dirtyphonics  \\r\\nFollow\", \"download_count\": 0, \"downloadable\": false, \"embeddable_by\": \"all\", \"favoritings_count\": 6010, \"genre\": \"Drum & Bass\", \"isrc\": \"\", \"label_id\": null, \"label_name\": \"Warner Bros Record\"}\n" +
//                    "  ]";


//            requestResult = StringEscapeUtils.escapeJava(requestResult);
            if (requestResult != null) {
                requestResult = requestResult.replaceAll("\u2028", "");
                requestResult = requestResult.replaceAll("\u2029", "");
            }
//            requestResult = requestResult.replaceAll("\\p{C}", "UUU");
//            requestResult = requestResult.replaceAll("\"description\": (.*),", "");

//            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
//            xstream.setMode(XStream.NO_REFERENCES);
//            xstream.alias("commentsresult", HemechooCommentsResultDto.class);
//            xstream.alias("comment", HemeChooCommentDto.class);
//            String result = xstream.toXML(commnetsResultDto);

            response.getWriter().write(callback + "(" + requestResult + ")");
        }

        if (meth != null && meth.equals("muzek_search_yt")) {

            String searchString = decode(request, "q");
            String next = request.getParameter("next");

            String originalSearchString = searchString;
            if (searchString != null) {
                searchString = URLEncoder.encode(searchString);
            }

            // get request from youtube api
            String requestString = "/youtube/v3/search?key=AIzaSyA2FmomPU_dYP7B82qYGwVgjnK6u2aCxFE&part=snippet&type=video&videoCategoryId=10&maxResults=50&q=" + searchString;
            if (next != null && next.length() > 0) {
                requestString = requestString + "&pageToken=" + next;
            }

            String requestResult = sendRequestHTTPS("www.googleapis.com", requestString);

            try{
                muzekQueryCacheService.create(originalSearchString, requestResult, MuzekQueryCacheImpl.QUERY_TYPE.YOU_TUBE_QUERY);
            }catch(Throwable e){

            }


            Object obj = JSONValue.parse(requestResult);
            JSONArray items = (JSONArray) ((JSONObject) obj).get("items");
            String nextPageToken = (String) ((JSONObject) obj).get("nextPageToken");

            ArrayList<MuzekTrackDto> dtoList = new ArrayList<MuzekTrackDto>();
            for (int i = 0; i < items.size(); i++) {
                MuzekTrackDto trackDto = new MuzekTrackDto();
                trackDto.setType("youtube");
                trackDto.setStream_url("youtube");

                JSONObject jsonObject = (JSONObject) items.get(i);
                trackDto.setTitle((String) ((JSONObject) (jsonObject.get("snippet"))).get("title"));
                trackDto.setId((String) ((JSONObject) (jsonObject.get("id"))).get("videoId"));
                trackDto.setArtwork_url((String) ((JSONObject) ((JSONObject) ((JSONObject) (jsonObject.get("snippet"))).get("thumbnails")).get("default")).get("url"));

                MuzekTrackUserDto user = new MuzekTrackUserDto();
                user.setUsername((String) ((JSONObject) (jsonObject.get("snippet"))).get("channelTitle"));
                trackDto.setUser(user);


                dtoList.add(trackDto);
            }

            MuzekTrackResultDto searchResult = new MuzekTrackResultDto();
            searchResult.setTracks(dtoList);
            searchResult.setNextPageToken(nextPageToken);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
//            xstream.alias("commentsresult", HemechooCommentsResultDto.class);
//            xstream.alias("comment", HemeChooCommentDto.class);
            String result = xstream.toXML(searchResult);
            result = result.substring(41, result.length() - 1);

            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_add_track")) {

            String userId = request.getParameter("user_id");
            String trackId = request.getParameter("track_id");
            String trackName = decode(request, "track_name");

            String artworkUrl = request.getParameter("artwork_url");
            String trackPermalink = request.getParameter("track_permalink_url");
            String username = request.getParameter("username");
            String userPermalink = request.getParameter("user_permalink_url");
            String type = request.getParameter("type");


            User user = userModelService.findUserByMuzekFBid(userId);
            if (user == null) {
                user = userModelService.createMuzekUser(userId, browserName);
            }

            MuzekTrackListImpl.MUZEK_TRACK_SOURCE source = MuzekTrackListImpl.MUZEK_TRACK_SOURCE.SOUNDCLOUD;
            if (type != null && type.equals("youtube")) {
                source = MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE;
            }
            muzekTrackListService.create(user.getIdentifier(), user.getMuzekFBid(), trackId, source,
                    trackName,
                    artworkUrl, trackPermalink, username, userPermalink);


//            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
//            xstream.setMode(XStream.NO_REFERENCES);
//            xstream.alias("commentsresult", HemechooCommentsResultDto.class);
//            xstream.alias("comment", HemeChooCommentDto.class);
//            String result = xstream.toXML(commnetsResultDto);

            response.getWriter().write(callback + "(true)");
        }

        if (meth != null && meth.equals("muzek_add_track_simple")) {

            String userId = request.getParameter("user_id");
            String trackIdentifier = request.getParameter("track_identifier");


            MuzekTrackList track = muzekTrackListService.findById(trackIdentifier);

            String trackName = track.getName();
            String artworkUrl = track.getArtworkUrl();
            String trackPermalink = track.getTrackPermalink();
            String username = track.getUsername();
            String userPermalink = track.getUser_permalink_url();
            String type = track.getSource().toString().toLowerCase();
            String trackId = track.getTrackId();


            User user = userModelService.findUserByMuzekFBid(userId);
            if (user == null) {
                user = userModelService.createMuzekUser(userId, browserName);
            }

            MuzekTrackListImpl.MUZEK_TRACK_SOURCE source = MuzekTrackListImpl.MUZEK_TRACK_SOURCE.SOUNDCLOUD;
            if (type != null && type.equals("youtube")) {
                source = MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE;
            }
            muzekTrackListService.create(user.getIdentifier(), user.getMuzekFBid(), trackId, source,
                    trackName,
                    artworkUrl, trackPermalink, username, userPermalink);


//            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
//            xstream.setMode(XStream.NO_REFERENCES);
//            xstream.alias("commentsresult", HemechooCommentsResultDto.class);
//            xstream.alias("comment", HemeChooCommentDto.class);
//            String result = xstream.toXML(commnetsResultDto);

            response.getWriter().write(callback + "(true)");
        }



        if (meth != null && meth.equals("muzek_add_f")) {

//            String userId = request.getParameter("user_id");
//            String friends = decode(request, "f");
//
//            String [] array = friends.split(",");
//            List<String> list = new ArrayList<String>(array.length);
//            for (String s : array) {
//                list.add(s);
//            }
//            muzekFriendsService.create(userId, list);

            response.getWriter().write(callback + "(true)");
        }

        if (meth != null && meth.equals("muzek_remove_track")) {

            String userId = request.getParameter("user_id");
            String trackId = request.getParameter("track_id");

            muzekTrackListService.remove(userId, trackId);


//            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
//            xstream.setMode(XStream.NO_REFERENCES);
//            xstream.alias("commentsresult", HemechooCommentsResultDto.class);
//            xstream.alias("comment", HemeChooCommentDto.class);
//            String result = xstream.toXML(commnetsResultDto);

            response.getWriter().write(callback + "(true)");
        }


        if (meth != null && meth.equals("muzek_recognize_track")) {

            String userId = decode(request, "user_id");
            String names = decode(request, "name");
            String[] namesArray = names.split("\\[\\]");

            Integer vk_imported = 0;
            Integer vk_failed = 0;
            Integer vk_total = 0;

            VkMusicImporter importer = new VkMusicImporter();
            importer.setMuzekQueryCacheService(muzekQueryCacheService);
            User user = userModelService.findUserByMuzekFBid(userId);

            for (int i = 0; i < namesArray.length; i++) {

                String artist = namesArray[i].split("-")[0];
                String title = namesArray[i].split("-")[1];

                String stringToSearch = artist + " " + title;
                MuzekTrackDto track = importer.getTrackFromYoutube(stringToSearch, userId);

                if (track != null) {
                    muzekTrackListService.create(user.getIdentifier(), user.getMuzekFBid(), track.getId(),
                            MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE,
                            track.getTitle(),
                            track.getArtwork_url(),
                            track.getPermalink_url(),
                            track.getUser().getUsername(), track.getUser().getPermalink_url(),
                            title, artist, 0l, 0l,
                            MuzekTrackListImpl.VK_IMPORT_STATUS.IMPORTED, null);
                    vk_imported++;

//                if ( (vk_imported + vk_failed) > 300){
//                    break;
//                }
                } else {
                    try {
                        muzekTrackListService.createHidden(user.getIdentifier(), user.getMuzekFBid(),
                                MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE,
                                title, artist, 0l, MuzekTrackListImpl.VK_IMPORT_STATUS.NOT_FOUND);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    vk_failed++;
                }

                String stat = vk_total + ":" + vk_imported + ":" + vk_failed;
                userModelService.updateVkImportStatus(user.getIdentifier(), stat, "", UserImpl.USER_TRACK_VK_IMPORT_STATUS.PROGRESS);

            }

            userModelService.updateVkImportButtonVisibilitySetInvisible(user.getIdentifier());

            //response.getWriter().write(callback + "(true)");

            List<MuzekFeed> feed = new ArrayList<MuzekFeed>();

            List<MuzekFeedDto> listDto = MuzekFeedDtoService.toDtoList(feed);

            MuzekFeedResultDto resultDto = new MuzekFeedResultDto();
            resultDto.setItems(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekFeedResultDto.class);
            xstream.alias("item", MuzekFeedDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");
        }


        if (meth != null && meth.equals("muzek_vk_user")) {

            String userId = decode(request, "id");
            String names = decode(request, "data");
            String offset = decode(request, "o");

            String[] userData = names.split("\\[\\]");

            String id = userData[0];
            String name = userData[1];
            String city = userData[2];
            String imgUrl = userData[3];

            User user = userModelService.findUserByMuzekFBid(userId);


            muzekVkUserService.create(userId, id, imgUrl, city, offset, name);

            //response.getWriter().write(callback + "(true)");

            List<MuzekFeed> feed = new ArrayList<MuzekFeed>();
            List<MuzekFeedDto> listDto = MuzekFeedDtoService.toDtoList(feed);
            MuzekFeedResultDto resultDto = new MuzekFeedResultDto();
            resultDto.setItems(listDto);
            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekFeedResultDto.class);
            xstream.alias("item", MuzekFeedDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);
            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_vk_track")) {

            String userId = decode(request, "id");
            String totalTracksNumber = decode(request, "t");
            String vkUserId = decode(request, "v");
            String offset = decode(request, "o");
            String names = decode(request, "data").replaceAll("&#776;", "")
                    .replaceAll("&#33;", "").replaceAll("&quot;", "").replaceAll("&#39;", "")
                    .replaceAll("&#9835;", "");

            //String[] userData = names.split("\\[\\]");

            //String track = userData[0];

            User user = userModelService.findUserByMuzekFBid(userId);

            muzekVkTrackService.create(userId, vkUserId, totalTracksNumber, offset, names);

            //response.getWriter().write(callback + "(true)");

            List<MuzekFeed> feed = new ArrayList<MuzekFeed>();
            List<MuzekFeedDto> listDto = MuzekFeedDtoService.toDtoList(feed);
            MuzekFeedResultDto resultDto = new MuzekFeedResultDto();
            resultDto.setItems(listDto);
            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekFeedResultDto.class);
            xstream.alias("item", MuzekFeedDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);
            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_send_message")) {

            String senderId = decode(request, "sender_id");
            String recipientId = decode(request, "recipient_id");
            String trackName = decode(request, "track_name");
            String recipientName = decode(request, "recipient_name");
            String trackType = decode(request, "track_type");
            String trackId = decode(request, "track_id");


            User user = userModelService.findUserByMuzekFBid(senderId);
            userModelService.updateUnreadMessagesStatus(user.getIdentifier(), "true");

            muzekMessageService.create(user,
                    recipientId, recipientName,
                    trackName, trackType, trackId);


            //response.getWriter().write(callback + "(true)");

            List<MuzekFeed> feed = new ArrayList<MuzekFeed>();
            List<MuzekFeedDto> listDto = MuzekFeedDtoService.toDtoList(feed);
            MuzekFeedResultDto resultDto = new MuzekFeedResultDto();
            resultDto.setItems(listDto);
            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekFeedResultDto.class);
            xstream.alias("item", MuzekFeedDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);
            response.getWriter().write(callback + "(" + result + ")");
        }



        if (meth != null && meth.equals("muzek_track_list")) {

            String userId = request.getParameter("user_id");
            String albumId = request.getParameter("album_id");
            String source = request.getParameter("source");
            String pageStr = request.getParameter("page");
            String limitStr = request.getParameter("limit");

            Integer page = Integer.valueOf(pageStr);
            Integer limit = Integer.valueOf(limitStr);

            User user = userModelService.findUserByMuzekFBid(userId);
            if (user == null) {
                user = userModelService.createMuzekUser(userId, browserName);
            }

            List<MuzekTrackList> list = muzekTrackListService.findTracksByUserFBid(user.getMuzekFBid(), albumId, source, page * limit, limit);

            List<MuzekTrackDto> listDto = MuzekTracksDtoService.toDtoList(list);

            MuzekTrackResultDto resultDto = new MuzekTrackResultDto();
            resultDto.setUnreadMessages(user.getHaveUnreadMessages());
            resultDto.setTracks(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("tracks", MuzekTrackResultDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_feed")) {

            String userId = request.getParameter("user_id");
            String pageStr = request.getParameter("page");
            String limitStr = request.getParameter("limit");

            Integer page = Integer.valueOf(pageStr);
            Integer limit = Integer.valueOf(limitStr);

            if (limit > 100){
                limit = 100;
            }

//            User user = userModelService.findUserByMuzekFBid(userId);
//            if (user == null) {
//
//            }

            List<MuzekFeed> feed = muzekFeedService.getFeed(page * limit, limit);



            List<MuzekFeedDto> listDto = MuzekFeedDtoService.toDtoList(feed);

            MuzekFeedResultDto resultDto = new MuzekFeedResultDto();
            resultDto.setItems(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekFeedResultDto.class);
            xstream.alias("item", MuzekFeedDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_people")) {

            String userId = request.getParameter("user_id");
            String searchString = decode(request, "s");

            List<User> users = userModelService.findFacebookUserByName(searchString);


            List<MuzekFeedDto> listDto = MuzekFeedDtoService.toUserDtoList(users);

            MuzekFeedResultDto resultDto = new MuzekFeedResultDto();
            resultDto.setItems(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekFeedResultDto.class);
            xstream.alias("item", MuzekFeedDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_messages")) {

            String userId = request.getParameter("user_id");
            String pageStr = request.getParameter("page");
            String limitStr = request.getParameter("limit");

            Integer page = Integer.valueOf(pageStr);
            Integer limit = Integer.valueOf(limitStr);

            if (limit > 100){
                limit = 100;
            }

            User user = userModelService.findUserByMuzekFBid(userId);
            userModelService.updateUnreadMessagesStatus(user.getIdentifier(), "false");

            List<MuzekMessage> messages = muzekMessageService.getMessages(page * limit, limit, userId);

            List<MuzekMessageDto> listDto = MuzekMessagesDtoService.toDtoList(messages);

            MuzekMessagesResultDto resultDto = new MuzekMessagesResultDto();
            resultDto.setItems(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekMessagesResultDto.class);
            xstream.alias("item", MuzekMessageDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_message_update")) {

            String userId = decode(request, "user_id");
            String trackId = decode(request, "track_id");
            String action = decode(request, "a");

            muzekMessageService.updateMessageStatus(userId, trackId, action);

            response.getWriter().write(callback + "(true)");
        }

        if (meth != null && meth.equals("muzek_peer")) {

            final long ONE_MINUTE = 60000;//millisecs
            final long ONE_HOUR = ONE_MINUTE * 60;
            final long ONE_DAY = ONE_HOUR * 24;
            final long THREE_DAYS = ONE_DAY * 3;
            final long SEVEN_DAYS = ONE_DAY * 7;

            Boolean showNow = false;
            USER_ACTIVITY_LEVEL userActivityLevel = USER_ACTIVITY_LEVEL.MORE_THAN_SEVEN_DAYS;

            String userId = request.getParameter("user_id");
            //userId = "100007851097084";

            User user = userModelService.findUserByMuzekFBid(userId);
            List<MuzekFeed> feed = new ArrayList<MuzekFeed>();

            if (user.getPeersStatus() != null && user.getPeersStatus().equals(UserImpl.USER_PEERS_STATUS.HAS_PEERS)) {

                if (user.getLastActivityTime() == null) {
                    userActivityLevel = USER_ACTIVITY_LEVEL.MORE_THAN_SEVEN_DAYS;
                } else if ((new Date().getTime() - user.getLastActivityTime().getTime() < ONE_DAY)) {
                    userActivityLevel = USER_ACTIVITY_LEVEL.DAILY;
                } else if ((new Date().getTime() - user.getLastActivityTime().getTime() < THREE_DAYS)) {
                    userActivityLevel = USER_ACTIVITY_LEVEL.THREE_DAYS;
                } else if ((new Date().getTime() - user.getLastActivityTime().getTime() < SEVEN_DAYS)) {
                    userActivityLevel = USER_ACTIVITY_LEVEL.SEVEN_DAYS;
                }


                switch (userActivityLevel) {
                    case DAILY:
                        break;
                    case THREE_DAYS:
                        if (user.getPeerShowedTime() == null) {
                            showNow = true;
                        } else if (new Date().getTime() - user.getPeerShowedTime().getTime() > 24 * ONE_HOUR) {
                            showNow = true;
                        }
                        break;
                    case SEVEN_DAYS:
                        if (user.getPeerShowedTime() == null) {
                            showNow = true;
                        } else if (new Date().getTime() - user.getPeerShowedTime().getTime() > 12 * ONE_HOUR) {
                            showNow = true;
                        }
                        break;
                    case MORE_THAN_SEVEN_DAYS:
                        if (user.getPeerShowedTime() == null) {
                            showNow = true;
                        } else if (new Date().getTime() - user.getPeerShowedTime().getTime() > 4 * ONE_HOUR) {
                            showNow = true;
                        }
                        break;
                }


                if (showNow) {
                    MuzekPeerRecord peerRecord = muzekPeerRecordService.findPeer(userId);

                    if (peerRecord != null) {

                        MuzekFeed fakeFeed = new MuzekFeedImpl();
                        fakeFeed.setTrackIdentifier(peerRecord.getTrackIdentifier());
                        fakeFeed.setTrackId(peerRecord.getTrackId());
                        fakeFeed.setTrackSource(peerRecord.getTrackSource());
                        fakeFeed.setUserFBid(peerRecord.getPeerFBid());
                        fakeFeed.setArtworkUrl(peerRecord.getArtworkUrl());
                        fakeFeed.setTrack_name(peerRecord.getTrack_name());
                        fakeFeed.setUser_name(peerRecord.getPeerName());
                        fakeFeed.setStatus(MuzekFeedImpl.MUZEK_FEED_ITEM_TYPE.ADD_TO_PLAYLIST);

                        feed.add(fakeFeed);
                    }

                    // log it
                    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String lastActTime = "null";
                    String lastPeerShowedTime = "null";
                    if (user.getLastActivityTime() != null) {
                        lastActTime = sdfDate.format(user.getLastActivityTime());
                    }
                    if (user.getPeerShowedTime() != null) {
                        lastPeerShowedTime = sdfDate.format(user.getPeerShowedTime());
                    }
                    activityLogModelService.createHemechooLog("MUZEK_peer_showed type=" +
                            userActivityLevel.toString() +
                            " lastActivity=" + lastActTime +
                            " lastPeerShowed=" + lastPeerShowedTime, userId, null, browserName, null, ip);

                }
            }

            List<MuzekFeedDto> listDto = MuzekFeedDtoService.toDtoList(feed);

            MuzekFeedResultDto resultDto = new MuzekFeedResultDto();
            resultDto.setItems(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("items", MuzekFeedResultDto.class);
            xstream.alias("item", MuzekFeedDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_album_list")) {

            String userId = request.getParameter("user_id");

            User user = userModelService.findUserByMuzekFBid(userId);
            if (user == null) {
                user = userModelService.createMuzekUser(userId, browserName);
            }

            List<MuzekAlbums> list = muzekAlbumsService.findTracksByUserFBid(user.getMuzekFBid());

            List<MuzekAlbumDto> listDto = MuzekAlbumDtoService.toDtoList(list);

            MuzekAlbumResultDto resultDto = new MuzekAlbumResultDto();
            resultDto.setAlbums(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("albums", MuzekAlbumResultDto.class);
            xstream.alias("album", MuzekAlbumDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("muzek_listeners")) {

            String userId = request.getParameter("user_id");
            String trackId = decode(request, "track_id");
            Integer limit = Integer.valueOf(decode(request, "li"));
            if (limit > 100){
                limit = 100;
            }

            if (trackId.equals("rYEDA3JcQqw") || trackId.equals("y6Sxv-sUYtM") ||
                    trackId.equals("nlcIKh6sBtc") ||trackId.equals("hT_nvWreIhg")) {

                List<MuzekListenerDto> listDto = new ArrayList<MuzekListenerDto>();
                MuzekListenerResultDto resultDto = new MuzekListenerResultDto();
                resultDto.setListeners(listDto);
                XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
                xstream.setMode(XStream.NO_REFERENCES);
                xstream.alias("listeners", MuzekListenerDto.class);
                xstream.alias("listeners", MuzekListenerResultDto.class);
                String result = xstream.toXML(resultDto);
                response.getWriter().write(callback + "(" + result + ")");
            } else{

                String friendsString = "";
                List<MuzekFriends> friends = muzekFriendsService.findFriends(userId);
                if (friends != null) {
                    for (MuzekFriends friend : friends) {
                        if (friend.getFriendId() != null && friend.getFriendId().length() > 0) {
                            friendsString += friendsString.length() > 0 ? "," + friend.getFriendId() : friend.getFriendId();
                        }
                    }
                }

                List<MuzekTrackList> tracks = muzekTrackListService.findTrackListeners(limit, trackId, userId, friendsString);
                List<MuzekListenerDto> listDto = MuzekTracksDtoService.toListenerDtoList(tracks);

                MuzekListenerResultDto resultDto = new MuzekListenerResultDto();
                resultDto.setListeners(listDto);

                XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
                xstream.setMode(XStream.NO_REFERENCES);
                xstream.alias("listeners", MuzekListenerDto.class);
                xstream.alias("listeners", MuzekListenerResultDto.class);
                String result = xstream.toXML(resultDto);


                response.getWriter().write(callback + "(" + result + ")");
            }
        }

        if (meth != null && meth.equals("muzek_radio")) {

            String radioId = request.getParameter("radio_id");

            User user = userModelService.findUserByMuzekFBid(radioId);
            if (user == null) {
                user = userModelService.createMuzekUser(radioId, browserName);
            }

            List<MuzekTrackList> list = muzekTrackListService.findTracksByUserFBid(user.getMuzekFBid(), null, null, 0, 100000);

            Random rn = new Random();
            Integer randomInt = rn.nextInt(list.size() - 1);
            List<MuzekTrackList> newList = new ArrayList<MuzekTrackList>();
            newList.add(list.get(randomInt));

            List<MuzekTrackDto> listDto = MuzekTracksDtoService.toDtoList(newList);

            MuzekTrackResultDto resultDto = new MuzekTrackResultDto();


            resultDto.setTracks(listDto);

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("tracks", MuzekTrackResultDto.class);
            xstream.alias("track", MuzekTrackDto.class);
            xstream.alias("user", MuzekTrackUserDto.class);
            String result = xstream.toXML(resultDto);


            response.getWriter().write(callback + "(" + result + ")");

        }


        if (meth != null && meth.equals("muzek_post_feedback")) {

            String userId = decode(request, "id");
            String rate = decode(request, "rate");
            String text = decode(request, "text");

            User user = userModelService.findUserByMuzekFBid(userId);

            String userRate = "rated ";
            if (rate != null) {
                userRate += rate;
            }

            userModelService.updateMuzekFeedback(user.getIdentifier(), userRate, text);

            response.getWriter().write(callback + "()");

        }

        if (meth != null && meth.equals("muzek_reorder")) {

            String userId = decode(request, "id");
            String trackIdentifier = decode(request, "identifier");
            String prevId = decode(request, "prev");
            String nextId = decode(request, "next");

            User user = userModelService.findUserByMuzekFBid(userId);

            Long trackId = Long.valueOf(trackIdentifier);

            Double sortFieldValue = 0d;

            Double prev = null;
            Double next = null;
            if (prevId != null) {
                prev = muzekTrackListService.findById(Long.valueOf(prevId)).getSortField();
            }
            if (nextId != null) {
                next = muzekTrackListService.findById(Long.valueOf(nextId)).getSortField();
            }

            if (prev != null && next != null) {

                sortFieldValue = (prev + next) / 2d;
            }
            if (prev == null && next != null) {
                sortFieldValue = next + 0.01;
            }
            if (prev != null && next == null) {
                sortFieldValue = prev - 0.01;
            }

            muzekTrackListService.updateSortField(trackId, sortFieldValue);

            activityLogModelService.createHemechooLog("MUZEK_sort " + sortFieldValue, userId, null, browserName, null, ip);

            response.getWriter().write(callback + "()");

        }

        if (meth != null && meth.equals("muzek_activate")) {

            String userId = decode(request, "id");

            User user = userModelService.findUserByMuzekFBid(userId);
            userModelService.activate(user.getIdentifier());

            response.getWriter().write(callback + "()");

        }


        if (meth != null && meth.equals("muzek_user_params")) {
            Boolean justCreated = false;
            String url = decode(request, "url");
            String country = locatorService.getCountry(ip);

            String userFBid = decode(request, "id");
            //activityLogModelService.createHemechooLog("MUZEK_loaded", userFBid, url, null, null, ip + " " + country);
            activityLogModelService.createHemechooLog("MUZEK_loaded", userFBid, url, null, null, ip);

            if (userFBid != null && userFBid.trim().length() > 0) {

                User user = userModelService.findUserByMuzekFBid(userFBid);

                if (user == null) {
                    user = userModelService.createMuzekUser(userFBid, browserName);
                    justCreated = true;

                    muzekTrackListService.setDefaultTracks(user);
                }

                EmbeddingParamsDto dto = new EmbeddingParamsDto();

                if (user != null) {
                    if (user.getMuzekShare() != null) {
                        dto.setShare(user.getMuzekShare());
                    } else {
                        //dto.setShare("false");
                    }
                    dto.setFeedback(user.getMuzekFeedbackStatus());
                    if (user.getMuzekStatus() != null && user.getMuzekStatus().equals(UserImpl.MUZEK_STATUS.JUST_CREATED)) {
                        dto.setCreated("true");
                    }

                    if (user.getMuzekVkImportStatus() != null) {
                        dto.setVkimport(user.getMuzekVkImportStatus());
                    }
                    if (user.getVk_import_status() != null) {
                        dto.setVkimportstatus(user.getVk_import_status().toString());
                    }
                    if (user.getVkImportStatistics() != null) {
                        dto.setVkimportstat(user.getVkImportStatistics());
                    }

                    if (user.getYoutube_import_status() != null) {
                        dto.setYoutubeimportstatus(user.getYoutube_import_status().toString());
                    }
                    if (user.getYoutubeImportStatistics() != null) {
                        dto.setYoutubeimportstat(user.getYoutubeImportStatistics());
                    }
                    if (user.getName() != null) {
                        dto.setUsername("true");
                    }


                    dto.setCountry(country);
//                dto.setCountry("US");

                    if (user.getVkAlbumsRaw() != null && user.getVkAlbumsRaw().length() > 0) {
                        if (!user.getVkAlbumsRaw().equals("{\"response\":[0]}")) {
                            dto.setAlbums("true");
                        }
                    }


                }


                XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
                xstream.setMode(XStream.NO_REFERENCES);
                xstream.alias("params", EmbeddingParamsDto.class);
                String result = xstream.toXML(dto);


                response.getWriter().write(callback + "(" + result + ")");

            } else {
                activityLogModelService.createHemechooLog("MUZEK_not_loaded", userFBid, url, null, null, ip);
                response.getWriter().write(callback + "(no user id)");
            }
        }

        if (meth != null && meth.equals("muzek_fetch")) {

            String userFBid = decode(request, "id");
            User user = userModelService.findUserByMuzekFBid(userFBid);
            String graphUser = "g";

            if (user.getGraph() == null) {
                activityLogModelService.createHemechooLog("MUZEK_fetching from graph", userFBid, null, null, null, ip);

                String requestString = "/" + userFBid;
                graphUser = getUserFromGraph("graph.facebook.com", requestString);

                userModelService.updateUserMuzekGraph(user.getIdentifier(), graphUser);
            }
            response.getWriter().write(callback + "(" + graphUser + ")");
        }


        if (meth != null && meth.equals("muzek_stat")) {

            String msg = decode(request, "msg");
            String userId = request.getParameter("id");
            String version = decode(request, "version");
            String url = decode(request, "url");

            activityLogModelService.createHemechooLog("MUZEK_" + msg, userId, url, browserName, null, ip, version);

            if (msg.contains("play_")){
                String track_id = msg.replaceAll("play_", "");
                muzekFeedService.create(userId, track_id, MuzekFeedImpl.MUZEK_FEED_ITEM_TYPE.LISTEN_TO);

                userModelService.updateUserLastActivityTime(userId);
            }


            EmbeddingParamsDto dto = new EmbeddingParamsDto();
            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("params", EmbeddingParamsDto.class);
            String result = xstream.toXML(dto);

            response.getWriter().write(callback + "(" + result + ")");

        }

        if (meth != null && meth.equals("muzek_update_username")) {

            String userId = decode(request, "id");
            String userName = decode(request, "name");

            userModelService.updateMuzekUsername(userId, userName);

            EmbeddingParamsDto dto = new EmbeddingParamsDto();
            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("params", EmbeddingParamsDto.class);
            String result = xstream.toXML(dto);

            response.getWriter().write(callback + "(" + result + ")");
        }

        if (meth != null && meth.equals("heme_signin")) {

            String login = decode(request, "l");
            String password = decode(request, "p");
            String clientUrl = decode(request, "url");

            UserDto userDto = new UserDto();
            User user = userModelService.findUserByCredentials(login, password);
            if (user == null) {
                activityLogModelService.createHemechooLog("heme sign in failed", "login:" + login + ", pwd:" + password, clientUrl, null, null, ip);
            } else {
                activityLogModelService.createHemechooLog("heme sign in success", "login:" + login + ", pwd:" + password, clientUrl, null, null, ip);
                user = userModelService.loginUser(user);
                userDto = UserDtoService.toDto(user);
            }

            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("user", UserDto.class);
            String result = xstream.toXML(userDto);

            response.getWriter().write(callback + "(" + result + ")");

        }







        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        response.getWriter().close();

        return null;
    }






    private String decode(HttpServletRequest request, String paramName) throws UnsupportedEncodingException {
        String userName = request.getParameter(paramName);
        if (userName != null) {
            userName = StringUtils.newStringUtf8(Base64.decodeBase64(userName));
            userName = java.net.URLDecoder.decode(userName, "UTF-8");

        }
        return userName;
    }


    String sendRequest(String host, String uri) {

        Service<HttpRequest, HttpResponse> client = null;

        System.out.println("Client creating: " + Thread.currentThread().getName());
        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                        //  .hosts("78.108.83.148:3128")

                        //.hosts(proxyDbContainer.getProxyDb().getIp())
                .hosts(host + ":80")
//                .hosts(host + ":443")
//                .tls(host)
                .retries(1)
                .hostConnectionLimit(1)
        );

        int readingTries = 0;
        String pageUri = uri;

        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, pageUri);   // 2
        request.setHeader(HttpHeaders.Names.HOST, host);
        request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
        request.setHeader(HttpHeaders.Names.CACHE_CONTROL, "max-age=0");
        request.setHeader(HttpHeaders.Names.ACCEPT, "ext/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        request.setHeader(HttpHeaders.Names.USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36");
        request.setHeader(HttpHeaders.Names.ACCEPT_ENCODING, "gzip,deflate,sdch");
        request.setHeader(HttpHeaders.Names.ACCEPT_LANGUAGE, "en-US,en;q=0.8,ru;q=0.6,uk;q=0.4");

        HttpResponse response = client.apply(request).apply();

        if (response.getStatus().getCode() == 200) {
            //byte[] array = response.getContent().array();
            String encoding = "utf8";//"cp1251";//EncodingDeterminer.getEncoding(array);
            String result = response.getContent().toString(Charset.forName("utf8"));

            return result;

        } else {
            System.out.println("==== Missing URI: " + pageUri);

        }
        return null;
    }


    String sendRequestHTTPS(String host, String uri) {

        Service<HttpRequest, HttpResponse> client = null;

        System.out.println("Client creating: " + Thread.currentThread().getName());
        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                        //  .hosts("78.108.83.148:3128")

                        //.hosts(proxyDbContainer.getProxyDb().getIp())
//                .hosts(host + ":80")
                .hosts(host + ":443")
                .tls(host)
                .retries(1)
                .hostConnectionLimit(1)
        );

        int readingTries = 0;
        String pageUri = uri;

        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, pageUri);   // 2
        request.setHeader(HttpHeaders.Names.HOST, host);
        request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
        request.setHeader(HttpHeaders.Names.CACHE_CONTROL, "max-age=0");
        request.setHeader(HttpHeaders.Names.ACCEPT, "ext/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        request.setHeader(HttpHeaders.Names.USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36");
        request.setHeader(HttpHeaders.Names.ACCEPT_ENCODING, "gzip,deflate,sdch");
        request.setHeader(HttpHeaders.Names.ACCEPT_LANGUAGE, "en-US,en;q=0.8,ru;q=0.6,uk;q=0.4");

        HttpResponse response = client.apply(request).apply();

        if (response.getStatus().getCode() == 200) {
            //byte[] array = response.getContent().array();
            String encoding = "utf8";//"cp1251";//EncodingDeterminer.getEncoding(array);
            String result = response.getContent().toString(Charset.forName("utf8"));

            return result;

        } else {
            System.out.println("==== Missing URI: " + pageUri);

        }
        return null;
    }

    String getUserFromGraph(String host, String uri) {

        Service<HttpRequest, HttpResponse> client = null;

        System.out.println("Client creating: " + Thread.currentThread().getName());
        client = ClientBuilder.safeBuild(ClientBuilder.get()    // 1
                .codec(Http.get())
                .timeout(new Duration(60000000000L))
                .connectTimeout(new Duration(60000000000L))
                .requestTimeout(new Duration(60000000000L))
                .tcpConnectTimeout(new Duration(60000000000L))
                .connectionTimeout(new Duration(60000000000L))
                        //  .hosts("78.108.83.148:3128")

                        //.hosts(proxyDbContainer.getProxyDb().getIp())
//                .hosts(host + ":80")
                .hosts(host + ":443")
                .tls(host)
                .retries(1)
                .hostConnectionLimit(1)
        );

        int readingTries = 0;
        String pageUri = uri;

        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, pageUri);   // 2
        request.setHeader(HttpHeaders.Names.HOST, host);
        request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
        request.setHeader(HttpHeaders.Names.CACHE_CONTROL, "max-age=0");
        request.setHeader(HttpHeaders.Names.ACCEPT, "ext/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        request.setHeader(HttpHeaders.Names.USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36");
        request.setHeader(HttpHeaders.Names.ACCEPT_ENCODING, "gzip,deflate,sdch");
        request.setHeader(HttpHeaders.Names.ACCEPT_LANGUAGE, "en-US,en;q=0.8,ru;q=0.6,uk;q=0.4");

        HttpResponse response = client.apply(request).apply();

        if (response.getStatus().getCode() == 200) {
            //byte[] array = response.getContent().array();
            String encoding = "utf8";//"cp1251";//EncodingDeterminer.getEncoding(array);
            String result = response.getContent().toString(Charset.forName("utf8"));

            return result;

        } else {
            System.out.println("==== Missing URI: " + pageUri);

        }
        return null;
    }

    /* Setters */


    public static String toString(Serializable o) throws IOException {

        Base64 base64 = new Base64();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();
        return new String(base64.encode(baos.toByteArray()));
    }




    @Autowired
    public void setActivityLogModelService(ActivityLogModelService activityLogModelService) {
        this.activityLogModelService = activityLogModelService;
    }

    @Autowired
    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }


    public MuzekTrackListService getMuzekTrackListService() {
        return muzekTrackListService;
    }

    @Autowired
    public void setMuzekTrackListService(MuzekTrackListService muzekTrackListService) {
        this.muzekTrackListService = muzekTrackListService;
    }

    @Autowired
    public void setMuzekAlbumsService(MuzekAlbumsService muzekAlbumsService) {
        this.muzekAlbumsService = muzekAlbumsService;
    }

    @Autowired
    public void setGoogleSpreadsheetService(GoogleSpreadsheetService googleSpreadsheetService) {
        this.googleSpreadsheetService = googleSpreadsheetService;
    }



    @Autowired
    public void setLocatorService(LocatorService locatorService) {
        this.locatorService = locatorService;
    }

    @Autowired
    public void setMuzekFriendsService(MuzekFriendsService muzekFriendsService) {
        this.muzekFriendsService = muzekFriendsService;
    }

    @Autowired
    public void setMuzekFeedService(MuzekFeedService muzekFeedService) {
        this.muzekFeedService = muzekFeedService;
    }

    @Autowired
    public void setMuzekPeerRecordService(MuzekPeerRecordService muzekPeerRecordService) {
        this.muzekPeerRecordService = muzekPeerRecordService;
    }

    @Autowired
    public void setMuzekQueryCacheService(MuzekQueryCacheService muzekQueryCacheService) {
        this.muzekQueryCacheService = muzekQueryCacheService;
    }

    @Autowired
    public void setMuzekVkTrackService(MuzekVkTrackService muzekVkTrackService) {
        this.muzekVkTrackService = muzekVkTrackService;
    }

    @Autowired
    public void setMuzekVkUserService(MuzekVkUserService muzekVkUserService) {
        this.muzekVkUserService = muzekVkUserService;
    }

    @Autowired
    public void setMuzekMessageService(MuzekMessageService muzekMessageService) {
        this.muzekMessageService = muzekMessageService;
    }
}
