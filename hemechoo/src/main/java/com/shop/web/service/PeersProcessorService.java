package com.shop.web.service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import com.shop.model.dao.MuzekTrackListDao;
import com.shop.model.persistence.MuzekFriends;
import com.shop.model.persistence.MuzekPeerRecord;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.User;
import com.shop.model.persistence.impl.MuzekPeerRecordImpl;
import com.shop.model.persistence.impl.UserImpl;
import com.shop.model.service.*;
import com.shop.web.dto.MuzekListenerDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class PeersProcessorService {


    UserModelService userModelService;
    MuzekTrackListService muzekTrackListService;
    MuzekFriendsService muzekFriendsService;
    MuzekPeerRecordService muzekPeerRecordService;


    public void processAllUsers() {

        List<User> users = userModelService.findUsesWithoutPeers();

        while (users.size() > 0) {

            for (User user : users) {

                userModelService.updatePeerRecordsStatus(user.getIdentifier(), UserImpl.USER_PEERS_STATUS.PROGRESS);

                List<MuzekPeerRecord> peerRecords = new ArrayList<MuzekPeerRecord>();

                List<MuzekTrackList> tracks = muzekTrackListService.findTracksByUserFBid(user.getMuzekFBid(), null, null, 0, 25);

                String friendsString = "";
//                List<MuzekFriends> friends = muzekFriendsService.findFriends(user.getMuzekFBid());
//                if (friends != null) {
//                    for (MuzekFriends friend : friends) {
//                        if (friend.getFriendId() != null && friend.getFriendId().length() > 0) {
//                            friendsString += friendsString.length() > 0 ? "," + friend.getFriendId() : friend.getFriendId();
//                        }
//                    }
//                }




                for (MuzekTrackList track : tracks) {


                    List<MuzekTrackList> trackList = null;
                    try{
                        trackList = muzekTrackListService.findTrackListenersWithoutFriends(2, track.getTrackId(), user.getMuzekFBid());

                    }catch(Exception e1){
                        try{
                            trackList = muzekTrackListService.findTrackListenersWithoutFriends(2, track.getTrackId(), user.getMuzekFBid());

                        }catch(Exception e2){
                            trackList = muzekTrackListService.findTrackListenersWithoutFriends(2, track.getTrackId(), user.getMuzekFBid());
                        }
                    }

                    try {
                        Thread.sleep(100);
                    } catch(InterruptedException ex) {

                    }

                    //List<MuzekListenerDto> listDto = MuzekTracksDtoService.toListenerDtoList(trackList);

                    for (MuzekTrackList peerTrack : trackList) {

                        try {

                            User peer = userModelService.findById(peerTrack.getUserId());
                            if (peer.getName() != null && !peer.getName().equals("Muzek") && peer.getName().length() > 0) {

                                MuzekPeerRecord peerRecord = new MuzekPeerRecordImpl();
                                peerRecord.setUserId(user.getIdentifier());
                                peerRecord.setUserFBid(user.getMuzekFBid());

                                peerRecord.setPeerId(peerTrack.getUserId());
                                peerRecord.setPeerFBid(peerTrack.getUserFBid());
                                peerRecord.setPeerName(peer.getName());

                                peerRecord.setTrack_name(peerTrack.getName());
                                peerRecord.setTrackId(peerTrack.getTrackId());
                                peerRecord.setTrackIdentifier(peerTrack.getIdentifier());
                                peerRecord.setArtworkUrl(peerTrack.getArtworkUrl());
                                peerRecord.setTrackSource(peerTrack.getSource().toString());
                                peerRecord.setStatus(MuzekPeerRecordImpl.MUZEK_PEER_STATUS.NOT_SHOWED);

                                peerRecords.add(peerRecord);
                            }

                        } catch (Throwable e) {

                        }

                    }


                }


                if (peerRecords.size() > 0) {
                    userModelService.updatePeerRecordsStatus(user.getIdentifier(), UserImpl.USER_PEERS_STATUS.HAS_PEERS);
                } else {
                    userModelService.updatePeerRecordsStatus(user.getIdentifier(), UserImpl.USER_PEERS_STATUS.NO_ANY_PEERS);
                }

                muzekPeerRecordService.createListOfPeers(peerRecords);

            }


            users = userModelService.findUsesWithoutPeers();
        }

    }

    @Autowired
    public void setUserModelService(UserModelService userModelService) {
        this.userModelService = userModelService;
    }

    @Autowired
    public void setMuzekTrackListService(MuzekTrackListService muzekTrackListService) {
        this.muzekTrackListService = muzekTrackListService;
    }

    @Autowired
    public void setMuzekFriendsService(MuzekFriendsService muzekFriendsService) {
        this.muzekFriendsService = muzekFriendsService;
    }

    @Autowired
    public void setMuzekPeerRecordService(MuzekPeerRecordService muzekPeerRecordService) {
        this.muzekPeerRecordService = muzekPeerRecordService;
    }
}
