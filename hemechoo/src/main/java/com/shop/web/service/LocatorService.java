package com.shop.web.service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import com.shop.model.dao.MuzekTrackListDao;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.service.BaseModelService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

public class LocatorService extends BaseModelService<MuzekTrackList, MuzekTrackListDao> {


    String ipDatabaseDir;

    DatabaseReader reader = null;




    public String getCountry(String ip) throws IOException, GeoIp2Exception {

        String countryString = "not defined";

        try {
            if (ip != null && (ip.equals("127.0.0.1") || ip.equals("localhost"))) {
                ip = "195.42.130.236"; // UA
//                ip = "54.152.133.7";  // US
//                ip = "93.158.134.3";  // RU

            }

            InetAddress ipAddress = InetAddress.getByName(ip);
            CityResponse city = reader.city(ipAddress);

            Country country = city.getCountry();

            countryString = country.getIsoCode();
        } catch (Throwable e) {

        }
        return countryString;
    }


    @Autowired
    public void setIpDatabaseDir(String ipDatabaseDir) {
        this.ipDatabaseDir = ipDatabaseDir;

        try {
            File database = new File(ipDatabaseDir + "/GeoLite2-City.mmdb");
            reader = new DatabaseReader.Builder(database).build();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }




}
