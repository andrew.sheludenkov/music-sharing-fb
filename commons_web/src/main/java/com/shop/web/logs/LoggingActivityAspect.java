package com.shop.web.logs;

import com.shop.model.auth.AuthenticatedUserHolder;
import com.shop.model.persistence.User;
import com.shop.model.service.ActivityLogModelService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
@Aspect
public class LoggingActivityAspect {
    private ActivityLogModelService activityLogModelService;
    private AuthenticatedUserHolder authenticatedUserHolder;

    public void logUserActivity(JoinPoint joinPoint){
        String description = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
        User user = authenticatedUserHolder.getLoggedInUser();
        if(user != null){
            activityLogModelService.createActivityLog(user, description);
        }
    }

    public void logServiceActivity(JoinPoint joinPoint){
        String description = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
        activityLogModelService.createActivityLog(null, description);
    }

    /* Setters */

    public void setActivityLogModelService(ActivityLogModelService activityLogModelService) {
        this.activityLogModelService = activityLogModelService;
    }

    public void setAuthenticatedUserHolder(AuthenticatedUserHolder authenticatedUserHolder) {
        this.authenticatedUserHolder = authenticatedUserHolder;
    }
}
