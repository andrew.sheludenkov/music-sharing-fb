package com.shop.web.logs;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 26.04.12
 */
@Aspect
public class LoggingFileAspect {
    private static Logger logger = Logger.getLogger("Activity_log");

    public void logActivityStart(JoinPoint pjp){
        logger.info("entering: " + pjp.getSignature().getName());
    }

    public void logActivityEnd(JoinPoint pjp){
        logger.info("finishing: " + pjp.getSignature().getName());
    }
}
