package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class MuzekTrackUserOdto extends JavaScriptObject{

    protected MuzekTrackUserOdto() {
    }

    public final native String getUserName()/*-{
        return this.username;
    }-*/;

    public final native String getPermalink()/*-{
        return this.permalink_url;
    }-*/;



}