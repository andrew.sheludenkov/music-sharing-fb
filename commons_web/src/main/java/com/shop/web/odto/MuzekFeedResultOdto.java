package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class MuzekFeedResultOdto extends JavaScriptObject {

    protected MuzekFeedResultOdto() {
    }

    public final native MuzekFeedResultOdto getResult() /*-{
        return this.result;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;


    public final native JsArray<MuzekFeedOdto> getFeedDtoList() /*-{
        return this.items.items;
    }-*/;

    public final native JsArray<MuzekFeedOdto> getDtoList() /*-{
        return this.feed;
    }-*/;

    public final native String getNextPageToken() /*-{
        return this.nextPageToken;
    }-*/;


}
