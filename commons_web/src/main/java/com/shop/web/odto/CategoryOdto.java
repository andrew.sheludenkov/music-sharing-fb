package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class CategoryOdto extends JavaScriptObject {

    protected CategoryOdto() {
    }

    public final native CategoryOdto getCategory() /*-{
        return this.category;
    }-*/;

    public final native int getId() /*-{
        return this.id;
    }-*/;

    public final native String getName() /*-{
        return this.name;
    }-*/;

    public final native JsArray<CategoryOdto> getChildren() /*-{
        return this.children;
    }-*/;

    public final native String getFullName() /*-{
        return this.fullName;
    }-*/;

    public final native Long getParentId() /*-{
        return this.parentId;
    }-*/;

    public final native boolean getVisible()/*-{
        return this.visible;
    }-*/;

    public final native Double getRank()/*-{
        return this.rank;
    }-*/;

}
