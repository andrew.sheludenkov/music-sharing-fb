package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class ProductCharacteristicOdto extends JavaScriptObject {

    protected ProductCharacteristicOdto() {
    }


    public final native String getCharacteristicName() /*-{
        return this.characteristicName;
    }-*/;

    public final native String getCharacteristicValue() /*-{
        return this.characteristicValue;
    }-*/;

}
