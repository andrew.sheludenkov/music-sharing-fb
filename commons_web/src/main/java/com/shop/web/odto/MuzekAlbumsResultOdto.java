package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class MuzekAlbumsResultOdto extends JavaScriptObject {

    protected MuzekAlbumsResultOdto() {
    }

    public final native MuzekAlbumsResultOdto getResult() /*-{
        return this.result;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;


    public final native JsArray<MuzekAlbumOdto> getAlbumsDtoList() /*-{
        return this.albums.albums;
    }-*/;






}
