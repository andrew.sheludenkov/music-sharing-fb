package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class MuzekTracksResultOdto extends JavaScriptObject {

    protected MuzekTracksResultOdto() {
    }

    public final native MuzekTracksResultOdto getResult() /*-{
        return this.result;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;


    public final native JsArray<MuzekTrackOdto> getTracksDtoList() /*-{
        return this.tracks.tracks;
    }-*/;

    public final native JsArray<MuzekTrackOdto> getYoutubeTracksDtoList() /*-{
        return this.tracks;
    }-*/;

    public final native String getNextPageToken() /*-{
        return this.nextPageToken;
    }-*/;

    public final native String getUnreadMessages() /*-{
        return this.tracks.unreadMessages;
    }-*/;

}
