package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 01.02.13
 */
public class HemeChooCommentOdto extends JavaScriptObject{

    protected HemeChooCommentOdto() {
    }

    public final native HemeChooCommentOdto getComment()/*-{
        return this.comment;
    }-*/;

    public final native String getName()/*-{
        return this.name;
    }-*/;

    public final native String getDate()/*-{
        return this.date;
    }-*/;

    public final native String getContent()/*-{
        return this.content;
    }-*/;

    public final native String getPros()/*-{
        return this.pros;
    }-*/;

    public final native String getCons()/*-{
        return this.cons;
    }-*/;

    public final native String getSub()/*-{
        return this.sub;
    }-*/;

    public final native String getHtml()/*-{
        return this.html;
    }-*/;

    public final native String getTyle()/*-{
        return this.type;
    }-*/;

    public final native String getBig()/*-{
        return this.big;
    }-*/;

    public final native String getSmall()/*-{
        return this.small;
    }-*/;

    public final native String getId()/*-{
        return this.id;
    }-*/;
}