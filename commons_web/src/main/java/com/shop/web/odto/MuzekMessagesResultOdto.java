package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class MuzekMessagesResultOdto extends JavaScriptObject {

    protected MuzekMessagesResultOdto() {
    }

    public final native MuzekMessagesResultOdto getResult() /*-{
        return this.result;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;


    public final native JsArray<MuzekMessagesOdto> getMessagesDtoList() /*-{
        return this.items.items;
    }-*/;

    public final native JsArray<MuzekMessagesOdto> getDtoList() /*-{
        return this.feed;
    }-*/;

    public final native String getNextPageToken() /*-{
        return this.nextPageToken;
    }-*/;


}
