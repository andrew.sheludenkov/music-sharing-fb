package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class HemechooCommentsResultOdto extends JavaScriptObject{

    protected HemechooCommentsResultOdto() {
    }

    public final native HemechooCommentsResultOdto getResult() /*-{
        return this.commentsresult;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;

    public final native String getHideDivId() /*-{
        return this.hide;
    }-*/;


    public final native JsArray<HemeChooCommentOdto> getCommentsList() /*-{
        return this.comments;
    }-*/;
}
