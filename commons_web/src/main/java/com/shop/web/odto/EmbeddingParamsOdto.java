package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class EmbeddingParamsOdto extends JavaScriptObject{

    protected EmbeddingParamsOdto() {
    }

    public final native String getBeforeId() /*-{
        return this.params.before;
    }-*/;

    public final native String getAfterId() /*-{
        return this.params.after;
    }-*/;

    public final native String getInsideId() /*-{
        return this.params.inside;
    }-*/;

    public final native String getReplaceId() /*-{
        return this.params.replaceid;
    }-*/;

    public final native String getBlockStyle() /*-{
        return this.params.styles;
    }-*/;

    public final native String getHost() /*-{
        return this.params.hosts;
    }-*/;

    public final native String getWidth() /*-{
        return this.params.width;
    }-*/;

    public final native String getHide() /*-{
        return this.params.hide;
    }-*/;

    public final native String getCss() /*-{
        return this.params.css;
    }-*/;
    public final native String getHtml() /*-{
        return this.params.html;
    }-*/;

    public final native String getImage() /*-{
        return this.params.image;
    }-*/;

    public final native String getImagesWidth() /*-{
        return this.params.imgwidth;
    }-*/;

    public final native String getComments() /*-{
        return this.params.comments;
    }-*/;

    public final native String getShare() /*-{
        return this.params.share;
    }-*/;

    public final native String getFeedback() /*-{
        return this.params.feedback;
    }-*/;

    public final native String getCreated() /*-{
        return this.params.created;
    }-*/;

    public final native String getVkImport() /*-{
        return this.params.vkimport;
    }-*/;

    public final native String getVkImportStatus() /*-{
        return this.params.vkimportstatus;
    }-*/;

    public final native String getVkImportStatistics() /*-{
        return this.params.vkimportstat;
    }-*/;

    public final native String getYoutubeImportStatus() /*-{
        return this.params.youtubeimportstatus;
    }-*/;

    public final native String getYoutubeImportStatistics() /*-{
        return this.params.youtubeimportstat;
    }-*/;

    public final native String getAlbums() /*-{
        return this.params.albums;
    }-*/;

    public final native String getCountry() /*-{
        return this.params.country;
    }-*/;

    public final native String getUsername() /*-{
        return this.params.username;
    }-*/;



}
