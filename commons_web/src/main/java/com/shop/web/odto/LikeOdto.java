package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class LikeOdto extends JavaScriptObject{

    protected LikeOdto() {
    }

    public final native String getFullName()/*-{
        return this.fullName;
    }-*/;

    public final native String getUsername()/*-{
        return this.username;
    }-*/;

    public final native int getUserId()/*-{
        return this.userId;
    }-*/;

     public final native int getProductId()/*-{
        return this.productId;
    }-*/;


}