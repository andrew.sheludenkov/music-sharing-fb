package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class ProductsResultOdto extends JavaScriptObject {

    protected ProductsResultOdto() {
    }

    public final native ProductsResultOdto getResult() /*-{
        return this.result;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;


    public final native JsArray<ProductOdto> getProductDtoList() /*-{
        return this.productDtoList;
    }-*/;

}
