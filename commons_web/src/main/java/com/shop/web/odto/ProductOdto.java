package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class ProductOdto extends JavaScriptObject {

    protected ProductOdto() {
    }

    public final native ProductOdto getProduct() /*-{
        return this.product;
    }-*/;

    public final native JsArray<ProductOdto> getList() /*-{
        return this.list;
    }-*/;

    public final native ProductOdto getCategory() /*-{
        return this.category;
    }-*/;

    public final native int getId() /*-{
        return this.productId;
    }-*/;

    public final native String getShowName() /*-{
        return this.showName;
    }-*/;

    public final native double getActualPrice() /*-{
        return this.actualPrice;
    }-*/;

    public final native JsArray<ProductCharacteristicOdto> getProductCharacteristicList() /*-{
        return this.productCharacteristicList;
    }-*/;

    public final native String getProductUrl()/*-{
        return this.productUrl;
    }-*/;

    public final native String getSmallImageUrl()/*-{
        return this.smallImageUrl;
    }-*/;

    public final native String getShortDescription()/*-{
        return this.shortDescription;
    }-*/;

    public final native String getLongDescription()/*-{
        return this.longDescription;
    }-*/;

    public final native String getBrand()/*-{
        return this.brand;
    }-*/;

    public final native int getAppMarketId()/*-{
        return this.appMarketId;
    }-*/;

    public final native String getAffiliateUrl()/*-{
        return this.affiliateUrl;
    }-*/;

    public final native float getReward ()/*-{
        return this.reward;
    }-*/;

    public final native float getSalePrice ()/*-{
        return this.salePrice;
    }-*/;

    public final native JsArray<LikeOdto> getLikesList() /*-{
        return this.likeDtoList;
    }-*/;
}


