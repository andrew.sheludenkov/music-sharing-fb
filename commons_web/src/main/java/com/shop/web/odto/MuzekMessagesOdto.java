package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class MuzekMessagesOdto extends JavaScriptObject{

    protected MuzekMessagesOdto() {
    }

    public final native String getIdentifier()/*-{
        return this.identifier;
    }-*/;

    public final native String getCreationDate()/*-{
        return this.creation_date;
    }-*/;

    public final native String getSenderId()/*-{
        return this.sender_user_fb_id;
    }-*/;

    public final native String getSenderName()/*-{
        return this.sender_user_name;
    }-*/;

    public final native String getRecipientId()/*-{
        return this.recepient_user_fb_id;
    }-*/;

     public final native String getRecipientName()/*-{
        return this.recipient_user_name;
    }-*/;

    public final native String isTrackAdded()/*-{
        return this.added;
    }-*/;

    public final native String isTrackListened()/*-{
        return this.listened;
    }-*/;



    public final native MuzekTrackOdto getTrack()/*-{
        return this.track;
    }-*/;

}