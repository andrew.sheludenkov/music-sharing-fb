package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class MuzekAlbumOdto extends JavaScriptObject{

    protected MuzekAlbumOdto() {
    }

    public final native String getId()/*-{
        return this.id;
    }-*/;


    public final native String getTitle()/*-{
        return this.title;
    }-*/;

    public final native String getType()/*-{
        return this.type;
    }-*/;



}