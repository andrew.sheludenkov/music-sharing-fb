package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class MuzekListenerOdto extends JavaScriptObject{

    protected MuzekListenerOdto() {
    }

    public final native String getId()/*-{
        return this.id;
    }-*/;


    public final native String getTitle()/*-{
        return this.title;
    }-*/;

    public final native String getType()/*-{
        return this.type;
    }-*/;



}