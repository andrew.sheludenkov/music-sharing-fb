package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class MuzekGraphOdto extends JavaScriptObject{

    protected MuzekGraphOdto() {
    }

    public final native String getId()/*-{
        return this.id;
    }-*/;

    public final native String getGender()/*-{
        return this.gender;
    }-*/;

    public final native String getLocale()/*-{
        return this.locale;
    }-*/;

     public final native String getName()/*-{
        return this.name;
    }-*/;



}