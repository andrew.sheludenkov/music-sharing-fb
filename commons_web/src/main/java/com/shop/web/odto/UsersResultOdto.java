package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class UsersResultOdto extends JavaScriptObject{

    protected UsersResultOdto() {
    }

    public final native UsersResultOdto getResult() /*-{
        return this.userresult;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;


    public final native JsArray<UserOdto> getUserDtoList() /*-{
        return this.userDtoList;
    }-*/;
}
