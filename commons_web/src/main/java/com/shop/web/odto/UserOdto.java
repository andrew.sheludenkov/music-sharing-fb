package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 01.02.13
 */
public class UserOdto extends JavaScriptObject{

    protected UserOdto() {
    }

    public final native UserOdto getUser()/*-{
        return this.user;
    }-*/;

    public final native String getFullName()/*-{
        return this.fullName;
    }-*/;

    public final native String getFacebookUid()/*-{
        return this.facebookUid;
    }-*/;

    public final native int getUserId()/*-{
        return this.id;
    }-*/;

    public final native int getUserUserId()/*-{
        return this.user.id;
    }-*/;

    public final native String getUserEmail()/*-{
        return this.email;
    }-*/;

    public final native String getUserPaymentEmail()/*-{
        return this.paypalEmail;
    }-*/;

    public final native String getUserName()/*-{
        return this.username;
    }-*/;

    public final native String getLoginHash()/*-{
        return this.user.loginHash;
    }-*/;
}