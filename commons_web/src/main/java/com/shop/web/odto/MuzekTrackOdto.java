package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class MuzekTrackOdto extends JavaScriptObject{

    protected MuzekTrackOdto() {
    }

    public final native String getId()/*-{
        return this.id;
    }-*/;

    public final native String getIdentifier()/*-{
        return this.identifier;
    }-*/;

    public final native String getStreamUrl()/*-{
        return this.stream_url;
    }-*/;

    public final native String getTitle()/*-{
        return this.title;
    }-*/;

     public final native String getProductId()/*-{
        return this.productId;
    }-*/;

    public final native String getArtworkUrl()/*-{
        return this.artwork_url;
    }-*/;

    public final native String getPermalinkUrl()/*-{
        return this.permalink_url;
    }-*/;

    public final native String getType()/*-{
        return this.type;
    }-*/;

    public final native MuzekTrackUserOdto getUser()/*-{
        return this.user;
    }-*/;

    public final native String getSortField()/*-{
        return this.sort;
    }-*/;



}