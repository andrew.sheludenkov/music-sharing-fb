package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class MuzekListenersResultOdto extends JavaScriptObject {

    protected MuzekListenersResultOdto() {
    }

    public final native MuzekListenersResultOdto getResult() /*-{
        return this.result;
    }-*/;


    public final native int getResultsFound() /*-{
        return this.resultsFound;
    }-*/;


    public final native JsArray<MuzekListenerOdto> getListenersDtoList() /*-{
        return this.listeners.listeners;
    }-*/;






}
