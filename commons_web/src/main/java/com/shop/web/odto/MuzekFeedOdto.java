package com.shop.web.odto;

import com.google.gwt.core.client.JavaScriptObject;

public class MuzekFeedOdto extends JavaScriptObject{

    protected MuzekFeedOdto() {
    }

    public final native String getCreationDate()/*-{
        return this.creation_date;
    }-*/;

    public final native String getFbuserId()/*-{
        return this.user_fb_id;
    }-*/;

    public final native String getTrackId()/*-{
        return this.track_id;
    }-*/;

    public final native String getUserName()/*-{
        return this.user_name;
    }-*/;

     public final native String getTrackName()/*-{
        return this.track_name;
    }-*/;

    public final native String getFeedItemType()/*-{
        return this.feed_item_type;
    }-*/;

    public final native String getArtworkUrl()/*-{
        return this.artwork_url;
    }-*/;

    public final native String getTrackSource()/*-{
        return this.track_source;
    }-*/;

    public final native String getTrackIdentifier()/*-{
        return this.track_identifier;
    }-*/;

    public final native MuzekTrackOdto getTrack()/*-{
        return this.track;
    }-*/;

}