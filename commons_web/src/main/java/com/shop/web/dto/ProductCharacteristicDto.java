package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 17.04.12
 */
public class ProductCharacteristicDto implements IsSerializable{
    private Long id;
    private String characteristicName;
    private String characteristicValue;
    private String source;

    public ProductCharacteristicDto() {
    }

    public ProductCharacteristicDto(String characteristicName, String characteristicValue, String source) {
        this.characteristicName = characteristicName;
        this.characteristicValue = characteristicValue;
        this.source = source;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCharacteristicName() {
        return characteristicName;
    }

    public void setCharacteristicName(String characteristicName) {
        this.characteristicName = characteristicName;
    }

    public String getCharacteristicValue() {
        return characteristicValue;
    }

    public void setCharacteristicValue(String characteristicValue) {
        this.characteristicValue = characteristicValue;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ProductCharacteristicDto){
            ProductCharacteristicDto target = (ProductCharacteristicDto)obj;
            return this.characteristicName.equals(target.getCharacteristicName()) && this.characteristicValue.equals(target.getCharacteristicValue());
        }

        return false;
    }
}
