package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 27.04.12
 */
public class UserDto implements IsSerializable{
    private Long id;
    private String name;
    private String username;
    private String password;
    private String loginHash;
    private Long appMarketId;
    private String email;
    private String deliveryAddress;
    private String sessionId;
    private String phone;
    private String fullName;
    private String facebookUid;
    private String paypalEmail;
    private Long activeShopId;
    private String linkedinToken;
    private String deviceToken;
    private String locale;
    private String role;
    private String carColor;
    String referralCodeUsages;
    String referralCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginHash() {
        return loginHash;
    }

    public void setLoginHash(String loginHash) {
        this.loginHash = loginHash;
    }

    public Long getAppMarketId() {
        return appMarketId;
    }

    public void setAppMarketId(Long appMarketId) {
        this.appMarketId = appMarketId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFacebookUid() {
        return facebookUid;
    }

    public void setFacebookUid(String facebookUid) {
        this.facebookUid = facebookUid;
    }

    public String getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public Long getActiveShopId() {
        return activeShopId;
    }

    public void setActiveShopId(Long activeShopId) {
        this.activeShopId = activeShopId;
    }

    public String getLinkedinToken() {
        return linkedinToken;
    }

    public void setLinkedinToken(String linkedinToken) {
        this.linkedinToken = linkedinToken;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getReferralCodeUsages() {
        return referralCodeUsages;
    }

    public void setReferralCodeUsages(String referralCodeUsages) {
        this.referralCodeUsages = referralCodeUsages;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}
