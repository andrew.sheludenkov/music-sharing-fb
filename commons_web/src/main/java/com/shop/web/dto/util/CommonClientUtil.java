package com.shop.web.dto.util;


import com.shop.web.dto.ProductPropertyDto;
import com.shop.web.dto.PropertyValueDto;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 09.01.12
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
public class CommonClientUtil {



    public static Double getFraction(Double num) {
        if (num != null) {
            Double rounded = Double.valueOf(Math.round(num * 100));
            return rounded / 100;
        } else {
            return null;
        }
    }

    public static StringBuffer formOptions(List<ProductPropertyDto> propertyDtoList) {
        StringBuffer options = new StringBuffer();
        for (ProductPropertyDto productPropertyDto : propertyDtoList) {
            String name = productPropertyDto.getId().toString();
            String values = "";
            for (PropertyValueDto s : productPropertyDto.getValues()) {
                values += values.length() > 0 ? "," + s.getValue().trim() : s.getValue().trim();
            }
            options.append( options.length() > 0 ? ":" + name + "-" + values : name + "-" + values);
        }
        return options;
    }


}
