package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Date;


public class ActivityDto implements IsSerializable{


    private Long id;
    private String activityType;
    private String description;
    private Date date;
    private UserDto user;



    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
