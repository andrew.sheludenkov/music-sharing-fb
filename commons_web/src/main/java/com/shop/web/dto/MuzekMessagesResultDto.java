package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;


public class MuzekMessagesResultDto implements IsSerializable, Serializable{
    private List<MuzekMessageDto> items;

    private String nextPageToken;

    public List<MuzekMessageDto> getItems() {
        return items;
    }

    public void setItems(List<MuzekMessageDto> items) {
        this.items = items;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }
}
