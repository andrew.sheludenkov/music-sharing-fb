package com.shop.web.dto.linkshare;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.shop.web.dto.UserDto;

import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 20.02.13
 */
public class ProductGroupDto implements IsSerializable{
    private Long id;
    private Long appMarketId;
    private Long productId;
    private String groupName;
    private String description;
    private String imageUrl;
    private List<UserDto> groupUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAppMarketId() {
        return appMarketId;
    }

    public void setAppMarketId(Long appMarketId) {
        this.appMarketId = appMarketId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<UserDto> getGroupUsers() {
        return groupUsers;
    }

    public void setGroupUsers(List<UserDto> groupUsers) {
        this.groupUsers = groupUsers;
    }
}
