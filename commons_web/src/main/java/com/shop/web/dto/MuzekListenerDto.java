package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class MuzekListenerDto implements IsSerializable{

    private String id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
