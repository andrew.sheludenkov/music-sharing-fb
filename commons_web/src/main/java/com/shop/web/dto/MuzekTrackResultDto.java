package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class MuzekTrackResultDto implements IsSerializable, Serializable{
    private List<MuzekTrackDto> tracks;

    private String nextPageToken;
    private String unreadMessages;

    public List<MuzekTrackDto> getTracks() {
        return tracks;
    }

    public void setTracks(List<MuzekTrackDto> tracks) {
        this.tracks = tracks;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(String unreadMessages) {
        this.unreadMessages = unreadMessages;
    }
}
