package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class VendorPriceFileImportDto implements IsSerializable {

    public enum IMPORT_STATUS {
        LOADED,
        PARSED,
        WRONG_FORMAT
    }

    private IMPORT_STATUS importStatus;

    public IMPORT_STATUS getImportStatus() {
        return importStatus;
    }

    public void setImportStatus(IMPORT_STATUS importStatus) {
        this.importStatus = importStatus;
    }
}
