package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class VariableDto implements IsSerializable{

    public enum VARIABLE{
        USD_GRN_EXCHANGE_RATE,
        CURRENT_CURRENCY,
        CHARACTERISTIC_SCAN_VERSION,
        IMAGES_DEST_LOCATION,
        URL_TO_SHOP,
        VENDORS_MAIL_LIST,

    }

    private Long id;
    private VARIABLE variable;

    private String value;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VARIABLE getVariable() {
        return variable;
    }

    public void setVariable(VARIABLE variable) {
        this.variable = variable;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "VariableDto{" +
                "value='" + value + '\'' +
                ", variable=" + variable +
                '}';
    }
}
