package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.03.13
 */
public class ShopDto implements IsSerializable{
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
