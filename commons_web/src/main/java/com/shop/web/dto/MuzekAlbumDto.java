package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class MuzekAlbumDto implements IsSerializable{

    private String type; // native, vk, youtube
    private String id;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
