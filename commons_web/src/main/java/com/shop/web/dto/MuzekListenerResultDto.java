package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class MuzekListenerResultDto implements IsSerializable, Serializable{
    private List<MuzekListenerDto> listeners;


    public List<MuzekListenerDto> getListeners() {
        return listeners;
    }

    public void setListeners(List<MuzekListenerDto> listeners) {
        this.listeners = listeners;
    }
}
