package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Date;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 30.11.12
 */
public class NewsDto implements IsSerializable{
    public enum NotificationStatus {
        DONT_NOTIFY,
        NOTIFY,
        NOTIFICATION_DONE
    }

    private Long id;
    private String name;
    private String newsContent;
    private Date date;
    private NotificationStatus notificationStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }
}
