package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 10.04.12
 */
public class VendorDto implements IsSerializable, Serializable {

    public enum CURRENCY implements Serializable{
        USD,
        GRN,
        RUR
    }

    private Long id;
    
    private String vendorName;
    private String description;
    private Integer priceColumnNumber;
    private String productNameColumnNumber;
    private String fileName;
    private CURRENCY currency;

    DiscountDto discountDto;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPriceColumnNumber() {
        return priceColumnNumber;
    }

    public void setPriceColumnNumber(Integer priceColumnNumber) {
        this.priceColumnNumber = priceColumnNumber;
    }

    public String getProductNameColumnNumber() {
        return productNameColumnNumber;
    }

    public void setProductNameColumnNumber(String productNameColumnNumber) {
        this.productNameColumnNumber = productNameColumnNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public CURRENCY getCurrency() {
        return currency;
    }

    public void setCurrency(CURRENCY currency) {
        this.currency = currency;
    }

    public DiscountDto getDiscountDto() {
        return discountDto;
    }

    public void setDiscountDto(DiscountDto discountDto) {
        this.discountDto = discountDto;
    }
}
