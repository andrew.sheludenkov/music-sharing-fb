package com.shop.web.dto.prices;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.shop.web.dto.VendorDto;

import java.io.Serializable;
import java.util.List;


public class VendorPricesSheetDto implements IsSerializable, Serializable {

    List<VendorDto> vendors;
    List<VendorDateHistory> dateHistories;

    public List<VendorDto> getVendors() {
        return vendors;
    }

    public void setVendors(List<VendorDto> vendors) {
        this.vendors = vendors;
    }

    public List<VendorDateHistory> getDateHistories() {
        return dateHistories;
    }

    public void setDateHistories(List<VendorDateHistory> dateHistories) {
        this.dateHistories = dateHistories;
    }


}
