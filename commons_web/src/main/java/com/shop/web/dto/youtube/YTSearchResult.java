package com.shop.web.dto.youtube;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;


public class YTSearchResult implements IsSerializable, Serializable {


    private String kind;


    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }
}
