package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class ShopPriceDto implements IsSerializable {

    private Double price;
    private String shopName;
    private Date date;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
