package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 11.12.12
 */
public class PropertyValueDto implements IsSerializable, Serializable{
    private String value;
    private Boolean show = true;
    private Integer rank = 1;

    public PropertyValueDto() {
    }

    public PropertyValueDto(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(obj instanceof PropertyValueDto){
            PropertyValueDto other = (PropertyValueDto)obj;
            boolean isEquals = value != null && other.getValue() != null ?  value.equals(other.getValue()) : value == null && other.getValue() == null;
            if(isEquals){
                isEquals = show != null && other.getShow() != null ?  show.equals(other.getShow()) : show == null && other.getShow() == null;
                if(isEquals){
                    isEquals = rank != null && other.getRank() != null ?  rank.equals(other.getRank()) : rank == null && other.getRank() == null;
                    return isEquals;
                }
            }
        }

        return false;
    }
}
