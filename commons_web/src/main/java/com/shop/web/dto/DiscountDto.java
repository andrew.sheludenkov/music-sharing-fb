package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class DiscountDto implements IsSerializable{

    public enum DISCOUNT_TYPE{
        SINGLE_PRODUCT_VALUE(3),
        SINGLE_PRODUCT_PERCENTAGE(2),
        CATEGORY_VENDOR(1),
        VENDOR(0);

        private Integer weight;

        DISCOUNT_TYPE(Integer weight){
            this.weight = weight;
        }

        public Integer getWeight() {
            return weight;
        }
    }
    
    DISCOUNT_TYPE discountType;

    private Long id;

    private Long vendorId;
    private Long categoryId;
    private Long productId;

    private Double percent;
    private Double value;


    private ProductDto product;
    private VendorDto vendor;
    private CategoryDto category;

    private UserDto userDto;

    public DISCOUNT_TYPE getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DISCOUNT_TYPE discountType) {
        this.discountType = discountType;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductDto getProduct() {
        return product;
    }

    public void setProduct(ProductDto product) {
        this.product = product;
    }

    public VendorDto getVendor() {
        return vendor;
    }

    public void setVendor(VendorDto vendor) {
        this.vendor = vendor;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    @Override
    public String toString() {
        return "DiscountDto{" +
                "discountType=" + discountType +
                ", id=" + id +
                ", vendorId=" + vendorId +
                ", categoryId=" + categoryId +
                ", productId=" + productId +
                ", percent=" + percent +
                ", value=" + value +
                ", product=" + product +
                ", vendor=" + vendor +
                ", category=" + category +
                '}';
    }
}
