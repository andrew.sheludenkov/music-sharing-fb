package com.shop.web.dto.prices;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.shop.web.dto.VendorDto;

import java.io.Serializable;
import java.util.List;

/**
* Created by IntelliJ IDEA.
* User: Andrew
* Date: 14.04.12
* Time: 0:28
* To change this template use File | Settings | File Templates.
*/
public class VendorDateHistory implements IsSerializable, Serializable {

    String date;
    List<VendorDto> vendors;
    List<VendorFilesHistory> files;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<VendorFilesHistory> getFiles() {
        return files;
    }

    public void setFiles(List<VendorFilesHistory> files) {
        this.files = files;
    }

    public List<VendorDto> getVendors() {
        return vendors;
    }

    public void setVendors(List<VendorDto> vendors) {
        this.vendors = vendors;
    }
}
