package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;


public class MuzekFeedResultDto implements IsSerializable, Serializable{
    private List<MuzekFeedDto> items;

    private String nextPageToken;

    public List<MuzekFeedDto> getItems() {
        return items;
    }

    public void setItems(List<MuzekFeedDto> items) {
        this.items = items;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }
}
