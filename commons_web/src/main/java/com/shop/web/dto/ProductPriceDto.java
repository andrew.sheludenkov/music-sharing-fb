package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class ProductPriceDto implements IsSerializable, Serializable {

    private Double price;
    private Date date;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
