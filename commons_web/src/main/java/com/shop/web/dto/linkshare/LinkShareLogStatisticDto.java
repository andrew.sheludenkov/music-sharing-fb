package com.shop.web.dto.linkshare;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 22.02.13
 */
public class LinkShareLogStatisticDto implements IsSerializable{
    private Map<Long, LinkShareLogStatisticItemDto> statistic = new HashMap<Long, LinkShareLogStatisticItemDto>();

    public void addToStatistic(LinkShareLogDto log){
        LinkShareLogStatisticItemDto item = statistic.get(log.getProductId());
        if(item != null){
            item.increaseQuantity();
        }else {
            item = new LinkShareLogStatisticItemDto(log);
            statistic.put(log.getProductId(), item);
        }
    }

    public List<LinkShareLogStatisticItemDto> getStatistic(){
        return  statistic.size() > 0 ? new ArrayList<LinkShareLogStatisticItemDto>(statistic.values()) : null;
    }
}
