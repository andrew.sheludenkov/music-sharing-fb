package com.shop.web.dto.prices;


import com.google.gwt.user.client.rpc.IsSerializable;
import com.shop.web.dto.VendorPriceFileImportDto;

import java.io.Serializable;

/**
* Created by IntelliJ IDEA.
* User: Andrew
* Date: 14.04.12
* Time: 0:27
* To change this template use File | Settings | File Templates.
*/
public class VendorFilesHistory implements IsSerializable, Serializable {

    String name;

    String dirName;

    VendorPriceFileImportDto vendorPriceFileImportDto;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public VendorPriceFileImportDto getVendorPriceFileImportDto() {
        return vendorPriceFileImportDto;
    }

    public void setVendorPriceFileImportDto(VendorPriceFileImportDto vendorPriceFileImportDto) {
        this.vendorPriceFileImportDto = vendorPriceFileImportDto;
    }
}
