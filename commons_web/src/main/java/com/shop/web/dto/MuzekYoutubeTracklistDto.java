package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class MuzekYoutubeTracklistDto implements IsSerializable{

    private String id;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
