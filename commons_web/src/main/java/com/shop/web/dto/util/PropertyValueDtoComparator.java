package com.shop.web.dto.util;

import com.shop.web.dto.PropertyValueDto;

import java.util.Comparator;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 20.12.12
 */
public class PropertyValueDtoComparator implements Comparator<PropertyValueDto>{
    @Override
    public int compare(PropertyValueDto o1, PropertyValueDto o2) {
        if ((o1.getShow() && o2.getShow())) {
            return compareRank(o1, o2);
        } else if(o1.getShow()){
            return -1;
        }else if(o2.getShow()){
            return 1;
        }else {
            return compareRank(o1, o2);
        }
    }

    protected int compareRank(PropertyValueDto o1, PropertyValueDto o2) {
        if (o1.getRank() > o2.getRank()) {
            return -1;
        } else if (o1.getRank() < o2.getRank()) {
            return 1;
        } else {
            return 0;
        }
    }
}
