package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class UserResultDto implements IsSerializable, Serializable{
    private List<UserDto> userDtoList;
    private Integer resultsFound;

    public List<UserDto> getUserDtoList() {
        return userDtoList;
    }

    public void setUserDtoList(List<UserDto> userDtoList) {
        this.userDtoList = userDtoList;
    }

    public Integer getResultsFound() {
        return resultsFound;
    }

    public void setResultsFound(Integer resultsFound) {
        this.resultsFound = resultsFound;
    }
}
