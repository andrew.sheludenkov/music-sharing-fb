package com.shop.web.dto;


import com.google.gwt.user.client.rpc.IsSerializable;
import com.shop.web.dto.util.CommonClientUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class ProductVendorDto implements IsSerializable, Serializable {



    private Double productPrice;
    private String productName;

    private Long vendorId;
    private String vendorName;
    private String vendorDescription;

    private VendorDto.CURRENCY currency;

    private List<ProductPriceDto> productPrices;
    private Boolean productAvailable;
    private Boolean productOverdue;


    public Double getProductPrice() {
        return CommonClientUtil.getFraction(productPrice);
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorDescription() {
        return vendorDescription;
    }

    public void setVendorDescription(String vendorDescription) {
        this.vendorDescription = vendorDescription;
    }

    public List<ProductPriceDto> getProductPrices() {
        return productPrices;
    }

    public void setProductPrices(List<ProductPriceDto> productPrices) {
        this.productPrices = productPrices;
    }

    public VendorDto.CURRENCY getCurrency() {
        return currency;
    }

    public void setCurrency(VendorDto.CURRENCY currency) {
        this.currency = currency;
    }

    public Boolean getProductAvailable() {
        return productAvailable;
    }

    public void setProductAvailable(Boolean productAvailable) {
        this.productAvailable = productAvailable;
    }

    public Boolean getProductOverdue() {
        return productOverdue;
    }

    public void setProductOverdue(Boolean productOverdue) {
        this.productOverdue = productOverdue;
    }
}
