package com.shop.web.dto.linkshare;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 22.02.13
 */
public class LinkShareLogStatisticItemDto implements IsSerializable{
    private LinkShareLogDto log;
    private Integer quantity;

    public LinkShareLogStatisticItemDto() {
        quantity = 0;
    }

    public LinkShareLogStatisticItemDto(LinkShareLogDto log) {
        this.log = log;
        this.quantity = 1;
    }

    public LinkShareLogDto getLog() {
        return log;
    }

    public void setLog(LinkShareLogDto log) {
        this.log = log;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void increaseQuantity(){
        quantity++;
    }
}
