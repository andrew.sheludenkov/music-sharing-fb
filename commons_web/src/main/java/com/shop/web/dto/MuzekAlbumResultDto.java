package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class MuzekAlbumResultDto implements IsSerializable, Serializable{
    private List<MuzekAlbumDto> albums;


    public List<MuzekAlbumDto> getAlbums() {
        return albums;
    }

    public void setAlbums(List<MuzekAlbumDto> albums) {
        this.albums = albums;
    }
}
