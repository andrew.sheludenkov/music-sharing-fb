package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.02.13
 */
public class HemechooCommentsResultDto implements IsSerializable, Serializable{
    private List<HemeChooCommentDto> comments;
    private Integer resultsFound;
    private String hide;

    public List<HemeChooCommentDto> getComments() {
        return comments;
    }

    public void setComments(List<HemeChooCommentDto> comments) {
        this.comments = comments;
    }

    public Integer getResultsFound() {
        return resultsFound;
    }

    public void setResultsFound(Integer resultsFound) {
        this.resultsFound = resultsFound;
    }

    public String getHide() {
        return hide;
    }

    public void setHide(String hide) {
        this.hide = hide;
    }
}
