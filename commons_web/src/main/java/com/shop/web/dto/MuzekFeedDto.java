package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class MuzekFeedDto implements IsSerializable{

    private String creation_date;
    private String user_fb_id;
    private String track_id;
    private String user_name;
    private String track_name;
    private String feed_item_type;
    private String artwork_url;
    private String track_source;
    private String track_identifier;

    private MuzekTrackDto track;


    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getUser_fb_id() {
        return user_fb_id;
    }

    public void setUser_fb_id(String user_fb_id) {
        this.user_fb_id = user_fb_id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getFeed_item_type() {
        return feed_item_type;
    }

    public void setFeed_item_type(String feed_item_type) {
        this.feed_item_type = feed_item_type;
    }

    public String getArtwork_url() {
        return artwork_url;
    }

    public void setArtwork_url(String artwork_url) {
        this.artwork_url = artwork_url;
    }

    public String getTrack_source() {
        return track_source;
    }

    public void setTrack_source(String track_source) {
        this.track_source = track_source;
    }

    public String getTrack_identifier() {
        return track_identifier;
    }

    public void setTrack_identifier(String track_identifier) {
        this.track_identifier = track_identifier;
    }

    public MuzekTrackDto getTrack() {
        return track;
    }

    public void setTrack(MuzekTrackDto track) {
        this.track = track;
    }
}
