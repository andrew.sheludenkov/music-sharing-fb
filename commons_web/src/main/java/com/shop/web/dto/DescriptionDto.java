package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Date;


public class DescriptionDto implements IsSerializable{

    public enum DESCRIPTION_TYPE{
        CATEGORY,
        PRODUCT
    }

    public enum PRIORITY{
        DESCRIPTION,
        GRAY
    }

    private Long id;
    private Long productId;
    private Long categoryId;
    private Date creationDate;

    private String text;

    private DESCRIPTION_TYPE descriptionType;
    private PRIORITY priority;

    public DescriptionDto() {
        priority = DescriptionDto.PRIORITY.DESCRIPTION;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public DESCRIPTION_TYPE getDescriptionType() {
        return descriptionType;
    }

    public void setDescriptionType(DESCRIPTION_TYPE descriptionType) {
        this.descriptionType = descriptionType;
    }

    public PRIORITY getPriority() {
        return priority;
    }

    public void setPriority(PRIORITY priority) {
        this.priority = priority;
    }
}
