package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class MuzekMessageDto implements IsSerializable{

    private String identifier;
    private String creation_date;
    private String sender_user_fb_id;
    private String sender_user_name;
    private String recepient_user_fb_id;
    private String recipient_user_name;
    private String track_id;
    private String track_source;
    private String track_name;
    private String added;
    private String listened;

    private MuzekTrackDto track;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getSender_user_fb_id() {
        return sender_user_fb_id;
    }

    public void setSender_user_fb_id(String sender_user_fb_id) {
        this.sender_user_fb_id = sender_user_fb_id;
    }

    public String getRecepient_user_fb_id() {
        return recepient_user_fb_id;
    }

    public void setRecepient_user_fb_id(String recepient_user_fb_id) {
        this.recepient_user_fb_id = recepient_user_fb_id;
    }

    public String getRecipient_user_name() {
        return recipient_user_name;
    }

    public void setRecipient_user_name(String recipient_user_name) {
        this.recipient_user_name = recipient_user_name;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getTrack_source() {
        return track_source;
    }

    public void setTrack_source(String track_source) {
        this.track_source = track_source;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getListened() {
        return listened;
    }

    public void setListened(String listened) {
        this.listened = listened;
    }

    public MuzekTrackDto getTrack() {
        return track;
    }

    public void setTrack(MuzekTrackDto track) {
        this.track = track;
    }

    public String getSender_user_name() {
        return sender_user_name;
    }

    public void setSender_user_name(String sender_user_name) {
        this.sender_user_name = sender_user_name;
    }
}
