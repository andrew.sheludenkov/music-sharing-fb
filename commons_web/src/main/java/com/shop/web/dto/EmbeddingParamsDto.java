package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class EmbeddingParamsDto implements IsSerializable{

    private String before;
    private String after;
    private String inside;
    private String replaceid;
    private String styles;
    private String hosts;
    private String width;
    private String hide;
    private String css;
    private String image;
    private String comments;
    private String imgwidth;

    private String share;
    private String feedback;
    private String created;

    private String vkimport;
    private String vkimportstatus;
    private String vkimportstat;

    private String youtubeimportstatus;
    private String youtubeimportstat;

    private String albums;
    private String country;

    private String username;



    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    public String getInside() {
        return inside;
    }

    public void setInside(String inside) {
        this.inside = inside;
    }

    public String getReplaceid() {
        return replaceid;
    }

    public void setReplaceid(String replaceid) {
        this.replaceid = replaceid;
    }

    public String getStyles() {
        return styles;
    }

    public void setStyles(String styles) {
        this.styles = styles;
    }

    public String getHosts() {
        return hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHide() {
        return hide;
    }

    public void setHide(String hide) {
        this.hide = hide;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getImgwidth() {
        return imgwidth;
    }

    public void setImgwidth(String imgwidth) {
        this.imgwidth = imgwidth;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getVkimport() {
        return vkimport;
    }

    public void setVkimport(String vkimport) {
        this.vkimport = vkimport;
    }

    public String getVkimportstatus() {
        return vkimportstatus;
    }

    public void setVkimportstatus(String vkimportstatus) {
        this.vkimportstatus = vkimportstatus;
    }

    public String getVkimportstat() {
        return vkimportstat;
    }

    public void setVkimportstat(String vkimportstat) {
        this.vkimportstat = vkimportstat;
    }

    public String getAlbums() {
        return albums;
    }

    public void setAlbums(String albums) {
        this.albums = albums;
    }

    public String getYoutubeimportstatus() {
        return youtubeimportstatus;
    }

    public void setYoutubeimportstatus(String youtubeimportstatus) {
        this.youtubeimportstatus = youtubeimportstatus;
    }

    public String getYoutubeimportstat() {
        return youtubeimportstat;
    }

    public void setYoutubeimportstat(String youtubeimportstat) {
        this.youtubeimportstat = youtubeimportstat;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
