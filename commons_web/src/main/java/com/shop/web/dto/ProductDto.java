package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class ProductDto implements IsSerializable, Serializable {

    public enum PUBLISH_STATUS{
        PUBLISHED,
        NOT_PUBLISHED,
        BLACK_LIST,
        ALL,
    }

    public enum AVAILABILITY{
        AVAILABLE, // в наличии
        ORDER, // под заказ
        CALL, // наличие уточняйте
        OVERDUE, // прайс просрочен
    }

    private Long productId;
    private Long idInPrice;
    private String itemName;
    private String itemCleanName;
    private List<ProductVendorDto> vendorList;
    private List<ShopPriceDto> recentShopPriceList;
    private List<ShopPriceDto> shopPriceList;
    private List<ProductCharacteristicDto> productCharacteristicList;
    private List<String> productImages;
    private List<DescriptionDto> descriptionProductDtoList;
    private List<DescriptionDto> descriptionCategoryDtoList;
    private Double actualPrice;
    private Double manualPrice;
    private Long sku;
    private Long appMarketId;


    private Integer status;
    private Boolean found;
    private List<Long> categoryIdList;
    private String categories;
    private String canonicalName;
    private String nonCanonicalName;

    private String mergedProducts;
    private String productUrl;
    private String imageUrl;
    private String smallImageUrl;
    private Boolean hasImages;

    private String showName;
    private Boolean isRed;
    private String brand;
    private String assignedName;

    private PUBLISH_STATUS publishStatus;
    private ProductDto.AVAILABILITY availability;

    private String shortDescription;
    private String longDescription;

    private String affiliateUrl;
    private Float reward;
    private Double salePrice;

    private List<LikeDto> likeDtoList;

    public List<LikeDto> getLikeDtoList() {
        return likeDtoList;
    }

    public void setLikeDtoList(List<LikeDto> likeDtoList) {
        this.likeDtoList = likeDtoList;
    }

    public List<ProductVendorDto> getVendorList() {
        return vendorList;
    }

    public void setVendorList(List<ProductVendorDto> vendorList) {
        this.vendorList = vendorList;
    }

    public Long getIdInPrice() {
        return idInPrice;
    }

    public void setIdInPrice(Long idInPrice) {
        this.idInPrice = idInPrice;
    }

    public String getItemName() {
        if (canonicalName != null) {
            return canonicalName;
        }
        if (nonCanonicalName != null) {
            return nonCanonicalName;
        }

        for (ProductVendorDto productVendorDto : vendorList) {
            if (productVendorDto.getProductName() != null) {
                return productVendorDto.getProductName();
            }
        }

        return "No name";
    }

    public Double getCustomerPrice() {
        return ((manualPrice != null) && (manualPrice > 0)) ? manualPrice : actualPrice;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getFound() {
        return found;
    }

    public void setFound(Boolean found) {
        this.found = found;
    }

    public String getCanonicalName() {
        return canonicalName;
    }

    public void setCanonicalName(String canonicalName) {
        this.canonicalName = canonicalName;
    }

    public String getNonCanonicalName() {
        return nonCanonicalName;
    }

    public void setNonCanonicalName(String nonCanonicalName) {
        this.nonCanonicalName = nonCanonicalName;
    }

    public List<ShopPriceDto> getRecentShopPriceList() {
        return recentShopPriceList;
    }

    public void setRecentShopPriceList(List<ShopPriceDto> recentShopPriceList) {
        this.recentShopPriceList = recentShopPriceList;
    }

    public List<ShopPriceDto> getShopPriceList() {
        return shopPriceList;
    }

    public void setShopPriceList(List<ShopPriceDto> shopPriceList) {
        this.shopPriceList = shopPriceList;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<ProductCharacteristicDto> getProductCharacteristicList() {
        return productCharacteristicList;
    }

    public void setProductCharacteristicList(List<ProductCharacteristicDto> productCharacteristicList) {
        this.productCharacteristicList = productCharacteristicList;
    }

    public List<String> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<String> productImages) {
        this.productImages = productImages;
    }

    public String getMergedProducts() {
        return mergedProducts;
    }

    public void setMergedProducts(String mergedProducts) {
        this.mergedProducts = mergedProducts;
    }

    public String getItemCleanName() {
        if (itemCleanName != null && itemCleanName.trim().length() > 0) {
            return itemCleanName;
        } else {
            return getItemName();
        }

    }

    public void setItemCleanName(String itemCleanName) {
        this.itemCleanName = itemCleanName;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public PUBLISH_STATUS getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(PUBLISH_STATUS publishStatus) {
        this.publishStatus = publishStatus;
    }

    public Boolean getHasImages() {
        return hasImages;
    }

    public void setHasImages(Boolean hasImages) {
        this.hasImages = hasImages;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    public String getSmallImageUrl() {
        return smallImageUrl;
    }

    public void setSmallImageUrl(String smallImageUrl) {
        this.smallImageUrl = smallImageUrl;
    }

    public Boolean getRed() {
        return isRed;
    }

    public void setRed(Boolean red) {
        isRed = red;
    }

    public List<DescriptionDto> getDescriptionProductDtoList() {
        return descriptionProductDtoList;
    }

    public void setDescriptionProductDtoList(List<DescriptionDto> descriptionProductDtoList) {
        this.descriptionProductDtoList = descriptionProductDtoList;
    }

    public List<DescriptionDto> getDescriptionCategoryDtoList() {
        return descriptionCategoryDtoList;
    }

    public void setDescriptionCategoryDtoList(List<DescriptionDto> descriptionCategoryDtoList) {
        this.descriptionCategoryDtoList = descriptionCategoryDtoList;
    }

    public AVAILABILITY getAvailability() {
        return availability;
    }

    public void setAvailability(AVAILABILITY availability) {
        this.availability = availability;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getAssignedName() {
        return assignedName;
    }

    public void setAssignedName(String assignedName) {
        this.assignedName = assignedName;
    }

    public Double getManualPrice() {
        return manualPrice;
    }

    public void setManualPrice(Double manualPrice) {
        this.manualPrice = manualPrice;
    }

    public Long getSku() {
        return sku;
    }

    public void setSku(Long sku) {
        this.sku = sku;
    }

    public List<Long> getCategoryIdList() {
        return categoryIdList;
    }

    public void setCategoryIdList(List<Long> categoryIdList) {
        this.categoryIdList = categoryIdList;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Long getAppmarketId() {
        return appMarketId;
    }

    public void setAppmarketId(Long appmarketId) {
        this.appMarketId = appmarketId;
    }

    public String getAffiliateUrl() {
        return affiliateUrl;
    }

    public void setAffiliateUrl(String affiliateUrl) {
        this.affiliateUrl = affiliateUrl;
    }

    public Float getReward() {
        return reward;
    }

    public void setReward(Float reward) {
        this.reward = reward;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }
}
