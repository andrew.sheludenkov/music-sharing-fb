package com.shop.web.dto;

import com.google.gwt.user.client.rpc.IsSerializable;


public class MuzekTrackUserDto implements IsSerializable{

    private String permalink_url;
    private String username;


    public String getPermalink_url() {
        return permalink_url;
    }

    public void setPermalink_url(String permalink_url) {
        this.permalink_url = permalink_url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
