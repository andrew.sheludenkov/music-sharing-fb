package com.shop.web.service;


import com.shop.model.persistence.impl.UserImpl;
import com.shop.web.dto.UserDto;
import com.shop.model.persistence.User;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 27.04.12
 */
public class UserDtoService {

    public static UserDto toDto(User user) {
        UserDto dto = new UserDto();

        dto.setId(user.getIdentifier());
        dto.setName(user.getName());
        dto.setUsername(user.getUsername());
        dto.setPassword(user.getPassword());
        dto.setLoginHash(user.getLoginHash());
        dto.setAppMarketId(user.getAppMarketId());

        dto.setSessionId(user.getSessionId());
        dto.setEmail(user.getEmail());
        dto.setDeliveryAddress(user.getDeliveryAddress());
        dto.setPhone(user.getPhone());
        dto.setFullName(user.getFullName());
        dto.setFacebookUid(user.getFacebookUid());
        dto.setPaypalEmail(user.getPaypalEmail());
        dto.setActiveShopId(user.getActiveShopId());

        dto.setCarColor(user.getCarColor());
        dto.setReferralCodeUsages("0");
        if (user.getUnboxerPromoUsage() != null) {
            dto.setReferralCodeUsages(user.getUnboxerPromoUsage().toString());
        }

        dto.setReferralCode(user.getReferralCode());

        return dto;
    }



    public static void merge(User user, UserDto userDto) {
        user.setName(userDto.getName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
    }

    public static User toModel(UserDto userDto) {
        User user = new UserImpl();

        user.setIdentifier(userDto.getId());
        user.setName(userDto.getName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setDeliveryAddress(userDto.getDeliveryAddress());
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        user.setFullName(userDto.getFullName());
        user.setLinkedinToken(userDto.getLinkedinToken());
        user.setDeviceToken(userDto.getDeviceToken());
        user.setUpperName(userDto.getName().toUpperCase());
        user.setLocale(userDto.getLocale());

        return user;
    }
}
