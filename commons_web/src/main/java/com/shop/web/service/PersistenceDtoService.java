package com.shop.web.service;

import com.shop.model.persistence.Persistence;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Roman
 * Timestamp: 10.12.12 16:25
 */
public class PersistenceDtoService {

    public List<Long> toIdList(List<? extends Persistence> persistenceList) {
        if (persistenceList == null) {
            return null;
        }
        List<Long> idList = new ArrayList<Long>();
        for (Persistence persistence : persistenceList) {
            idList.add(persistence.getIdentifier());
        }
        return idList;
    }

}
