package com.shop.web.service;

import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import com.shop.web.dto.MuzekListenerDto;
import com.shop.web.dto.MuzekTrackDto;
import com.shop.web.dto.MuzekTrackUserDto;

import java.util.ArrayList;
import java.util.List;

//import com.google.gwt.i18n.server.testing.Child;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class MuzekTracksDtoService {


    public static MuzekTrackDto toDto(MuzekTrackList track) {

        MuzekTrackDto dto = new MuzekTrackDto();

        dto.setId(track.getTrackId());
        dto.setIdentifier(track.getIdentifier().toString());
        String streamUrl = "https://api.soundcloud.com/tracks/" + track.getTrackId() + "/stream";
        dto.setStream_url(streamUrl);
        dto.setTitle(track.getName());

        dto.setArtwork_url(track.getArtworkUrl());
        dto.setPermalink_url(track.getTrackPermalink());

        MuzekTrackUserDto user = new MuzekTrackUserDto();
        user.setPermalink_url(track.getUser_permalink_url());
        user.setUsername(track.getUsername());

        if (track.getSource().equals(MuzekTrackListImpl.MUZEK_TRACK_SOURCE.YOUTUBE)){
            dto.setType("youtube");
        }

        dto.setUser(user);
        dto.setSort(track.getSortField().toString());

        return dto;
    }

    public static MuzekListenerDto toListenerDto(MuzekTrackList track) {

        MuzekListenerDto dto = new MuzekListenerDto();

        dto.setId(track.getUserFBid());


        return dto;
    }

    public static List<MuzekTrackDto> toDtoList(List<MuzekTrackList> comments) {
        List<MuzekTrackDto> list = new ArrayList<MuzekTrackDto>();

        if (comments != null) {
            for (MuzekTrackList comment : comments) {
                list.add(toDto(comment));
            }
        }

        return list;
    }

    public static List<MuzekListenerDto> toListenerDtoList(List<MuzekTrackList> comments) {
        List<MuzekListenerDto> list = new ArrayList<MuzekListenerDto>();

        if (comments != null) {
            for (MuzekTrackList comment : comments) {
                if (comment.getUserFBid() != null){
                    list.add(toListenerDto(comment));
                }
            }
        }

        return list;
    }



}
