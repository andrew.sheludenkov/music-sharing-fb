package com.shop.web.service;

import com.shop.model.persistence.MuzekAlbums;
import com.shop.model.persistence.MuzekTrackList;
import com.shop.model.persistence.impl.MuzekAlbumsImpl;
import com.shop.model.persistence.impl.MuzekTrackListImpl;
import com.shop.web.dto.MuzekAlbumDto;
import com.shop.web.dto.MuzekTrackDto;
import com.shop.web.dto.MuzekTrackUserDto;

import java.util.ArrayList;
import java.util.List;

//import com.google.gwt.i18n.server.testing.Child;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class MuzekAlbumDtoService {


    public static MuzekAlbumDto toDto(MuzekAlbums album) {

        MuzekAlbumDto dto = new MuzekAlbumDto();

        String type = "native";

        if (album.getAlbumSource().equals(MuzekAlbumsImpl.ALBUM_SOURCE.VK)){
            dto.setId(album.getVkAlbumId().toString());
            dto.setTitle(album.getVkAlbumTitle());
            dto.setType("vk");
        }
        if (album.getAlbumSource().equals(MuzekAlbumsImpl.ALBUM_SOURCE.YOUTUBE)){
            if (album.getYoutubeAlbumId()!= null){
                dto.setId(album.getYoutubeAlbumId().toString());
            }
            dto.setTitle(album.getYoutubeAlbumTitle());
            dto.setType("youtube");
        }


        return dto;
    }

    public static List<MuzekAlbumDto> toDtoList(List<MuzekAlbums> comments) {
        List<MuzekAlbumDto> list = new ArrayList<MuzekAlbumDto>();

        if (comments != null) {
            for (MuzekAlbums comment : comments) {
                list.add(toDto(comment));
            }
        }

        return list;
    }



}
