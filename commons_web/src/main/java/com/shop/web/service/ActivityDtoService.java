package com.shop.web.service;

import com.shop.web.dto.ActivityDto;
import com.shop.model.persistence.ActivityLog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class ActivityDtoService {


    public ActivityDto toDto(ActivityLog activity) {

        ActivityDto activityDto = new ActivityDto();
        activityDto.setId(activity.getIdentifier());
        activityDto.setActivityType(activity.getType().toString());
        activityDto.setDate(activity.getDateOfAction());
        activityDto.setDescription(activity.getDescription());
        if (activity.getUser() != null) {
            activityDto.setUser(UserDtoService.toDto(activity.getUser()));
        }

        return activityDto;
    }

    public List<ActivityDto> toDtoList(List<ActivityLog> discountList) {

        List<ActivityDto> dtoList = new ArrayList<ActivityDto>();

        for (ActivityLog discount : discountList) {
            dtoList.add(toDto(discount));
        }

        return dtoList;
    }


}
