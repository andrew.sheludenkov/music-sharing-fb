package com.shop.web.service;

import com.shop.model.persistence.MuzekFeed;
import com.shop.model.persistence.MuzekMessage;
import com.shop.web.dto.MuzekFeedDto;
import com.shop.web.dto.MuzekMessageDto;
import com.shop.web.dto.MuzekTrackDto;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//import com.google.gwt.i18n.server.testing.Child;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class MuzekMessagesDtoService {


    public static MuzekMessageDto toDto(MuzekMessage message) {

        MuzekMessageDto dto = new MuzekMessageDto();
        dto.setIdentifier(message.getIdentifier().toString());

        dto.setRecepient_user_fb_id(message.getRecipientFbId());
        dto.setRecipient_user_name(message.getRecipient_name());

        dto.setSender_user_fb_id(message.getSenderFBid());
        dto.setSender_user_name(message.getSender_name());

        dto.setAdded(message.getAdded());
        dto.setListened(message.getListened());

        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMM, HH:mm");
        DateTimeFormatter engFmt = fmt.withLocale(new Locale("ru"));
        DateTime dt = new DateTime(message.getCreationDate());
        dt = dt.plusHours(3);
        dto.setCreation_date(dt.toString(engFmt));

        MuzekTrackDto trackDto = new MuzekTrackDto();
        trackDto.setTitle(message.getTrack_name());
        //trackDto.setIdentifier(String.valueOf(feed.getTrackIdentifier()));
        trackDto.setId(message.getTrackId());
        //trackDto.setArtwork_url(feed.getArtworkUrl());
        if (message.getTrackSource().equals("YOUTUBE")){
            trackDto.setType("youtube");
        }
        String streamUrl = "https://api.soundcloud.com/tracks/" + message.getTrackId() + "/stream";
        trackDto.setStream_url(streamUrl);


        dto.setTrack(trackDto);

        return dto;
    }



    public static List<MuzekMessageDto> toDtoList(List<MuzekMessage> feed) {
        List<MuzekMessageDto> list = new ArrayList<MuzekMessageDto>();

        if (feed != null) {
            for (MuzekMessage item : feed) {
                list.add(toDto(item));
            }
        }

        return list;
    }



}
