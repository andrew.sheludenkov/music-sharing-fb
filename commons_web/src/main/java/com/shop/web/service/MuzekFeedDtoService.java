package com.shop.web.service;

import com.shop.model.persistence.MuzekFeed;
import com.shop.model.persistence.User;
import com.shop.web.dto.MuzekFeedDto;
import com.shop.web.dto.MuzekTrackDto;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//import com.google.gwt.i18n.server.testing.Child;

/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 05.03.12
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class MuzekFeedDtoService {


    public static MuzekFeedDto toDto(MuzekFeed feed) {

        MuzekFeedDto dto = new MuzekFeedDto();

        dto.setUser_name(feed.getUser_name());
        dto.setTrack_name(feed.getTrack_name());
        dto.setArtwork_url(feed.getArtworkUrl());
        dto.setFeed_item_type(feed.getStatus().toString());
        dto.setTrack_id(feed.getTrackId());
        dto.setTrack_source(feed.getTrackSource());
        dto.setUser_fb_id(feed.getUserFBid());
        dto.setTrack_identifier(String.valueOf(feed.getTrackIdentifier()));

        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMM, HH:mm");
        DateTimeFormatter engFmt = fmt.withLocale(new Locale("ru"));
        DateTime dt = new DateTime(feed.getCreationDate());
        dt = dt.plusHours(3);
        dto.setCreation_date(dt.toString(engFmt));

        MuzekTrackDto trackDto = new MuzekTrackDto();
        trackDto.setTitle(feed.getTrack_name());
        trackDto.setIdentifier(String.valueOf(feed.getTrackIdentifier()));
        trackDto.setId(feed.getTrackId());
        trackDto.setArtwork_url(feed.getArtworkUrl());
        if (feed.getTrackSource().equals("YOUTUBE")){
            trackDto.setType("youtube");
        }
        String streamUrl = "https://api.soundcloud.com/tracks/" + feed.getTrackId() + "/stream";
        trackDto.setStream_url(streamUrl);


        dto.setTrack(trackDto);

        return dto;
    }



    public static List<MuzekFeedDto> toDtoList(List<MuzekFeed> feed) {
        List<MuzekFeedDto> list = new ArrayList<MuzekFeedDto>();

        if (feed != null) {
            for (MuzekFeed item : feed) {
                list.add(toDto(item));
            }
        }

        return list;
    }


    public static MuzekFeedDto toDto(User user) {

        MuzekFeedDto dto = new MuzekFeedDto();

        dto.setUser_name(user.getName());
        dto.setUser_fb_id(user.getMuzekFBid());

        return dto;
    }



    public static List<MuzekFeedDto> toUserDtoList(List<User> feed) {
        List<MuzekFeedDto> list = new ArrayList<MuzekFeedDto>();

        if (feed != null) {
            for (User item : feed) {
                list.add(toDto(item));
            }
        }

        return list;
    }

}
