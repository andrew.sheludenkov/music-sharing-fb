package com.shop.util;

import java.util.*;

/**
 * User: Roman
 * Timestamp: 21.11.12 17:23
 */
public class MultiKeyMap<K1, K2, V> {
    private Map<K1, Map<K2,V>> map;

    public MultiKeyMap() {
        map = new HashMap<K1, Map<K2, V>>();
    }

    public void put(K1 key1, K2 key2, V value) {
        if (!map.containsKey(key1)) {
            map.put(key1, new HashMap<K2, V>());
        }
        map.get(key1).put(key2, value);
    }

    public V get(K1 key1, K2 key2) {
        if (map.containsKey(key1)) {
            return map.get(key1).get(key2);
        }
        return null;
    }

    public Set<K1> keySet() {
        return map.keySet();
    }
}
