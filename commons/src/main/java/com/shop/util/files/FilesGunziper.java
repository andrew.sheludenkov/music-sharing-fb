package com.shop.util.files;

import java.io.*;
import java.util.zip.GZIPInputStream;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 29.12.12
 */
public class FilesGunziper {

    public void gunzipFilesInDirectory(String sourceDirPath, String remoteDirPath, boolean removeZips) throws IOException {
        File sourceDir = new File(sourceDirPath);
        if(sourceDir.exists()){
            File remoteFileDir = new File(remoteDirPath);
            if(!remoteFileDir.exists()){
                remoteFileDir.mkdirs();
            }
            String[] files = sourceDir.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".gz");
                }
            });

            if(files != null && files.length > 0){
                for(String file : files){
                    String resFileName = remoteDirPath.endsWith("/") ? remoteDirPath : remoteDirPath + "/";
                    resFileName = resFileName + file.replace(".gz", "");
                    gunzipFile(new File(sourceDir.getPath() + "/" + file), resFileName, removeZips);
                }
            }
        }
    }

    public void gunzipFile(File fullGzipFile, String outputFilePath, boolean removeZip) throws IOException {
        if (fullGzipFile.exists() && fullGzipFile.length() > 0) {
            File outputFile = new File(outputFilePath);
            if(outputFile.exists()){
                outputFile.delete();
            }
            outputFile.createNewFile();
            GZIPInputStream inputStream = new GZIPInputStream(new FileInputStream(fullGzipFile));
            FileOutputStream outputStream = new FileOutputStream(outputFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0){
                outputStream.write(buf, 0, len);
            }

            inputStream.close();
            outputStream.close();
        }
    }
}
