package com.shop.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.List;
import java.util.Set;


public class LuceneResult implements IsSerializable{


    private List<Long> resultSet;
    private Integer resultsFound;

    public List<Long> getResultSet() {
        return resultSet;
    }

    public void setResultSet(List resultSet) {
        this.resultSet = resultSet;
    }

    public Integer getResultsFound() {
        return resultsFound;
    }

    public void setResultsFound(Integer resultsFound) {
        this.resultsFound = resultsFound;
    }
}
