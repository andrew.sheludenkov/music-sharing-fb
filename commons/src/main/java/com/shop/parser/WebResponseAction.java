package com.shop.parser;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 05.03.12
 */
public interface WebResponseAction<C> {

    C onSuccess(String result);

    void onFailure(String cause);
}
