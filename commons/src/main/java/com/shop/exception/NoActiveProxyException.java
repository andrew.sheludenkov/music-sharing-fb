package com.shop.exception;

import org.apache.log4j.Logger;

/**
 * @author Lopatin Igor
 *         mail to igorjavadev@gmail.com
 *         Date: 11.04.12
 */
public class NoActiveProxyException extends RuntimeException{
    private static Logger logger = Logger.getLogger(NoActiveProxyException.class);

    public NoActiveProxyException() {
        logger.warn("There are no active proxy.");
    }
}
